import {
  successJSON,
  errorJSON,
  JSONLD,
  showFile,
  errorNotAuthorized
}                        from '../responses';
import certificateDomain from '../domains/Certificate';
import issuerDomain from '../domains/Issuer';

import {
  CreateCertsParams,
  CreateLLCertParams,
  SignCertsParams
} from '../domains/Certificate';

import { UserType, ICert, IIssuer, IUser } from '../models';
import { ErrorMessages }                   from '../errors';
import fileStorage                         from '../services/FileStorage';
import pguApiService                       from '../services/PguReceiver';


type SignCertsBody = Pick<SignCertsParams, 'certIds'>;
type PublishCertsBody = SignCertsBody;
type CreateLLCertBody = Exclude<CreateLLCertParams, 'externalId'>;

interface IFiles {
  [key: string]: {
    name: string;
    key: string;
    date: string;
  };
}

export default {

  'GET /certificates/loadPgu': async ({ req }) => {
    const { issuerId, templateId } = req.query;

    if (!issuerId) {
      return errorJSON('issuerId request parameter not found.', 500);
    }
    if (!templateId) {
      return errorJSON('templateId request parameter not found.', 500);
    }

    try {
      await certificateDomain.loadPGU(issuerId/*||'5dcaafdb3913cb580d87982d'*/, templateId/*||'5dcbd1c8f73f1e64b8011058'*/);
      return successJSON({ success: true });
    } catch (e) {
      return errorJSON(e, 500);
    }
  },

  'GET /certificates/loadMgu': async () => {
    try {
      await certificateDomain.loadMGU();
      return successJSON({ success: true });
    } catch (err) {
      console.error(err);
      return errorJSON(err, 500);
    }
  },

  'GET /certificates/': async ({ user }) => {
    if (!(await user.isAuthenticated())) {
      return errorNotAuthorized();
    }

    const userType = user.details.type;
    const holderId: string = user.details._id;

    if (userType === UserType.IssuerAmbassador) {
      const issuerId: string = (<string[]>user.details.issuerIds)[0];
      const issuer = await issuerDomain.getIssuerById(issuerId);
      let certs = await certificateDomain.getIssuerCerts(issuerId);
      if (issuer.shortName === 'pgu') {
        certs = await Promise.all(certs.map(i => {
          return pguApiService.findAndSubstitutePrivateData(i.credentialSubject.documentRegNumber + '', i);
        }));
      }

      return successJSON(certs);
    }

    if (userType === UserType.Student) {
      const certs = await certificateDomain.getHolderCerts(holderId);
      return successJSON(certs);
    }

    return errorJSON(ErrorMessages.InvalidUserType, 400);
  },

  'GET /certificates/:code': async ({ req, user }) => {

    const code: string = req.params.code;

    const isAdmin = await user.isAdmin();
    const isIssuer = await user.isIssuer();

    if (!req.session.views) {
      req.session.views = {};
    }
    const visited = req.session.views[code];
    if (!visited) {
      req.session.views[code] = true;
    }

    const origCert = await certificateDomain.getCertByCode(code, !visited);

    const issuerName = (<IIssuer>origCert.issuer).shortName;
    let certHolderId;
    if ('holder' in origCert) {
      certHolderId = (<IUser>origCert.holder)._id.toString();
    }

    const certIssuerId = (<IIssuer>origCert.issuer)._id.toString();
    let cert: ICert;

    const userDetails = user.details;
    const userId = userDetails ? userDetails._id.toString() : null;
    const issuerId = userDetails ? userDetails.issuerIds[0].toString() : null;

    if (!(userId === certHolderId || (issuerId === certIssuerId && isIssuer) || isAdmin || origCert.visible)) {
      return errorNotAuthorized();
    }

    if (issuerName === 'pgu') {
      const regNumber = <string>origCert.credentialSubject.documentRegNumber;
      cert = await pguApiService.findAndSubstitutePrivateData(regNumber, origCert);
    } else {
      cert = origCert;
    }
    return successJSON(cert);
  },

  'GET /certificates/:code/image': async ({ req }) => {
    const code: string = req.params.code;
    const cert = await certificateDomain.getCertByCode(code);
    if (!cert) {
      return errorJSON(ErrorMessages.NotFound, 404);
    }
    if (cert.certificateSocialMediaImage) {
      return showFile(cert.certificateSocialMediaImage.buffer, 'document-preview.jpg');
    }
  },

  'GET /documents/:code/status': async ({ req }) => {
    const code: string = req.params.code;
    const cert = await certificateDomain.getCertByCode(code);
    if (!cert) {
      return errorJSON(ErrorMessages.NotFound, 404);
    }

    const { hostname, protocol } = req;
    const origin = `${protocol}://${hostname}`;

    const status = {
      id: `${origin}/api/documents/${code}/status`,
      description: 'Status of documents',
      verifiableCredential: []
    };

    return JSONLD(status);
  },

  'POST /certificates/': async ({ req, user }) => {
    if (!(await user.isAuthenticated())) {
      return errorNotAuthorized();
    }

    const isIssuer = await user.isIssuer();

    if (!isIssuer) {
      return errorJSON(ErrorMessages.NotAuthorized, 403);
    }

    const body: CreateCertsParams = req.body;
    const issuer = user.details.issuerIds[0];

    const { hostname, protocol } = req;
    const origin = `${protocol}://${hostname}`;
    const lang = req.i18n.languages[0];
    const data = { issuer, hostname, origin, lang, ...body };
    const certs = await certificateDomain.createCerts(data);

    return successJSON(certs);
  },

  'POST /sendEmail/': async ({ req, user }) => {
    if (!(await user.isAuthenticated())) {
      return errorNotAuthorized();
    }

    const isIssuer = await user.isIssuer();

    if (!isIssuer) {
      return errorJSON(ErrorMessages.NotAuthorized, 403);
    }

    const body: { certId: string } = req.body;

    const { hostname, protocol } = req;
    const origin = `${protocol}://${hostname}`;
    const lang = req.i18n.languages[0];
    const data = { hostname, origin, lang, ...body };
    await certificateDomain.sendCertCreationEmail(data);

    return successJSON({success: "ok"});
  },

  'DELETE /certificates': async ({ req, user }) => {
    if (!(await user.isAuthenticated())) {
      return errorNotAuthorized();
    }

    const isIssuer = await user.isIssuer();

    if (!isIssuer) {
      return errorJSON(ErrorMessages.NotAuthorized, 403);
    }
    const certs = await certificateDomain.deleteCerts(req.body);

    return successJSON(certs);
  },

  'POST /certificates/sign': async ({ req, user }) => {
    if (!(await user.isAuthenticated())) {
      return errorNotAuthorized();
    }

    const isIssuer = await user.isIssuer();

    if (!isIssuer) {
      return errorJSON(ErrorMessages.NotAuthorized, 403);
    }

    const body: SignCertsBody = req.body;

    const issuerId: string = (<string[]>user.details.issuerIds)[0];
    const data = { issuerId, ...body };
    const certs = await certificateDomain.signCerts(data);

    return successJSON(certs);
  },

  'POST /certificates/publish': async ({ req, user }) => {
    if (!(await user.isAuthenticated())) {
      return errorNotAuthorized();
    }

    const isIssuer = await user.isIssuer();

    if (!isIssuer) {
      return errorJSON(ErrorMessages.NotAuthorized, 403);
    }

    const body: PublishCertsBody = req.body;

    const issuerId: string = (<string[]>user.details.issuerIds)[0];
    const { hostname, protocol } = req;
    const origin = `${protocol}://${hostname}`;
    const lang = req.i18n.languages[0];
    const data = { issuerId, hostname, lang, origin, ...body };
    const certs = await certificateDomain.publishCertsInBlockchain(data);

    return successJSON(certs);
  },

  'POST /certificates/:code/visibility': async ({ req, user }) => {
    if (!(await user.isAuthenticated())) {
      return errorNotAuthorized();
    }

    const isStudent = await user.isStudent();
    const isAdmin = await user.isAdmin();
    const code: string = req.params.code;
    const { visible }: { visible: boolean } = req.body;
    let cert = await certificateDomain.getCertByCode(code);
    const holderId = user.details._id.toString();
    const certHolder = (<IUser>cert.holder)._id.toString();

    if (isAdmin || (isStudent && holderId === certHolder)) {
      cert = await certificateDomain.updateCert({ shareableLinkCode: code,}, { visible });
      return successJSON(cert);
    }

    return errorJSON(ErrorMessages.NotAuthorized, 403);
  },

  'POST /certificates/:code/files': async ({ req }) => {
    const code: string = req.params.code;
    req.filePath = code;
    const files: IFiles = await new Promise((resolve, reject) => {
      fileStorage.upload(req, null, err => {
        if (err) {
          reject(err);
        } else {
          const uploadedFiles = {};
          (req.files || []).forEach(f => {
            uploadedFiles[f.etag.replace(/"/g, '')] = {
              date: new Date(),
              key: f.key,
              name: f.originalname
            };
          });
          resolve(uploadedFiles);
        }
      });
    });

    const cert = await certificateDomain.getCertByCode(code);
    const { credentialSubject } = cert;
    let newCredentialSubject;
    const { cloudFiles, ...cs } = credentialSubject;
    if (cloudFiles) {
      newCredentialSubject = {
        cloudFiles: Object.assign(cloudFiles, files),
        ...cs
      };
    } else {
      newCredentialSubject = {
        cloudFiles: files,
        ...credentialSubject
      };
    }
    // tslint:disable-next-line:max-line-length
    const certObj = await certificateDomain.updateCert({ shareableLinkCode: code }, { credentialSubject: newCredentialSubject });
    return successJSON(certObj);
  },

  'DELETE /certificates/:code/files/:fileHash': async ({ req }) => {
    const code: string = req.params.code;
    const fileHash: string = req.params.fileHash;
    const cert = await certificateDomain.getCertByCode(code);
    const { credentialSubject } = cert;
    const cloudFiles = Object.assign({}, credentialSubject.cloudFiles);
    delete cloudFiles[fileHash];
    const newCredentialSubject = {
      ...credentialSubject,
      cloudFiles
    };
    // tslint:disable-next-line:max-line-length
    // @ts-ignore
    const certObj = await certificateDomain.updateCert({ shareableLinkCode: code }, { credentialSubject: newCredentialSubject });
    return successJSON(certObj);
  },

  'POST /certificates/lingualeo/:externalId': async ({ req }) => {
    const externalId: string = req.params.externalId;
    let cert: ICert = await certificateDomain.getCertByExternalId(externalId);

    if (!cert) {
      const { hostname, protocol } = req;
      const body = <CreateLLCertBody>req.body;
      const origin = `${protocol}://${hostname}`;
      const lang = req.i18n.languages[0];
      const data = { externalId, hostname, origin, lang, ...body };

      cert = await certificateDomain.createLinguaLeoCert(data);
    }

    return successJSON(cert);
  }

};
