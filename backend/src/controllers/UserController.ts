import env                   from '../environments/environment';
import userDomain            from '../domains/User';
import issuerDomain          from '../domains/Issuer';
import emailService          from '../services/Email';
import auth                  from '../utils/auth';
import {
  success,
  successJSON,
  errorNotAuthorized,
  errorBadRequest,
  errorForbidden,
  errorNotFound,
  errorTooManyRequests
}                            from '../responses';
import { IIssuer, UserType } from '../models';

export default {

  'GET /users/': async ({ user }) => {
    const userData = user.details;

    if (!(await user.isAuthenticated())) {
      return errorNotAuthorized();
    }

    let issuer: IIssuer;

    if (userData.type === UserType.IssuerAmbassador) {
      issuer = await issuerDomain.getIssuerById(userData.issuerIds[0]);
    }

    return successJSON({ user: userData, issuer });
  },

  'GET /users/user/lingualeo/:emailHash': async ({ req }) => {
    const emailHash: string = req.params.emailHash;
    const issuer = await issuerDomain.getIssuer({ shortName: 'lingualeo' });
    const user = await userDomain.getOrCreateUsersByEmail([emailHash], issuer._id);

    return successJSON(user);
  },

  'PUT /user/password': async ({ user, req }) => {
    if (!(await user.isAuthenticated())) {
      return errorNotAuthorized();
    }

    const { oldPassword, newPassword } = req.body;

    if (!oldPassword || !newPassword) {
      return errorBadRequest();
    }

    const isSuccess = await userDomain.changePassword({
      userId: user.details._id,
      oldPassword,
      newPassword
    });

    if (isSuccess) {
      const session = req.session!;
      auth.closeSession(session);
      return success();
    } else {
      return errorForbidden();
    }
  },

  'POST /user/password/reset/send-link': async ({ req }) => {
    const { email } = req.body;
    if (!email) {
      return errorBadRequest();
    }

    const user = await userDomain.getUser({ email });
    if (!user) {
      return errorNotFound();
    }

    const userId = user._id;

    if (await userDomain.passwordReset.getActiveToken(userId)) {
      return errorTooManyRequests();
    }

    const token: string = await userDomain.passwordReset.createToken(userId);

    const { protocol, hostname } = req;
    const origin = `${protocol}://${hostname}`;

    // @ts-ignore
    const frontendUri = env.passwordReset.frontendUri;
    const link = `${origin}${frontendUri}${token}`;

    const lang = req.i18n.languages[0];

    await emailService.sendPasswordResetEmail({
      email,
      link,
      lang
    });

    return success();
  },

  'POST /user/password/reset/new': async ({ req }) => {
    const { token, newPassword } = req.body;

    if (!token || !newPassword) {
      return errorBadRequest();
    }

    const isSuccess = await userDomain.passwordReset.changePasswordByToken({
      token,
      newPassword
    });

    if (isSuccess) {
      const session = req.session!;
      auth.closeSession(session);
      return success();
    } else {
      return errorForbidden();
    }
  }

};
