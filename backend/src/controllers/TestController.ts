import { successJSON } from '../responses';

export default {

  'GET /': async () => {
    return successJSON({ apiHealth: '😉Fine' });
  },

  'GET /test/error': async () => {
    throw new Error('This is an test error. The service should continue to work correctly after it.');
  }

};
