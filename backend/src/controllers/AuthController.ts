import { sha256 } from 'js-sha256';

import { successJSON, redirect, errorJSON }                from '../responses';
import userDomain, { RegisterUserParams, LoginUserParams } from '../domains/User';
import { UserType }                                        from '../models';
import certificateDomain                                   from '../domains/Certificate';
import { ErrorMessages }                                   from '../errors';

type RegisterUserBody = Exclude<RegisterUserParams, 'lang' | 'origin' | 'hostname'>;
type PGURegisterUserBody = RegisterUserBody & {
  name: string;
  surname: string;
  secondName: string;
  documentRegNumber: string;
};
type LoginUserBody = LoginUserParams;


export default {

  'POST /auth/signup': async ({ req, user }) => {
    const { hostname, protocol } = req;
    const body: RegisterUserBody = req.body;
    const origin = `${protocol}://${hostname}`;
    const lang = req.i18n.languages[0];

    const params = {
      ...body,
      lang,
      origin,
      hostname
    };

    // only `admin` can create `issuer_ambassador` or `admin`
    params.type = (await user.isAdmin()) ? body.type : UserType.Student;

    const callerUserType = user.type;
    await userDomain.registerUser(params, callerUserType);
    return successJSON();
  },

  'POST /auth/signup/pgu': async ({ req, user }) => {
    const { hostname, protocol } = req;
    const body: PGURegisterUserBody = req.body;

    const origin = `${protocol}://${hostname}`;
    const lang = req.i18n.languages[0];

    const { name, surname, secondName, documentRegNumber } = body;

    const certs = await certificateDomain.getCertByAnyKeys({
      'credentialSubject.name': sha256(name.toLowerCase().trim()),
      'credentialSubject.surname': sha256(surname.toLowerCase().trim()),
      'credentialSubject.secondName': sha256(secondName.toLowerCase().trim()),
      'credentialSubject.documentRegNumber': documentRegNumber
    });

    if (certs.length === 0) {
      return errorJSON(ErrorMessages.NoMatchedDocuments, 400);
    }

    const params = {
      ...body,
      lang,
      origin,
      hostname,
      issuerIds: certs.map(cert => <string>cert.issuer),
      // only 'admin' can create 'issuer_ambassador' or 'admin'
      type: (await user.isAdmin()) ? body.type : UserType.Student
    };

    const callerUserType = user.type;
    const registeredUser = await userDomain.registerUser(params, callerUserType);

    for (const cert of certs) {
      await certificateDomain.updateCert({_id: cert._id}, {
        holder: registeredUser._id
      });
    }

    return successJSON();
  },

  'GET /auth/confirm': async ({ req }) => {
    const code: string = req.query.code;
    const check = await userDomain.checkUserConfirmationLink(code);

    const { protocol, hostname } = req;
    const origin = `${protocol}://${hostname}`;

    if (!check) {
      return redirect(`${origin}/app/error/invalid-link`);
    }

    return redirect(`${origin}/app/login?confirmation=success`);
  },

  'POST /auth/login': async ({ req }) => {
    const body: LoginUserBody = req.body;
    const session = req.session!;

    const { user, issuer } = await userDomain.login(body);

    session!.userId = user._id;

    return successJSON({ user, issuer });
  },

  'POST /auth/logout': async ({ req }) => {
    const session = req.session!;

    session!.userId = null;

    return successJSON();
  }

};
