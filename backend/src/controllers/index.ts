import authController        from './AuthController';
import blockchainController  from './BlockchainController';
import certificateController from './CertificateController';
import issuerController      from './IssuerController';
import landingController     from './LandingController';
import templateController    from './TemplateController';
import testController        from './TestController';
import userController        from './UserController';


const controllers = Object.assign({},
  authController,
  blockchainController,
  certificateController,
  issuerController,
  landingController,
  templateController,
  testController,
  userController
);

export default controllers;