import issuerDomain                                   from '../domains/Issuer';
import { CreateIssuerParams, UpdateIssuerParams }     from '../domains/Issuer';
import { ErrorMessages }                              from '../errors';
import { successJSON, errorJSON, errorNotAuthorized } from '../responses';


export default {
  
  'GET /issuers/': async ({ user }) => {
    if (!(await user.isAuthenticated())) {
      return errorNotAuthorized();
    }
    
    const isIssuer = await user.isIssuer();

    if (!isIssuer) {
      return errorJSON(ErrorMessages.NotAuthorized, 403);
    }

    const issuerId = (<string[]>user.details.issuerIds)[0];
    const issuer = await issuerDomain.getIssuerById(issuerId);
    return successJSON(issuer);
  },
  
  'GET /issuers/:id': async ({ req, user }) => {
    const id: string = req.params.id;
    const isOwner = await user.isIssuerOwner(id);
    const isAdmin = await user.isAdmin();

    if (!(isOwner || isAdmin)) {
      return errorJSON(ErrorMessages.NotAuthorized, 403);
    }

    const issuer = await issuerDomain.getIssuerById(id);
    return successJSON(issuer);
  },
  
  'POST /issuers/': async ({ req, user }) => {
    if (!(await user.isAuthenticated())) {
      return errorNotAuthorized();
    }
    
    const isAdmin = await user.isAdmin();

    if (!isAdmin) {
      return errorJSON(ErrorMessages.NotAuthorized, 403);
    }
    const body: CreateIssuerParams = req.body;
    const issuer = await issuerDomain.createIssuer(body);
    return successJSON(issuer);
  },
  
  'PATCH /issuers/:id': async ({ req, user }) => {
    if (!(await user.isAuthenticated())) {
      return errorNotAuthorized();
    }
    
    const id: string = req.params.id;
    const isOwner = await user.isIssuerOwner(id);
    const isAdmin = await user.isAdmin();

    if (!(isOwner || isAdmin)) {
      return errorJSON(ErrorMessages.NotAuthorized, 403);
    }
    
    const body: UpdateIssuerParams = req.body;
    const issuer = await issuerDomain.updateIssuer(id, body);
    return successJSON(issuer);
  },
  
  'PATCH /issuers/': async ({ req, user }) => {
    if (!(await user.isAuthenticated())) {
      return errorNotAuthorized();
    }
    
    const isIssuer = await user.isIssuer();

    if (!isIssuer) {
      return errorJSON(ErrorMessages.NotAuthorized, 403);
    }

    const issuerId = (<string[]>user.details.issuerIds)[0];
    const body: UpdateIssuerParams = req.body;
    const issuer = await issuerDomain.updateIssuer(issuerId, body);
    return successJSON(issuer);
  },
  
  'DELETE /issuers/:id': async ({ req, user }) => {
    if (!(await user.isAuthenticated())) {
      return errorNotAuthorized();
    }
    
    const isAdmin = await user.isAdmin();

    if (!isAdmin) {
      return errorJSON(ErrorMessages.NotAuthorized, 403);
    }
    
    const id: string = req.params.id;
    const issuer = await issuerDomain.deleteIssuer(id);
    return successJSON(issuer);
  },
  
};
