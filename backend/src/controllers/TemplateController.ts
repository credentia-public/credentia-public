import templateDomain                                   from '../domains/Template';
import { ICreateTemplateParams, IUpdateTemplateParams } from '../domains/Template';
import { ErrorMessages }                                from '../errors';
import { successJSON, errorJSON, errorNotAuthorized }   from '../responses';


export default {
  
  'GET /templates/': async ({ user }) => {
    if (!(await user.isAuthenticated())) {
      return errorNotAuthorized();
    }
    
    const issuerId: string = (<string[]>user.details.issuerIds)[0];
    const templates = await templateDomain.getIssuerTemplates(issuerId);
    return successJSON(templates);
  },
  
  'GET /templates/:id': async ({ req, user }) => {
    if (!(await user.isAuthenticated())) {
      return errorNotAuthorized();
    }
    
    const isIssuer = await user.isIssuer();
    const isAdmin = await user.isAdmin();

    if (!(isIssuer || isAdmin)) {
      return errorJSON(ErrorMessages.NotAuthorized, 403);
    }
    const id: string = req.params.id;
    const template = await templateDomain.getTemplateById(id);
    return successJSON(template);
  },
  
  'POST /templates/': async ({ req, user }) => {
    if (!(await user.isAuthenticated())) {
      return errorNotAuthorized();
    }
    
    const isIssuer = await user.isIssuer();
    const isAdmin = await user.isAdmin();

    if (!(isIssuer || isAdmin)) {
      return errorJSON(ErrorMessages.NotAuthorized, 403);
    }
    const body: ICreateTemplateParams = req.body;
    const template = await templateDomain.createTemplate(body);
    return successJSON(template);
  },
  
  'PATCH /templates/:id': async ({ req, user }) => {
    const isIssuer = await user.isIssuer();
    const isAdmin = await user.isAdmin();

    if (!(isIssuer || isAdmin)) {
      return errorJSON(ErrorMessages.NotAuthorized, 403);
    }
    
    const id: string = req.params.id;
    const body: Partial<IUpdateTemplateParams> = req.body;
    const template = await templateDomain.updateTemplate(id, body);
    return successJSON(template);
  },
  
  'DELETE /templates/:id': async ({ req, user }) => {
    const isIssuer = await user.isIssuer();
    const isAdmin = await user.isAdmin();

    if (!(isIssuer || isAdmin)) {
      return errorJSON(ErrorMessages.NotAuthorized, 403);
    }
    const id: string = req.params.id;
    await templateDomain.deleteTemplate(id);
    return successJSON();
  },
  
};
