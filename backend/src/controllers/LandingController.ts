import subscriberDomain from '../domains/Subscriber';
import { successJSON }  from '../responses';

export interface ISubscribeParams {
  name: string;
  email: string;
  phone: string;
}

export default {
  'POST /landing/subscribe': async ({ req }) => {
    const body = <ISubscribeParams>req.body;
    const referer = req.get('referer'); // https://credentia.ru/tr/
    let lang: string;

    if (referer) {
      if (referer.includes('credentia.')) {
        const match = referer.match(/\/(ru|en|tr)\//);
        lang = match ? match[1] : 'en';
      } else if (referer.includes('e-diplom.kz')) {
        lang = 'kk';
      }
    } else {
      lang = 'en';
    }

    const params = {
      email: body.email,
      name: body.name,
      phone: body.phone,
      lang
    };

    await subscriberDomain.createSubscriber(params);
    return successJSON();
  },
};
