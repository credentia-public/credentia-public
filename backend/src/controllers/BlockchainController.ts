import Boom from '@hapi/boom';

import smartContract, { Chains } from '../services/SmartContract';
import { successJSON }           from '../responses';

export default {

  'GET /blockchain/certificates': async ({ req }) => {
    const hash: string = req.query.hash;
    const certId: string = req.query.certId;
    const chain: Chains = req.query.chain || Chains.TestNetOld;

    if (hash && certId) {
      throw Boom.badRequest('hash or certId. One param only');
    } else if (hash) {
      const ethCert: string = await smartContract.getSmartContract(chain).getCertByHash(hash);
      const certBuf = Buffer.from(ethCert, 'base64');
      return successJSON(JSON.parse(certBuf.toString()));
    } else if (certId) {
      return successJSON(await smartContract.getSmartContract(chain).getCertById(certId));
    } else {
      return successJSON(await smartContract.getSmartContract(chain).getCertList());
    }

  },

  'GET /blockchain/txs/:chain/:txId': async ({ req }) => {
    const txId: string = req.params.txId;
    const chain: Chains = req.params.chain || Chains.TestNetOld;

    const tx = await smartContract.getSmartContract(chain).getTxDetail(txId);
    return successJSON(tx);
  }

};
