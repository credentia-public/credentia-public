import { Connection } from 'mongoose';
import crypto         from 'crypto';
import path           from 'path';

import database              from '../services/Database';
import logger                from '../services/Logger';
import { getDownloadStream } from '../models/utils';
import { IFiles }            from '../models';
import env                   from '../environments/environment';

export async function up(next) {
  await database.listen();
  const client: Connection = database.getConnection();
  const certs = client.collection('certificates');
  const cursor = await certs.find({
    'credentialSubject.cloudFiles': { $exists: true }
  });
  while(await cursor.hasNext()) {
    const doc = await cursor.next();
    const files = doc.credentialSubject.cloudFiles;

    if (Array.isArray(files)) {
      logger.debug(`Certificate ${doc._id} started update`);
      const newFiles = {};

      for (const f of files) {
        const fileURL = `${env.cloudStorage.endpoint_url}/${f}`;
        const downloadStream = await getDownloadStream(encodeURI(fileURL));
        const md5Stream = crypto.createHash('md5').setEncoding('hex');
        const md5: string = await new Promise((res, rej) => {
          downloadStream.pipe(md5Stream)
          .on('finish', () => {
            logger.debug(`Certificate ${doc._id} file ${f} md5 calculated`);
            res(md5Stream.read());
          })
          .on('error', (err) => {
            rej(err);
          });
        });

        newFiles[md5] = {
          name: path.basename(fileURL),
          key: f
        };
      }

      await certs.updateOne({ _id: doc._id }, {
        $set: {
          'credentialSubject.cloudFiles': newFiles
        }
      });
      logger.debug(`Certificate ${doc._id} updated`);
    }
  }

  logger.info(`Migration ${path.basename(__filename)} completed`);
  next();
}

export async function down(next) {
  await database.listen();
  const client: Connection = database.getConnection();
  const certs = client.collection('certificates');
  const cursor = await certs.find({
    'credentialSubject.cloudFiles': { $exists: true }
  });

  while(await cursor.hasNext()) {
    const doc = await cursor.next();
    const files: IFiles = doc.credentialSubject.cloudFiles;

    if (!Array.isArray(files)) {
      logger.debug(`Certificate ${doc._id} started rollback`);
      const newFiles = Object.values(files).map(f => f.key);

      await certs.updateOne({ _id: doc._id }, {
        $set: {
          'credentialSubject.cloudFiles': newFiles
        }
      });
      logger.debug(`Certificate ${doc._id} rollbacked`);
    }
  }
  logger.info(`Migration ${path.basename(__filename)} rollbacked`);
  next();
}
