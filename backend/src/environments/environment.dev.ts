import local from './environment.local';

// const env = global.process.env;

export default Object.assign({}, local, {
  env: 'development',
  isDev: false,
  db: {
    url: '_YOUR_VALUE_HERE_',
    autoIndex: false,
    poolSize: 20,
    reconnectTries: 60,
    reconnectInterval: 5000,
    useNewUrlParser: true,
    useFindAndModify: false
  },
  session: {
    name: 'session',
    secret: '_YOUR_VALUE_HERE_',
    resave: false,
    saveUninitialized: false,
    cookie: {
      secure: false,
      sameSite: false,
      httpOnly: true,
      maxAge: 14 * 24 * 60 * 60 * 1000 // expires in 14 days
    }
  },
});
