import path from 'path';

const env = global.process.env;

export default {
  env: env.APP_ENV || 'local',
  isDev: env.NODE_ENV === 'development',
  cors: {
    credentials: true
  },
  shareUrl: '/app/student/share?code=',
  email: {
    confirmUrl: '/api/auth/confirm?code=',
    name: 'Credentia',
    email: '_YOUR_VALUE_HERE_',
    notifyEmail: '_YOUR_VALUE_HERE_',
    otherNotifyEmails: {
      tr: ['_YOUR_VALUE_HERE_'],
      kk: ['_YOUR_VALUE_HERE_']
    },
    apiKey: '_YOUR_VALUE_HERE_'
  },
  passwordReset: {
    frontendUri: '/app/password-reset?token=',
    tokenMaxAge: 60 * 60 * 1000
  },
  confLinkMaxAge: 86400000,
  tokenMaxAge: 315360000000,
  port: env.APP_PORT || 3000,
  frontendPort: 8000,
  logger: {
    level: env.LOG_LEVEL || 'info',
    useLevelLabels: true,
    name: 'app',
    base: null,
    prettyPrint: {
      levelFirst: true,
      translateTime: true
    }
  },
  bluebird: {
    // Enable warnings
    warnings: true,
    // Enable long stack traces
    longStackTraces: true,
    // Enable cancellation
    cancellation: false,
    // Enable monitoring
    monitoring: false
  },
  helmet: {},
  db: {
    url: process.env.MONGODB_URI || 'mongodb://127.0.0.1:27017/credentia',
    autoIndex: false,
    poolSize: 20,
    reconnectTries: 60,
    reconnectInterval: 5000,
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false
  },
  session: {
    name: 'session',
    secret: env.SESSION_SECRET || '_YOUR_VALUE_HERE_',
    resave: false,
    saveUninitialized: false,
    cookie: {
      secure: false,
      sameSite: false,
      httpOnly: true,
      maxAge: 14 * 24 * 60 * 60 * 1000 // expires in 14 days
    }
  },
  i18next: {
    debug: false,
    fallbackLng: 'en',
    whitelist: ['en', 'ru', 'tr', 'kk'],
    nonExplicitWhitelist: true,
    preload: ['en', 'ru', 'tr', 'kk'],
    load: 'languageOnly',
    lowerCaseLng: true,
    backend: {
      // path where resources get loaded from
      loadPath: path.resolve(__dirname, '../files/locales/{{lng}}/{{ns}}.json'),
      addPath: path.resolve(__dirname, '../files/locales/{{lng}}/{{ns}}.missing.json')
    },
    detection: {
      order: ['querystring', 'cookie', 'header'],
      lookupCookie: 'lang',
      lookupQuerystring: 'lang',
    }
  },
  blockchain: {
    'testnet-old': {
      solVersion: '0.5.16',
      chainId: 3,
      chain: 'ropsten',
      className: 'CredentiaCertRegistry',
      httpProvider: 'https://ropsten.infura.io/v3/_YOUR_VALUE_HERE_',
      address: '_YOUR_VALUE_HERE_',
      privateKey: '_YOUR_VALUE_HERE_',
      factoryPath: 'files/blockchain/testnet-old.sol',
      factoryAddress: '_YOUR_VALUE_HERE_',
      gasPrice: '0x09184e72a000',
      gasPrice2: '0x4A817C800', // 0xB2D05E00 = 3 gwei; 0x4A817C800 = 20 gwei
      gasLimit: '0x2710'
    },
    testnet: {
      solVersion: '0.6.3',
      chainId: 3,
      chain: 'ropsten',
      className: 'DocRegistry',
      httpProvider: 'https://ropsten.infura.io/v3/_YOUR_VALUE_HERE_',
      address: '_YOUR_VALUE_HERE_',
      privateKey: '_YOUR_VALUE_HERE_',
      factoryPath: 'files/blockchain/mainnet.sol',
      factoryAddress: '_YOUR_VALUE_HERE_',
      gasPrice: '0x09184e72a000',
      gasPrice2: '0x4A817C800', // 0xB2D05E00 = 3 gwei; 0x4A817C800 = 20 gwei
      gasLimit: '0x2710'
    },
    mainnet: {
      solVersion: '0.6.3',
      chainId: 1,
      className: 'DocRegistry',
      httpProvider: 'https://mainnet.infura.io/v3/_YOUR_VALUE_HERE_',
      address: '_YOUR_VALUE_HERE_',
      privateKey: '_YOUR_VALUE_HERE_',
      factoryPath: 'files/blockchain/mainnet.sol',
      factoryAddress: '_YOUR_VALUE_HERE_',
      gasPrice: '0x09184e72a000',
      gasPrice2: '0x4A817C800', // 0xB2D05E00 = 3 gwei; 0x4A817C800 = 20 gwei
      gasLimit: '0x2710'
    }
  },
  cloudStorage: {
    endpoint_url: '_YOUR_VALUE_HERE_',
    endpoint:'_YOUR_VALUE_HERE_',
    aws_access_key_id: '_YOUR_VALUE_HERE_',
    aws_secret_access_key: '_YOUR_VALUE_HERE_',
    Bucket: 'credentia-files'
  },
  pgu: {
    url: '_YOUR_VALUE_HERE_'
  }
};
