import { Document, Schema, Model, model, HookNextFunction } from 'mongoose';

import { logQueryTime } from './utils';

export interface ILink {
  _id: any;
  code: string;
  userId: string;
  createdAt: Date;
}

export interface ILinkDocument extends ILink, Document {}

export const LinksSchema: Schema = new Schema({
  code: {
    type: String,
    unique: true,
    required: true
  },
  userId: {
    type: String,
    required: true,
    unique: true
  },
  createdAt: Date
});

logQueryTime(LinksSchema);

LinksSchema.pre<ILinkDocument>('save', function(this: ILinkDocument, next: HookNextFunction) {
  const now = new Date();

  if (!this.createdAt) {
    this.createdAt = now;
  }
  next();
});

export const Link: Model<ILinkDocument> = model<ILinkDocument>('Link', LinksSchema);
