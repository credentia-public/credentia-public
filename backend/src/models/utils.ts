import { Query, Schema } from 'mongoose';
import fs from 'fs';
import path from 'path';
import axios from 'axios';

import logger from '../services/Logger';


// @ts-ignore
const actions = [
  // 'save',
  'update',
  'updateOne',
  'updateMany',
  'remove',
  'deleteOne',
  'deleteMany',
  'find',
  'findOne',
  'findOneAndDelete',
  'findOneAndRemove',
  'findOneAndUpdate'
];

export type Q = Query<any> & {
  _startTime: number;
  model: {
    modelName: string;
  }
  op: string;
  _conditions: any;
};


export async function startTime(this: Q) {
  this._startTime = Date.now();
}

export async function endTime(this: Q) {
  const model = this.model.modelName;
  const op = this.op;
  const conditions = this._conditions;
  const time = Date.now() - this._startTime;

  if (this._startTime != null) {
    logger.debug(`${model} ${op} ${JSON.stringify(conditions)} ${time}ms`);
  }
}

export function logQueryTime(_schema: Schema) {

  return;

  // actions.forEach(m => {
  //   schema.pre(m, startTime);
  //   schema.post(m, endTime);
    // schema.pre('save', startTime);
    // schema.post('save', endTime);
  // });
}

export async function downloadFile(downloadURL: string, dir?: string) {
  const fileName = path.basename(downloadURL);
  const filePath = path.resolve(dir || __dirname, fileName);
  const writer = fs.createWriteStream(filePath);
  const stream = await getDownloadStream(downloadURL);

  stream.pipe(writer);

  return new Promise((resolve, reject) => {
    writer.on('finish', resolve);
    writer.on('error', reject);
  });
}

export async function getDownloadStream(downloadURL: string) {
  const response = await axios({
    url: downloadURL,
    method: 'GET',
    responseType: 'stream'
  });

  return response.data;
}
