import { Document, Schema, Model, model, HookNextFunction } from 'mongoose';

import { logQueryTime } from './utils';

export interface IUpdateTemplateParams {
  name: string;
  textColor: string;
  bgColor: string;
  logoPath: string;
  imagePreviewPath: string;
}

export interface ICreateTemplateParams extends IUpdateTemplateParams {
  issuerId: string;
}

export interface ITemplate extends ICreateTemplateParams {
  _id: any;
  updatedAt: Date;
  createdAt: Date;
}

export interface ITemplateDocument extends ITemplate, Document {}

export const TemplateSchema: Schema = new Schema({
  issuerId: String,
  name: String,
  textColor: {
    type: String,
    default: '#FFFFFF'
  },
  bgColor: {
    type: String,
    default: '#C6B0B0'
  },
  logoPath: {
    type: String,
    default:  '/static/images/badge.svg'
  },
  imagePreviewPath: {
    type: String
  },
  updatedAt: Date,
  createdAt: Date
});

logQueryTime(TemplateSchema);

TemplateSchema.pre<ITemplateDocument>('save', function(this: ITemplateDocument, next: HookNextFunction) {
  const now = new Date();

  if (!this.createdAt) {
    this.createdAt = now;
  }
  this.updatedAt = now;
  next();
});

export const Template: Model<ITemplateDocument> = model<ITemplateDocument>('Template', TemplateSchema);
