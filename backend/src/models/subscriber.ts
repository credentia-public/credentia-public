import { Document, Schema, Model, model, HookNextFunction } from 'mongoose';

import { logQueryTime } from './utils';

export interface ISubscriber {
  _id: any;
  name: string;
  email: string;
  phone: string;
  updatedAt: Date;
  createdAt: Date;
}

export interface ISubscriberDocument extends ISubscriber, Document {}

export const SubscriberSchema: Schema = new Schema({
  name: String,
  email: String,
  phone: String,
  updatedAt: Date,
  createdAt: Date
});

logQueryTime(SubscriberSchema);

SubscriberSchema.pre<ISubscriberDocument>('save', function(this: ISubscriberDocument, next: HookNextFunction) {
  const now = new Date();

  if (!this.createdAt) {
    this.createdAt = now;
  }
  this.updatedAt = now;
  next();
});

export const Subscriber: Model<ISubscriberDocument> = model<ISubscriberDocument>('Subscriber', SubscriberSchema);
