import { Document, Schema, Model, model, HookNextFunction } from 'mongoose';

import { logQueryTime } from './utils';

export interface IKeys {
  _id: any;
  shareableLinkCode: string;
  keys: any;
  createdAt: Date;
  updatedAt: Date;
}

export interface IKeysDocument extends IKeys, Document {}

export const KeysSchema = new Schema({
  shareableLinkCode: String,
  keys: { type: Schema.Types.Mixed, unique : true, required : true, dropDups: true },
  createdAt: Date,
  updatedAt: Date
});

logQueryTime(KeysSchema);

KeysSchema.pre<IKeysDocument>('save', function(this: IKeysDocument, next: HookNextFunction) {
  const now = new Date();

  if (!this.createdAt) {
    this.createdAt = now;
  }

  this.updatedAt = now;
  next();
});

KeysSchema.set('toJSON', {
  getters: false,
  versionKey: false,
  transform: (_, ret) => {
    ret.privateKey = undefined;
    return ret;
  }
});

export const Keys: Model<IKeysDocument> = model<IKeysDocument>('Keys', KeysSchema);
