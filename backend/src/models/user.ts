import { Document, Schema, Model, model, HookNextFunction } from 'mongoose';

import { logQueryTime } from './utils';

type IIssuer = import('./issuer').IIssuer;

export enum UserStatus {
  Unregistered = 'unregistered',
  Unverified = 'unverified',
  Active = 'active'
}

export enum UserType {
  IssuerAmbassador = 'issuer_ambassador', Student = 'student', Admin = 'admin'
}

export interface IUser {
  _id: any;
  email: string;
  password?: string;
  passwordResetToken?: string;
  passwordResetTokenCreatedAt?: Date;
  name?: string;
  surname?: string;
  secondName?: string;
  birthDate?: string;
  type: UserType;
  status: UserStatus;
  issuerIds: string[] | IIssuer[];
  updatedAt: Date;
  createdAt: Date;
  privateKey: string;
  ethAddress: string;
  publicKey: string;
}

export interface IUserDocument extends IUser, Document {
  ambassadorIssuerId: string;
}

export const UsersSchema: Schema = new Schema({
  email: {
    type: String,
    unique: true,
    trim: true
  },
  password: {
    type: String
  },
  passwordResetToken: String,
  passwordResetTokenCreatedAt: Date,
  name: String,
  surname: String,
  secondName: String,
  type: {
    type: UserType,
    enum: UserType
  },
  status: {
    type: UserStatus,
    enum: UserStatus,
    default: UserStatus.Unregistered
  },
  issuerIds: [{ type: Schema.Types.ObjectId, ref: 'Issuer' }],
  updatedAt: Date,
  createdAt: Date,
  privateKey: String,
  ethAddress: String,
  publicKey: String
});

logQueryTime(UsersSchema);

// tslint:disable-next-line:only-arrow-functions
UsersSchema.virtual('ambassadorIssuerId').get(function(this: IUserDocument) {
  const issuerId = this.issuerIds[0];

  return this.populated('issuerIds') ? issuerId : issuerId.toString();
});

UsersSchema.pre<IUserDocument>('save', function(this: IUserDocument, next: HookNextFunction) {
  const now = new Date();

  if (!this.createdAt) {
    this.createdAt = now;
  }

  this.updatedAt = now;
  next();
});

UsersSchema.set('toJSON', {
  getters: false,
  versionKey: false,
  transform: (_, ret) => {
    ret.password = undefined;
    ret.privateKey = undefined;
    return ret;
  }
});


export const User: Model<IUserDocument> = model<IUserDocument>('User', UsersSchema);
