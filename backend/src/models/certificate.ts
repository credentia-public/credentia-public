import { Document, Schema, Model, model, HookNextFunction } from 'mongoose';

import { ITemplate }    from './template';
import { IUser }        from './user';
import { IIssuer }      from './issuer';
import { logQueryTime } from './utils';
import { Chains }       from '../services/SmartContract';

export interface IFiles {
  [key: string]: {
    name: string;
    key: string;
    date: string;
  };
}

export type CredentialSubject = {
  [key: string]: string | number;
} & {
  title: string;
  name: string;
  surname?: string;
  secondName?: string;
  files?: string[];
  cloudFiles?: IFiles;
};

export interface ICert {
  _id: any;
  externalId?: string;
  template: string | ITemplate;
  group: string;
  holder: string | IUser;
  issuer: string | IIssuer;
  chain: Chains;
  visible: boolean;
  shareableLinkCode: string;
  views: number;
  certificateSocialMediaImage: {
    buffer: Buffer;
    select: false
  };
  credentialSubject: CredentialSubject;
  txId?: string;
  proof?: {
    proofType: string;
    verificationMethod: string;
    jws: string;
    created: Date;
  };
  createdAt: Date;
  updatedAt: Date;
}

export interface ICertDocument extends ICert, Document {
}

export const CertSchema: Schema = new Schema({
  externalId: String,
  template: {
    type: Schema.Types.ObjectId,
    ref: 'Template'
  },
  group: String,
  issuer: {
    type: Schema.Types.ObjectId,
    ref: 'Issuer'
  },
  holder: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  chain: {
    type: Chains,
    enum: Chains
  },
  visible: Boolean,
  shareableLinkCode: String,
  views: {
    type: Number,
    default: 0
  },
  certificateSocialMediaImage: Buffer,
  credentialSubject: Schema.Types.Mixed,
  txId: String,
  proof: {
    proofType: String,
    verificationMethod: String,
    jws: String,
    created: Date
  }
}, { collection: 'certificates' });

logQueryTime(CertSchema);

CertSchema.pre<ICertDocument>('save', function (this: ICertDocument, next: HookNextFunction) {
  const now = new Date();

  if (!this.createdAt) {
    this.createdAt = now;
  }

  this.updatedAt = now;
  next();
});

CertSchema.set('toJSON', {
  getters: false,
  versionKey: false,
  transform: (_, ret) => {
    ret.certificateSocialMediaImage = undefined;
    return ret;
  }
});

export const Cert: Model<ICertDocument> = model<ICertDocument>('Cert', CertSchema);
