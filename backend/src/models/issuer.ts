import { Document, Schema, Model, model, HookNextFunction } from 'mongoose';

import { logQueryTime } from './utils';
import { Chains }       from '../services/SmartContract';

export interface IIssuer {
  _id: any;
  name: string;
  shortName: string;
  address: string;
  phone: string;
  availableCertsAmount: number;
  chain: Chains;
  privateKey: string;
  ethAddress: string;
  publicKey: string;
  createdAt: Date;
  updatedAt: Date;
}

export interface IIssuerDocument extends IIssuer, Document {}

export const IssuerSchema = new Schema({
  name: String,
  shortName: String,
  address: String,
  phone: String,
  availableCertsAmount: Number,
  chain: {
    type: Chains,
    enum: Chains
  },
  privateKey: String,
  ethAddress: String,
  publicKey: String,
  createdAt: Date,
  updatedAt: Date
});

logQueryTime(IssuerSchema);

IssuerSchema.pre<IIssuerDocument>('save', function(this: IIssuerDocument, next: HookNextFunction) {
  const now = new Date();

  if (!this.createdAt) {
    this.createdAt = now;
  }

  this.updatedAt = now;
  next();
});

IssuerSchema.set('toJSON', {
  getters: false,
  versionKey: false,
  transform: (_, ret) => {
    ret.privateKey = undefined;
    return ret;
  }
});

export const Issuer: Model<IIssuerDocument> = model<IIssuerDocument>('Issuer', IssuerSchema);
