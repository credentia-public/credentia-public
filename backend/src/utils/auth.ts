import userDomain          from '../domains/User';
import { IUser, UserType } from '../models';

export class DetailedUser {
  details: IUser;

  constructor(details: any) {
    this.details = details;
  }

  isAuthenticated(): Promise<boolean> {
    return Promise.resolve(Boolean(this.details && this.details._id));
  }

  isResourceOwner(): Promise<boolean> {
    throw new Error('Not implemented');
  }

  isIssuerOwner(issuerId: string) {
    return this.isIssuer().then(isIssuer => {
      return isIssuer && (<string[]>this.details.issuerIds).find(i => i === issuerId);
    });
  }

  isInRole(role: UserType): Promise<boolean> {
    const userRole = this.details.type;

    return Promise.resolve(role === userRole);
  }

  isAdmin(): Promise<boolean> {
    return this.isAuthenticated().then(authenticated => {
      return authenticated && this.isInRole(UserType.Admin);
    });
  }

  isIssuer(): Promise<boolean> {
    return this.isAuthenticated().then(authenticated => {
      return authenticated && this.isInRole(UserType.IssuerAmbassador);
    });
  }

  isStudent(): Promise<boolean> {
    return this.isAuthenticated().then(authenticated => {
      return authenticated && this.isInRole(UserType.Student);
    });
  }
}


export default {

  async getBySession(session) {
    let detailedUser;

    if (session.userId) {
      const user = await userDomain.getUser({ _id: session.userId });
      detailedUser = new DetailedUser(user);
    } else {
      detailedUser = new DetailedUser(null);
    }

    return detailedUser;
  },

  closeSession(session) {
    session!.userId = null;
  }

};
