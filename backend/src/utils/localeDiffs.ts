import enLocale from '../files/locales/en/translation.json';
import ruLocale from '../files/locales/ru/translation.json';
import trLocale from '../files/locales/tr/translation.json';
import kkLocale from '../files/locales/kk/translation.json';


const locales = [
  {
    title: 'en',
    tree: enLocale
  },
  {
    title: 'ru',
    tree: ruLocale,
  },
  {
    title: 'tr',
    tree: trLocale
  },
    {
    title: 'kk',
    tree: kkLocale
  },
];

const generateDiffList = (locale1: any, locale2: any, stack: string[] = []) => {
  
  const tree1keys = Object.keys(locale1.tree);
  const tree2keys = Object.keys(locale2.tree);

  let missingKeysText = '';

  const missing = (inArr: any[], comparingArr: any[]) => {
    return comparingArr.filter((item: string) => inArr.indexOf(item) < 0);
  };

  const crosses = (inArr: any[], comparingArr: any[]) => {
    return comparingArr.filter((item: string) => inArr.indexOf(item) >= 0);
  };

  const chained = (key: string) => [...stack, key].join(' 🠆 ');

  const locale1renderer = (key: string) =>
    `[${locale1.title}] ${chained('?')} ┃ [${locale2.title}] ${chained(key)}\n`;
    
  const locale2renderer = (key: string) =>
    `[${locale1.title}] ${chained(key)} ┃ [${locale2.title}] ${chained('?')}\n`;

  missingKeysText += [
    ...missing(tree1keys, tree2keys).map(locale1renderer),
    ...missing(tree2keys, tree1keys).map(locale2renderer)
  ].join('');

  crosses(tree1keys, tree2keys).map((key: string) => {

    const subtree1 = locale1.tree[key];
    const subtree2 = locale2.tree[key];

    if (typeof subtree1 !== typeof subtree2) {
      throw new Error(`Different types: ${chained(key)}`);
    }

    if (typeof subtree1 === 'object' && typeof subtree2 === 'object') {
      const locale1part = {
        title: locale1.title,
        tree: subtree1
      };
      const locale2part = {
        title: locale2.title,
        tree: subtree2
      };
      missingKeysText += generateDiffList(locale1part, locale2part, [...stack, key]);
    }

  });

  return missingKeysText;

};

const getList = function() {
  let diffList = '';
  
  if (locales.length > 1) {
    for (let i = 0; i < locales.length; i++) {
      for (let j = i + 1; j < locales.length; j++) {
        const locale1 = locales[i];
        const locale2 = locales[j];
        const pairDiffList = generateDiffList(locale1, locale2);
        if (pairDiffList) {
          const pairTitle = `${locale1.title} Δ ${locale2.title}`;
          const pairTitleDivider = `━`.repeat(pairTitle.length);
          diffList += `\n${pairTitle}\n${pairTitleDivider}\n${pairDiffList}`;
        }
      }
    }
  }
  
  return diffList;
};

if (process.env.EXIT_ON_DIFFS) {
  const diffList = getList();
  if (diffList) {
    console.log(diffList);
    process.exit(1);
  }
}

export default { getList };
