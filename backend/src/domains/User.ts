import uuidv4 from 'uuid/v4';
import argon  from 'argon2';
import Boom   from '@hapi/boom';

import { IIssuer, ILink, IUser, IUserDocument, Link, User, UserStatus, UserType } from '../models';

import smartContract, { Chains }                 from '../services/SmartContract';
import emailService                              from '../services/Email';
import issuerDomain                              from './Issuer';
import { EmailErrorMessages, UserErrorMessages } from '../errors';
import env                                       from '../environments/environment';


export type GetUserParams = Partial<Pick<IUser,
  '_id' | 'email' | 'name' | 'surname' | 'secondName' | 'ethAddress' | 'publicKey'>>;

export type RegisterUserParams = Pick<IUser,
  'email' | 'password' | 'name' | 'surname' | 'secondName' | 'birthDate' | 'type' | 'issuerIds'> & {
  lang: string;
  origin: string;
  hostname: string;
};

export type LoginUserParams = Pick<IUser, 'email' | 'password'>;


const userWoPassAndKey = {
  password: 0,
  passwordResetToken: 0,
  passwordResetTokenCreatedAt: 0,
  privateKey: 0
};


async function getUser(params: GetUserParams): Promise<IUser> {
  return User.findOne(params, userWoPassAndKey).lean().exec();
}

async function getOrCreateUsersByEmail(emails: string[], issuerId: string): Promise<IUser[]> {
  const existingUsers: IUser[] = await User.find({ email: { $in: emails } }, userWoPassAndKey).lean().exec();
  const existingEmails = existingUsers.map(u => u.email);
  const newEmails = emails.filter(e => !existingEmails.includes(e));
  let newUsers: IUserDocument[] = [];

  if (newEmails.length > 0) {
    newUsers = await User.create(newEmails.map(e => {
      // account created in all chains
      const wallet = smartContract.getSmartContract(Chains.MainNet).createAccount();

      return {
        email: e,
        issuerIds: [issuerId],
        type: UserType.Student,
        status: UserStatus.Unregistered,
        ethAddress: wallet.address,
        privateKey: wallet.privateKey,
        publicKey: wallet.publicKey
      };
    }));
  }

  return existingUsers.concat(newUsers.map(u => u.toJSON()));
}

async function getIssuerUsers(issuerId: string): Promise<IUser[]> {
  return User.find({ issuerIds: issuerId, type: UserType.Student }, userWoPassAndKey).lean().exec();
}

async function registerUser(params: RegisterUserParams, callerUserType: UserType): Promise<IUser> {
  const { lang, origin, hostname, issuerIds, email, password, type, name, surname, secondName, birthDate } = params;

  let user: IUser | null;
  let passwordHash: string;

  user = await User.findOne({ email }, userWoPassAndKey).lean().exec();

  if (password) {
    passwordHash = await argon.hash(password!);
  }

  if (user) {
    switch (type) {
      case UserType.Student: {
        if (user.status === UserStatus.Active) {
          throw Boom.badRequest(UserErrorMessages.AlreadyExist);
        } else if (user.status === UserStatus.Unregistered) {
          const update = {
            name,
            surname,
            secondName,
            birthDate,
            status: UserStatus.Unverified,
            password: passwordHash,
          };
          user = await User.findByIdAndUpdate(user._id, update,
            {new: true, fields: userWoPassAndKey, omitUndefined: true}).lean().exec();
        }
        break;
      }
      case UserType.IssuerAmbassador: {
        throw Boom.badRequest(UserErrorMessages.AlreadyExist);
        break;
      }
    }
  } else { // create new user
    const status = (callerUserType === UserType.Admin) ? UserStatus.Active : UserStatus.Unverified;
    // account created in all chains
    const wallet = smartContract.getSmartContract(Chains.MainNet).createAccount();
    const newUser = await User.create({
      email,
      password: passwordHash,
      issuerIds,
      name,
      surname,
      secondName,
      birthDate,
      type,
      status,
      ethAddress: wallet.address,
      privateKey: wallet.privateKey,
      publicKey: wallet.publicKey
    });
    user = newUser.toJSON();
  }

  if (user.status !== UserStatus.Active) {
    const uuid = uuidv4();

    await Link.create({
      userId: user!._id,
      code: uuid
    });

    try {
      await emailService.sendSignupEmail({
        uuid,
        email,
        lang,
        hostname,
        origin
      });
    } catch (err) {
      throw Boom.internal(EmailErrorMessages.EmailProviderError);
    }
  }

  return user!;
}

async function login(params: LoginUserParams) {
  const { email, password } = params;
  const user: IUserDocument = await User.findOne({ email }, {privateKey: 0}).exec();
  let issuer: IIssuer;

  if (!user || user && user.status === UserStatus.Unregistered) {
    throw Boom.badRequest(UserErrorMessages.NotRegistered);
  }

  if (user.status === UserStatus.Unverified) {
    throw Boom.badRequest(UserErrorMessages.NotVerified);
  }

  const match = await argon.verify(user.password, password);
  if (!match) {
    throw Boom.badRequest(UserErrorMessages.InvalidPassword);
  }

  if (user.type === UserType.IssuerAmbassador) {
    issuer = await issuerDomain.getIssuerById(user.ambassadorIssuerId);
  }

  return { user, issuer };
}

async function checkUserConfirmationLink(code: string) {
  const link: ILink = await Link.findOneAndDelete({
    code
  }).lean();
  const now = Date.now();

  if (!link) {
    return false;
  }

  if (now - link.createdAt.getTime() > env.confLinkMaxAge) {
    return false;
  } else {
    await User.findOneAndUpdate({ _id: link.userId }, {
      status: UserStatus.Active
    });

    return true;
  }
}

interface ChangePasswordParams {
  userId: string;
  oldPassword: string;
  newPassword: string;
}

async function changePassword(params: ChangePasswordParams) {
  const { userId, oldPassword, newPassword } = params;
  const user: IUserDocument = await User.findOne({ _id: userId }).lean().exec();
  const currentPasswordHash = user.password;

  const match = await argon.verify(currentPasswordHash, oldPassword);

  if (match) {
    setPassword({
      user,
      newPassword
    });
    return true;
  } else {
    return false;
  }
}

interface ChangePasswordByTokenParams {
  token: string;
  newPassword: string;
}

const passwordReset = {

  async createToken(userId: string) {
    const newToken = uuidv4();
    const now = +(new Date());

    await User.findOneAndUpdate({ _id: userId }, {
      passwordResetToken: newToken,
      passwordResetTokenCreatedAt: now
    });

    return newToken;
  },

  async getActiveToken(userId: string) {
    const user = await User.findOne({ _id: userId }).lean().exec();
    if (!user.passwordResetToken || !user.passwordResetTokenCreatedAt) {
      return false;
    }
    const now = +(new Date());
    if (now - user.passwordResetTokenCreatedAt < env.passwordReset.tokenMaxAge) {
      return user.passwordResetToken;
    }
    return false;
  },

  deleteToken(userId: string) {
    return User.findOneAndUpdate({ _id: userId }, {
      passwordResetToken: null,
      passwordResetTokenCreatedAt: null
    }).exec();
  },

  async changePasswordByToken(params: ChangePasswordByTokenParams) {
    const { token, newPassword } = params;
    const user = await User.findOne({ passwordResetToken: token }).lean().exec();

    if (!user) {
      return false;
    }
    const now = +(new Date());
    if (now - user.passwordResetTokenCreatedAt < env.passwordReset.tokenMaxAge) {
      await setPassword({
        user,
        newPassword
      });
      await passwordReset.deleteToken(user._id);

      return true;
    }
    return false;
  }

};


interface SetPasswordParams {
  user: IUser;
  newPassword: string;
}

async function setPassword(params: SetPasswordParams) {
  const { user, newPassword } = params;
  const newPasswordHash = await argon.hash(newPassword);

  return  User.findOneAndUpdate({ _id: user._id }, {
    password: newPasswordHash,
  }).exec();
}


export default {
  getUser,
  getOrCreateUsersByEmail,
  getIssuerUsers,
  registerUser,
  login,
  checkUserConfirmationLink,
  changePassword,
  passwordReset
};
