import { ISubscriberDocument, Subscriber }     from '../models';
import emailService                            from '../services/Email';


export interface ISubscribeParams {
  email: string;
  name: string;
  phone: string;
  lang: string;
}

export default {
  createSubscriber
};

async function createSubscriber(params: ISubscribeParams): Promise<ISubscriberDocument> {
  const { email, name, phone } = params;

  const subscriber = await Subscriber.create({
    name,
    email,
    phone
  });

  await emailService.subscribeUser(params);

  return subscriber;
}
