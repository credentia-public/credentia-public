import { ITemplate, ICreateTemplateParams, Template, IUpdateTemplateParams } from '../models';

export { ICreateTemplateParams, IUpdateTemplateParams };

export default {
  getTemplate,
  getTemplateById,
  getIssuerTemplates,
  createTemplate,
  updateTemplate,
  deleteTemplate
};

export function getTemplate(query: Partial<ITemplate>): Promise<ITemplate> {
  return Template.findOne(query).lean().exec();
}

export function getTemplateById(templateId: string): Promise<ITemplate> {
  return Template.findById(templateId).lean().exec();
}

export function getIssuerTemplates(issuerId: string): Promise<ITemplate[]> {
  return Template.find({issuerId}).lean().exec();
}

export function createTemplate(params: ICreateTemplateParams) {
  return Template.create(params);
}

export function updateTemplate(templateId: string, data: Partial<IUpdateTemplateParams>): Promise<ITemplate> {
  return Template.findByIdAndUpdate(templateId, data, {new: true}).lean().exec();
}

export function deleteTemplate(templateId: string): Promise<void> {
  return Template.findByIdAndDelete(templateId).lean().exec();
}
