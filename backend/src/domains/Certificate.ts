import fs     from 'fs';
import path   from 'path';
import uuidv4 from 'uuid/v4';
import Boom   from '@hapi/boom';
import xml2js from 'xml2js';


import { Cert, ICert, ICertDocument, IUser, Keys } from '../models';
import issuerDomain                                from './Issuer';
import userDomain                                  from './User';
import templateDomain                              from './Template';

import converter         from '../services/Converter';
import signService       from '../services/SignService';
import emailService      from '../services/Email';
import logger            from '../services/Logger';
import smartContract     from '../services/SmartContract';
import pguReceiver       from '../services/PguReceiver';
import { ErrorMessages } from '../errors';


export type CreateCertsParams = Pick<ICert, 'issuer' | 'template' | 'group' | 'credentialSubject'> & {
  holders: Pick<IUser, 'email' | 'name' | 'surname' | 'secondName'>[];
};

export type GetCertParams = Partial<Pick<ICert, '_id' | 'externalId' | 'shareableLinkCode' | 'txId' | 'holder' | 'issuer'>>;

export type DeleteCertsParams = Pick<ICert, 'issuer'> & {
  certsIds: string[];
};

export type CreateLLCertParams = {
  externalId: string;
  course: string;
  emailHash: string;
  issuerName: string;
  templateName: string;
  name: string;
  surname: string;
  secondName: string;
};

export type SignCertsParams = {
  issuerId: string;
  certIds: string[];
};

export type PublishCertsParams = SignCertsParams & {
  hostname: string;
  lang: string;
  origin: string;
};


export default {
  getCertByCode,
  getCertById,
  getHolderCerts,
  getIssuerCerts,
  getCertByExternalId,
  getCertByAnyKeys,
  updateCert,
  createCerts,
  deleteCerts,
  signCerts,
  sendCertCreationEmail,
  publishCertsInBlockchain,
  createLinguaLeoCert,
  loadPGU,
  loadMGU
};


async function getCertById(certId: string) {
  const cert: ICert = await Cert.findOne({ _id: certId }, {
    certificateSocialMediaImage: 0
  })
  .lean()
  .exec();

  return cert;
}

async function getHolderCerts(userId: string) {
  const certs: ICert[] = await Cert.find({ holder: userId }, {
    certificateSocialMediaImage: 0
  })
  .populate('template')
  .populate('issuer')
  .lean()
  .exec();

  return certs;
}

async function getIssuerCerts(issuerId: string): Promise<ICert[]> {
  return Cert.find({ issuer: issuerId }, {
    certificateSocialMediaImage: 0
  })
  .populate('template')
  .lean().exec();
}

async function getCertByCode(code: string, incrementView = false): Promise<ICert> {
  return Cert.findOneAndUpdate({
    shareableLinkCode: code
  }, {
    $inc: {
      views: Number(incrementView)
    }
  }, {
    new: true
  })
  .populate('issuer')
  .populate('template')
  .populate('holder')
  .lean()
  .exec();
}

async function getCertByExternalId(externalId: string): Promise<ICert> {
  return Cert.findOne({
    externalId
  }, {
    certificateSocialMediaImage: 0
  })
  .lean()
  .exec();
}

async function getCertByAnyKeys(inFlt: any): Promise<ICertDocument[]> {
  return Cert.find(inFlt).exec();
}

async function updateCert(query: GetCertParams, data: RecursivePartial<ICert>): Promise<ICert> {
  return Cert.findOneAndUpdate(query, data, {
    new: true,
    fields: {
      certificateSocialMediaImage: 0
    }
  }).lean().exec();
}

async function createCerts(params: CreateCertsParams): Promise<ICertDocument[]> {
  const { credentialSubject, holders } = params;
  const issuerId = <string>params.issuer;
  const templateId = <string>params.template;
  const group = <string>params.group;

  const template = await templateDomain.getTemplateById(templateId);
  const issuer = await issuerDomain.getIssuerById(issuerId);
  const image = await converter.generateCertSocialImage({
    title: credentialSubject.title,
    textColor: template.textColor,
    bgColor: template.bgColor,
    imagePath: template.logoPath
  });
  const issuerData = {
    name: issuer.name,
    address: issuer.address,
    phone: issuer.phone,
    publicKey: issuer.publicKey
  };
  const users = await userDomain.getOrCreateUsersByEmail(holders.map(h => h.email), issuerId);

  const certsData = users.map(u => {
    const { name, surname, secondName, email } = holders.find(h => h.email === u.email);
    const certCredentialSubject = Object.assign({}, {
      email,
      name,
      surname,
      secondName,
      issuerData
    }, credentialSubject);

    return {
      template: templateId,
      group,
      holder: u._id,
      issuer: issuer._id,
      visible: true,
      shareableLinkCode: uuidv4(),
      certificateSocialMediaImage: image,
      credentialSubject: certCredentialSubject
    };
  });

  return Cert.create(certsData);
}

async function deleteCerts(params: DeleteCertsParams): Promise<void> {
  return Cert.deleteMany({
    issuer: params.issuer,
    txId: {
      $exists: false
    },
    $or: params.certsIds.map(id => {
      return {
        _id: id
      };
    })
  }).lean().exec();
}

async function signCerts(params: SignCertsParams): Promise<ICert[]> {
  const { issuerId, certIds } = params;
  const privateKey = await issuerDomain.getIssuerPrivateKey(issuerId);
  const certs: ICert[] = await Cert.find({ _id: { $in: certIds } }, {
    certificateSocialMediaImage: 0
  }).lean().exec();
  const proofs = [];
  await Promise.all<ICert>(certs.map(c => {

    const proof = signService.sign(privateKey, JSON.stringify(c.credentialSubject));

    proofs.push(proof);
    return Cert.updateOne({ _id: c._id }, {
      proof: {
        proofType: proof.type,
        verificationMethod: proof.verificationMethod,
        jws: proof.jws,
        created: proof.created
      }
    }).lean().exec();
  }));

  return certs.map((c, i) => {
    c.proof = proofs[i].proof;
    return c;
  });
}

async function sendCertCreationEmail(params: {
  certId: string,
  hostname: string,
  lang: string,
  origin:string
}): Promise<void> {
  const { certId, hostname, lang, origin } = params;
  const c: ICert = await Cert.findById(certId).populate('holder').lean().exec();
  const issuer = await issuerDomain.getIssuerById(<string>c.issuer);

    const email = (<IUser>c.holder).email;
    const { surname, name, secondName } = c.credentialSubject;

    emailService.sendCertCreationToHolder({
      email,
      issuerName: issuer.name,
      fullName: [surname, name, secondName].filter(notEmpty => notEmpty).join(' '),
      certLink: `${origin}/app/document/${c.shareableLinkCode}`,
      regLink: `${origin}/app/signup`,
      lang
    })
    .catch(err => {
      logger.error(err, `Failed to send email, address: ${email}, hostname: ${hostname}`);
    });
}

async function publishCertsInBlockchain(params: PublishCertsParams): Promise<ICert[]> {
  const { issuerId, certIds, hostname, lang, origin } = params;
  const issuer = await issuerDomain.getIssuerById(issuerId);
  console.log(issuer)
  const privateKey = await issuerDomain.getIssuerPrivateKey(issuerId);
  const certs1: ICert[] = await Cert.find({ _id: { $in: certIds } }, {
    certificateSocialMediaImage: 0
  }).populate('holder').lean().exec();

  let certs;
  if (issuer.shortName === 'pgu') {
    certs = await Promise.all(certs1.map(async (i: ICert): Promise<ICert> => {
      return await pguReceiver.findAndSubstitutePrivateData(<string>i.credentialSubject.documentRegNumber, i);
    }));
  } else {
    certs = certs1;
  }

  const certsAmount = issuer.availableCertsAmount;
  const newCertsAmount = certsAmount - certIds.length;

  if (newCertsAmount < 0 && certsAmount !== -1) {
    throw Boom.badRequest(ErrorMessages.CertLimitExceeded);
  }

  const certsToSign = certs.filter(c => c.proof === undefined).map(c => c._id);
  await signCerts({ certIds: certsToSign, ...params });

  const txs = await smartContract
  .getSmartContract(issuer.chain)
  .signAndSend(privateKey, certs.filter(c => (<IUser>c.holder).ethAddress !== undefined).map(c => {
    const certId = c._id.toString();
    return {
      certId,
      data: c.credentialSubject,
      holder: (<IUser>c.holder).ethAddress
    };
  }));

  await Promise.all<ICert>(certs.map((c, i) => {
    return Cert.updateOne({ _id: c._id }, {
      txId: txs[i],
      chain: issuer.chain
    }).lean().exec();
  }));

  if (certsAmount !== -1) {
    await issuerDomain.updateIssuer(issuerId, {
      availableCertsAmount: newCertsAmount
    });
  }

  for (const c of certs) {
    const email = (<IUser>c.holder).email;
    const { surname, name, secondName } = c.credentialSubject;

    emailService.sendCertCreationToHolder({
      email,
      issuerName: issuer.name,
      fullName: [surname, name, secondName].filter(notEmpty => notEmpty).join(' '),
      certLink: `${origin}/app/document/${c.shareableLinkCode}`,
      regLink: `${origin}/app/signup`,
      lang
    })
    .catch(err => {
      logger.error(err, `Failed to send email, address: ${email}, hostname: ${hostname}`);
    });
  }

  return Cert.find({ _id: { $in: certIds } }, {
    certificateSocialMediaImage: 0
  }).lean().exec();
}

async function createLinguaLeoCert(params: CreateLLCertParams): Promise<ICert> {
  const { externalId, issuerName, course, templateName, emailHash, name, surname, secondName } = params;
  const issuer = await issuerDomain.getIssuer({ shortName: issuerName });
  const template = await templateDomain.getTemplate({ issuerId: issuer._id, name: templateName });
  const image = await converter.generateCertSocialImage({
    title: course,
    textColor: template.textColor,
    bgColor: template.bgColor,
    imagePath: template.logoPath
  });
  const issuerData = {
    name: issuer.name,
    address: issuer.address,
    phone: issuer.phone,
    publicKey: issuer.publicKey
  };
  const users = await userDomain.getOrCreateUsersByEmail([emailHash], issuer._id);
  const proof = await signService.sign(issuer.privateKey, JSON.stringify({
    course,
    name,
    surname,
    secondName,
    issuer: issuerData
  }));

  const certs = await Cert.create({
    externalId,
    template: template._id,
    group: '',
    holder: users[0]._id,
    issuer: issuer._id,
    visible: true,
    shareableLinkCode: uuidv4(),
    certificateSocialMediaImage: image,
    credentialSubject: {
      course,
      name,
      surname,
      secondName
    },
    proof: {
      proofType: proof.type,
      verificationMethod: proof.verificationMethod,
      jws: proof.jws,
      created: proof.created
    }
  });

  return certs;
}

async function loadPGU(issuerId: string, templateId: string) {
  const data = await pguReceiver.getDataForDB();

  const template = await templateDomain.getTemplateById(templateId);
  const issuer = await issuerDomain.getIssuerById(issuerId);

  // const dataAddCnt = await Promise.all(data.map(async i =>{
  //   const c = {
  //     count: await Cert.find({
  //       'credentialSubject.name': i.name,
  //       'credentialSubject.date': i.date,
  //       'credentialSubject.surname': i.surname,
  //       'credentialSubject.secondName': i.secondName,
  //       'credentialSubject.documentRegNumber': i.documentRegNumber
  //     }).lean(true).countDocuments(),
  //     ...i
  //   };
  //   return c;
  // }));

  // const fltCerts = dataAddCnt.filter(ii => {
  //   return ii.count === 0;
  // }).map(j => {
  //   const { count: _cnt, ...other } = j;
  //   return { ...other }
  // });

  const dataAddCnt = await Promise.all(data.map(async i => {
    const keys = await Keys.find({ 'keys.documentRegNumber': i.documentRegNumber, 'keys.date': i.date }).lean(true);
    let shareableLinkCode: string;
    if (keys.length === 0) {
      const newKey = await Keys.create({
        shareableLinkCode: uuidv4(),
        keys: { documentRegNumber: i.documentRegNumber, date: i.date }
      });
      shareableLinkCode = newKey.shareableLinkCode;
    } else {
      shareableLinkCode = keys[0].shareableLinkCode;
    }

    const c = {
      count: await Cert.find({
        'credentialSubject.name': i.name,
        'credentialSubject.date': i.date,
        'credentialSubject.surname': i.surname,
        'credentialSubject.secondName': i.secondName,
        'credentialSubject.documentRegNumber': i.documentRegNumber
      }).lean(true).countDocuments(),
      shareableLinkCode,
      ...i
    };
    return c;
  }));

  const fltCerts = dataAddCnt.filter(i => {
    return i.count === 0;
  }).map(j => {
    const { count: _cnt, ...other } = j;
    return { ...other };
  });

  // tslint:disable-next-line:no-string-literal
  // delete fltCerts['count'];

  const certs = fltCerts.map(async d => {
    const image = await converter.generateCertSocialImage({
      title: d.qualification,
      textColor: template.textColor,
      bgColor: template.bgColor,
      imagePath: template.logoPath
    });
    const { shareableLinkCode, ...other } = d;
    const c = {
      template: templateId,
      issuer: issuer._id,
      group: '544206',
      visible: false,
      shareableLinkCode,
      certificateSocialMediaImage: image,
      credentialSubject: { ...other }
    };

    return Cert.create(c);
  });

  return Promise.all(certs);
}

async function loadMGU() {
  const parser = new xml2js.Parser({
    explicitArray: false
  });

  const issuerName = 'mgu';
  const templateName = 'diplom';
  const template = await templateDomain.getTemplate({ name: templateName });
  const issuer = await issuerDomain.getIssuer({ shortName: issuerName });

  const content = await new Promise((res, rej) => {
    // tslint:disable-next-line:no-shadowed-variable
    fs.readFile(path.resolve(__dirname, '../files/test_data/mgu.xml'), async (err, data) => {
      if (err) {
        rej(err);
      }

      res(data);
    });
  });
  const data = await parser.parseStringPromise(content);
  const d = data.diploma_supplement;
  const personal = d.Personal_Information;
  const qual = d.Qualification;
  const results = d.Contents_and_Results;
  const programma = results.Programme_Detailes.Programme_item;
  const courseWorks = results.Programme_Detailes.Course_Papers.Course_Paper;
  const practice = results.Programme_Detailes.Practical_Training.Practice;
  const finalExam = results.Programme_Detailes.Final_State_Examinations.Exam;
  const thesis = results.Programme_Detailes.Final_Qualifying_Paper;
  let currentModule;
  const courses = {};
  const practices = practice.Number.map((_n, i) => {
    return {
      name: practice.Title_Rus[i].trim(),
      credits: practice.Credits[i].trim(),
      hours: practice.Academic_Hours[i].trim(),
      mark: practice.Mark_Rus[i].trim()
    };
  });

  const image = await converter.generateCertSocialImage({
    title: `Диплом ${d.Level_of_Education_Information.Level_of_Education_Rus.trim()}`,
    textColor: template.textColor,
    bgColor: template.bgColor,
    imagePath: template.logoPath
  });

  programma.Number.forEach((_n, i) => {
    if (!programma.Number[i].trim()) {
      currentModule = programma.Course_title_Rus[i].trim();
      courses[currentModule] = [];
    } else {
      courses[currentModule].push({
        name: programma.Course_title_Rus[i].trim(),
        credits: programma.Credits[i].trim(),
        hours: programma.Academic_Hours[i].trim(),
        mark: programma.Mark_Rus[i].trim()
      });
    }
  });

  const c = {
    template: template._id,
    issuer: issuer._id,
    visible: false,
    shareableLinkCode: uuidv4(),
    certificateSocialMediaImage: image,
    credentialSubject: {
      imageLogoURL: '/static/images/mgu@1.png',
      documentNumber: qual.Field_of_study.Diplom.trim(),
      title: `Диплом ${d.Level_of_Education_Information.Level_of_Education_Rus.trim()}`,
      city: 'г. Москва',
      documentRegNumber: results.Programme_DetailesReg_Number.trim(),
      studentId: personal.Student_Id.trim(),
      issuerName: d.Name_and_status_of_awarding_institution.Name_Rus.trim() + d.Name_and_status_of_awarding_institution.Status_Rus.trim(),
      qualification: qual.Name_and_Date_of_qualification.Name_of_qualification_Rus.trim().toLowerCase(),
      specialization: qual.Field_of_study.Special.Special_Rus.trim(),
      date: qual.Name_and_Date_of_qualification.Date_of_qualification.trim(),
      name: personal.Family_Name.Family_Name_Rus.trim(),
      surname: personal.Given_Name.Given_Name_Rus.trim(),
      secondName: '',
      holderBirthday: personal.Date_of_birth.trim(),
      DocumentSpecialty: qual.Field_of_study.Special.Special_Rus.trim(),
      HolderEduPrevious: personal.Prev_education_Doc.Prev_education_Doc_Rus.trim(),
      HolderEduStart: '',
      HolderEduEnd: qual.Name_and_Date_of_qualification.Date_of_qualification.trim(),
      HolderEduYears: d.Length_of_Full_time_Programme.Length_Rus.trim(),
      ModeStudy: results.Mode_of_study.Mode_of_study_Rus.trim(),
      HolderEduCourses: courses,
      HolderEduCoursework: [{
        name: courseWorks.Title_Rus.trim(),
        credits: courseWorks.Credits.trim(),
        hours: courseWorks.Academic_Hours.trim(),
        mark: courseWorks.Mark_Rus.trim()
      }],
      HolderEduPractice: practices,
      HolderEduExitExam: [{
        name: finalExam.Subject_Title_Rus.trim(),
        credits: finalExam.Credits.trim(),
        hours: finalExam.Academic_Hours.trim(),
        mark: finalExam.Mark_Rus.trim()
      }],
      HolderEduThesis: [{
        name: thesis.Title_Rus.trim(),
        credits: thesis.Credits.trim(),
        hours: thesis.Academic_Hours.trim(),
        mark: thesis.Mark_Rus.trim()
      }]
    }
  };

  return Cert.create(c);
}
