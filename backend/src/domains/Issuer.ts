import { IIssuer, Issuer } from '../models';
import smartContract       from '../services/SmartContract';
import env from '../environments/environment';

export default {
  getIssuer,
  getIssuerById,
  getIssuerPrivateKey,
  createIssuer,
  updateIssuer,
  deleteIssuer
};

export type GetIssuerParams = Partial<Exclude<IIssuer, 'privateKey' | 'createdAt' | 'updatedAt'>>;
export type CreateIssuerParams = Pick<IIssuer, 'name' | 'address' | 'phone' | 'shortName' | 'chain' | 'availableCertsAmount'>;
export type UpdateIssuerParams = Partial<Exclude<IIssuer, '_id' | 'createdAt' | 'updatedAt'>>;


function getIssuer(query: GetIssuerParams): Promise<IIssuer> {
  return Issuer.findOne(query, {
    privateKey: 0
  }).lean().exec();
}

function getIssuerById(issuerId: string): Promise<IIssuer> {
  return Issuer.findById(issuerId, {
    privateKey: 0
  }).lean().exec();
}

async function getIssuerPrivateKey(issuerId: string): Promise<string> {
  const issuer: IIssuer = await Issuer.findById(issuerId).lean().exec();

  return issuer.privateKey;
}

function createIssuer(params: CreateIssuerParams) {
  const { name, address, phone, shortName, chain } = params;
  let { availableCertsAmount } = params;
  const keys = smartContract.getSmartContract(chain).createAccount();

  if (env.env === 'development' && availableCertsAmount === undefined) {
    availableCertsAmount = -1;
  }

  return Issuer.create({
    name, address, phone, availableCertsAmount,
    shortName: shortName || name.trim().toLowerCase().replace(/[^a-z0-9]/g, ''),
    privateKey: keys.privateKey,
    ethAddress: keys.address,
    publicKey: keys.publicKey
  });
}

function updateIssuer(issuerId: string, params: UpdateIssuerParams): Promise<IIssuer> {
  return Issuer.findByIdAndUpdate(issuerId, params, { new: true, fields: {
    privateKey: 0
  }}).lean().exec();
}

function deleteIssuer(issuerId: string): Promise<void> {
  return Issuer.findByIdAndDelete(issuerId).lean().exec();
}
