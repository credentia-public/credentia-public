import 'marko/node-require'; // needful to import controllers
// @ts-ignore
import colors from 'colors';

import * as describedApi from './credentia-api.postman_collection.json'
import controllers from '../controllers';

const replaces = {
  // postman: express
  '{{api}}': '',
  '{{': ':',
  '}}': ''
};

const describedApiPoints = describedApi.item.reduce((acc, folder) => {
  const folderPoints = [];
  folder.item.forEach(point => {
    const pointMethod = point.request.method;
    let pointUrl = point.request.url.raw;

    if (~pointUrl.indexOf('?')) {
      pointUrl = pointUrl.substring(0, pointUrl.indexOf('?'));
    }

    Object.entries(replaces).forEach(([ search, replacement ]) => {
      pointUrl = pointUrl.split(search).join(replacement);
    });

    const pointSignature = `${pointMethod} ${pointUrl}`;
    folderPoints.push(pointSignature);
  });
  return [...acc, ...folderPoints];
}, []);

const implementedApiPoints = Object.keys(controllers);

const allPoints = [...new Set([
  ...describedApiPoints,
  ...implementedApiPoints
])];

const columns = {
  'Described': describedApiPoints,
  'Implemented': implementedApiPoints
};

const is = something => something ? colors.green('✅') : colors.red('❌');

const isIntegration = allPoints.every(point => {
  return ~describedApiPoints.indexOf(point) && ~implementedApiPoints.indexOf(point);
});

const createIntegrationMap = () => {
  const integrationMapTitle = [
    ...Object.keys(columns),
    'Endpoint'
  ].join(' | ');
  const integrationMapContent = allPoints.reduce((acc, point) => {

    const row = [
      ...Object.keys(columns).map(columnName => {
        const set = columns[columnName];
        return is(~set.indexOf(point)) + ' ';
      }),
      point
    ].join(' ');

    return [...acc, row];
  }, []).join('\n');
  return integrationMapTitle + '\n\n' + integrationMapContent;
};

const report = [
  `▒▒▒ API endpoints:`,
  createIntegrationMap(),
  `${describedApiPoints.length} described, ${implementedApiPoints.length} implemented`,
].join('\n\n');

console.log(report);

if (isIntegration) {
  process.exit(0);
} else {
  console.error('The described and implemented points are different. Stop.');
  process.exit(1);
}
