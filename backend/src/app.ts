import bodyParser                                   from 'body-parser';
import Boom                                         from '@hapi/boom';
import mongoStoreFactory                            from 'connect-mongo';
import cors                                         from 'cors';
import express, { NextFunction, Request, Response } from 'express';
import session                                      from 'express-session';
import responseTime                                 from 'response-time';
import http                                         from 'http';
import { AddressInfo }                              from 'net';
import stoppable                                    from 'stoppable';
import i18next, { InitOptions }                     from 'i18next';
import Backend                                      from 'i18next-node-fs-backend';
import i18nextMiddleware                            from 'i18next-express-middleware';

import env               from './environments/environment';
import { ErrorMessages } from './errors';
import database          from './services/Database';
import logger            from './services/Logger';
import detailedUser      from './utils/auth';
import localeDiffs       from './utils/localeDiffs';

import controllers       from './controllers'


const expressApp: express.Express = express();
const httpServer = stoppable(http.createServer());


const start = async () => {
  await database.runMigrations();
  await database.listen();

  await setMiddlewares();

  httpServer.on('request', expressApp);

  httpServer.on('listening', () => {
    const { address } = <AddressInfo>httpServer.address();

    logger.info(`Server started on ${address}:${env.port}`);
  });

  httpServer.on('error', (err: Error) => {
    logger.error(err, 'Error starting server');
  });

  httpServer.listen(env.port);

  const localeDiffsList = localeDiffs.getList();

  if (localeDiffsList) {
    logger.error(`There are differences in the locales: \n${localeDiffsList}`);
  }

};


const stop = async (): Promise<void> => {
  await database.close();

  return new Promise(res => {
    httpServer.stop(() => {
      res();
    });
  });
};

const setMiddlewares = async () => {
  const app = expressApp;

  await i18next
  .use(Backend)
  .use(i18nextMiddleware.LanguageDetector)
  .init(<InitOptions>env.i18next);

  const MongoStore = mongoStoreFactory(session);

  const sessionOptions = Object.assign({}, env.session, {
    store: new MongoStore({
      mongooseConnection: database.getConnection(),
      ttl: 14 * 24 * 60 * 60, // save session 14 days
      stringify: false
    })
  });

  app.use(responseTime((req: Request, _res: Response, time: number) => {
    logger.debug(`${req.method} ${req.url} ${time}ms`);
  }));
  app.use(i18nextMiddleware.handle(i18next));
  app.use(session(sessionOptions));
  // app.use(helmet(env.helmet));
  app.use(bodyParser.json({ limit: '50mb' }));
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));
  app.use(cors(env.cors));
  // app.use(function pageVisits(req, _, next) {
  //   if (!req.session.views) {
  //     req.session.views = {};
  //   }
  //
  //   next();
  // });

  const apiMethods = ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'];

  Object.entries(controllers).map(controller => {

    const [methodPath, handler] = controller;
    const [method, path] = methodPath.split(' ');

    if (!path) {
      throw Boom.notFound(ErrorMessages.NotFound);
    }
    if (!method || !apiMethods.includes(method)) {
      throw Boom.methodNotAllowed(ErrorMessages.MethodNotAllowed);
    }

    app[method.toLowerCase()]('/api' + path, async (req, res, next) => {
      try {
        const userSession = req.session;
        const user = await detailedUser.getBySession(userSession);
        // @ts-ignore
        const responser = await handler({
          req,
          res,
          next,
          user
        });
        if (responser) {
          responser(res);
        } else {
          throw new Error(`'${methodPath}' has no response`);
        }
      } catch (err) {
        next(err);
      }
    });

    return null;

  });


  // @ts-ignore
  app.use((err: Boom, req: Request, res: Response, _next: NextFunction) => {
    const boomError = Boom.isBoom(err) ? err : Boom.internal(ErrorMessages.Internal);

    if (err.isServer || !Boom.isBoom(err)) {
      logger.error(err);
    }

    return res.status(boomError.output.statusCode).json({ status: 'error', ...boomError.output.payload });
  });
};

export default { start, stop };
