import { CustomError } from 'ts-custom-error';

export enum ErrorMessages {
  BadRequest = 'bad_request',
  NotAuthenticated = 'not_authenticated',
  Internal = 'internal_error',
  ValidationError = 'validation_error',
  InvalidCurrency = 'invalid_currency',
  InvalidUserType = 'invalid_user_type',
  TooManyRequests = 'too_many_requests',
  NotAuthorized = 'not_authorized',
  OperationNotAllowed = 'not_allowed',
  NotFound = 'not_found',
  MethodNotAllowed = 'method_not_found',
  CertLimitExceeded = 'certs_limit_exceeded',
  NotEnoughGas = 'not_enough_gas',
  NoMatchedDocuments = 'no_matched_documents'
}

export enum UserErrorMessages {
  NotRegistered = 'not_registered',
  NotVerified = 'not_verified',
  InvalidPassword = 'invalid_password',
  AlreadyExist = 'already_exist',
}

export enum EmailErrorMessages {
  EmailProviderError = 'email_internal_error'
}

export class ApiError extends CustomError {
  statusCode: number;

  constructor(message: string, statusCode: number) {
    super(message);
    this.statusCode = statusCode;
  }
}
