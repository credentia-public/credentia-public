import Boom              from '@hapi/boom';
import mime              from 'mime-types';

import { ErrorMessages } from './errors';

export function success() {
  return res => {
    res.status(200).json({ status: 'ok' });
  };
}

export function errorBadRequest() {
  return res => {
    const boom = Boom.badRequest(ErrorMessages.BadRequest);
    res.status(400).json({ status: 'error', ...boom.output.payload });
  };
}

export function errorNotAuthorized() {
  return res => {
    const boom = Boom.unauthorized(ErrorMessages.NotAuthenticated);
    res.status(401).json({ status: 'error', ...boom.output.payload });
  };
}

export function errorForbidden() {
  return res => {
    const boom = Boom.forbidden(ErrorMessages.OperationNotAllowed);
    res.status(403).json({ status: 'error', ...boom.output.payload });
  };
}

export function errorNotFound() {
  return res => {
    const boom = Boom.notFound(ErrorMessages.NotFound);
    res.status(404).json({ status: 'error', ...boom.output.payload });
  };
}

export function errorTooManyRequests() {
  return res => {
    const boom = Boom.tooManyRequests(ErrorMessages.TooManyRequests);
    res.status(429).json({ status: 'error', ...boom.output.payload });
  };
}

export function errorInternal() {
  return res => {
    const boom = Boom.internal(ErrorMessages.Internal);
    res.status(500).json({ status: 'error', ...boom.output.payload });
  };
}

export function successJSON(data?: any) {
  return res => {
    res.status(200).json({ status: 'ok', data });
  };
}

export function errorJSON(error: string, statusCode: number) {
  return res => {
    res.status(statusCode).json({ status: 'error', error });
  };
}

export function JSONLD(data: any = {}) {
  return res => {
    const output = JSON.stringify(data, null, 2);
    res.set({
      'Content-Type': 'application/ld+json'
    });
    res.status(200).send(output);
  };
}

export function redirect(to: string) {
  return res => {
    res.redirect(to);
  };
}

export function showFile(data: Buffer, fileName: string) {
  return res => {
    const contentType = mime.contentType(fileName);
    res.set({
      'Content-Type': contentType
    });
    res.send(data);
  };
}

export function sendFile(data: Buffer, fileName: string) {
  return res => {
    res.set({
      'Content-Disposition': `attachment; filename="${encodeURI(fileName)}"`,
      'Content-Type': 'application/octet-stream',
    });
    res.send(data);
  };
}
