export interface ICreateLLCertBody {
  courseId: string;
  course: string;
  name: string;
  surname: string;
  secondName: string;
  emailHash: string;
  date: string;
  issuerName: string;
  templateName: string;
}

export interface ICreateLLCertParams extends ICreateLLCertBody {
  externalId: string;
  hostname: string;
  origin: string;
  lang: string;
}

