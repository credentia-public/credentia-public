import crypto from 'crypto';
import ecdsa  from 'ecdsa-secp256k1';

export function signSecp256k1(pkHex, msg) {
  const privateKeyNum = BigInt(pkHex);
  const sha1 = crypto.createHash('sha256').update(msg).digest('hex');
  const msgDataNum = BigInt(`0x${sha1}`);
  const signData = ecdsa.sign(privateKeyNum, msgDataNum);
  return signData.r.toString(16) + signData.s.toString(16);
}

export function verifySecp256k1(privateKey, signature, msg) {
  const privateKeyNum = BigInt(privateKey);
  const publicKeyPoint = ecdsa.getPublicKeyPoint(privateKeyNum);

  const publicKeyPointDict = {
    x: BigInt('0x' + ecdsa.publicKeyPoint2HexStr(publicKeyPoint).substring(4, 68)),
    y: BigInt('0x' + ecdsa.publicKeyPoint2HexStr(publicKeyPoint).substring(68, 132))
  };

  const sha1 = crypto.createHash('sha256').update(msg).digest('hex');
  const msgDataNum = BigInt(`0x${sha1}`);

  const sign = {
    r: BigInt('0x' + signature.substring(0, 64)),
    s: BigInt('0x' + signature.substring(64, 128))
  };

  return ecdsa.verify(publicKeyPointDict, sign, msgDataNum);
}
