import { Request } from 'express';
import aws        from 'aws-sdk';
import multer     from 'multer';
import multerS3   from 'multer-s3';

import env      from '../environments/environment.local';

interface MulterRequest extends Request {
  filePath: string;
}

// const spacesEndpoint = new aws.Endpoint(env.cloudStorage.endpoint);

const s3 = new aws.S3({
  endpoint: env.cloudStorage.endpoint,
  accessKeyId: env.cloudStorage.aws_access_key_id,
  secretAccessKey: env.cloudStorage.aws_secret_access_key
});

const upload = multer({
  storage: multerS3({
    s3,
    bucket: env.cloudStorage.Bucket,
    acl: 'public-read',
    contentType: multerS3.AUTO_CONTENT_TYPE,
    key(req: MulterRequest, file, cb) {
      if (!req.filePath) {
        const err = new Error('To upload file filePath should be defined on request object');
        cb(err);
      } else {
        cb(null, `${req.filePath}/${file.originalname}`);
      }
    }
  })
}).array('cert-files', 10);

function download(path: string, fileName: string): Promise<any> {
  const params = { Bucket: env.cloudStorage.Bucket, Key: path + fileName };
  const obj = new Promise((res, rej) => {
    s3.getObject(params, (err, data) => {
      if (err) rej(err);
      else res(data);
    });
  });
  return obj;
}

// function listBuckets() {
//   const buckets = new Promise((res, rej) => {
//     s3.listBuckets({}, (err, data) => {
//       if (err) {
//         rej(err);
//       } else {
//         res(data.Buckets);
//       }
//     });
//   });
//
//   return buckets;
// }

export default {
  upload,
  download,
  // listBuckets
};
