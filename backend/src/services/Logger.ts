import pino                     from 'pino';
import { Logger as PinoLogger } from 'pino';

import env from '../environments/environment';

const stream = pino.destination(1);
const logger: PinoLogger = pino(env.logger, stream);

export default logger;