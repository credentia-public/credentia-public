import path from 'path';
import url  from 'url';
import {
  createCanvas,
  registerFont,
  loadImage,
  CanvasRenderingContext2D,
  Image
} from 'canvas';

import env from '../environments/environment';

export default {
  generateCertSocialImage,
}


const padding = 12;
const fontSize = 18;

registerFont(
  path.resolve(__dirname, '../files/fonts/IBMPlexSans/IBMPlexSans-Regular.ttf'),
  { family: 'IBMPlexSans' }
);

export interface IGenerateCertSocialImageParams {
  width?: number;
  height?: number;
  title: string;
  textColor: string;
  bgColor: string;
  imagePath: string;
}


async function generateCertSocialImage(params: IGenerateCertSocialImageParams): Promise<Buffer> {
  const { width = 300, height = 300 } = params;
  const canvas = createCanvas(width, height);
  const ctx = canvas.getContext('2d');
  ctx.imageSmoothingEnabled = false;
  ctx.antialias = 'subpixel';
  ctx.imageSmoothingEnabled = true;
  ctx.patternQuality = 'best';
  ctx.filter = 'bilinear';
  drawBackground(ctx, params.bgColor);
  drawTitle(ctx, params.title, params.textColor);
  await drawImage(ctx, params.imagePath);

  return new Promise((res, rej) => {
    canvas.toBuffer((err, buf) => {
      if (err) {
        rej(err);
      } else {
        res(buf);
      }
    }, 'image/png', {
      compressionLevel: 9,
      filters: canvas.PNG_FILTER_NONE
    });
  });
}

const drawBackground = (ctx: CanvasRenderingContext2D, bgColor: string) => {
  const { width, height } = ctx.canvas;
  // BACKGROUND
  // Set rectangle and corner values
  const radius = 16;

  ctx.fillStyle = bgColor;
  ctx.strokeStyle = bgColor;
  // Set faux rounded corners
  ctx.lineJoin = 'round';
  ctx.lineWidth = radius;

  // Change origin and dimensions to match true size (a stroke makes the shape a bit larger)
  ctx.strokeRect(radius / 2, radius / 2, width - radius, height - radius);
  ctx.fillRect(radius / 2, radius / 2, height - radius, height - radius);
}

const drawTitle = (ctx: CanvasRenderingContext2D, text: string, color: string) => {
  const { width } = ctx.canvas;

  ctx.font = 'normal 400 18px IBMPlexSans';
  ctx.fillStyle = color;
  const lines = getLines(ctx, text, width - padding * 2);
  lines.forEach((line, i) => {
    ctx.fillText(line, padding, padding + ((i + 1) * fontSize));
  });
}

const getLines = (ctx, text, maxWidth) => {
  const words = text.split(' ');
  const lines = [];
  let currentLine = words[0];

  for (let i = 1; i < words.length; i++) {
    const word = words[i];
    const width = ctx.measureText(currentLine + ' ' + word).width;
    if (width < maxWidth) {
      currentLine += ' ' + word;
    } else {
      lines.push(currentLine);
      currentLine = word;
    }
  }
  lines.push(currentLine);
  return lines;
}

async function drawImage(ctx: CanvasRenderingContext2D, imagePath: string) {
  const { width, height } = ctx.canvas;
  const urlObj = url.parse(imagePath);
  let image: Image;

  if (urlObj.hostname) {
    image = await loadImage(imagePath);
  } else if (path.isAbsolute(imagePath)) {
    const url = `http://localhost:${env.frontendPort}/${imagePath}`;
    image = await loadImage(url);
  } else {
    image = await loadImage(path.resolve(__dirname, imagePath));
  }
  const ratio = image.width / image.height;
  const imageWidth = image.width + padding * 2 > width ? width - padding * 2 : image.width;
  const imageHeight = imageWidth / ratio;

  ctx.drawImage(image,
    (width - imageWidth) / 2,
    height - (padding * 2) - imageHeight, imageWidth, imageHeight);
}
