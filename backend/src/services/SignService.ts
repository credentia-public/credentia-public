import { signSecp256k1, verifySecp256k1 } from './DigitalSignatureSecp256k1';

export default {
  sign,
  verify
}

function sign(privateKey: string, data: string) {
  const jws = signSecp256k1(privateKey, data);

  return {
    jws,
    created: new Date(),
    type: 'secp256k1',
    verificationMethod: 'https://www.npmjs.com/package/ecdsa-secp256k1'
  };
}

function verify(privateKey: string, signature: string, data: any) {
  // const instance = Wallet.fromPrivateKey(Buffer.from(user.privateKey.substr(2, 66), 'hex'));
  // const pubKey = instance.getPublicKey();
  return verifySecp256k1(privateKey, signature, data);
}
