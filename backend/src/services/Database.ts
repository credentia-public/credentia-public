import path from 'path';
import mongoose from 'mongoose';
// tslint:disable-next-line:no-var-requires
import migrate from 'migrate';
// import migrate from 'mongo-migrations';

import env    from '../environments/environment';
import logger from './Logger';


let client;

export default {

  async runMigrations() {
    return new Promise((res, rej) => {
      migrate.load({
        stateStore: '.migrate',
        migrationsDirectory: path.resolve(__dirname, '../migrations')
      }, (err: Error, set: any) => {
        if (err) {
          rej(err);
        } else {
          // tslint:disable-next-line:no-shadowed-variable
          set.up(err => {
            if (err) {
              rej(err);
            } else {
              res();
            }
          });
        }
      });
    });
  },

  async listen() {
    const { url, ...config } = env.db;

    logger.info('Connecting to ' + url);

    if (process.env.MONGO_DEBUG) {
      mongoose.set('debug', true);
    }

    try {
      client = await mongoose.connect(url, config);
      logger.info('Connected to MongoDB');
      return client;
    } catch (e) {
      logger.error(e, 'Failed to connect to MongoDB');
      throw e;
    }
  },

  async close() {
    return client.disconnect();
  },

  getConnection() {
    return client.connection;
  }
};
