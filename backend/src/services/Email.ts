import sendgrid        from '@sendgrid/mail';
import path            from 'path';
import i18next         from 'i18next';
import { CustomError } from 'ts-custom-error';
import Template        from 'marko/src/runtime/html/Template';

import env             from '../environments/environment';
import logger          from './Logger';

type MailData = import('@sendgrid/helpers/classes/mail').MailData;

export class SMTPError extends CustomError {
  statusCode: number;
  body: any;

  constructor(message: string, statusCode: number, body: any) {
    super(message);
    this.statusCode = statusCode;
    this.body = body;
  }
}

export interface ICertCreationEmailParams {
  fullName: string;
  issuerName: string;
  certLink: string;
  regLink: string;
  lang: string;
  email: string;
}

export interface ISignUpEMailParams {
  uuid: string;
  email: string;
  lang: string;
  origin: string;
  hostname: string;
}

export interface IUserSubscribedEmailParams {
  email: string;
  name: string;
  phone: string;
  lang: string;
}

export interface IPasswordResetEmailParams {
  email: string;
  link: string;
  lang: string;
}

// tslint:disable-next-line:no-var-requires
const confirmEmail: Template = require(path.resolve(__dirname, '../files/templates/emails/reg_confirm.marko'));
// tslint:disable-next-line:no-var-requires
const notifyNewSubscriberEmail: Template = require(
  path.resolve(__dirname, '../files/templates/emails/new_subscriber_notify.marko')
);
// tslint:disable-next-line:no-var-requires
const certCreationEmail: Template = require(path.resolve(__dirname, '../files/templates/emails/cert_creation.marko'));

// tslint:disable-next-line:no-var-requires
const passwordResetEmail: Template = require(path.resolve(__dirname, '../files/templates/emails/password_reset.marko'));


// @ts-ignore
sendgrid.setApiKey(env.email.apiKey);


export default {
  sendEmail,
  sendSignupEmail,
  sendNotifyNewSubscriber,
  subscribeUser,
  sendCertCreationToHolder,
  sendPasswordResetEmail
};

async function sendEmail(params: MailData[]) {
  let responses;
  let firstResponse;

  for (const e of params) {
    const { subject, from, to, html } = e;
    logger.debug('Sending email', { subject, from, to });

    if (env.env === 'local') {
      logger.debug('(Mocked, no real send)');
      logger.debug('━━━━━━━━━━━━━━━━━━━━━━━━━━━━');
      logger.debug(html);
      logger.debug('━━━━━━━━━━━━━━━━━━━━━━━━━━━━');
    }
  }

  if (env.env === 'local') {
    return Promise.resolve({
      statusCode: 200,
      message: 'success',
      body: null
    });
  }
  console.log(params)
  try {
    responses = await sendgrid.send(params, true); // multiple reponses for every email that was sent
    firstResponse = responses[0][0]; // for now we are only interested in first response
    (<any>responses).forEach(r => {
      logger.debug('Response from SMTP service', {
        statusCode: r[0].statusCode,
        message: r[0].statusMessage,
        body: r[0].body
      });
    });

    if (firstResponse.statusCode >= 300) {
      throw new SMTPError(firstResponse.statusMessage, firstResponse.statusCode, firstResponse.body);
    }
  } catch (err) {
    logger.error(err, 'Email service error');
    throw err;
  }

  return {
    statusCode: firstResponse.statusCode,
    message: firstResponse.statusMessage,
    body: firstResponse.body
  };
}

async function sendSignupEmail(params: ISignUpEMailParams) {
  const { uuid, email, lang, hostname, origin } = params;
  const link = `${origin}${env.email.confirmUrl}${uuid}`;

  if (env.env === 'local') {
    logger.info(`Link: ${link}`);
  }

  const t = i18next.getFixedT(lang);
  const emailData = { t, link, lang, hostname };

  const html = await new Promise((res, rej) => {
    confirmEmail.renderToString(emailData, (err, result) => {
      if (err) {
        rej(err);
      } else {
        res(result);
      }
    });
  });

  const emailToSent = [{
    html: <string>html,
    text: link,
    subject: t('emails.confirm.subject', { domain: hostname }),
    from: {
      name: env.email.name,
      email: env.email.email
    },
    to: {
      email
    }
  }];

  return sendEmail(emailToSent);
}

async function sendNotifyNewSubscriber(params: IUserSubscribedEmailParams) {
  const { email, name, phone, lang } = params;
  const t = i18next.t.bind(i18next);
  const emailData = { t, email, name, phone };
  const text = `
    name: ${name}
    email: ${email}
    phone: ${phone}
  `;
  const to = [{
    name: 'Subscribe Robot',
    email: env.email.notifyEmail
  }];
  const otherNotifyEmails = env.email.otherNotifyEmails[lang];

  if (otherNotifyEmails) {
    otherNotifyEmails.forEach(e => {
      to.push({
        name: 'Subscribe Robot',
        email: e
      });
    });
  }

  const html = await new Promise((res, rej) => {
    notifyNewSubscriberEmail.renderToString(emailData, (err, result) => {
      if (err) {
        rej(err);
      } else {
        res(result);
      }
    });
  });

  const emailToSent = [{
    html: <string>html,
    text,
    subject: i18next.t('emails.notify.subject'),
    from: {
      name: env.email.name,
      email: env.email.email
    },
    to
  }];

  return sendEmail(emailToSent);
}

async function subscribeUser(params: IUserSubscribedEmailParams) {
  const { email, name, phone, lang } = params;

  logger.debug('Subscribing use with email', { email, name, phone, lang });

  const notifyEmail = sendNotifyNewSubscriber(params);
  // const subscribe: Promise<{ result: true }> = new Promise((res, rej) => {
  //   sendpulse.addEmails((data: any) => {
  //     if (!data.result) {
  //       logger.error('Email service error', {
  //         response: data,
  //         email,
  //         name,
  //         phone
  //       });
  //       rej(data);
  //     } else {
  //       res(data);
  //     }
  //     logger.debug(data);
  //   }, env.email.subscribersBookId, [{
  //     email,
  //     variables: {
  //       name,
  //       Phone: phone
  //     }
  //   }]);
  // });

  return Promise.all([/*subscribe, */notifyEmail]);
}

async function sendCertCreationToHolder(params: ICertCreationEmailParams) {
  const { certLink, regLink, email, lang, issuerName, fullName } = params;
  const t = i18next.getFixedT(lang);
  const emailData = { t, lang, certLink, regLink, fullName };

  const html = await new Promise((res, rej) => {
    certCreationEmail.renderToString(emailData, (err, result) => {
      if (err) {
        rej(err);
      } else {
        res(result);
      }
    });
  });

  const emailToSent = [{
    html: <string>html,
    text: certLink,
    subject: t('emails.cert_created.subject', { issuerName }),
    from: {
      name: env.email.name,
      email: env.email.email
    },
    to: {
      email
    }
  }];

  return sendEmail(emailToSent);
}


async function sendPasswordResetEmail(params: IPasswordResetEmailParams) {
  const { email, link, lang } = params;

  if (env.env === 'local') {
    logger.info(`PasswordReset link: ${link}`);
  }

  const t = i18next.getFixedT(lang);
  const emailData = { t, link, lang };

  const html = await new Promise((res, rej) => {
    passwordResetEmail.renderToString(emailData, (err, result) => {
      if (err) {
        rej(err);
      } else {
        res(result);
      }
    });
  });

  const emailToSent = [{
    html: <string>html,
    text: link,
    subject: t('emails.password_reset.subject'),
    from: {
      name: env.email.name,
      email: env.email.email
    },
    to: {
      email
    }
  }];

  return sendEmail(emailToSent);
}
