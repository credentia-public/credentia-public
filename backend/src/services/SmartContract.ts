import solc_0_5_16           from 'solc-0-5-16';
import solc_0_6_3            from 'solc-0-6-3';
import fs                    from 'fs';
import path                  from 'path';
import Boom                  from '@hapi/boom';
import { SHA3 }              from 'sha3';
import Web3                  from 'web3';
import { Transaction as Tx } from 'ethereumjs-tx';
import Wallet                from 'ethereumjs-wallet';

import env               from '../environments/environment.local';
import { ErrorMessages } from '../errors';

export interface ICertInputParam {
  certId: string;
  data: object;
  holder: string;
}

export interface IGetTxObjParams {
  certId: string;
  data: string;
  holder: string;
  issuer: string;
  innonce?: string;
  gasPriceWei?: number;
  mygas?: number;
}

export enum Chains {
  MainNet = 'mainnet', TestNet = 'testnet', TestNetOld = 'testnet-old'
}

class SmartContract {
  protected web3: Web3;
  protected factoryInfo: {
    jsonContractSource: string;
    smc: any;
    abi: any;
    source: string;
  };
  protected chainConf;
  protected contract;

  constructor(chain: string) {
    const chainConf = env.blockchain[chain];
    const factoryPath = path.resolve(__dirname, '..', chainConf.factoryPath);
    const source = fs.readFileSync(factoryPath, 'UTF-8');
    const filename = path.basename(chainConf.factoryPath);

    const jsonContractSource = JSON.stringify({
      language: 'Solidity',
      sources: {
        [filename]: {
          content: source
        }
      },
      settings: {
        outputSelection: {
          '*': {
            '*': ['*']
          }
        }
      }
    });

    const compilers = {
      '0.5.16': solc_0_5_16,
      '0.6.3': solc_0_6_3
    };

    const compiler = compilers[chainConf.solVersion];
    if (!compiler) {
      throw new Error(`Unknown compiler version ${chainConf.solVersion}. Only ${Object.keys(compilers).join(', ')} are known.`);
    };

    const smc = JSON.parse(compiler.compile(jsonContractSource));
    if (smc.errors) {
      throw new Error(JSON.stringify(smc.errors, null, ' '));
    }
    const abi = smc.contracts[filename][chainConf.className].abi;
    const factoryInfo = { jsonContractSource, smc, abi, source };

    this.web3 = new Web3(new Web3.providers.HttpProvider(chainConf.httpProvider));
    this.factoryInfo = factoryInfo;
    this.chainConf = chainConf;
    this.contract = new this.web3.eth.Contract(this.factoryInfo.abi, this.chainConf.factoryAddress);
  }

  createAccount() {
    const res = this.web3.eth.accounts.create();
    const wallet = Wallet.fromPrivateKey(Buffer.from(res.privateKey.substr(2, 66), 'hex'));

    return {
      address: res.address,
      privateKey: res.privateKey,
      publicKey: wallet.getPublicKeyString()
    };
  }

  getBalance(ethAddress: string) {
    return this.web3.eth.getBalance(ethAddress);
  }

  protected getInstance(certId: string, data: string, holder: string) {
    const contractFunction = this.createCert(certId, data, holder);
    const functionAbi = contractFunction.encodeABI();
    return { contractFunction, functionAbi };
  }

  protected async estimateCert(certId: string, data: string, holder: string, issuer: string) {
    const { contractFunction } = this.getInstance(certId, data, holder);
    const estimatedGas = await contractFunction.estimateGas({ from: issuer });
    const currentGasPrice = await this.web3.eth.getGasPrice();
    return {
      estimatedGas,
      currentGasPrice: (BigInt(currentGasPrice) * 10n).toString(16),
      currentGasLimit: this.chainConf.gasLimit
    };
  }

  protected async getTxObj(params: IGetTxObjParams) {
    const { certId, data, holder, issuer, gasPriceWei, mygas, innonce } = params;
    const nonce = (await this.web3.eth.getTransactionCount(issuer)).toString(16);
    const issueNonce = innonce ? innonce : nonce;

    const { estimatedGas, currentGasPrice, currentGasLimit } = await this.estimateCert(certId, data, holder, issuer);
    const { functionAbi } = this.getInstance(certId, data, holder);
    // @ts-ignore
    const gPrice = gasPriceWei ? gasPriceWei.toString(16) : currentGasPrice;
    const gas = mygas ? mygas : estimatedGas;

    const txParams = {
      gasPrice: this.chainConf.gasPrice2, // gPrice,
      gasLimit: currentGasLimit,
      gas,
      to: this.chainConf.factoryAddress,
      data: functionAbi,
      from: issuer,
      nonce: '0x' + issueNonce,
      chainID: this.chainConf.chainId
    };

    return new Tx(txParams, { chain: this.chainConf.chainId });
  }

  send(tx: Tx) {
    const serializedTx = tx.serialize();

    return new Promise<string>((res, rej) => {
      this.web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'))
        .on('transactionHash', (transactionHash: string) => {
          res(transactionHash);
        })
        .on('error', err => {
          rej(err);
        });
    });
  }

  async signAndSend(privateKey: string, input: ICertInputParam[], gasPriceWei?: number, mygas?: number) {
    console.log('Input - ' + input)
    console.log('PK - ' + privateKey)
    const keyBuffer = Buffer.from(privateKey.substr(2, 66), 'hex');
    const wallet = Wallet.fromPrivateKey(keyBuffer);
    const ethAddress = wallet.getAddressString();
    const balance = BigInt(await this.getBalance(ethAddress));
    console.log('ethAddress - ' + ethAddress)
    console.log('Balance - ' + balance)
    if (balance === 0n) {
      throw Boom.badRequest(ErrorMessages.NotEnoughGas);
    }

    let nonce = (await this.web3.eth.getTransactionCount(ethAddress /* env.blockchain.address */)) - 1;
    const txObjPromises = input.map(i => {
      nonce = nonce + 1;

      const cs = i.data;
      const sha3 = new SHA3(256);
      sha3.update(JSON.stringify(cs));
      const csHash = sha3.digest('hex');

      return this.getTxObj({
        certId: i.certId,
        data: Buffer.from(JSON.stringify({ csHash })).toString('base64'),
        holder: i.holder,
        issuer: ethAddress,
        innonce: nonce.toString(16),
        gasPriceWei,
        mygas
      });
    });
    const txObjects = await Promise.all(txObjPromises);
    const txSgObjPromises = txObjects.map(tx => {
      tx.sign(keyBuffer);

      return this.send(tx);
    });

    return Promise.all(txSgObjPromises);
  }

  getTx(txId: string) {
    return this.web3.eth.getTransaction(txId);
  }

  getTxDetail(txId: string) {
    return this.web3.eth.getTransactionReceipt(txId);
  }

  createCert(certId: string, data: string, holder: string) {
    // tslint:disable-next-line:max-line-length
    return this.contract.methods.createDoc(this.web3.utils.fromAscii(certId), data, holder); // web3.utils.hexToBytes('0x'+certId),
  }

  getCertList() {
    return this.contract.methods.getDocs().call();
  }

  getCertByHash(hash: string): Promise<string> {
    return this.contract.methods.docData(hash).call();
  }

  getCertById(certId: string) {
    return this.contract.methods.docById(this.web3.utils.fromAscii(certId)).call();
  }
}

class SmartContractOld extends SmartContract {
  createCert(certId: string, data: string, holder: string) {
    // tslint:disable-next-line:max-line-length
    return this.contract.methods.createCert(this.web3.utils.fromAscii(certId), data, holder); // web3.utils.hexToBytes('0x'+certId),
  }

  getCertList() {
    return this.contract.methods.getCerts().call();
  }

  getCertByHash(hash: string): Promise<string> {
    return this.contract.methods.certData(hash).call();
  }

  getCertById(certId: string) {
    return this.contract.methods.certById(this.web3.utils.fromAscii(certId)).call();
  }
}

class SmartContractManager {
  private smartContracts = {};
  private smartContractBuilderByChain = {
    mainnet: SmartContract,
    testnet: SmartContract,
    'testnet-old': SmartContractOld
  };

  constructor() {
    Object.keys(env.blockchain).forEach(chain => {
      this.smartContracts[chain] = new this.smartContractBuilderByChain[chain](chain);
    });
  }

  getSmartContract(chain: Chains) {
    return this.smartContracts[chain];
  }
}

export default new SmartContractManager();
