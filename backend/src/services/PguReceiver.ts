import axios, { AxiosInstance, ResponseType } from 'axios';
import { sha256 } from 'js-sha256';

import env                                    from '../environments/environment';
import logger                                 from './Logger';

type ICert = import('../models/certificate').ICert;

export interface PGUUserIDFields {
  documentRegNumber: string;
  surname: string;
  name: string;
  secondName: string;
  holderBirthday: string;
}

interface IHolderEduCoursework {
  name: string;
  theme: string;
  mark: string;
}

interface IHolderEduPractice {
  name: string;
  credits: string;
  hours: string;
  mark: string;
}

interface IHolderEduExitExam {
  name: string;
  mark: string;
}

interface IHolderEduThesi {
  name: string;
  theme: string;
  mark: string;
}

interface IHolderEduCours {
  name: string;
  credits: string;
  hours: string;
  mark: string;
}

interface ICredentialSubject {
  IssuerName: string;
  IssuerLogo: string;
  IssuerStamp: string;
  IssuerPersonATitle: string;
  IssuerPersonAName: string;
  IssuerPersonBTitle: string;
  IssuerPersonBName: string;
  IssuerPersonCTitle: string;
  IssuerPersonCName: string;
  DocumentCity: string;
  DocumentTitle: string;
  DocumentNo: string;
  DocumentDate: string;
  DocumentQualification: string;
  DocumentSpecialty: string;
  DocumentSpecialization: string;
  DocumentRegNo: string;
  DocumentRegDate: string;
  HolderLastName2: string;
  HolderFirstName2: string;
  HolderSecondaryName2: string;
  HolderLastName: string;
  HolderFirstName: string;
  HolderSecondaryName: string;
  HolderBirthday: string;
  HolderEduPrevious: string;
  HolderEduEntryExam: string;
  HolderEduStart: string;
  HolderEduEnd: string;
  HolderEduYears: string;
  HolderEduCoursework: IHolderEduCoursework[];
  HolderEduPractice: IHolderEduPractice[];
  HolderEduExitExam: IHolderEduExitExam[];
  HolderEduThesis: IHolderEduThesi[];
  HolderEduCourses: IHolderEduCours[];
}

type ITCredentialSubject = Omit<ICredentialSubject,
  'IssuerName'|
  'DocumentNo'|
  'DocumentTitle'|
  'DocumentCity'|
  'DocumentRegNo'|
  'IssuerLogo'|
  'DocumentQualification'|
  'DocumentSpecialization'|
  'DocumentDate'|
  'IssuerPersonATitle'|
  'IssuerPersonAName'|
  'IssuerPersonBTitle'|
  'IssuerPersonBName'|
  'IssuerPersonCTitle'|
  'IssuerPersonCName'|
  'HolderLastName'|
  'HolderFirstName'|
  'HolderSecondaryName'|
  'HolderBirthday'> &
{
  imageLogoURL: string;
  documentNumber: string;
  title: string;
  city: string;
  documentRegNumber: string;
  issuerName: string;
  qualification: string;
  specialization: string;
  date: string;
  issuerPersonATitle: string;
  issuerPersonAName: string;
  issuerPersonBTitle: string;
  issuerPersonBName: string;
  issuerPersonCTitle: string;
  issuerPersonCName: string;
  name: string;
  surname: string;
  secondName: string;
  holderBirthday: string;
};

class PguReceiver {
  error: Error;

  readonly data: Promise<PGUUserIDFields[]>;
  private request: AxiosInstance;
  private requestOptions = {
    responseType: 'json' as ResponseType
  };

  constructor() {
    this.request = axios.create(this.requestOptions);
    this.data = this.getPrivateData();
  }

  getPrivateData = async (): Promise<PGUUserIDFields[]> => {
    try {
      const res = await this.request.get<ICredentialSubject[]>(env.pgu.url);

      return res.data.map(d => {
        return {
          documentRegNumber: d.DocumentRegNo,
          surname: d.HolderLastName,
          name: d.HolderFirstName,
          secondName: d.HolderSecondaryName,
          holderBirthday: d.HolderBirthday
        };
      });
    } catch (err) {
      this.error = err;
      logger.error(err);

      return [];
    }
  };

  getDataForDB = async (): Promise<ITCredentialSubject[]> => {
    try {
      const res = await this.request.get<ICredentialSubject[]>(env.pgu.url);
      return this.transform(res.data);
    } catch (err) {
      this.error = err;
      logger.error(err);

      return [];
    }
  };

  transform = (data: ICredentialSubject[]): ITCredentialSubject[] => {
    // const { IssuerLogo } = inData[0];
    // const options = {
    //   hostname: 'lk.pnzgu.ru',
    //   port: 443,
    //   path: IssuerLogo,
    //   method: 'GET',
    //   timeout: 30000
    // };
    // const logo = 'data:image/png;base64,' + (await httpsRequest(options, undefined)).toString('base64');

    return data.map((i: ICredentialSubject) => {
      const {
        IssuerName,
        DocumentNo,
        DocumentTitle,
        DocumentCity,
        DocumentRegNo,
        IssuerLogo,
        DocumentQualification,
        DocumentSpecialization,
        DocumentDate,
        IssuerPersonATitle,
        IssuerPersonAName,
        IssuerPersonBTitle,
        IssuerPersonBName,
        IssuerPersonCTitle,
        IssuerPersonCName,
        HolderLastName,
        HolderFirstName,
        HolderSecondaryName,
        HolderBirthday,
        ...other
      } = i;

      return {
        imageLogoURL: 'https://lk.pnzgu.ru/images/logo_pnzgu.png',
        documentNumber: DocumentNo,
        title: DocumentTitle,
        city: DocumentCity,
        documentRegNumber: DocumentRegNo,
        // IssuerLogo: logo,
        issuerName: IssuerName
          .replace('&LAQUO;', '"')
          .replace('&RAQUO;', '"')
          .replace('&laquo;', '"')
          .replace('&raquo;', '"'),
        qualification: DocumentQualification,
        specialization: DocumentSpecialization,
        date: DocumentDate,
        issuerPersonATitle: IssuerPersonATitle,
        issuerPersonAName: IssuerPersonAName,
        issuerPersonBTitle: IssuerPersonBTitle,
        issuerPersonBName: IssuerPersonBName,
        issuerPersonCTitle: IssuerPersonCTitle,
        issuerPersonCName: IssuerPersonCName,
        name: sha256(HolderFirstName.toLowerCase().trim()),
        surname: sha256(HolderLastName.toLowerCase().trim()),
        secondName: sha256(HolderSecondaryName.toLowerCase().trim()),
        holderBirthday: sha256(HolderBirthday.toLowerCase().trim()),
        ...other
      };
    });
  };

  findAndSubstitutePrivateData = async (regNum: string, cert: ICert): Promise<ICert> => {
    const data = await this.data;
    const userData = data.find(d => d.documentRegNumber === regNum);

    try{
      cert.credentialSubject.name = userData.name;
      cert.credentialSubject.surname = userData.surname;
      cert.credentialSubject.secondName = userData.secondName;
      cert.credentialSubject.HolderBirthday = userData.holderBirthday;
    } catch(e) {
      console.log(e);
    }

    return  cert;
  };
}

export default new PguReceiver();
