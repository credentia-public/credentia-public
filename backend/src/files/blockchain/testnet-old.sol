pragma solidity^0.5.1;
contract CredentiaCert{
    address owner;
    string data;
    address holder;

    constructor(string memory in_data, address in_holder) public {
        owner = msg.sender;
        holder = in_holder;
        data = in_data;
    }

    function setData(string memory in_data) public {
        require(msg.sender == owner);
        data = in_data;
    }

    function getData() public view returns (string memory) {
        require(msg.sender==holder||msg.sender==owner);
        return data;
    }
}


contract CredentiaCertRegistry {
    mapping(address=>bool) deployers;
    mapping(address=>address) certs;
    address[] certKeys;
    bytes32[] certIds;
    mapping(bytes32=>address) certsId;
    address owner;

    constructor() public {
        owner = msg.sender;
    }

    function addDeployer(address deployer) public {
        require(msg.sender == owner);
        deployers[deployer] = true;
    }

    function checkAddress(address deployer) public view returns (bool result) {
        bool res = deployers[deployer];
        return res;
    }

    function removeDeployer(address deployer) public {
        require(msg.sender==owner);
        deployers[deployer] = false;
    }

    function getCerts() public view returns (address[] memory){
        return certKeys;
    }
    
    function getCertIdList() public view returns (bytes32[] memory){
        return certIds;
    }

    function createCert(bytes32 certId, string memory data, address holder) public returns (address result){
        //bool res = deployers[msg.sender];
        require(certsId[certId]==0x0000000000000000000000000000000000000000);
            address c = address(new CredentiaCert(data, holder));
            certs[c] = msg.sender;
            certsId[certId] = c;
            certIds.push(certId);
            certKeys.push(c);
            return c;
    }

    function replaceCertData(address sc, string memory data) public returns (address result){
        CredentiaCert(sc).setData(data);
        return sc;
    }

    function certData(address cert)public view returns (string memory result){
        string memory data = CredentiaCert(cert).getData();
        return data;
    }
    
    function certById(bytes32 certId)public view returns (address result){
        require(certsId[certId]!=0x0000000000000000000000000000000000000000);
        return certsId[certId];
    }
}
