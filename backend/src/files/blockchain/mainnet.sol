pragma solidity^0.6.3;
contract Doc {
  address owner;
  string data;
  address holder;

  constructor(string memory in_data, address in_holder) public {
    owner = msg.sender;
    holder = in_holder;
    data = in_data;
  }

  function setData(string memory in_data) public {
    require(msg.sender == owner);
    data = in_data;
  }

  function getData() public view returns (string memory) {
    require(msg.sender==holder||msg.sender==owner);
    return data;
  }
}


contract DocRegistry {
  mapping(address=>bool) deployers;
  mapping(address=>address) docs;
  address[] docKeys;
  bytes32[] docIds;
  mapping(bytes32=>address) docsId;
  address owner;

  constructor() public {
    owner = msg.sender;
  }

  function addDeployer(address deployer) public {
    require(msg.sender == owner);
    deployers[deployer] = true;
  }

  function checkAddress(address deployer) public view returns (bool result) {
    bool res = deployers[deployer];
    return res;
  }

  function removeDeployer(address deployer) public {
    require(msg.sender==owner);
    deployers[deployer] = false;
  }

  function getDocs() public view returns (address[] memory){
    return docKeys;
  }

  function getDocIdList() public view returns (bytes32[] memory){
    return docIds;
  }

  function createDoc(bytes32 docId, string memory data, address holder) public returns (address result){
    //bool res = deployers[msg.sender];
    require(docsId[docId]==0x0000000000000000000000000000000000000000);
    address c = address(new Doc(data, holder));
    docs[c] = msg.sender;
    docsId[docId] = c;
    docIds.push(docId);
    docKeys.push(c);
    return c;
  }

  function replaceDocData(address sc, string memory data) public returns (address result){
    Doc(sc).setData(data);
    return sc;
  }

  function docData(address doc)public view returns (string memory result){
    string memory data = Doc(doc).getData();
    return data;
  }

  function docById(bytes32 docId)public view returns (address result){
    require(docsId[docId]!=0x0000000000000000000000000000000000000000);
    return docsId[docId];
  }
}
