# Summary

Бэкенд для сервиса выпуска сертификатов


# How to install

Установите зависимости:

* Node.js 12 (⚠️ под Node.js 14 не работает)
* yarn
* MongoDB - https://docs.mongodb.com/manual/installation/#mongodb-community-edition-installation-tutorials

Далее:

```
git clone https://gitlab.com/credentiaru/credentia-certs-backend.git
cd credentia-certs-backend
yarn install
yarn build
```

# How to use

## Запуск локально

`yarn dev` - Запуск бэка; более подробный вывод: `LOG_LEVEL=debug yarn dev`

### Используя локальную БД

1) `sudo service mongod start` - Запуск базы

2) `DB_CREDENTIALS=mongodb://localhost:27017/credentia yarn dev`

Сервис поднимется по адресу: `http://localhost:3000/`

Проверить доступность API: http://localhost:3000/api


## API: описание, тесты, документация, публикация

Описание и тесты API создаются с помощью Postman. Документация API генерируется.

Файлы для импорта/экспорта хранятся в директории `/api` проекта:

```
credentia-api.postman_collection.json - Коллекция запросов (с тестами)
credentia-local.postman_environment.json
credentia-dev.postman_environment.json
credentia-prod.postman_environment.json
credentia-public-prod.postman_environment - ...окружения
```

### Изменение описания и тестов API

1) Облачная функция "Team" не используется (т.к. нет привязки к коммитам, легко сломать, нет ревью)
2) В начале работы импортируем файлы из `/api`
3) При изменениях в реализации API следует корректировать коллекцию/окружения
4) В конце работы, если коллекция/окружения менялись, экспортируем файлы в `/api`
5) В коллекции `credentia-api.postman_collection.json` **не должно быть чувствительной информации**!
6) В окружении `credentia-public-prod.postman_environment.json` **не должно быть чувствительной информации**!

### Публикация документации

Публичная документация API требует обновления при обновлении прода:

1) Используется отдельный postman-аккаунт
2) Импортируется пара файлов: `credentia-api.postman_collection.json`, `credentia-public-prod.postman_environment.json`
3) При импорте коллекции выбираем "Replace", при этом прошлая ссылка на публичную документацию перестаёт действовать
4) credentia-api -> Publish docs
5) На открывшейся странице выбираем Environment `credentia-public-prod`
6) Публикуем, генерируется страница с документацией на новом URL
7) Обновляем ссылку на документацию в алиасе https://tiny.cc/credentia-api


# Работа на DEV-сервере (https://dev.credentia.ru/)

~~Настроен автодеплой из мастера.~~ (сервер временно отключен)

Статус автодеплоя: https://gitlab.com/credentiaru/credentia-certs-backend/pipelines


# Работа на PROD-сервере (https://dashboard.credentia.ru/)

## Настройка

`/etc/nginx/sites-enabled` - конфиги nginx

## Запуск

```
cd /var/www/credentia-certs-backend/
mv src/environments/environment.prod.ts src/environments/environment.ts
pm2 start "DB_CREDENTIALS='ДОСТУП_К_БАЗЕ' yarn start" --name "back"
```

`pm2 logs` - Смотреть логи

## Обновление и перезапуск

Скриптом обновления (обновит и перезагрузит бэк)

```
cd ~/credentia-certs-frontend
sh update.sh
```

## Просмотр логов

```
pm2 log
```


# Решение проблем

## Не билдится / не запускается

- Обновите node и yarn до последней версии
- Проверьте готовность базы `sudo cat /var/log/mongodb/mongod.log | grep "waiting for connections on port 27017"`
- Введите команду `reboot`


# Cмарт контракты и блокчейн

https://gitlab.com/credentiaru/credentia-certs-backend/blob/master/src/files/blockchain/mainnet.sol 
Sample Video
1. remix.ethereum.org 
2. вставляем контракт
3. в разделе deploy выбераем env:web3 provider вставляем  адрес ноды .
4. деплоим (выбрать DocumenttRegistry, нажать ораньжевую Deploy). после деплоя копируем созданный адрес контракта и вставляем в https://gitlab.com/credentiaru/credentia-certs-backend/blob/master/src/environments/environment.local.ts#L102 (factoryAddress)
5. https://gitlab.com/credentiaru/credentia-certs-backend/blob/master/src/environments/environment.local.ts#L97 тут должна быть такая же нода как и в п 3 (httpProvider)


bonus: как поднять свой блокчейн
```
git clone https://github.com/jpmorganchase/quorum-examples.git
cd  quorum-examples
docker-compose up -d
```
