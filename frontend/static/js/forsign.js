function CertificateAdjuster()
{
}

CertificateAdjuster.prototype.extract = function(from, what)
{
    certName = "";

    var begin = from.indexOf(what);

    if(begin>=0)
    {
        var end = from.indexOf(', ', begin);
        certName = (end<0) ? from.substr(begin) : from.substr(begin, end - begin);
    }

    return certName;
}

CertificateAdjuster.prototype.Print2Digit = function(digit)
{
    return (digit<10) ? "0"+digit : digit;
}

CertificateAdjuster.prototype.GetCertDate = function(paramDate)
{
    var certDate = new Date(paramDate);
    return this.Print2Digit(certDate.getUTCDate())+"."+this.Print2Digit(certDate.getMonth()+1)+"."+certDate.getFullYear() + " " +
             this.Print2Digit(certDate.getUTCHours()) + ":" + this.Print2Digit(certDate.getUTCMinutes()) + ":" + this.Print2Digit(certDate.getUTCSeconds());
}

CertificateAdjuster.prototype.GetCertName = function(certSubjectName)
{
    return this.extract(certSubjectName, 'CN=');
}

CertificateAdjuster.prototype.GetIssuer = function(certIssuerName)
{
    return this.extract(certIssuerName, 'CN=');
}

CertificateAdjuster.prototype.GetCertInfoString = function(certSubjectName, certFromDate)
{
    return this.extract(certSubjectName,'CN=') + "; Выдан: " + this.GetCertDate(certFromDate);
}



    var CADESCOM_CADES_BES = 1;
    var CAPICOM_CURRENT_USER_STORE = 2;
    var CAPICOM_MY_STORE = "My";
    var CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED = 2;
    var CAPICOM_CERTIFICATE_FIND_SUBJECT_NAME = 1;
    var CADESCOM_BASE64_TO_BINARY = 1;
    
    
    
    

	
	function FillCertList_Async(lstId) {
	    cadesplugin.async_spawn(function *() {
	        try {
			var oStore = yield cadesplugin.CreateObjectAsync("CAdESCOM.Store");
			if (!oStore) {
			    alert("store failed");
			    return;
			}
	
	            yield oStore.Open();
	        }
	        catch (err) {
	            alert("Ошибка при открытии хранилища: " + cadesplugin.getLastError(err));
	            return;
	        }
	
	        var lst = document.getElementById(lstId);
	        if(!lst)
	        {
	            return;
	        }
	        var certCnt;
	        var certs;
	
	        try {
	            certs = yield oStore.Certificates;
	            certCnt = yield certs.Count;
	        }
	        catch (err) {
	        	alert(cadesplugin.getLastError(err))
	            return;
	        }

	        if(certCnt == 0)
	        {
	            return;
	        }
	
	        for (var i = 1; i <= certCnt; i++) {
	            var cert;
	            try {
	                cert = yield certs.Item(i);
	            }
	            catch (err) {
	                alert("Ошибка при перечислении сертификатов: " + cadesplugin.getLastError(err));
	                return;
	            }
	
	            var oOpt = document.createElement("OPTION");

	            var dateObj = new Date();
	            try {
	                var ValidToDate = new Date((yield cert.ValidToDate));
	                var ValidFromDate = new Date((yield cert.ValidFromDate));
	                var Validator = yield cert.IsValid();
	                var IsValid = yield Validator.Result;
	                if(dateObj< ValidToDate && (yield cert.HasPrivateKey()) && IsValid) {
	                    oOpt.text = new CertificateAdjuster().GetCertInfoString(yield cert.SubjectName, ValidFromDate);
	                }
	                else {
	                    continue;
	                }
	            }
	            catch (err) {
	                alert("Ошибка при получении свойства SubjectName: " + cadesplugin.getLastError(err));
	            }
	            try {
	                oOpt.value = yield cert.Thumbprint;
	            }
	            catch (err) {
	                alert("Ошибка при получении свойства Thumbprint: " + cadesplugin.getLastError(err));
	            }
	
	            lst.options.add(oOpt);
	        }
	
	        yield oStore.Close();
	    });//cadesplugin.async_spawn
	}

    
    function SignCreate(certSubjectName, dataToSign) {
        return new Promise(function(resolve, reject){
            cadesplugin.async_spawn(function *(args) {
            	
				var thumbprint = certSubjectName.split(" ").reverse().join("").replace(/\s/g, "").toUpperCase();
            	
                try {
                    var oStore = yield cadesplugin.CreateObjectAsync("CAdESCOM.Store");
                    yield oStore.Open(CAPICOM_CURRENT_USER_STORE, CAPICOM_MY_STORE, CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);
                    var CertificatesObj = yield oStore.Certificates;
                    
                    var oCertificates = yield CertificatesObj.Find(cadesplugin.CAPICOM_CERTIFICATE_FIND_SHA1_HASH, thumbprint);
                    
                    //var oCertificates = yield CertificatesObj.Find(CAPICOM_CERTIFICATE_FIND_SUBJECT_NAME, certSubjectName);

                    var Count = yield oCertificates.Count;
                    
                    if (Count == 0) {
                        throw("Certificate not found: " + args[0]);
                    }
                    var oCertificate = yield oCertificates.Item(1);
                    var oSigner = yield cadesplugin.CreateObjectAsync("CAdESCOM.CPSigner");
                    yield oSigner.propset_Certificate(oCertificate);

                    var oSignedData = yield cadesplugin.CreateObjectAsync("CAdESCOM.CadesSignedData");
                    yield oSignedData.propset_ContentEncoding(CADESCOM_BASE64_TO_BINARY);
                    yield oSignedData.propset_Content(dataToSign);

                    var sSignedMessage = yield oSignedData.SignCades(oSigner, CADESCOM_CADES_BES, true);

                    yield oStore.Close();

                    args[2](sSignedMessage);
                }
                catch (err)
                {
                    args[3]("Failed to create signature. Error: " + cadesplugin.getLastError(err));
                }
            }, certSubjectName, dataToSign, resolve, reject);
        });
    }

 function run() {
        var oCertName = document.getElementById("CertName");
        var sCertName = oCertName.value;
        if ("" == sCertName) {
            alert("Введите имя сертификата (CN).");
            return;
        }
        
        // Тест подписания строки произвольной длинны
        //var dataInBase64 = window.btoa(document.getElementById("source").value);
		var dataInBase64 = window.btoa(unescape(encodeURIComponent(document.getElementById("source").value)));
		var thenable = SignCreate(sCertName, dataInBase64);

        thenable.then(
            function (result){
            	document.getElementById('signature').value=result;
            }, function(err) {
            	document.getElementById('signature').value = '';
            	alert(err)
            });
        
    }


    function Verify(sSignedMessage, dataToVerify) {
    	return new Promise(function(resolve, reject){
            cadesplugin.async_spawn(function *(args) {
			    
			    var oSignedData = yield cadesplugin.CreateObjectAsync("CAdESCOM.CadesSignedData");
			    try {
			        // Значение свойства ContentEncoding должно быть задано
			        // до заполнения свойства Content
			        yield oSignedData.propset_ContentEncoding(CADESCOM_BASE64_TO_BINARY);
			        yield oSignedData.propset_Content(dataToVerify);
			        yield oSignedData.VerifyCades(sSignedMessage, CADESCOM_CADES_BES, true);
			        args[2]("Verify");
			        
			    } catch (err) {
			        args[3]("Failed to verify signature. Error: " + cadesplugin.getLastError(err));
			    }
			    
			}, sSignedMessage, dataToVerify, resolve, reject);
        });
    }

	function ver() {
    	var dataInBase64 = window.btoa(document.getElementById("source").value);
    	var sSignedMessage = document.getElementById('signature').value;
		var verifyResult = Verify(sSignedMessage, dataInBase64);
		verifyResult.then(function(res){
			alert(res)
		}, function(err){
			alert(err)
		});
	}
   

    var canPromise = !!window.Promise;
    if(canPromise) {
        cadesplugin.then(function () {
                FillCertList_Async('CertName');
               },
               function(error) {
                   alert(error);
               }
       );
    } else {
        window.addEventListener("message", function (event){
        	alert(31);
            if (event.data == "cadesplugin_loaded") {
                alert(321)
            } else if(event.data == "cadesplugin_load_error") {
                   alert("Ошибка");
            }
            },
        false);
        window.postMessage("cadesplugin_echo_request", "*");
	}
	
window.createSignature = run;
