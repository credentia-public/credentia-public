# Reference

Дизайн приложения: https://www.figma.com/file/dKkTuIbaFiEvDrCBl2EO47/Application (приостановлен)

Дизайн дизайнера бланков: https://www.figma.com/file/t1l48zh2vFisFTwosZrnd1/Interface (в процессе)


# Установка проекта

Установите зависимости:

* node
* yarn
* nginx

Далее:

```
git clone https://gitlab.com/credentiaru/credentia-certs-frontend.git
cd credentia-certs-frontend
yarn install --production=false
```

# Запуск локально

## Два уровня запуска

`yarn dev` - запуск webpack-server, для обычной разработки фронта.

`yarn start` - запуск express (pdf-рендерер) + скомпиленный front-dist.


## Используя local-backend (local-API)

Для этого понадобится локальный связующий прокси-сервер на основе nginx:

* Скопируйте `credentia.nginx.conf` в папку с настройками nginx `/etc/nginx/conf.d/` (путь может отличаться)
* Запустите прокси-сервер `sudo nginx`
* Запустите локальный backend, он поднимется по адресу `http://localhost:3000/`
* Запустите локальный frontend: `yarn dev`, он поднимется по адресу `http://localhost:8000/`


Cвязка из фронта и бэка станет доступна по адресу `http://credentia.localhost/`

Останов: `sudo nginx -s stop`


## Используя удалённое dev-API

или `API_URL=https://dev.credentia.ru/api yarn dev`

Запуск на Windows: `$env:API_URL = 'https://dev.credentia.ru/api'; yarn dev`

Сервис запустится по адесу http://localhost:8000 и готов к работе/разработке


# Работа на PROD-сервере (https://dashboard.credentia.ru/)

## Настройка

`/etc/nginx/sites-enabled` - конфиги nginx

## Запуск

```
cd /var/www/credentia-certs-frontend/
mv src/environments/environment.prod.ts src/environments/environment.ts
pm2 start "yarn start" --name "front"
```

## Обновление и перезапуск

Скриптом обновления (обновит и перезагрузит фронт)

```
cd ~/credentia-certs-frontend
sh update.sh
```

## Просмотр логов

```
pm2 log
```

# Как добавить нового клиента выпускающего сертификаты?

Пока нет админки, это делается вручную, запросами к API через Postman (убедитесь, что используется актуальная коллекция и env-файл):

1. Авторизоваться под админом - запрос `POST /auth/login`
2. Создать Issuer - запрос `POST /issuers/`, задать `shortName: {issuerShortName}` и пр.
   В ответе будет нужен `ethAddress` и `issuerId`=`_id`
3. Пополнить эфиром `ethAddress` - https://teth.bitaps.com
4. Добавить шаблон для документов:
    - Подготовить файлы шаблона:
        - tsx-шаблон в `/components/templates/{issuerShortName}` c именем `{templateName}.tsx` (для начала можно скопировать содержимое другого шаблона)
        - изображения в `/static/images/{issuerShortName}`
    - Создать шаблон - запрос `POST /templates/`, указать `issuerId`, `name: {templateName}` и пути к изображениям
5. Зарегистрировать нового пользователя - запрос `POST /auth/signup`, указать `type: "issuer_ambassador"`, `issuerId: "issuerId"`

Теперь можно заходить под новым пользователем и выпускать сертификаты, периодически пополняя `ethAddress`.

Если нужно создать сразу много сертов из таблицы использовать API https://tiny.cc/credentia-api "Create certificate(s)" (если нужно из гугл таблиц https://screenshots.wpmix.net/chrome_n0cL2gh1P0NSyNSlD6iL2g4gDD01dZvE.png )



# Создание/отображение сертификатов извне

Для этого используйте:

* Путь `/app/certificate/external`
* Параметр `data` - закодированные данные сертификата в base62
* Параметр `embedded` для скрытия элементов интерфейса

## Пример

Исходные данные:

`{"name":"Иванов Иван Иванович","emailHash":"c8eb74df5a9a3c22379f031256f68c33","courseId":"1234","course":"Элементарный уровень","date":"2019-10-22T13:55:35.687Z"}`

Данные в base62: https://base62.io/

```
2FrW958cowvx5t094Jq4s1SUiLD7LbRpr0z6n7VSPs3ykBQgR0OEyP8zUuSWSSKI7z8UQyC8JaeVKQIQv9VFI2LdVwmj7pk3kRJXcYTju4PylTS9JXB4yJC5kbyNSjzTb1TprBeMHjkPW337ScInm8t4BcayJYXFsxkBDXN3CW34Xv5wrFKNTCaAyB0IiKiX7UKBNNN8RaeXVXgnVaHIJvNBDnKMx27XAAuvCngM5AxDhpZxDOxX39ZA2BjzRR48nUM2oTbv4qhp
```

Итоговый путь:

```
/app/certificate/external?embedded=true&data=2FrW958cowvx5t094Jq4s1SUiLD7LbRpr0z6n7VSPs3ykBQgR0OEyP8zUuSWSSKI7z8UQyC8JaeVKQIQv9VFI2LdVwmj7pk3kRJXcYTju4PylTS9JXB4yJC5kbyNSjzTb1TprBeMHjkPW337ScInm8t4BcayJYXFsxkBDXN3CW34Xv5wrFKNTCaAyB0IiKiX7UKBNNN8RaeXVXgnVaHIJvNBDnKMx27XAAuvCngM5AxDhpZxDOxX39ZA2BjzRR48nUM2oTbv4qhp
```


# Решение проблем

## На проде всё сломалось

Можно `reboot`.
