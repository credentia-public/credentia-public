const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',
  devtool: "inline-source-map",
  context: path.resolve(__dirname),
  target: 'web',
  entry: {
    main: './src/index.tsx'
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: 'http://credentia.localhost/static/',
    filename: '[name].bundle.js',
    chunkFilename: '[name].bundle.js',
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js"]
  },
  module: {
    rules: [
      // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
      {
        test: /\.tsx?$/,
        loader: "ts-loader",
        include: [
          path.resolve(__dirname, "src")
        ],
        exclude: [
          path.resolve(__dirname, "src/server")
        ],
        options: {
          transpileOnly: true,
          experimentalWatchApi: true,
        },
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
        include: [
          path.resolve(__dirname, "node_modules")
        ],
      },
    ]
  },
  plugins: [
    new webpack.ProgressPlugin(),
    new HtmlWebpackPlugin({
      title: 'Credentia',
      template: './src/index.ejs',
      templateParameters: {
        title: 'Credentia',
        env: 'dev'
      }
    })
  ],
  // optimization: {
  //   // Automatically split vendor and commons
  //   // https://twitter.com/wSokra/status/969633336732905474
  //   // https://medium.com/webpack/webpack-4-code-splitting-chunk-graph-and-the-splitchunks-optimization-be739a861366
  //   splitChunks: {
  //     chunks: 'all',
  //     name: false,
  //   },
  // },
  devServer: {
    compress: false,
    hot: true,
    overlay: true,
    port: 8000,
    historyApiFallback: {
      rewrites: [
        {
          from: /^\/*$/,
          to: '/static/index.html'
        },
        {
          from: /^\/app.*$/,
          to: '/static/index.html'
        },
      ]
    },
    clientLogLevel: 'debug',
    watchContentBase: true,
    public: 'credentia.localhost',
    publicPath: 'http://credentia.localhost/static/',
    allowedHosts: [
      'credentia.localhost'
    ]
  },
};
