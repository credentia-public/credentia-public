import i18n from './i18n';

export default {

  getLocalDocType(templateName: string) {
    const templateNameTitles: any = {
      // template name: localTitle
      main: i18n.t('doc_type.certificate'),
      cert: i18n.t('doc_type.certificate'),
      certificate: i18n.t('doc_type.certificate'),
      diplom: i18n.t('doc_type.diploma'),
      diploma: i18n.t('doc_type.diploma'),
      passport: i18n.t('doc_type.passport')
    };
    const docType = templateName in templateNameTitles ?
      templateNameTitles[templateName] : i18n.t('doc_type.unknown');

    return docType;
  }
};
