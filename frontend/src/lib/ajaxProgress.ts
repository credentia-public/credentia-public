// tslint:disable-next-line:interface-name
export interface UploadParams {
  method: string;
  url: string;
  body: any;
  onProgress: (i: number) => void;
  onFailure: (err: IUploadError) => void;
  onCanceled: () => void;
  onComplete: (e: any) => void;
}

export interface IUploadError {
  status: number;
  statusText: string;
  response?: any;
}

export const uploadXHR = ({ method, url, body, onProgress, onComplete, onFailure, onCanceled }: UploadParams) => {
    const xhr = new XMLHttpRequest();
    const progressCallback = (e: ProgressEvent<XMLHttpRequestEventTarget>) => {
      if (e.lengthComputable) {
        const progress = e.loaded / e.total;
        onProgress(progress);
      }
    };

    const networkErrorCallback = () => {
      onFailure({
        status: 0,
        statusText: 'network_error'
      });
    };

    xhr.withCredentials = true;
    xhr.responseType = 'json';
    xhr.upload.addEventListener('progress', progressCallback);
    xhr.upload.addEventListener('error', networkErrorCallback);
    xhr.upload.addEventListener('abort', onCanceled);
    xhr.onreadystatechange = _ => {
      const { readyState, status, statusText, response } = xhr;
      if (readyState === 4) {
        if (status === 0) {
          return;
        }

        if (status >= 200 && status < 400) {
          onComplete(response);
        } else {
          onFailure({
            status,
            statusText,
            response
          });
        }
      }
    };
    xhr.open(method, url, true);
    xhr.send(body);

    return () => {
      xhr.upload.removeEventListener('progress', progressCallback);
      xhr.upload.removeEventListener('error', networkErrorCallback);
      xhr.upload.removeEventListener('abort', onCanceled);
      xhr.onreadystatechange = null;
      xhr.abort();
    };
};
