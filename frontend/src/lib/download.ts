import { getCertFile } from '../api/certificate';

const download = {

  JSONfile(data: any, fileName: string) {
    const json = JSON.stringify(data, null, '\t');
    const fileContent = new Blob([json], { type: 'application/json' });
    download.file(fileContent, fileName);
  },

  certFile(path: string) {
    const [certCode, fileName] = path.split('/');

    getCertFile(certCode, fileName).then(blob => {
      download.file(blob, fileName);
    });
  },

  file(fileContent: any, fileName: string) {
    if (window.navigator.msSaveOrOpenBlob) { // IE10+
      window.navigator.msSaveOrOpenBlob(fileContent, fileName);
    } else { // Others
      const a = document.createElement('a');
      const url = URL.createObjectURL(fileContent);
      a.href = url;
      a.download = fileName;
      document.body.appendChild(a);
      a.click();
      setTimeout(() => {
        document.body.removeChild(a);
        window.URL.revokeObjectURL(url);
      }, 0);
    }
  }

};

export default download;
