import * as EmailValidator from 'email-validator';

// it is difficult to define letters using regular expression
// therefore, such a function is used while there is no need eastern languages are used
const isLetter = (c: string) => c.toLowerCase() !== c.toUpperCase();

const isAllowedChar = (c: string) => isLetter(c) !== (c === '-');

export default {

  isValidEmail(email: string = '') {
    return EmailValidator.validate(email);
  },

  isValidFullName(fullName: string) {
    return fullName.length >= 3 &&
      fullName.split(' ').every(namePart => {
        return (
          // namePart must contain characters
          namePart &&
          // only letters and dashes (e.g. "John-Michael Scott")
          namePart.split('').every(isAllowedChar) &&
          // no extreme or double dashes
          (namePart.includes('-') ? !(
            /-$/.test(namePart) ||
            /^-/.test(namePart) ||
            /--/.test(namePart)
          ) : true)
        )
      });
  }
};
