import i18next, { InitOptions } from 'i18next';
import XHR                      from 'i18next-xhr-backend';
import LanguageDetector         from 'i18next-browser-languagedetector';
import { initReactI18next }     from 'react-i18next';

import env from '../environments/environment';

i18next
.use(XHR)
.use(LanguageDetector)
.use(initReactI18next) // bind react-i18next to the instance
.init(<InitOptions>env.i18n);

export default i18next;
