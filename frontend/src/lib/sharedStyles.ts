const color = {
  orange: '#FF9356',
  darkBlue: '#202949',
  white: '#fff',
  error: '#FC605C',
  success: '#34c749',
  disabled: {
    bg: '#ddd',
    border: '#bbb',
    text: '#aaa'
  }
};

const gaps = {
  vertical: {
    paddingTop: '40px',
    paddingBottom: '40px'
  },
  horizontal: {
    paddingLeft: '20px',
    paddingRight: '20px'
  }
};

const button = {
  width: '100%',
  border: 'none',
  outline: 'none',
  borderRadius: '8px',
  backgroundColor: '#FF9356',
  color: '#fff',
  cursor: 'pointer',
  fontSize: 'inherit',
  fontFamily: 'inherit',
  '&:disabled': {
    backgroundColor: '#F4F4F6',
    color: '#B2B5BD',
    cursor: 'initial'
  },
  '&:hover': {
    opacity: 0.9
  }
};

const inputText = {
  width: '100%',
  height: '30px',
  padding: '0 0.3em'
};

const panel = {
  border: '1px solid #ddd',
  borderRadius: '4px',
  backgroundColor: '#8881',
};

const formMessage = {
  margin: '2em 0',
  border: '1px solid #bbb',
  borderRadius: '8px',
  backgroundColor: '#eee',
  backgroundPosition: '15px 15px',
  backgroundSize: '20px',
  backgroundRepeat: 'no-repeat',
  padding: '15px 15px 15px 45px',
  '&.error': {
    border: '1px solid #FC605C',
    backgroundColor: '#FC605C22',
    backgroundImage: 'url(/static/images/error.svg)',
    color: '#FC605C',
    '& a': {
      color: '#FC605C',
    }
  },
  '&.success': {
    border: '1px solid #34c749',
    backgroundColor: '#34c74911',
    backgroundImage: 'url(/static/images/success.svg)',
    color: '#34c749',
    '& a': {
      color: '#34c749'
    }
  },
};

export {
  color,
  gaps,
  button,
  inputText,
  panel,
  formMessage
};
