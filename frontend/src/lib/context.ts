import { createMuiTheme } from '@material-ui/core/styles';

import themes, { overrides } from './themes';

const theme = createMuiTheme({...themes.default, ...overrides});

// @ts-ignore
function createPageContext({ store }) {
  return {
    theme
  };
}

// @ts-ignore
export default function getContext({ store }) {
  if (!(process as any).browser) {
    return createPageContext({ store });
  }

  if (!(global as any).INIT_MATERIAL_UI) {
    (global as any).INIT_MATERIAL_UI = createPageContext({ store });
  }

  return (global as any).INIT_MATERIAL_UI;
}
