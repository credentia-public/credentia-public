import { CustomError } from 'ts-custom-error';

export enum ErrorMessages {
  NotAuthenticated = 'not_authenticated',
  NotFound = 'not_found',
  Internal = 'internal_error',
  ValidationError = 'validation_error',
  InvalidCurrency = 'invalid_currency',
  TooManyRequests = 'too_many_requests',
  NotAuthorized = 'not_authorized',
  OperationNotAllowed = 'not_allowed',
  NoMatchedDocuments = 'no_matched_documents'
}

export enum UserErrorMessages {
  NotRegistered = 'not_registered',
  NotVerified = 'not_verified',
  InvalidPassword = 'invalid_password',
  AlreadyExist = 'already_exist',
}

export enum EmailErrorMessages {
  EmailProviderError = 'email_internal_error'
}

export enum FormErrors {
  Required = 'required',
  Invalid = 'invalid'
}

export class ApiError extends CustomError {
  statusCode: number;
  data: any;

  constructor(message: string, statusCode: number, data: any) {
    super(message);
    this.statusCode = statusCode;
    this.data = data;
  }
}
