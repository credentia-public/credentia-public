import { BrowserRouter as Router, Route, Switch, Redirect, RouteProps } from 'react-router-dom';
import React, { FunctionComponent, Suspense }                           from 'react';
import loadable                                                         from '@loadable/component';

import Loader       from '../components/common/Loader';
import Store        from '../store';
import { UserType } from '../store/domain/user'

// Auth
const Login = loadable(() => import(
  /* webpackChunkName: "login-page" */
  '../components/pages/login')
);
const SignUp = loadable(() => import(
  /* webpackChunkName: "signup-page" */
  '../components/pages/signup')
);
const SignUpSuccess = loadable(() => import(
  /* webpackChunkName: "signup-page" */
  '../components/pages/signup/success')
);
const PasswordReset = loadable(() => import(
  /* webpackChunkName: "password-reset-page" */
  '../components/pages/password_reset')
);

// Profile
const Settings = loadable(() => import(
  /* webpackChunkName: "settings-page" */
  '../components/pages/settings')
);
const SettingsChangePassword = loadable(() => import(
  /* webpackChunkName: "settings-change-password-page" */
  '../components/pages/settings/password')
);

// Public document
const Document = loadable(() => import(
  /* webpackChunkName: "document-page" */
  '../components/pages/document')
);
const DocumentPDF = loadable(() => import(
  /* webpackChunkName: "document-page" */
  '../components/pages/document/pdf')
);
const DocumentImage = loadable(() => import(
  /* webpackChunkName: "document-page" */
  '../components/pages/document/pdf')
);
const DocumentVerify = loadable(() => import(
  /* webpackChunkName: "document-page" */
  '../components/pages/document/verify')
);
const DocumentVerifyManual = loadable(() => import(
  /* webpackChunkName: "document-page" */
  '../components/pages/document/verifyManual')
);

// Issuer panel
const IssuerDocuments = loadable(() => import(
  /* webpackChunkName: "document-page" */
  '../components/pages/issuer/documents')
);
const DocumentFiles = loadable(() => import(
  /* webpackChunkName: "document-page" */
  '../components/pages/issuer/files')
);
const DocumentSign = loadable(() => import(
  /* webpackChunkName: "document-page" */
  '../components/pages/issuer/sign')
);

// Issuer master
const MasterChooseTemplate = loadable(() => import(
  /* webpackChunkName: "document-page" */
  '../components/pages/issuer/master/chooseTemplate')
);
const MasterFillData = loadable(() => import(
  /* webpackChunkName: "document-page" */
  '../components/pages/issuer/master/fillData')
);
const MasterFillHolders = loadable(() => import(
  /* webpackChunkName: "document-page" */
  '../components/pages/issuer/master/fillHolders')
);

// Holder panel
const HolderDocuments = loadable(() => import(
  /* webpackChunkName: "document-page" */
  '../components/pages/holder/documents')
);

// 404
const NoMatch = loadable(() => import(
  /* webpackChunkName: "document-page" */
  '../components/pages/_error')
);

// A wrapper for <Route> that redirects to the login
// screen if you're not yet authenticated.
const PrivateRoute: FunctionComponent<RouteProps> = ({ children, ...rest }) => {
  const store = Store.getStore();
  const session = store.sessionStore;

  return (
    <Route
      {...rest}
      render={({ location }) =>
        session.user ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: '/app/login',
              state: { from: location }
            }}
          />
        )
      }
    />
  );
};

const getUserType = () => {
  const store = Store.getStore();
  const session = store.sessionStore;
  const userType = session && session.user && session.user.type;
  return userType;
};

const getRedirectFromRoot = () => {
  const userType = getUserType();
  let redirectUrl;
  switch (userType) {
    case UserType.IssuerAmbassador:
      redirectUrl = '/app/issuer/documents';
      break;
    case UserType.Student:
      redirectUrl = '/app/holder/documents';
      break;
    case UserType.Admin:
      redirectUrl = '/app/admin';
      break;
    default:
      redirectUrl = '/app/login';
  }
  return redirectUrl;
};

export default function AppRouter() {
  return (
    <Router>
      <Suspense fallback={Loader}>
        <Switch>
          <Route exact path='/' render={() =>
            <Redirect to={getRedirectFromRoot()}/>
          }>
          </Route>

          <Route exact path='/app/login' component={Login}/>
          <Route exact path='/app/signup' component={SignUp}/>
          <Route exact path='/app/signup/success' component={SignUpSuccess}/>
          <Route exact path='/app/password-reset' component={PasswordReset}/>

          <Route exact path='/app/settings' component={Settings}/>
          <Route exact path='/app/settings/password' component={SettingsChangePassword}/>

          <Route exact path='/app/document/:id' component={Document}/>
          <Route exact path='/app/document/:id/pdf/raw' component={DocumentPDF}/>
          <Route exact path='/app/document/:id/image/:type/raw' component={DocumentImage}/>
          <Route exact path='/app/document/:id/verify' component={DocumentVerify}/>
          <Route exact path='/app/document/:id' component={DocumentVerifyManual}/>

          <PrivateRoute exact path='/app/issuer/documents'>
            <Route component={IssuerDocuments}/>
          </PrivateRoute>

          <PrivateRoute exact path='/app/issuer/master/chooseTemplate'>
            <Route component={MasterChooseTemplate}/>
          </PrivateRoute>
          <PrivateRoute exact path='/app/issuer/master/fillData'>
            <Route component={MasterFillData}/>
          </PrivateRoute>
          <PrivateRoute exact path='/app/issuer/master/fillHolders'>
            <Route component={MasterFillHolders}/>
          </PrivateRoute>

          <PrivateRoute exact path='/app/document/:id/files'>
            <Route component={DocumentFiles}/>
          </PrivateRoute>
          <PrivateRoute exact path='/app/document/:id/sign'>
            <Route component={DocumentSign}/>
          </PrivateRoute>

          <PrivateRoute exact path='/app/holder/documents'>
            <Route component={HolderDocuments}/>
          </PrivateRoute>

          <Route component={NoMatch} />
        </Switch>
      </Suspense>
    </Router>
  );
}
