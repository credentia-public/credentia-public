import { create }                                              from 'jss';
import jssExtend                                               from 'jss-plugin-extend';
import { createStyles, jssPreset, StylesProvider, withStyles } from '@material-ui/styles';
import { Provider }                                            from 'mobx-react';
import React, { Component, Suspense }                          from 'react';
import { toast }                                               from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';

import Store        from '../../store';
import Router       from '../../router';
import Loader       from '../common/Loader';
// import env   from '../../environments/environment';

const store = Store.init();

toast.configure({
  autoClose: 8000,
  draggable: false,
  position: 'bottom-right',
  hideProgressBar: true
});

const jss = create({
  plugins: [jssExtend(), ...jssPreset().plugins]
});

// tslint:disable-next-line:interface-name
export interface State {
  loading: boolean;
}

class App extends Component<{}, State> {

  constructor(props: any) {
    super(props);

    this.state = {
      loading: true
    };
  }

  async componentDidMount(): Promise<void> {
    try {
      await store.sessionStore.getUser();
      this.setState({
        loading: false
      });
    } catch (e) {
      this.setState({
        loading: false
      });
    }
  }

  render() {
    const { loading } = this.state;

    return (
      <Suspense fallback={null}>
        <StylesProvider jss={jss}>
          <Provider store={store}>
            {loading ? <Loader/> : <Router/>}
          </Provider>
        </StylesProvider>
      </Suspense>
    );
  }
}

const styles = createStyles({
  '@global': {
    body: {
      fontFamily: '"IBM Plex Sans", sans-serif'
    },
    '.hide': {
      display: 'none !important'
    },
    '*': {
      boxSizing: 'border-box'
    }
  }
});

export default withStyles(styles)(App);
