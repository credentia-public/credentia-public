import React                        from 'react';
import { createStyles, withStyles } from '@material-ui/styles';
import { WithStyles }               from '@material-ui/core';
import classnames from 'classnames';

// tslint:disable-next-line:interface-name
export interface Props extends WithStyles<typeof styles>, React.HTMLProps<HTMLInputElement> {

}

// tslint:disable-next-line:interface-name no-empty-interface
export interface State {}

export function Checkbox({ classes, ...rest}: Props) {
  return (
    <div className={classes.root}>
      <div className={classnames(classes.bullet, {
        hide: !rest.checked
      })}/>
      <input className={classes.input} type='checkbox' {...rest}/>
    </div>
  );
}

const styles = createStyles({
  root: {
    width: '18px',
    height: '18px',
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '4px',
    backgroundColor: '#FF9356'
  },
  bullet: {
    width: '8px',
    height: '8px',
    borderRadius: '50%',
    backgroundColor: '#fff'
  },
  input: {
    position: 'absolute',
    opacity: 0,
    width: '100%',
    height: '100%',
    cursor: 'pointer'
  }
});

export default withStyles(styles)(Checkbox);
