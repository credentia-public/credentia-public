import React                                from 'react';
import { withTranslation, WithTranslation } from 'react-i18next';
import { WithStyles }                       from '@material-ui/core';
import { withStyles }                       from '@material-ui/styles';
import { Link }                             from 'react-router-dom';

// tslint:disable-next-line:interface-name
interface Props extends WithTranslation, WithStyles<typeof styles> {
  to: string;
}

const Back = ({ to, classes, t }: Props) => {
  return (
    <div className={classes.back}>
      <Link to={to} className={classes.link}>
        <span>⟵ {t('go_back')}</span>
      </Link>
    </div>
  );
};

const styles = () => ({
  back: {
    marginBottom: '1.5em'
  },
  link: {
    display: 'inline-block',
    border: '1px solid #FF9356',
    borderRadius: '6px',
    padding: '7px 9px',
    textDecoration: 'none',
    color: '#FF9356',
    '&:hover': {
      background: '#FF9356',
      color: 'white'
    }
  }
});

export default withStyles(styles)(
  withTranslation('common')(Back)
);
