import React                        from 'react';
import { createStyles, withStyles } from '@material-ui/styles';
import { WithStyles }               from '@material-ui/core';
import classnames from 'classnames';

// tslint:disable-next-line:interface-name
export interface Props extends WithStyles<typeof styles>, React.HTMLProps<HTMLInputElement> {
  status?: string;
  labelText?: string;
  errorMessage?: string;
}

// tslint:disable-next-line:interface-name no-empty-interface
export interface State {}

class TextField extends React.PureComponent<Props, State> {
  render() {
    const { labelText, errorMessage, classes, status, ...rest } = this.props;
    return (
      <div className={classes.root}>
        <label className={classnames(classes.label, {
          error: errorMessage
        })} htmlFor={rest.id}>{labelText}</label>
        <input className={classnames(classes.input, {
          error: errorMessage
        })} id={rest.id} {...rest}/>
        <span className={classes.msg}>{errorMessage || status}</span>
      </div>
    );
  }
}

const styles = createStyles({
  msg: {
    color: '#FC605C',
    fontSize: '12px'
  },
  root: {
    height: '54px',
  },
  label: {
    display: 'block',
    fontSize: '13px',
    lineHeight: '17px',
    fontFamily: 'IBM Plex Sans, sans-serif',
    color: '#8F94A4',
    '&.error': {
      color: '#FC605C',
    }
  },
  input: {
    color: '#202949',
    border: 'none',
    borderBottom: '1px solid rgba(32, 41, 73, 0.2)',
    fontSize: '18px',
    lineHeight: '23px',
    outline: 'none',
    width: '100%',
    padding: '7px 0',
    '&.error': {
      borderBottomColor: '#FC605C'
    }
  }
});

export default withStyles(styles)(TextField);
