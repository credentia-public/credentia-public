import React          from 'react';
import { makeStyles } from '@material-ui/core/styles';

// tslint:disable-next-line:interface-name
interface Props {
  url: string;
  size: number;
  alt?: string;
}

const useStyles = makeStyles({
  root: {
    padding: '12px',
    borderRadius: '50%',
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: '#F6F6F8'
    }
  },
  image: {
    display: 'block'
  }
});

export default function Icon({ url, size, alt }: Props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <img className={classes.image} src={url} alt={alt} width={size} height={size}/>
    </div>
  );
}
