import React, { useState } from 'react';
import { makeStyles }      from '@material-ui/styles';
import classnames          from 'classnames';
import { useTranslation }  from 'react-i18next';

const useStyles = makeStyles({
  langSelector: {
    position: 'relative',
    lineHeight: '30px',
    fontSize: '13px',
    cursor: 'pointer',
    userSelect: 'none',
    '&:hover': {
      backgroundColor: '#F6F6F8'
    }
  },
  languagesList: {
    display: 'none',
    position: 'absolute',
    bottom: '100%',
    left: 0,
    minWidth: '100%',
    boxShadow: '0px 0px 4px 0px #F6F6F8',
    border: '1px solid #ddd',
    backgroundColor: '#fff',
    '&.opened': {
      display: 'block'
    }
  },
  language: {
    padding: '0px 6px',
    '&:hover': {
      backgroundColor: '#F6F6F8'
    },
    '&.current': {
      opacity: 0.5
    }
  },
  currentLanguageTitle: {}
});

const LangSelector = () => {
  const [isOpened, setIsOpened] = useState(false);
  const { i18n } = useTranslation();
  const classes = useStyles();

  const toggle = () => setIsOpened(!isOpened);

  const selectLang = (lang: string) => {
    toggle();
    i18n.changeLanguage(lang);
  };

  const currentLang = i18n.language;

  let currentLangTitle = 'Language';

  switch (currentLang) {
    case 'ru':
      currentLangTitle = 'Русский';
      break;
    case 'en':
      currentLangTitle = 'English';
      break;
    case 'tr':
      currentLangTitle = 'Türk';
      break;
  }

  return (
    <div className={classes.langSelector}>
      <div className={classnames(classes.language, 'current')} onClick={toggle}>
        <span className={classes.currentLanguageTitle}>{currentLangTitle}</span>&nbsp;<span>▾</span>
      </div>
      <div className={classnames(classes.languagesList, isOpened ? 'opened' : null)}>
        <div className={classes.language} onClick={() => selectLang('en')}>English</div>
        <div className={classes.language} onClick={() => selectLang('ru')}>Русский</div>
        <div className={classes.language} onClick={() => selectLang('tr')}>Türk</div>
      </div>
    </div>
  );
};

export default LangSelector;
