import React                                from 'react';
import { WithTranslation, withTranslation } from 'react-i18next';
import { createStyles, WithStyles }         from '@material-ui/core';
import { withStyles }                       from '@material-ui/styles';

// tslint:disable-next-line:interface-name
interface Props extends WithTranslation, WithStyles<typeof styles> {
}

const Loader = ({ classes, t }: Props) => {
  return (
    <div className={classes.preloader}>
      <img className={classes.logo} src='/static/images/logo.svg'/>
      <span className={classes.text}>{t('loading')}...</span>
    </div>
  );
};

const styles = createStyles({
  preloader: {
    marginTop: '1em',
    marginBottom: '1em',
    textAlign: 'center'
  },
  logo: {
    display: 'inline-block',
    verticalAlign: 'middle',
    width: 20,
    height: 20,
    animation: '$blink 1.3s infinite linear'
  },
  '@keyframes blink': {
    '0%': { opacity: 0.2 },
    '50%': { opacity: 1 },
    '100%': { opacity: 0.2 }
  },
  text: {
    verticalAlign: 'middle',
    fontSize: 16,
    lineHeight: '1em',
    marginLeft: '0.5em'
  }
});

export default withStyles(styles)(
  withTranslation('common')(Loader)
);
