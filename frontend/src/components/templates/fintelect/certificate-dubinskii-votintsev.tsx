// @ts-nocheck
import React, { Component, RefObject }      from 'react';
import { WithStyles }                       from '@material-ui/core';
import { WithStyles }                       from '@material-ui/core';
import { withTranslation, WithTranslation } from 'react-i18next';
import i18next                              from 'i18next';

import { withTranslation } from '../../../lib/i18n';

// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, WithStyles<typeof styles> {
  title: string;
  name: string;
  surname: string;
  secondName: string;
}

export interface State {
  width?: number;
}
export class Certificate extends Component<Props, {}> {

  private rootRef: RefObject<HTMLDivElement>;

  constructor(props: any) {
    super(props);
    this.rootRef = React.createRef<HTMLDivElement>();
    this.state = {
      width: null
    };
  }

  componentDidMount() {
    this.resizeWatcher();
    window.addEventListener('resize', () => this.resizeWatcher());
  }

  componentWillUnmount() {
    window.removeEventListener('resize', () => this.resizeWatcher());
  }

  resizeWatcher = () => {
    this.setState(() => ({ width: this.rootRef.current ? this.rootRef.current.offsetWidth : 300 }));
  };

  render() {
    const { width } = this.state;
    const { t, classes, title, surname, name, secondName, date } = this.props;

    const bgFileName = 'certificate-dubinskii-votintsev.png';

    const bg = {
      width: 1280,
      height: 899,
      url:  `/static/images/fintelect/${bgFileName}`
    };

    const fullName = [surname, name, secondName].filter(part => part).join(' ');

    const dateFormatted = date ? (new Date(date)).toLocaleString('ru', {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric'
    }) : null;

    const style = {
      content: {
        border: '1px solid #ddd',
        paddingBottom: bg.height / bg.width * 100 + '%',
        backgroundImage: `url("${bg.url}")`,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        display: 'flex',
        overflow: 'hidden',
        position: 'relative'
      },
      field: {
        position: 'absolute',
        wordBreak: 'break-word',
        textAlign: 'center'
      },
      fullName: {
        top: '41%',
        left: '5%',
        width: '90%',
        fontSize: `${width * 0.035}px`,
      },
      title: {
        top: '60%',
        left: '10%',
        width: '80%',
        fontSize: `${width * 0.028}px`
      },
      date: {
        top: '84.2%',
        left: '40%',
        width: '20%',
        textAlign: 'center',
        fontSize: `${width * 0.018}px`
      },
    };

    return (
      <div ref={this.rootRef}>
        <section style={style.content}>
          <div style={{ ...style.field, ...style.fullName }}>{fullName}</div>
          <div style={{ ...style.field, ...style.title }}>{title}</div>
          <div style={{ ...style.field, ...style.date }}>{dateFormatted}</div>
        </section>
        {this.props.children}
      </div>
    );
  }
}

export default withTranslation('common')(
  (Certificate)
);
