// @ts-nocheck
import React, { Component }                 from 'react';
import { createStyles, WithStyles }         from '@material-ui/core';
import { withStyles }                       from '@material-ui/styles';
import { withTranslation, WithTranslation } from 'react-i18next';

// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, WithStyles<typeof styles> {
  imageLogoURL: string;
}

export class Certificate extends Component<Props, {}> {
  render() {

    const {
      t, classes,
      title, name, surname, secondName,
      qualification, specialization,
      HolderEduCoursework, HolderEduPractice,
      HolderEduExitExam, HolderEduThesis, HolderEduCourses, HolderBirthday, HolderEduPrevious, DocumentSpecialty,
      HolderEduYears, HolderEduStart, HolderEduEnd, HolderEduEntryExam
    } = this.props;

    const fullName = [surname, name, secondName].filter(_ => _).join(' ');

    return (
      <div>
        <div className={classes.titleContainer}>
          <div className={classes.header}>
            <img
              src="/static/images/permwinterschool/cert-image-logo.png"
              width="114px"
            />
          </div>
          <div className={classes.body}>
            <div className={classes.title}>Certificate of program completion</div>
            <div className={classes.name}>{fullName}</div>
            <div className={classes.divider} />
            <div className={classes.info}>
              <div className={classes.infoLeft}>Money<br/>and People</div>
              <div className={classes.infoRight}>
                <ul className={classes.list}>
                  <li className={classes.listItem}>Electronic Markets</li>
                  <li className={classes.listItem}>Financial Institutions and Risk Management</li>
                  <li className={classes.listItem}>Blockchains and Digital Assets</li>
                  <li className={classes.listItem}>Digital Identity and Governance</li>
                  <li className={classes.listItem}>Humanity in the Digital Era</li>
                </ul>
              </div>
            </div>
          </div>
          <aside className={classes.aside}>
            Perm Winter School <span>'20</span>
          </aside>
          <div className={classes.footer}>
            <div className={classes.date}>February 1-2</div>
            <div className={classes.location}>Perm</div>
          </div>
          <div className={classes.figure1}/>
          <div className={classes.figure2}/>
        </div>
        
        {this.props.children}
      </div>
    );
  }
}

const styles = createStyles({
  titleContainer: {
    display: 'flex',
    flexDirection: 'column',
    height: '595px',
    width: '850px',
    maxWidth: '100%',
    padding: '35px',
    margin: '0 auto',
    fontSize: '10px',
    backgroundColor: '#fff',
    boxShadow: 'inset 0 0 0 1px rgba(0, 0, 0, .05)',
    color: '#000100',
    fontFamily: '"Work Sans", "Open Sans",-apple-system,BlinkMacSystemFont, "Roboto", sans-serif',
    lineHeight: 1.35,
    position: 'relative',
    overflow: 'hidden',
    '@media (max-width: 480px)': {
      height: 'auto',
      padding: '35px 80px 100px 35px'
    }
  },
  header: {
    marginBottom: '40px'
  },
  body: {
    width: '420px',
    maxWidth: '100%',
    margin: '0 auto'
  },
  title: {
    fontSize: '38px',
    lineHeight: '36px',
    fontWeight: 'bold',
    marginBottom: '24px'
  },
  name: {
    fontSize: '16px',
    lineHeight: '25px',
    fontWeight: 'bold',
    textTransform: 'uppercase',
    color: '#cb225b',
    marginBottom: '20px'
  },
  divider: {
    borderBottom: '1px solid #262424',
    margin: '30px 0'
  },
  info: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    paddingTop: '15px'
  },
  infoLeft: {
    fontSize: '13px',
    fontWeight: 600,
    width: '28%',
    marginBottom: '10px',
    '@media (max-width: 480px)': {
      width: '100%'
    }
  },
  infoRight: {
    width: '65%',
    '@media (max-width: 480px)': {
      width: '100%'
    }
  },
  list: {
    listStyle: 'none',
    padding: '0',
    margin: '0',
    fontSize: '12px',
    lineHeight: '17px'
  },
  listItem: {
    marginBottom: '5px'
  },
  aside: {
    position: 'absolute',
    top: '92px',
    right: '-16px',
    fontWeight: 900,
    fontSize: '11px',
    transform: 'rotate(-90deg)',
    '& span': {
      color: '#dd8144'
    }
  },
  footer: {
    position: 'absolute',
    bottom: '23px',
    left: '35px',
    fontSize: '13px',
    lineHeight: '17px'
  },
  date: {
    fontWeight: 600
  },
  location: {
    color: '#cb205b',
    fontSize: '9px',
    fontWeight: 'bold',
    textTransform: 'uppercase'
  },
  figure1: {
    position: 'absolute',
    top: '190px',
    left: '-290px',
    width: '417px',
    height: '380px',
    backgroundImage: 'url("/static/images/permwinterschool/cert-image-figure1.png")',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    pointerEvents: 'none',
    '@media (max-width: 767px)': {
      display: 'none'
    }
  },
  figure2: {
    position: 'absolute',
    bottom: '-120px',
    right: '-135px',
    width: '340px',
    height: '318px',
    backgroundImage: 'url("/static/images/permwinterschool/cert-image-figure2.png")',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    pointerEvents: 'none',
    '@media (max-width: 767px)': {
      display: 'none'
    }
  }
});

export default withTranslation('common')(
  withStyles(styles)(Certificate)
);

