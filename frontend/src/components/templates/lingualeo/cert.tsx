// @ts-nocheck
import React, { Component }                 from 'react';
import { WithStyles }                       from '@material-ui/core';
import { createStyles, withStyles }         from '@material-ui/styles';
import { withTranslation, WithTranslation } from 'react-i18next';

// tslint:disable-next-line:interface-name
interface Props extends WithTranslation, WithStyles<typeof styles> {
  courseName: string;
  course: string;
  name: string;
  date: string;
}

class Certificate extends Component<Props> {

  render() {
    const { t, classes, name, date, course } = this.props;

    const courseName = course ? course : '';

    const timestamp = Date.parse(date);
    const dateFormatted = !timestamp ? '' : (new Date(timestamp)).toLocaleDateString('ru', {
      day: 'numeric',
      month: 'long',
      year: 'numeric'
    });

    return (
      <div className={classes.root}>
        <div className={classes.header}>
          <img src='/static/images/lingualeo/logo.svg' alt='lingualeo'/>
          <h1 className={classes.heading}>{t('certificate')}</h1>
        </div>
        <div className={classes.person}>{name}</div>
        <div className={classes.description}>
          Успешно завершил(а) интерактивный курс английского языка
          «<strong>{courseName}</strong>»
          на обучающем веб-сервисе {' '}
          <strong>Lingualeo.com</strong>
        </div>
        <div className={classes.descriptionSmall}>
          Успешно завершил(а) курс
          «<strong>{courseName}</strong>»
          на <strong>Lingualeo.com</strong>
        </div>
        <div className={classes.bottomContent}>
          <div className={classes.date}>{dateFormatted}</div>
          <div className={classes.stamp}>
            <img src='/static/images/lingualeo/stamp.png' alt='Печать'/>
            <span className={classes.sign}>Твой Leo</span>
          </div>
        </div>
        <div className={classes.leaves}></div>
      </div>
    );
  }
}

const styles = theme => createStyles({
  root: {
    margin: '0 auto',
    position: 'relative',
    backgroundColor: '#f8f8f2',
    backgroundImage: 'url(/static/images/lingualeo/stroke.png)',
    backgroundSize: '100% auto',
    backgroundRepeat: 'repeat-y',
    color: '#393939',
    textShadow: '-2px -2px 2px #f8f8f2, -2px 2px 2px #f8f8f2, 2px -2px 2px #f8f8f2, 2px 2px 2px #f8f8f2',
    padding: '30px',
    textAlign: 'center',
    overflow: 'hidden',
    zIndex: 0,

    '@media (min-width: 992px)': {
      height: '586px',
      padding: '48px 0 0 0'
    },
    '@media (max-width: 400px)': {
      padding: '15px'
    },
    '@media (max-width: 319px)': {
      paddingTop: '20px',
      paddingLeft: '10px',
      paddingRight: '10px'
    }
  },

  leaves: {
    position: 'absolute',
    left: '-1px',
    bottom: '-1px',
    width: '29%',
    height: '84%',
    backgroundImage: 'url(/static/images/lingualeo/leaves.png)',
    backgroundSize: '100% 100%',
    zIndex: -1,

    '@media (max-width: 400px)': {
      width: '50%',
      opacity: '.2'
    }
  },

  header: {
    marginBottom: '40px',

    '@media (max-width: 400px)': {
      marginBottom: '10px'
    },
    '@media (max-width: 319px)': {
      marginBottom: '20px'
    }
  },

  heading: {
    fontWeight: '300',
    textTransform: 'uppercase',
    fontSize: '54px',
    padding: '68px 0 0',
    margin: '0',
    lineHeight: 1.2,

    '@media (max-width: 991px)': {
      paddingTop: '40px',
      fontSize: '46px'
    },
    '@media (max-width: 480px)': {
      fontSize: '36px'
    },
    '@media (max-width: 400px)': {
      paddingTop: '15px'
    },
    '@media (max-width: 319px)': {
      fontSize: '21px',
      paddingTop: '0',
      display: 'none'
    }
  },

  person: {
    fontWeight: 'bold',
    fontSize: '30px',
    marginBottom: '15px',
    overflow: 'hidden',
    textOverflow: 'ellipsis',

    '@media (max-width: 480px)': {
      fontSize: '24px'
    },
    '@media (max-width: 319px)': {
      lineHeight: '1',
      fontSize: '20px',
      marginBottom: '20px'
    }
  },

  description: {
    margin: '0 auto 50px',
    maxWidth: '560px',
    fontSize: '18px',
    lineHeight: 1.8,

    '@media (max-width: 400px)': {
      marginBottom: '20px',
      lineHeight: 1.4
    },
    '@media (max-width: 319px)': {
      display: 'none'
    }
  },

  descriptionSmall: {

    '@media (min-width: 319px)': {
      display: 'none'
    },

    '@media (max-width: 319px)': {
      fontWeight: 400,
      fontSize: '15px',
      marginBottom: '20px'
    }
  },

  bottomContent: {
    margin: '0 auto',
    maxWidth: '630px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexWrap: 'wrap',

    '@media (max-width: 991px)': {
      maxWidth: '400px'
    }
  },

  date: {
    flex: '0 0 auto',
    width: 'auto',
    maxWidth: '100%',
    paddingRight: '15px',
    paddingLeft: '15px',

    '@media (max-width: 575px)': {
      marginBottom: '25px',
      width: '100%'
    },
    '@media (max-width: 400px)': {
      marginBottom: '15px'
    },
    '@media (max-width: 319px)': {
      marginBottom: '10px',
      fontSize: '10px',
      fontWeight: 700
    }
  },

  stamp: {
    flex: '0 0 auto',
    width: 'auto',
    maxWidth: '100%',
    paddingRight: '15px',
    paddingLeft: '15px',

    '@media (max-width: 575px)': {
      width: '100%'
    },
    '@media (max-width: 319px)': {
      fontSize: '11px'
    },

    '& img': {
      height: '88px',
      width: '88px',
      marginRight: '.5rem',
      verticalAlign: 'middle',
      borderStyle: 'none',

      '@media (max-width: 400px)': {
        width: '40px',
        height: '40px'
      },
      '@media (max-width: 319px)': {
        width: '20px',
        height: '20px'
      }
    }

  }

});

export default withTranslation('common')(
  withStyles(styles)(Certificate)
);
