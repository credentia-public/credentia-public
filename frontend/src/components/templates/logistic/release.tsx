// @ts-nocheck
import React, { Component, RefObject }      from 'react';
import { WithStyles }                       from '@material-ui/core';
import { withTranslation, WithTranslation } from 'react-i18next';

// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, WithStyles<typeof styles> {
  t: i18next.TFunction;
  title: string;
  date: string;
}

export interface State {
  width?: number;
}

export class Certificate extends Component<Props, {}> {

  private rootRef: RefObject<HTMLDivElement>;

  constructor(props: any) {
    super(props);
    this.rootRef = React.createRef<HTMLDivElement>();
    this.state = {
      width: null
    };
  }

  componentDidMount() {
    this.resizeWatcher();
    window.addEventListener('resize', () => this.resizeWatcher());
  }

  componentWillUnmount() {
    window.removeEventListener('resize', () => this.resizeWatcher());
  }

  resizeWatcher = () => {
    this.setState(() => ({ width: this.rootRef.current ? this.rootRef.current.offsetWidth : 300 }));
  };

  render() {
    const { width } = this.state;
    const { t, classes, date, title } = this.props;

    const dateFormatted = date ? (new Date(date)).toLocaleString('ru', {
      year: 'numeric',
      month: '2-digit',
      day: 'numeric'
    }) : null;

    const bg = {
      width: 1261,
      height: 1185,
      url: '/static/images/niissu/release.png'
    };

    const style = {
      content: {
        border: '1px solid #ddd',
        paddingBottom: bg.height / bg.width * 100 + '%',
        backgroundImage: `url("${bg.url}")`,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        fontFamily: 'Academy',
        overflow: 'hidden',
        position: 'relative'
      },
      field: {
        position: 'absolute',
        wordBreak: 'break-word'
      },
      date: {
        top: '43.1%',
        left: '39%',
        width: '35%',
        fontSize: `${width * 0.025}px`
      },
      title: {
        top: '48.4%',
        left: '27%',
        width: '66%',
        fontSize: `${width * 0.025}px`
      }
    };

    return (
      <div ref={this.rootRef}>
        <section style={style.content}>
          <div style={{ ...style.field, ...style.date }}>{dateFormatted}</div>
          <div style={{ ...style.field, ...style.title }}>{title}</div>
        </section>
        {/*this.props.children*/}
      </div>
    );
  }
}

export default withTranslation('common')(
  (Certificate)
);
