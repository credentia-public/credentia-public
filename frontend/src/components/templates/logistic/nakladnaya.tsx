// @ts-nocheck
import React, { Component, Ref, RefObject } from 'react';
import { WithStyles }                       from '@material-ui/core';
import { withTranslation, WithTranslation } from 'react-i18next';

// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, WithStyles<typeof styles> {
  title: string;
  name: string;
  surname: string;
  date: string;
  id: string;
  sign: string;
}

export interface State {
  width?: number;
}

export class Certificate extends Component<Props, {}> {

  private rootRef: RefObject<HTMLDivElement>;

  constructor(props: any) {
    super(props);
    this.rootRef = React.createRef<HTMLDivElement>();
    this.state = {
      width: null
    };
  }

  componentDidMount() {
    this.resizeWatcher();
    window.addEventListener('resize', () => this.resizeWatcher());
  }

  componentWillUnmount() {
    window.removeEventListener('resize', () => this.resizeWatcher());
  }

  resizeWatcher = () => {
    // this.setState(() => ({ width: this.ref ? this.ref.offsetWidth : 200 }));
    console.log('resizeWatcher', this.rootRef.current ? this.rootRef.current.offsetWidth : null)
    this.setState(() => ({ width: this.rootRef.current ? this.rootRef.current.offsetWidth : 300 }));
  };

  render() {
    const { width } = this.state;
    const {
      t, classes, date, documentNumber, title, name, secondName, surname, sign
    } = this.props;

    const practicalyWidth = width <= 688 ? 688 : width;
    const style = {
      wrapper: {
        backgroundImage: 'url(/static/images/niissu/nakladnaya.jpg)',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        width: `${practicalyWidth}px`,
        minWidth: '688px',
        minHeight: '480px',
        height: `${practicalyWidth / 1.43}px`,
        margin: '0 auto',
        display: 'flex',
        fontFamily: 'Academy',
        overflow: 'hidden'
      },
      leftPartWrapper: {
        width: '40%',
      },
      leftPart: {
        width: '100%',
        paddingTop: `${practicalyWidth / 4.5}px`,
        position: 'relative',
        display: 'inline-block',
        height: '55%',
        verticalAlign: 'top',
        float: 'left',
        marginLeft: '20%'
      },
      rigthPart: {
        height: '100%',
        width: '50%',
        marginTop: `${practicalyWidth / 11.4}px`,
        marginLeft: `${practicalyWidth / 10.4}px`,
        fontSize: `${practicalyWidth / 77.5}px`,
      },
      textArea: {
        width: '100%',
        textAlign: 'center'
      },
      h1: {
        margin: '0',
        fontSize: `${practicalyWidth / 41}px`
      },
      h3: {
        margin: '0',
        fontSize: `${practicalyWidth / 60}px`,
        marginBottom: `${practicalyWidth / 18.5}px`,
      },
      number: {
        color: '#c16b47',
        marginBottom: `${practicalyWidth / 4.12}px`,
      },
      registerNumber: {
        fontSize: `${practicalyWidth / 77.5}px`,
      },
      register: {
        textAlign: 'right',
        width: '100%',
        marginLeft: `${-(practicalyWidth / 19.4)}px`,
      },
      content: {
        width: '95%',
        marginLeft: '7%'
      },
      name: {
        textAlign: 'center',
        margin: '5% 0',
      },
      paragraphMargin: {
        margin: '10% 0',
        marginTop: '5%',
      },
      curse: {
        margin: '15% 0px 15%',
        textAlign: 'center',
      },
      strong: {
        marginLeft: '20%',
      },
      timehMargin: {
        margin: `${practicalyWidth / 46.5}px 0 ${practicalyWidth / 17.8}px`,
      },
      lead: {
        textAlign: 'right',
      },
      mP: {
        marginLeft: '30%',
        fontSize: `${practicalyWidth / 110}px`,
      },
      leadGroup: {
        marginBottom:`${practicalyWidth / 25.6}px`,
        width: `${practicalyWidth / 2.8}px`,
      },
      city: {
        width: `${practicalyWidth / 3.3}px`,
        display: 'flex',
        justifyContent: 'space-between',
        marginLeft: `${practicalyWidth / 42.6}px`,
      },
      topLead: {
        textAlign: 'right',
        marginRight: `${-(practicalyWidth / 51)}px`,
      },
      underLine: {
        borderBottom: '1px solid black',
        position: 'relative',
        bottom: '2px',
        left: '10px'
      },
      underLineCity: {
        borderBottom: '1px solid black',
        position: 'relative',
        bottom: '2px',
        width: `${(practicalyWidth / 10)}px`,
        textAlign: 'center'
      },
      preview: {
        backgroundImage: 'url(/static/images/ithub_passport_bg.jpg)',
        width: '100vw',
        height: '100vh',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        padding: '30px 10px',
        textAlign: 'center'
      }
    };

    if (width <= 300 && width !== null) {
      return (
        <div ref={this.rootRef} style={style.preview}
             onClick={this.props.onClick}>
          <div>
            {title}
          </div>
        </div>
      );
    }

    return (
      <div
        ref={this.rootRef}
        style={{ height: '100%', width: '100%' }}
      >
        <div style={{ width: `${practicalyWidth}px` }}>
          <section style={style.wrapper}>
            <div style={style.leftPartWrapper}>
              <div style={style.leftPart}>
                <div style={style.textArea}>
                  <h1 style={style.h1}>{t('doc_type.passport')}</h1>
                  <h3 style={style.h3}>о повышении квалицикации</h3>
                  <div style={style.number}>
                    ПК № {documentNumber}
                  </div>
                </div>
              </div>
{/*              <div style={style.register}>
                <span style={style.registerNumber}>
                Регистрационный номер
                  <span style={style.underLine}>
                    222222222
                  </span>
                </span>
              </div>*/}
            </div>
            <div style={style.rigthPart}>
              <div>
                <div style={style.content}>
                  <p>Настоящее удостоверение выдано</p>
                  <h2 style={style.name}> {surname} {name} {secondName}</h2>
                  <p style={style.paragraphMargin}>в том, что он(а) с 22 ноября 2019 г. по 20 ноября 2020 г.</p>
                  <p style={style.paragraphMargin}>прошел(а) повышение квалификации в (на)</p>
                  <h3 style={style.curse}>{title}</h3>
                  <p style={style.timehMargin}>В объеме <strong style={style.strong}>500 часов</strong></p>
                  <div>
                    <div style={style.leadGroup}>
                      <p style={style.topLead}>
                        <em>
                          Руководитель
                          <span style={style.underLine}>
                            Тот Самый Человек
                          </span>
                        </em>
                      </p>
                      <span style={style.mP}>М.П</span>
                      <p style={style.lead}>
                        <em>
                          Секретарь
                          <span style={style.underLine}>
                            Тот Самый Человек
                          </span>
                        </em>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div>
                <div style={style.city}>
                  <span>Город</span>
                  <span style={style.underLineCity}>
                    Москва
                  </span>
                  <span>год</span>
                  <span style={style.underLineCity}>2019</span>
                </div>
              </div>
            </div>
          </section>
        </div>
        {this.props.children}
      </div>
    );
  }
}

export default withTranslation('common')(
  (Certificate)
);
