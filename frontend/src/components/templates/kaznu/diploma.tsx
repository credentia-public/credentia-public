// @ts-nocheck
import React, { Component, Fragment, RefObject } from 'react';
import { createStyles, WithStyles }              from '@material-ui/core';
import { withStyles }                            from '@material-ui/styles';
import classnames                                from 'classnames';
import { withTranslation, WithTranslation }      from 'react-i18next';

// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, WithStyles<typeof styles> {
  title: string;
  name: string;
  surname: string;
  secondName: string;
  qualification: string;
  specialization: string;
  DocumentSpecialty: string;
  DocumentRegDate: string;
  HolderLastName2: string;
  HolderFirstName2: string;
  HolderSecondaryName2: string;
  HolderBirthday: string;
  HolderEduPrevious: string;
  HolderEduEntryExam: string;
  HolderEduStart: string;
  HolderEduEnd: string;
  HolderEduYears: string;
  HolderEduCoursework: Array<{ name: string; theme: string; mark: string; }>;
  HolderEduPractice: Array<{ name: string; credits: string; hours: string; mark: string; }>;
  HolderEduExitExam: Array<{ name: string; mark: string; }>;
  HolderEduThesis: Array<{ name: string; theme: string; mark: string; }>;
  HolderEduCourses: Array<{ name: string; credits: string; hours: string; mark: string; }>;
}

export class Certificate extends Component<Props, {}> {

  private rootRef: RefObject<HTMLDivElement>;

  constructor(props: any) {
    super(props);
    this.rootRef = React.createRef<HTMLDivElement>();
    this.state = {
      width: null
    };
  }

  componentDidMount() {
    this.onResize();
    window.addEventListener('resize', this.onResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize);
  }

  onResize = () => {
    this.setState(() => ({ width: this.rootRef.current ? this.rootRef.current.offsetWidth : 300 }));
  };

  render() {
    const {
      t, classes,
      title, name, surname, secondName,
      qualification, specialization,
      HolderEduCoursework, HolderEduPractice,
      HolderEduExitExam, HolderEduThesis, HolderEduCourses, HolderBirthday, HolderEduPrevious, DocumentSpecialty,
      HolderEduYears, HolderEduStart, HolderEduEnd, HolderEduEntryExam
    } = this.props;

    const fullName = [surname, name, secondName].filter(_ => _).join(' ');

    const { width } = this.state;

    const bg = {
      width: 1056,
      height: 816
    };

    // tslint:disable-next-line:no-shadowed-variable
    const styles = {
      firstList: {
        overflow: 'hidden',
        margin: '0 auto',
        paddingBottom: bg.height / bg.width * 100 + '%',
        backgroundColor: '#f1f1f1',
        backgroundImage: 'url("/static/images/kaznu/diploma.png")',
        backgroundSize: '100%',
        backgroundRepeat: 'no-repeat',
        color: '#020032',
        fontSize: '16px',
        position: 'relative'
      },
      field: {
        position: 'absolute',
        overflow: 'hidden',
        textAlign: 'center',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap'
      },
      fullName: {
        left: '50%',
        top: '46%',
        width: '82%',
        transform: 'translate(-52%)',
        fontSize: width * 0.032
      },
      title: {
        left: '50%',
        top: '56%',
        width: '70%',
        transform: 'translate(-50%)',
        fontSize: width * 0.02,
        lineHeight: '2em',
        whiteSpace: 'normal'
      }
    };

    return (
      <Fragment>

        <div ref={this.rootRef} style={styles.firstList}>
          <div style={{ ...styles.field, ...styles.fullName }}>
            {fullName}
          </div>
          <div style={{ ...styles.field, ...styles.title }}>
            {title}
          </div>
        </div>

        {this.props.children}

        {specialization && qualification &&
        <div className={classes.detailsContainer}>

          <div className={classes.detailsTitle}>
            {specialization}
          </div>

          <div className={classes.detailsSubtitle}>
            {qualification}
          </div>

          <div className='record'>
            <h3>{t('master.name')}</h3>
            <div>{surname} {name} {secondName}</div>
          </div>

          <div className='record'>
            <h3>{t('birth_date')}</h3>
            <div>{HolderBirthday}</div>
          </div>

          <div className='record'>
            <h3>{t('diploma.holder_edu_previous')}</h3>
            <div>{HolderEduPrevious}</div>
          </div>

          {HolderEduEntryExam &&
          <div className='record'>
            <h3>{t('diploma.entry_exam')}</h3>
            <div>{HolderEduEntryExam}</div>
          </div>
          }

          <div className='record'>
            <h3>{t('diploma.qualification')}</h3>
            <div>{qualification && qualification.toLowerCase()}</div>
          </div>

          <div className='record'>
            <h3>{t('diploma.specialization')}</h3>
            <div>{DocumentSpecialty}</div>
          </div>

          <div className='record'>
            <h3>{t('diploma.start_date')}</h3>
            <div>{HolderEduStart}</div>
          </div>

          <div className='record'>
            <h3>{t('diploma.end_date')}</h3>
            <div>{HolderEduEnd}</div>
          </div>

          <div className='record'>
            <h3>{t('diploma.education_period')}</h3>
            <div>{HolderEduYears}</div>
          </div>

          {HolderEduCoursework && HolderEduCoursework.length &&
          <div className={classes.detailsBlock}>
            <div className={classes.detailsBlockTitle}>
              {t('diploma.course_works')}
            </div>
            <div className={classes.tableWrapper}>
              <table>
                <thead>
                <tr>
                  <th>{t('diploma.discipline')}</th>
                  <th>{t('diploma.theme')}</th>
                  <th>{t('grade')}</th>
                </tr>
                </thead>
                <tbody>
                {HolderEduCoursework.map(c => {
                  return (
                    <tr key={c.name}>
                      <td>{c.name}</td>
                      <td>{c.theme}</td>
                      <td>{c.mark}</td>
                    </tr>
                  );
                })}
                </tbody>
              </table>
            </div>
          </div>
          }

          {HolderEduPractice && HolderEduPractice.length &&
          <div className={classnames(classes.detailsBlock, 'practice')}>
            <div className={classes.detailsBlockTitle}>
              {t('diploma.practise')}
            </div>
            <div className={classes.tableWrapper}>
              <table>
                <thead>
                <tr>
                  <th>{t('diploma.name')}</th>
                  <th>{t('diploma.zet')}</th>
                  <th>{t('diploma.hours')}</th>
                  <th>{t('grade')}</th>
                </tr>
                </thead>
                <tbody>
                {HolderEduPractice.map(c => {
                  return (
                    <tr key={c.name}>
                      <td>{c.name}</td>
                      <td>{c.credits}</td>
                      <td>{c.hours}</td>
                      <td>{c.mark}</td>
                    </tr>
                  );
                })}
                </tbody>
              </table>
            </div>
          </div>
          }

          {HolderEduCourses && HolderEduCourses.length &&
          <div className={classnames(classes.detailsBlock, 'disciplines')}>
            <div className={classes.detailsBlockTitle}>
              {t('diploma.disciplines')}
            </div>
            <div className={classes.tableWrapper}>
              <table>
                <thead>
                <tr>
                  <th>{t('diploma.name')}</th>
                  <th>{t('diploma.zet')}</th>
                  <th>{t('diploma.hours')}</th>
                  <th>{t('grade')}</th>
                </tr>
                </thead>
                <tbody>
                {HolderEduCourses.map(c => {
                  return (
                    <tr key={c.name}>
                      <td>{c.name}</td>
                      <td>{c.credits}</td>
                      <td>{c.hours}</td>
                      <td>{c.mark}</td>
                    </tr>
                  );
                })}
                </tbody>
              </table>
            </div>
          </div>
          }

          {HolderEduThesis && HolderEduThesis.length &&
          <div className={classnames(classes.detailsBlock)}>
            <div className={classes.detailsBlockTitle}>
              {t('diploma.graduate_work')}
            </div>
            <div className={classes.tableWrapper}>
              <table>
                <thead>
                <tr>
                  <th>{t('diploma.name')}</th>
                  <th>{t('diploma.theme')}</th>
                  <th>{t('grade')}</th>
                </tr>
                </thead>
                <tbody>
                {HolderEduThesis.map(c => {
                  return (
                    <tr key={c.name}>
                      <td>{c.name}</td>
                      <td>{c.theme}</td>
                      <td>{c.mark}</td>
                    </tr>
                  );
                })}
                </tbody>
              </table>
            </div>
          </div>
          }

          {HolderEduExitExam && HolderEduExitExam.length &&
          <div className={classnames(classes.detailsBlock, 'state-exam')}>
            <div className={classes.detailsBlockTitle}>
              {t('diploma.state_exam')}
            </div>
            <div className={classes.tableWrapper}>
              <table>
                <thead>
                <tr>
                  <th>{t('diploma.name')}</th>
                  <th>{t('grade')}</th>
                </tr>
                </thead>
                <tbody>
                {HolderEduExitExam.map(c => {
                  return (
                    <tr key={c.name}>
                      <td>{c.name}</td>
                      <td>{c.mark}</td>
                    </tr>
                  );
                })}
                </tbody>
              </table>
            </div>
          </div>
          }

        </div>
        }
      </Fragment>
    );
  }
}

const mobile = '@media (max-width: 580px)';

const styles = createStyles({
  detailsContainer: {
    margin: '24px auto',
    backgroundColor: '#fff',
    color: '#202949',
    '@media (max-width: 575px)': {
      marginTop: '40px'
    },
    '& h3': {
      fontSize: '18px',
      margin: '0 0 5px'
    },
    '& .record': {
      padding: '0 24px',
      marginBottom: '24px',
      [mobile]: {
        padding: '0 0'
      }
    }
  },
  detailsTitle: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '0 24px',
    marginBottom: '10px',
    fontSize: '41px',
    lineHeight: '103%',
    fontWeight: 'bold',
    color: '#202949',
    '& img': {
      cursor: 'pointer'
    },
    [mobile]: {
      padding: '0 0',
      fontSize: '28px'
    }
  },
  detailsSubtitle: {
    padding: '0 24px',
    marginBottom: '50px',
    fontSize: '41px',
    fontWeight: 'bold',
    lineHeight: '103%',
    color: '#202949',
    opacity: 0.2,
    [mobile]: {
      padding: '0 0',
      fontSize: '28px'
    }
  },
  detailsBlock: {
    marginBottom: '20px',
    padding: '24px',
    background: 'rgba(244, 244, 246)',
    borderRadius: '6px',
    [mobile]: {
      padding: '10px'
    },
    '& table': {
      tableLayout: 'fixed',
      borderCollapse: 'collapse',
      [mobile]: {
        fontSize: '13px'
      }
    },
    '& table th': {
      padding: '5px',
      borderBottom: '1px solid rgba(32, 41, 73, 0.2);'
    },
    '& table tr th:first-child': {
      textAlign: 'left',
      paddingLeft: 0
    },
    '& th:last-child': {
      textAlign: 'right',
      paddingRight: 0
    },
    '& table td': {
      padding: '10px',
      verticalAlign: 'top',
      paddingRight: '20px',
      [mobile]: {
        padding: '5px'
      }
    },
    '& table td:first-child': {
      paddingLeft: 0
    },
    '& table td:last-child': {
      textAlign: 'right',
      paddingRight: 0
    },
    '&.practice, &.disciplines': {
      '& table th:nth-child(2)': {
        width: '100px'
      }
    },
    '&.state-exam': {
      '& table': {
        width: '100%'
      }
    }
  },
  detailsBlockTitle: {
    fontSize: '2em',
    fontWeight: 500,
    marginBottom: '20px',
    [mobile]: {
      fontSize: '1.5em'
    }
  },
  tableWrapper: {
    overflowX: 'auto'
  }
});

export default withTranslation('common')(
  withStyles(styles)(Certificate)
);
