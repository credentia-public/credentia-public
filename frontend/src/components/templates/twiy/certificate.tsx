// @ts-nocheck
import React, { Component, RefObject }      from 'react';
import { createStyles, WithStyles }         from '@material-ui/core';
import { withStyles }                       from '@material-ui/styles';
import { withTranslation, WithTranslation } from 'react-i18next';

import { QRCode } from 'react-qr-svg';


// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, WithStyles<typeof styles> {
  shareableLinkCode: string;
  url: string;
  name: string;
  surname: string;
  secondName: string;
  title: string;
  date: string;
  signature: string;
}

export class Certificate extends Component<Props, {}> {

  private rootRef: RefObject<HTMLDivElement>;

  constructor(props: any) {
    super(props);
    this.rootRef = React.createRef<HTMLDivElement>();
    this.state = {
      width: null
    };
  }

  componentDidMount() {
    this.resizeWatcher();
    window.addEventListener('resize', () => this.resizeWatcher());
  }

  componentWillUnmount() {
    window.removeEventListener('resize', () => this.resizeWatcher());
  }

  resizeWatcher = () => {
    this.setState(() => ({ width: this.rootRef.current ? this.rootRef.current.offsetWidth : 300 }));
  };

  render() {
    const {
      t, classes,
      shareableLinkCode, url,
      name, surname, secondName,
      title,
      date,
      signature,
    } = this.props;

    //console.log('url = ', url)
    //console.log('shareableLinkCode = ', shareableLinkCode)

    const fieldsToShow = {
      fullName: [name, surname].filter(_ => _).join(' '),
      title: `«${title}»`,
      date: date ? (new Date(date)).toLocaleString('ru', {
        year: 'numeric',
        month: 'long',
        day: 'numeric'
      }).replace(' г.', '') : null,
      shareableLinkCode: `id: ${shareableLinkCode}`,
      QR: url,
      signature,
    };

    const { width } = this.state;

    const bgWidth = 1052;
    const bgHeight = 744;

    const debug = false

    const styles = {
      root: {
        overflow: 'hidden',
        margin: '0 auto',
        backgroundImage: debug ? 'url("/static/images/twiy/reference.jpg")' : null,
        backgroundRepeat: 'no-repeat',
        backgroundSize: '100% auto'
      },
      underlay: {
        position: 'relative',
        paddingBottom: bgHeight / bgWidth * 100 + '%',
        backgroundColor: 'white',
        backgroundImage: 'url("/static/images/twiy/certificate.png")',
        backgroundSize: '100%',
        opacity: debug ? 0.4 : 1
      },
      content: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        border: '1px solid black',
        color: 'black'
      },
      field: {
        position: 'absolute',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        color: '#333'
      },
      fullName: {
        left: '7%',
        top: '42.7%',
        width: '66%',
        fontSize: width * 0.06,
        fontWeight: 'bold',
        lineHeight: '1.1em',
        letterSpacing: '0.0666em',
        // 1 or 2 rows
        transform: 'translateY(-50%)',
        overflow: 'unset',
        textOverflow: 'unset',
        whiteSpace: 'normal',
        wordSpacing: '9999999px',
        //fontFamily: 'CooperHewitt'
      },
      title: {
        left: '7%',
        top: '61%',
        width: '64%',
        fontSize: width * 0.032,
        fontWeight: 'bold',
        textTransform: 'uppercase'
      },
      date: {
        left: '22.3%',
        top: '84.5%',
        width: '19%',
        fontSize: width * 0.0145
      },
      shareableLinkCode: {
        left: '8.9%',
        top: '92%',
        width: '24%',
        fontSize: width * 0.01
      },
      QR: {
        left: '8.9%',
        top: '73.1%',
        width: width * 0.117,
      },
      signature: {
        left: '59%',
        top: '74.6%',
        width: '19%',
        fontSize: width * 0.02
      },
    };

    return (
      <div ref={this.rootRef} style={styles.root}>
        <div style={styles.underlay}>
          <div style={styles.content}>
            {Object.entries(fieldsToShow).map(([fieldName, fieldValue]) =>
              <div
                style={{ ...styles.field, ...styles[fieldName] }}
                key={fieldName}
              >
                {fieldName === 'QR' ?
                  <QRCode
                    bgColor="#FFFFFF"
                    fgColor="#000000"
                    level="Q"
                    //style={{ width: width * 0.1 }}
                    value={fieldValue}
                  />
                  :
                  fieldValue
                }
              </div>
            )}
          </div>
        </div>

        {this.props.children}

      </div>
    );
  }
}

export default withTranslation('common')(
  (Certificate)
);
