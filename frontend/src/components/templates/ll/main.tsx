// @ts-nocheck
import React, { Component }                 from 'react';
import { createStyles, WithStyles }         from '@material-ui/core';
import { withStyles }                       from '@material-ui/styles';
import { withTranslation, WithTranslation } from 'react-i18next';

// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, WithStyles<typeof styles> {
  imageLogoURL: string;
  title: string;
  name: string;
  surname: string;
  secondName: string;
  issuerName: string;
  qualification: string;
  specialization: string;
  city: string;
  date: string;
  documentNumber: string;
  issuerPersonATitle: string;
  issuerPersonAName: string;
  issuerPersonBTitle: string;
  issuerPersonBName: string;
  issuerPersonCTitle: string;
  issuerPersonCName: string;
  documentRegNumber: string;
}

export class Certificate extends Component<Props, {}> {
  render() {
    const {
      t, classes, imageLogoURL, issuerName, city, date, documentNumber, title, name, surname, secondName,
      qualification, specialization, issuerPersonATitle, issuerPersonAName, issuerPersonBTitle, issuerPersonBName,
      issuerPersonCTitle, issuerPersonCName, documentRegNumber
    } = this.props;

    return (
      <div className={classes.titleContainer}>
        <div className={classes.issuer}>
          <div>
            <img src='https://lingualeo.com/neo-static/logo-desktop.a5807373af1c5062718940d8c9c7e398.svg'/>
          </div>
          <div className={classes.issuerNameContainer}>
            <div className={classes.issuerName}>Выпущен Лингвалео</div>
            <div className={classes.documentAddInfo}>
              <div>{city}</div>
              <div>{date}</div>
              <div>{documentNumber}</div>
            </div>
          </div>
        </div>
        <div className={classes.title}>
          СЕРТИФИКАТ. Выдан {name} {surname}. Данный документ подтверждает, что успешно прошел тестирование на уровень
          знание английскоко "{qualification}" в системе Lingualeo
        </div>
        <div className={classes.signatureBlock}>
          <div>
            чтение
          </div>
          <div className={classes.signature}>
            <div>90%</div>
          </div>
        </div>
        <div className={classes.signatureBlock}>
          <div>лексика</div>
          <div className={classes.signature}>
            <div>90%</div>
          </div>
        </div>
        <div className={classes.signatureBlock}>
          <div>аудирование</div>
          <div className={classes.signature}>
            <div>90%</div>
          </div>
        </div>
        <div className={classes.signatureBlock}>
          <div>грамматика</div>
          <div className={classes.signature}>
            <div>90%</div>
          </div>
        </div>
        <div className={classes.signatureBlock}>
          <div>письмо</div>
          <div className={classes.signature}>
            <div>90%</div>
          </div>
        </div>

        <div className={classes.ids}>
          <div>{t('diploma.reg_number')} - {documentRegNumber}</div>
        </div>
      </div>
    );
  }
}

const styles = createStyles({
  titleContainer: {
    display: 'flex',
    flexDirection: 'column',
    height: '650px', // TODO remove after implementing cert for testing
    padding: '24px',
    margin: '0 auto',
    borderRadius: '8px',
    textAlign: 'center',
    fontSize: '10px',
    backgroundColor: '#414042',
    color: '#fff'
  },
  issuer: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '80px'
  },
  issuerNameContainer: {
    width: '200px',
    lineHeight: '150%',
    fontFamily: '"IBM Plex Sans", sans-serif',
    fontSize: '1em'
  },
  issuerName: {
    marginBottom: '15px',
    letterSpacing: '0.055em',
    textTransform: 'uppercase',
    textAlign: 'left'
  },
  documentAddInfo: {
    display: 'flex',
    justifyContent: 'space-between',
    color: 'rgba(255, 255, 255, 0.5)'
  },
  title: {
    flex: 1,
    fontSize: '3em',
    lineHeight: '130%'
  },
  signatureBlock: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: '20px'
  },
  signature: {
    width: '90px',
    textAlign: 'right',
    '& img': {
      marginBottom: '10px'
    }
  },
  ids: {
    textAlign: 'center',
    color: 'rgba(255, 255, 255, 0.5)'
  }
});

export default withTranslation('common')(
  withStyles(styles)(Certificate)
);
