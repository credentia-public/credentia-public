// @ts-nocheck
import React, { Component, RefObject }      from 'react';
import { WithStyles }                       from '@material-ui/core';
import { withTranslation, WithTranslation } from 'react-i18next';

// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, WithStyles<typeof styles> {
  title: string;
  name: string;
  surname: string;
  date: string;
  id: string;
  sign: string;
}

export class Certificate extends Component<Props, {}> {

  private rootRef: RefObject<HTMLDivElement>;

  constructor(props: any) {
    super(props);
    this.rootRef = React.createRef<HTMLDivElement>();
    this.state = {
      width: null
    };
  }

  componentDidMount() {
    this.resizeWatcher();
    window.addEventListener('resize', () => this.resizeWatcher());
  }

  componentWillUnmount() {
    window.removeEventListener('resize', () => this.resizeWatcher());
  }

  resizeWatcher = () => {
    this.setState(() => ({ width: this.rootRef.current ? this.rootRef.current.offsetWidth : 300 }));
  };

  render() {
    const {
      t, classes, date, title, name, surname, secondName, sign
    } = this.props;

    const dateFormatted = date ? (new Date(date)).toLocaleString('ru', {
      year: 'numeric',
      month: '2-digit',
      day: 'numeric'
    }) : null;

    const fullName = [surname, name, secondName].filter(part => part).join(' ');

    const { width } = this.state;

    const isReferenceEnabled = false;

    const styles = {
      root: {
        padding: width > 700 ? '30px' : '30px 0px',
        margin: '0 auto',
        borderRadius: '8px',
        textAlign: 'center',
        backgroundColor: '#ffffff',
        color: 'black',
        backgroundImage: isReferenceEnabled ? 'url(/static/images/ithub/cert-reference.png)' : '',
        backgroundRepeat: isReferenceEnabled ? 'no-repeat' : '',
        backgroundSize: isReferenceEnabled ? '103%' : '',
        backgroundPosition: isReferenceEnabled ? '33% -2.1%' : ''
      },
      content: {
        top: `${width / 20}px`,
        opacity: isReferenceEnabled ? 0.5 : ''
      },
      issuer: {
        display: 'flex',
        justifyContent: 'space-between',
        marginBottom: '80px'
      },
      inside: {
        border: `${width / 160}px solid black`,
        padding: `${width / 16.5}px ${width / 7.5}px ${width / 22}px`,
        position: 'relative'
      },
      logo: {
        position: 'absolute',
        top: `-${width / 48}px`,
        right: `-${width / 60}px`,
        width: '100%',
        textAlign: 'right'
      },
      title: {
        margin: `0 -${width / 12}px ${width / 19}px`,
        textTransform: 'uppercase',
        fontWeight: '900',
        fontSize: `${width / 12}px`,
        letterSpacing: '0.04em'
      },
      subtitle1: {
        fontSize: `${width / 45}px`
      },
      name: {
        margin: `${width / 100}px -${width / 12}px`,
        fontSize: `${width / 19}px`,
        fontWeight: 'bold'
      },
      subtitle2: {
        fontSize: `${width / 45}px`
      },
      titleName: {
        margin: `${width / 22}px`,
        marginBottom: '5em',
        fontSize: `${width / 35}px`,
        fontWeight: '900'
      },
      timing: {
        fontSize: `${width / 45}px`
      },
      certBottom: {
        display: 'flex',
        justifyContent: 'space-between',
        marginTop: `${width / 17}px`,
        fontSize: `${width / 65}px`,
        lineHeight: '1.3em',
        textAlign: 'left'
      },
      date: {
        marginTop: `${width / 100}px`
      }
    };

    return (
      <div ref={this.rootRef} style={styles.root}>
        <div style={styles.inside}>
          <div style={styles.logo}>
            <img src='/static/images/ithub_logo.png' width={`${width / 3.2}`}/>
          </div>
          <div style={styles.content}>
            <div style={styles.title}>
              {t('master.ithub_cert')}
            </div>
            <div style={styles.subtitle1}>
              {t('master.ithub_cert_certifies')}:
            </div>
            <div style={styles.name}>
              {fullName}
            </div>
            <div style={styles.subtitle2}>
              {t('master.ithub_cert_certifies_programm')}:
            </div>
            <div style={styles.titleName}>
              «{title}»
            </div>
            <div style={styles.timing}>
              {t('doc_page.ithub.timing', {
                number_of_hours: '16',
                date_start: '13.11.2019',
                date_finish: '14.11.2019'
              })}
            </div>
            <div style={styles.certBottom}>
              <div>
                {t('master.director')}:
                <br/>
                Сумбатян М.С.
              </div>
              {dateFormatted &&
              <div style={styles.date}>{t('doc_page.ithub.date')}: {dateFormatted}</div>
              }
            </div>
          </div>
        </div>
        {this.props.children}
      </div>
    );
  }
}

export default withTranslation('common')(
  (Certificate)
);
