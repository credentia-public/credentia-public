import React, { Component, Fragment }       from 'react';
import { createStyles, WithStyles }         from '@material-ui/core';
import { withStyles }                       from '@material-ui/styles';
import { withTranslation, WithTranslation } from 'react-i18next';

// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, WithStyles<typeof styles> {
  imageLogoURL: string;
  title: string;
  name: string;
  surname: string;
  secondName: string;
  issuerName: string;
  qualification: string;
  specialization: string;
  city: string;
  date: string;
  documentNumber: string;
  issuerPersonATitle: string;
  issuerPersonAName: string;
  issuerPersonBTitle: string;
  issuerPersonBName: string;
  issuerPersonCTitle: string;
  issuerPersonCName: string;
  documentRegNumber: string;
}

export class Certificate extends Component<Props, {}> {
  render() {
    const {
      t, classes, imageLogoURL, issuerName, city, date, documentNumber, title, name, surname, secondName,
      qualification, specialization, issuerPersonATitle, issuerPersonAName, issuerPersonBTitle, issuerPersonBName,
      issuerPersonCTitle, issuerPersonCName, documentRegNumber
    } = this.props;

    return (
      <Fragment>
        <div className={classes.titleContainer}>
          <div className={classes.issuer}>
            <div>
              <img src='https://dashboard.credentia.ru/static/images/asi_logo_short.png'/>
            </div>
            <div className={classes.issuerNameContainer}>
              <div className={classes.issuerName}>{issuerName}</div>
              <div className={classes.documentAddInfo}>
                <div>Москва</div>
                <div>2019</div>
                <div>{documentNumber}</div>
              </div>
            </div>
          </div>
          <div className={classes.title}>
            <span>Персональный цифровой сертификат</span>
            <br/><br/>
            <span>Владелец: </span><span> {name} {surname}</span>
            <br/><br/>
            <span>{title}</span>
          </div>

          <div className={classes.ids}>
            <div>Регистрационный номер - {documentRegNumber}</div>
          </div>
        </div>
        {this.props.children}
      </Fragment>
    );
  }
}

const styles = createStyles({
  titleContainer: {
    display: 'flex',
    flexDirection: 'column',
    height: '650px', // TODO remove after implementing cert for testing
    padding: '24px',
    margin: '0 auto',
    borderRadius: '8px',
    textAlign: 'center',
    fontSize: '10px',
    backgroundColor: '#3391d2',
    color: '#fff'
  },
  issuer: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '80px'
  },
  issuerNameContainer: {
    width: '200px',
    lineHeight: '150%',
    fontFamily: '"IBM Plex Sans", sans-serif',
    fontSize: '1em'
  },
  issuerName: {
    marginBottom: '15px',
    letterSpacing: '0.055em',
    textTransform: 'uppercase',
    textAlign: 'left'
  },
  documentAddInfo: {
    display: 'flex',
    justifyContent: 'space-between',
    color: 'rgba(255, 255, 255, 0.5)'
  },
  title: {
    flex: 1,
    fontSize: '3em',
    lineHeight: '130%'
  },
  signatureBlock: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: '20px'
  },
  signature: {
    width: '90px',
    textAlign: 'left',
    '& img': {
      marginBottom: '10px'
    }
  },
  ids: {
    textAlign: 'center',
    color: 'rgba(255, 255, 255, 0.5)'
  }
});

export default withTranslation('common')(
  withStyles(styles)(Certificate)
);
