import React, { Component, Fragment } from 'react';
import { createStyles, WithStyles }   from '@material-ui/core';
import { withStyles }                 from '@material-ui/styles';
import { withTranslation, WithTranslation } from 'react-i18next';

// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, WithStyles<typeof styles> {
  logoPath: string;
  title: string;
  email: string;
}

export class Certificate extends Component<Props, {}> {
  render() {
    const { t, classes, title, email } = this.props;

    return (
      <Fragment>
        <div className={classes.titleContainer}>
          <div className={classes.issuer}>
            <div>
              <img src='/static/images/niissu/military_move.png'/>
            </div>
          </div>
          <div className={classes.title}>
            <span>{title}</span>
          </div>
          <div className={classes.id}>
            {email && email.replace('_iot@credentia.me', '')}
          </div>
        </div>
      </Fragment>
    );
  }
}

const styles = createStyles({
  titleContainer: {
    display: 'flex',
    flexDirection: 'column',
    padding: '24px',
    margin: '0 auto',
    borderRadius: '8px',
    textAlign: 'center',
    fontSize: '10px'
  },
  issuer: {
    display: 'flex',
    justifyContent: 'center',
    '& img': {
      width: '50%'
    }
  },
  title: {
    flex: 1,
    fontSize: '3em',
    lineHeight: '130%',
    marginBottom: '10px'
  },
  id: {
    textAlign: 'center',
    fontSize: '16px'
  }
});

export default withTranslation('common')(
  withStyles(styles)(Certificate)
);
