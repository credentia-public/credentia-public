// @ts-nocheck
import React, { Component, RefObject }      from 'react';
import { createStyles, WithStyles }         from '@material-ui/core';
import { withStyles }                       from '@material-ui/styles';
import { withTranslation, WithTranslation } from 'react-i18next';

// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, WithStyles<typeof styles> {
  name: string;
  surname: string;
  secondName: string;
  id: string;
  title: string;
  date: string;
  position: string;
  signature: string;
  disciplines: string;
}

export class Certificate extends Component<Props, {}> {

  private rootRef: RefObject<HTMLDivElement>;

  constructor(props: any) {
    super(props);
    this.rootRef = React.createRef<HTMLDivElement>();
    this.state = {
      width: null
    };
  }

  componentDidMount() {
    this.resizeWatcher();
    window.addEventListener('resize', () => this.resizeWatcher());
  }

  componentWillUnmount() {
    window.removeEventListener('resize', () => this.resizeWatcher());
  }

  resizeWatcher = () => {
    this.setState(() => ({ width: this.rootRef.current ? this.rootRef.current.offsetWidth : 300 }));
  };

  render() {
    const {
      t, classes,
      name, surname, secondName,
      title,
      date,
      position,
      signature,
      disciplines
    } = this.props;

    const fieldsToShow = {
      fullName: [surname, name, secondName].filter(_ => _).join(' '),
      date,
      position,
      signature
    };

    const { width } = this.state;

    const bgWidth = 1052;
    const bgHeight = 744;

    const styles = {
      root: {
        overflow: 'hidden',
        margin: '0 auto'
      },
      underlay: {
        position: 'relative',
        paddingBottom: bgHeight / bgWidth * 100 + '%',
        backgroundColor: 'white',
        backgroundImage: 'url("/static/images/sibur/certificate.svg")',
        backgroundSize: '100%'
      },
      content: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        color: 'black'
      },
      field: {
        position: 'absolute',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        color: '#4f4c4c'
      },
      fullName: {
        left: '40%',
        top: '47%',
        width: '46%',
        fontSize: width * 0.025,
        fontStyle: 'italic'
      },
      date: {
        left: '50%',
        top: '63.4%',
        width: '19%',
        fontSize: width * 0.02
      },
      position: {
        left: '50%',
        top: '67.2%',
        width: '35%',
        height: '3.2em',
        lineHeight: '1.6em',
        fontSize: width * 0.016,
        whiteSpace: 'normal'
      },
      signature: {
        left: '59%',
        top: '74.6%',
        width: '19%',
        fontSize: width * 0.02
      },
      disciplines: {
        overflowX: 'auto',
        paddingRight: '1px'
      }
    };

    return (
      <div ref={this.rootRef} style={styles.root}>

        <div style={styles.underlay}>
          <div style={styles.content}>
            {Object.entries(fieldsToShow).map(([fieldName, fieldValue]) =>
              <div
                style={{ ...styles.field, ...styles[fieldName] }}
                key={fieldName}
              >{fieldValue}</div>
            )}
          </div>
        </div>

        {disciplines &&
        <div style={styles.disciplines} dangerouslySetInnerHTML={{ __html: disciplines }}>
        </div>
        }

        {this.props.children}

      </div>
    );
  }
}

export default withTranslation('common')(
  (Certificate)
);
