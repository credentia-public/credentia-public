// @ts-nocheck
import React, { Component, Fragment, RefObject } from 'react';
import { createStyles, WithStyles }              from '@material-ui/core';
import { withStyles }                            from '@material-ui/styles';
import classnames                                from 'classnames';
import { withTranslation, WithTranslation }      from 'react-i18next';

// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, WithStyles<typeof styles> {
  imageLogoURL: string;
  title: string;
  name: string;
  surname: string;
  secondName: string;
  issuerName: string;
  qualification: string;
  specialization: string;
  city: string;
  date: string;
  documentNumber: string;
  issuerPersonATitle: string;
  issuerPersonAName: string;
  issuerPersonBTitle: string;
  issuerPersonBName: string;
  issuerPersonCTitle: string;
  issuerPersonCName: string;
  IssuerStamp: string;
  documentRegNumber: string;
  DocumentSpecialty: string;
  DocumentRegDate: string;
  HolderLastName2: string;
  HolderFirstName2: string;
  HolderSecondaryName2: string;
  holderBirthday: string;
  HolderEduPrevious: string;
  HolderEduEntryExam: string;
  HolderEduStart: string;
  HolderEduEnd: string;
  HolderEduYears: string;
  HolderEduCoursework: Array<{ name: string; theme: string; mark: string; }>;
  HolderEduPractice: Array<{ name: string; credits: string; hours: string; mark: string; }>;
  HolderEduExitExam: Array<{ name: string; mark: string; }>;
  HolderEduThesis: Array<{ name: string; theme: string; mark: string; }>;
  HolderEduCourses: Array<{ name: string; credits: string; hours: string; mark: string; }>;
}

export class Certificate extends Component<Props, {}> {

  private rootRef: RefObject<HTMLDivElement>;

  constructor(props: any) {
    super(props);
    this.rootRef = React.createRef<HTMLDivElement>();
    this.state = {
      width: null
    };
  }

  componentDidMount() {
    this.resizeWatcher();
    window.addEventListener('resize', () => this.resizeWatcher());
  }

  componentWillUnmount() {
    window.removeEventListener('resize', () => this.resizeWatcher());
  }

  resizeWatcher = () => {
    this.setState(() => ({ width: this.rootRef.current ? this.rootRef.current.offsetWidth : 300 }));
  };

  render() {
    const {
      t, classes, imageLogoURL, issuerName, city, date, documentNumber, title, name, surname, secondName,
      qualification, specialization, issuerPersonATitle, issuerPersonAName, issuerPersonBTitle, issuerPersonBName,
      issuerPersonCTitle, issuerPersonCName, documentRegNumber, HolderEduCoursework, HolderEduPractice,
      HolderEduExitExam, HolderEduThesis, HolderEduCourses, holderBirthday, HolderEduPrevious, DocumentSpecialty,
      HolderEduYears, HolderEduStart, HolderEduEnd, HolderEduEntryExam
    } = this.props;

    const { width } = this.state;

    // tslint:disable-next-line:no-shadowed-variable
    const styles = {
      titleContainer: {
        height: width * 1058 / 750,
        padding: width * 0.11,
        margin: '0 auto',
        borderRadius: '8px',
        border: '1px solid #f9fcf1',
        textAlign: 'center',
        backgroundColor: '#f9fcf1',
        backgroundImage: 'url("/static/images/mgu/diploma.jpg")',
        backgroundRepeat: 'no-repeat',
        backgroundSize: '100%',
        color: '#6d9280'
      },
      holderName: {
        marginTop: width * 0.75,
        marginBottom: width * 0.02,
        fontSize: width * 0.04,
        fontWeight: 'bold'
      },
      title: {
        fontSize: width * 0.03
      }
    };

    return (
      <Fragment>
        <div ref={this.rootRef} className={classes.titleContainer} style={styles.titleContainer}>
          {/*<div className={classes.issuer}>
            <div>
              <img src={imageLogoURL}/>
            </div>
            <div className={classes.issuerNameContainer}>
              <div className={classes.issuerName}>{issuerName}</div>
              <div className={classes.documentAddInfo}>
                <div>{city}</div>
                <div>{date}</div>
                <div>{documentNumber}</div>
              </div>
            </div>
          </div>*/}
          <div className={classes.holderName} style={styles.holderName}>
            {surname} {name} {secondName}
          </div>
          <div className={classes.title} style={styles.title}>
            освоил(а) программу {qualification && qualification.toLowerCase()} по направлению подготовки
            <div className={classes.specialization}>{specialization}</div>
          </div>
          <div className={classes.signatureBlock}>
            <div>
              {issuerPersonATitle}
            </div>
            <div className={classes.signature}>
              <div>{issuerPersonAName}</div>
            </div>
          </div>
          <div className={classes.signatureBlock}>
            <div>
              {issuerPersonBTitle}
            </div>
            <div className={classes.signature}>
              <div>{issuerPersonBName}</div>
            </div>
          </div>
          <div className={classes.signatureBlock}>
            <div>
              {issuerPersonCTitle}
            </div>
            <div className={classes.signature}>
              <div>{issuerPersonCName}</div>
            </div>
          </div>
          <div className={classes.ids}>
            <div>{t('diploma.reg_number')} - {documentRegNumber}</div>
          </div>
        </div>
        {this.props.children}
        <div className={classes.detailsContainer}>
          <div className={classes.detailsTitle}>
            {specialization}
          </div>
          <div className={classes.detailsSubtitle}>
            {qualification}
          </div>
          <div className='record'>
            <h3>{t('master.name')}</h3>
            <div>{surname} {name} {secondName}</div>
          </div>
          <div className='record'>
            <h3>{t('birth_date')}</h3>
            <div>{holderBirthday}</div>
          </div>
          <div className='record'>
            <h3>{t('diploma.holder_edu_previous')}</h3>
            <div>{HolderEduPrevious}</div>
          </div>
          {HolderEduEntryExam && (
            <div className='record'>
              <h3>{t('diploma.entry_exam')}</h3>
              <div>{HolderEduEntryExam}</div>
            </div>
          )}
          <div className='record'>
            <h3>{t('diploma.qualification')}</h3>
            <div>{qualification && qualification.toLowerCase()}</div>
          </div>
          <div className='record'>
            <h3>{t('diploma.specialization')}</h3>
            <div>{DocumentSpecialty}</div>
          </div>
          <div className='record'>
            <h3>{t('diploma.start_date')}</h3>
            <div>{HolderEduStart}</div>
          </div>
          <div className='record'>
            <h3>{t('diploma.end_date')}</h3>
            <div>{HolderEduEnd}</div>
          </div>
          <div className='record'>
            <h3>{t('diploma.education_period')}</h3>
            <div>{HolderEduYears}</div>
          </div>

          {HolderEduCoursework && HolderEduCoursework.length !== 0 ? (<div className={classes.detailsBlock}>
            <div className={classes.detailsBlockTitle}>
              {t('diploma.course_works')}
            </div>
            <table>
              <thead>
              <tr>
                <th>{t('diploma.name')}</th>
                <th>{t('diploma.zet')}</th>
                <th>{t('diploma.hours')}</th>
                <th>{t('grade')}</th>
              </tr>
              </thead>
              <tbody>
              {HolderEduCoursework.map(c => {
                return (
                  <tr key={c.name}>
                    <td>{c.name}</td>
                    <td>{c.credits}</td>
                    <td>{c.hours}</td>
                    <td>{c.mark}</td>
                  </tr>
                );
              })}
              </tbody>
            </table>
          </div>) : (<div></div>)}
          {HolderEduPractice && HolderEduPractice.length !== 0 ? (
            <div className={classnames(classes.detailsBlock, 'practice')}>
              <div className={classes.detailsBlockTitle}>
                {t('diploma.practise')}
              </div>
              <table>
                <thead>
                <tr>
                  <th>{t('diploma.name')}</th>
                  <th>{t('diploma.zet')}</th>
                  <th>{t('diploma.hours')}</th>
                  <th>{t('grade')}</th>
                </tr>
                </thead>
                <tbody>
                {HolderEduPractice.map(c => {
                  return (
                    <tr key={c.name}>
                      <td>{c.name}</td>
                      <td>{c.credits}</td>
                      <td>{c.hours}</td>
                      <td>{c.mark}</td>
                    </tr>
                  );
                })}
                </tbody>
              </table>
            </div>) : (<div></div>)}
          {HolderEduCourses && Object.keys(HolderEduCourses).length !== 0 ? (
            <div className={classnames(classes.detailsBlock, 'disciplines')}>
              <div className={classes.detailsBlockTitle}>
                {t('diploma.disciplines')}
              </div>
              <table>
                <thead>
                <tr>
                  <th>{t('diploma.name')}</th>
                  <th>{t('diploma.zet')}</th>
                  <th>{t('diploma.hours')}</th>
                  <th>{t('grade')}</th>
                </tr>
                </thead>
                <tbody>
                {Object.entries(HolderEduCourses).map(([key, value]) => {
                  const moduleTitle = (
                    <tr>
                      <td className={classes.moduleTitle} colSpan={4}>{key}</td>
                    </tr>
                  );
                  const courses = value.map(v => {
                    return (
                      <tr key={v.name}>
                        <td>{v.name}</td>
                        <td>{v.credits}</td>
                        <td>{v.hours}</td>
                        <td>{v.mark}</td>
                      </tr>
                    );
                  });

                  courses.unshift(moduleTitle);
                  return courses;
                })}
                </tbody>
              </table>
            </div>) : (<div></div>)}
          {HolderEduThesis && HolderEduThesis.length !== 0 ? (
            <div className={classnames(classes.detailsBlock)}>
              <div className={classes.detailsBlockTitle}>
                {t('diploma.graduate_work')}
              </div>
              <table>
                <thead>
                <tr>
                  <th>{t('diploma.name')}</th>
                  <th>{t('diploma.zet')}</th>
                  <th>{t('diploma.hours')}</th>
                  <th>{t('grade')}</th>
                </tr>
                </thead>
                <tbody>
                {HolderEduThesis.map(c => {
                  return (
                    <tr key={c.name}>
                      <td>{c.name}</td>
                      <td>{c.credits}</td>
                      <td>{c.hours}</td>
                      <td>{c.mark}</td>
                    </tr>
                  );
                })}
                </tbody>
              </table>
            </div>) : (<div></div>)}
          {HolderEduExitExam && HolderEduExitExam.length !== 0 ? (
            <div className={classnames(classes.detailsBlock, 'state-exam')}>
              <div className={classes.detailsBlockTitle}>
                {t('diploma.state_exam')}
              </div>
              <table>
                <thead>
                <tr>
                  <th>{t('diploma.name')}</th>
                  <th>{t('diploma.zet')}</th>
                  <th>{t('diploma.hours')}</th>
                  <th>{t('grade')}</th>
                </tr>
                </thead>
                <tbody>
                {HolderEduExitExam.map(c => {
                  return (
                    <tr key={c.name}>
                      <td>{c.name}</td>
                      <td>{c.credits}</td>
                      <td>{c.hours}</td>
                      <td>{c.mark}</td>
                    </tr>
                  );
                })}
                </tbody>
              </table>
            </div>) : (<div></div>)}
        </div>
      </Fragment>
    );
  }
}

const styles = createStyles({
  issuer: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '50px'
  },
  issuerNameContainer: {
    width: '200px',
    lineHeight: '150%',
    fontFamily: '"IBM Plex Sans", sans-serif',
    fontSize: '1em'
  },
  issuerName: {
    marginBottom: '15px',
    letterSpacing: '0.055em',
    textTransform: 'uppercase',
    textAlign: 'left'
  },
  documentAddInfo: {
    display: 'flex',
    justifyContent: 'space-between',
    color: 'rgba(255, 255, 255, 0.5)'
  },
  specialization: {
    paddingTop: '20px',
    fontWeight: 500
  },
  signatureBlock: {
    display: 'none',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: '20px',
    visibility: 'hidden'
  },
  signature: {
    width: '90px',
    textAlign: 'left',
    '& img': {
      marginBottom: '10px'
    }
  },
  ids: {
    display: 'none',
    textAlign: 'center',
    color: 'rgba(255, 255, 255, 0.5)'
  },
  detailsContainer: {
    margin: '24px auto',
    backgroundColor: '#fff',
    color: '#202949',
    '@media (max-width: 575px)': {
      marginTop: '40px'
    },
    '& h3': {
      fontSize: '18px',
      margin: '0 0 5px'
    },
    '& .record': {
      padding: '0 24px',
      marginBottom: '24px'
    }
  },
  detailsTitle: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '0 24px',
    marginBottom: '10px',
    fontSize: '41px',
    lineHeight: '103%',
    fontWeight: 'bold',
    color: '#202949',
    '& img': {
      cursor: 'pointer'
    }
  },
  detailsSubtitle: {
    padding: '0 24px',
    marginBottom: '50px',
    fontSize: '41px',
    fontWeight: 'bold',
    lineHeight: '103%',
    color: '#202949',
    opacity: 0.2
  },
  detailsBlock: {
    marginBottom: '20px',
    padding: '24px',
    background: 'rgba(244, 244, 246)',
    borderRadius: '6px',
    '& table': {
      width: '100%',
      borderCollapse: 'collapse'
    },
    '& table th': {
      padding: '5px',
      borderBottom: '1px solid rgba(32, 41, 73, 0.2);'
    },
    '& table tr th:first-child': {
      textAlign: 'left',
      paddingLeft: 0
    },
    '& th:last-child': {
      textAlign: 'right',
      paddingRight: 0
    },
    '& table td': {
      padding: '10px',
      verticalAlign: 'top',
      paddingRight: '20px',
      textAlign: 'center'
    },
    '& table td:first-child': {
      textAlign: 'left',
      paddingLeft: '20px'
    },
    '& table td:last-child': {
      textAlign: 'right',
      paddingRight: 0
    },
    '&.practice, &.disciplines': {
      '& table th:nth-child(2)': {
        width: '100px'
      }
    },
    '&.state-exam': {
      '& table': {
        width: '100%'
      }
    }
  },
  detailsBlockTitle: {
    fontSize: '2em',
    fontWeight: 500,
    marginBottom: '20px'
  },
  moduleTitle: {
    fontWeight: 'bold',
    textAlign: 'left !important',
    paddingLeft: '0 !important'
  }
});

export default withTranslation('common')(
  withStyles(styles)(Certificate)
);
