// @ts-nocheck
import React, { Component, RefObject }      from 'react';
import { createStyles, WithStyles }         from '@material-ui/core';
import { withStyles }                       from '@material-ui/styles';
import { withTranslation, WithTranslation } from 'react-i18next';
import { QRCode } from 'react-qr-svg';

// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, WithStyles<typeof styles> {
  shareableLinkCode: string;
  url: string;
  name: string;
  surname: string;
  secondName: string;
  id: string;
  title: string;
  date: string;
  position: string;
  signature: string;
}

export class Certificate extends Component<Props, {}> {

  private rootRef: RefObject<HTMLDivElement>;

  constructor(props: any) {
    super(props);
    this.rootRef = React.createRef<HTMLDivElement>();
    this.state = {
      width: null
    };
  }

  componentDidMount() {
    this.resizeWatcher();
    window.addEventListener('resize', () => this.resizeWatcher());
  }

  componentWillUnmount() {
    window.removeEventListener('resize', () => this.resizeWatcher());
  }

  resizeWatcher = () => {
    this.setState(() => ({ width: this.rootRef.current ? this.rootRef.current.offsetWidth : 300 }));
  };

  render() {
    const {
      t, classes,
      shareableLinkCode, url,
      name, surname, secondName,
      title,
      date,
      rtDemo1,
      rtDemo2,
      rtDemo3,
    } = this.props;

    const fieldsToShow = {
      //fullName: [surname, name, secondName].filter(_ => _).join(' '),
      date: date ? (new Date(date)).toLocaleString('ru', {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric'
      }).replace(' г.', '') : null,
      title,
      rtDemo1,
      rtDemo2,
      rtDemo3,
      shareableLinkCode: `id: ${shareableLinkCode}`,
      QR: url,
    };

    const { width } = this.state;

    const bgWidth = 1169;
    const bgHeight = 827;

    const styles = {
      root: {
        //overflow: 'hidden',
        margin: '0 auto'
      },
      underlay: {
        position: 'relative',
        paddingBottom: bgHeight / bgWidth * 100 + '%',
        backgroundColor: 'white',
        backgroundImage: 'url("/static/images/rttec/certificate.jpg")',
        backgroundSize: '100%'
      },
      content: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        color: 'black',
        border: '1px solid gray',
        backgroundColor: '#aaa1',
        boxShadow: '2px 2px 6px 2px #eee',
      },
      field: {
        position: 'absolute',
        //overflow: 'hidden',
        //textOverflow: 'ellipsis',
        //whiteSpace: 'nowrap',
        color: '#4f4c4c'
      },
      /*fullName: {
        left: '36%',
        top: '22%',
        width: '46%',
        fontSize: width * 0.025,
        fontStyle: 'italic'
      },*/
      field_title: {
        left: '39%',
        top: '4%',
        width: '35%',
        fontSize: width * 0.02,
        fontWeight: 'bold'
      },
      field_date: {
        left: '89.5%',
        top: '4%',
        width: '9%',
        fontSize: width * 0.01,
        fontWeight: 'bold'
      },
      field_rtDemo1: {
        left: '6%',
        top: '28.4%',
        width: '88%',
        fontSize: width * 0.017,
        fontStyle: 'italic',
        fontWeight: 'bold'
      },
      field_rtDemo2: {
        left: '6%',
        top: '39%',
        width: '88%',
        fontSize: width * 0.017,
        fontStyle: 'italic',
        fontWeight: 'bold'
      },
      field_rtDemo3: {
        left: '6%',
        top: '56.5%',
        width: '88%',
        fontSize: width * 0.017,
        fontStyle: 'italic',
        fontWeight: 'bold'
      },
      field_shareableLinkCode: {
        left: '71.9%',
        top: '1.3%',
        width: '24%',
        fontSize: width * 0.01,
        textAlign: 'right'
      },
      field_QR: {
        left: '83%',
        top: '8%',
        width: width * 0.13,
      },
    };

    return (
      <div ref={this.rootRef} style={styles.root}>

        <div style={styles.underlay}>
          <div style={styles.content}>
            {Object.entries(fieldsToShow).map(([fieldName, fieldValue]) =>
              <div
                style={{ ...styles.field, ...styles['field_' + fieldName] }}
                key={fieldName}
              >
                {fieldName === 'QR' ?
                  <QRCode
                    bgColor="#FFFFFF"
                    fgColor="#000000"
                    level="Q"
                    //style={{ width: width * 0.1 }}
                    value={fieldValue}
                  />
                  :
                  fieldValue
                }
              </div>
            )}
          </div>
        </div>

        {this.props.children}

      </div>
    );
  }
}

export default withTranslation('common')(
  (Certificate)
);
