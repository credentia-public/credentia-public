import React, { Component }                 from 'react';
import { createStyles, WithStyles }         from '@material-ui/core';
import { withStyles }                       from '@material-ui/styles';
import { withTranslation, WithTranslation } from 'react-i18next';

// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, WithStyles<typeof styles> {
  imageLogoURL: string;
  title: string;
  name: string;
  surname: string;
  secondName: string;
  issuerName: string;
  qualification: string;
  specialization: string;
  city: string;
  date: string;
  documentNumber: string;
  issuerPersonATitle: string;
  issuerPersonAName: string;
  issuerPersonBTitle: string;
  issuerPersonBName: string;
  issuerPersonCTitle: string;
  issuerPersonCName: string;
  documentRegNumber: string;
}

export class Certificate extends Component<Props, {}> {
  render() {
    const {
      t, classes, imageLogoURL, issuerName, city, date, documentNumber, title, name, surname, secondName,
      qualification, specialization, issuerPersonATitle, issuerPersonAName, issuerPersonBTitle, issuerPersonBName,
      issuerPersonCTitle, issuerPersonCName, documentRegNumber
    } = this.props;

    return (
      <div className={classes.titleContainer}>
        <div className={classes.issuer}>
          <div>
            <img src={imageLogoURL}/>
          </div>
          <div className={classes.issuerNameContainer}>
            <div className={classes.issuerName}>{issuerName}</div>
            <div className={classes.documentAddInfo}>
              <div>{city}</div>
              <div>{date}</div>
              <div>{documentNumber}</div>
            </div>
          </div>
        </div>
        <div className={classes.title}>
          {t('diploma.title', {
            type: title[0].toUpperCase() + title.toLowerCase().substr(1),
            name,
            surname,
            second_name: secondName,
            qualification,
            specialization
          })}
        </div>
        <div className={classes.signatureBlock}>
          <div>
            {issuerPersonATitle}
          </div>
          <div className={classes.signature}>
            <div>{issuerPersonAName}</div>
          </div>
        </div>
        <div className={classes.signatureBlock}>
          <div>{issuerPersonBTitle}</div>
          <div className={classes.signature}>
            <div>{issuerPersonBName}</div>
          </div>
        </div>
        <div className={classes.signatureBlock}>
          <div>{issuerPersonCTitle}</div>
          <div className={classes.signature}>
            <div>{issuerPersonCName}</div>
          </div>
        </div>
        <div className={classes.ids}>
          <div>{t('diploma.reg_number')} - {documentRegNumber}</div>
        </div>
      </div>
    );
  }
}

const styles = createStyles({
  titleContainer: {
    display: 'flex',
    flexDirection: 'column',
    height: '650px', // TODO remove after implementing cert for testing
    padding: '24px',
    margin: '0 auto',
    borderRadius: '8px',
    textAlign: 'center',
    fontSize: '10px',
    backgroundColor: '#226390',
    color: '#fff'
  },
  issuer: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '80px'
  },
  issuerNameContainer: {
    width: '200px',
    lineHeight: '150%',
    fontFamily: '"IBM Plex Sans", sans-serif',
    fontSize: '1em'
  },
  issuerName: {
    marginBottom: '15px',
    letterSpacing: '0.055em',
    textTransform: 'uppercase',
    textAlign: 'left'
  },
  documentAddInfo: {
    display: 'flex',
    justifyContent: 'space-between',
    color: 'rgba(255, 255, 255, 0.5)'
  },
  title: {
    flex: 1,
    fontSize: '3em',
    lineHeight: '130%'
  },
  signatureBlock: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: '20px'
  },
  signature: {
    width: '90px',
    textAlign: 'left',
    '& img': {
      marginBottom: '10px'
    }
  },
  ids: {
    textAlign: 'center',
    color: 'rgba(255, 255, 255, 0.5)'
  }
});

export default withTranslation('common')(
  withStyles(styles)(Certificate)
);
