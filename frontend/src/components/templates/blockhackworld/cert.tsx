import React, { Component, Fragment } from 'react';
import { createStyles, WithStyles }   from '@material-ui/core';
import { withStyles }                 from '@material-ui/styles';

// tslint:disable-next-line:interface-name
export interface Props extends WithStyles<typeof styles> {
  title: string;
  name: string;
  surname: string;
  secondName: string;
}

export class Certificate extends Component<Props, {}> {
  render() {
    const { title, surname, name, secondName, classes } = this.props;

    const fullName = [surname, name, secondName].filter(part => part).join(' ');

    return (
      <Fragment>

        <div className={classes.docContainer}>
          <div className={classes.header}>
            <img className={classes.logo} src='/static/images/blockhackworld/logo.png' />
          </div>
          <div className={classes.body}>
            <div className={classes.name}>{fullName}</div>
            <div className={classes.divider}/>
          </div>
          <div className={classes.text}>{title}</div>
          <aside className={classes.aside}>
            BlockHack World <span>'20</span>
          </aside>
          <div className={classes.footer}>
            <div className={classes.footerLeft}>
              <div className={classes.date}>February 29</div>
              <div className={classes.location}>San francisco</div>
            </div>
            <div className={classes.footerRight}>
              <div className={classes.bottomText}>DeFi and interoperability</div>
            </div>
          </div>
        </div>

        {this.props.children}

      </Fragment>
    );
  }
}

const mobile = '@media (max-width: 580px)';

const styles = createStyles({
  docContainer: {
    display: 'flex',
    flexDirection: 'column',
    margin: '0 auto',
    padding: '35px',
    backgroundColor: '#0c2230',
    color: '#fff',
    fontFamily: '"Work Sans", "Open Sans",-apple-system,BlinkMacSystemFont, "Roboto", sans-serif',
    position: 'relative',
    overflow: 'hidden',
    [mobile]: {
      padding: '20px'
    }
  },
  header: {
    margin: '20px 0',
    textAlign: 'center',
    [mobile]: {
      margin: '20px 0',
    }
  },
  logo: {
    width: '300px',
    [mobile]: {
      width: '200px'
    }
  },
  aside: {
    position: 'absolute',
    top: '92px',
    right: '-16px',
    fontWeight: 900,
    fontSize: '11px',
    transform: 'rotate(-90deg)',
    '& span': {
      color: '#dd8144'
    },
    [mobile]: {
      fontSize: '10px',
      top: '74px',
      right: '-22px'
    },
    '@media (max-width: 400px)': {
      display: 'none'
    }
  },
  body: {
    maxWidth: '420px',
    margin: '0 auto'
  },
  title: {
    fontSize: '38px',
    lineHeight: '36px',
    fontWeight: 'bold',
    marginBottom: '24px'
  },
  name: {
    color: '#cb225b',
    fontSize: '46px',
    lineHeight: '50px',
    marginBottom: '19px',
    textAlign: 'center',
    marginTop: '15px',
    [mobile]: {
      fontSize: '23px'
    }
  },
  text: {
    textAlign: 'center',
    fontSize: '28px',
    marginTop: '10px',
    marginBottom: '110px',
    [mobile]: {
      fontSize: '14px',
      marginBottom: '55px',
    }
  },
  divider: {
    borderBottom: '1px solid #262424',
    margin: '30px 0',
    [mobile]: {
      margin: '15px 0'
    }
  },
  info: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    paddingTop: '15px'
  },
  infoLeft: {
    fontSize: '13px',
    fontWeight: 600,
    width: '28%',
    marginBottom: '10px',
    [mobile]: {
      width: '100%'
    }
  },
  infoRight: {
    width: '65%',
    [mobile]: {
      width: '100%'
    }
  },
  footer: {
    display: 'flex',
    fontSize: '13px',
    lineHeight: '1.5em',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  footerLeft: {
    marginRight: '1em',
    minWidth: '80px'
  },
  footerRight: {
    marginLeft: '1em'
  },
  date: {
    fontWeight: 600
  },
  location: {
    color: '#cb205b',
    fontSize: '9px',
    fontWeight: 'bold',
    textTransform: 'uppercase'
  },
  bottomText: {
    textAlign: 'right',
    fontSize: '20px',
    [mobile]: {
      right: '20px',
      fontSize: '13px'
    }
  }
});

export default withStyles(styles)(Certificate);
