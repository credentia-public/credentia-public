import React, { Component }                 from 'react';
import { MobXProviderContext }              from 'mobx-react';
import { WithTranslation, withTranslation } from 'react-i18next';
import { WithStyles }                       from '@material-ui/core';
import { createStyles, withStyles }         from '@material-ui/styles';
import classnames                           from 'classnames';
import { Link, withRouter, RouteComponentProps }                 from 'react-router-dom';

import { color } from '../../lib/sharedStyles';

// tslint:disable-next-line:interface-name
interface Props extends WithTranslation, RouteComponentProps, WithStyles<typeof styles> {
  store?: import('../../store').default;
}

class Header extends Component<Props> {
  static contextType = MobXProviderContext;

  context!: React.ContextType<typeof MobXProviderContext>;

  logout = () => {
    const { history } = this.props;
    this.context.store.sessionStore.logout();
    history.replace('/app/login');
  };

  render() {
    const { t, classes } = this.props;
    const { user, issuer } = this.context.store.sessionStore;

    let logo = '/static/images/logo.svg'

    if (issuer && issuer.name === 'РТ-Техприемка') {
      logo = '/static/images/rttec/logo.png'
    }

    return (
      <header className={classes.root}>
        <Link to='/'>
          <img className={classes.logo} src={logo} alt={t('credentia_logo')}/>
        </Link>
        <div className={classes.rightBlock}>
          {
            user
              ?
              <React.Fragment>
                <div>
                  <Link to='/app/settings' className={classes.settingsLink}>
                    <div className={classnames(classes.avatar, 'icon')}/>
                    <span className={classes.email}>{user.email}</span>
                  </Link>
                </div>
                <div className='icon' onClick={this.logout} title={t('sign_out')}>
                  <img src='/static/images/turn_off.svg'
                       alt={t('logout_icon')}/>
                </div>
              </React.Fragment>
              :
              <Link to='/app/login'>
                <div className='icon'>
                  <img className={classes.signInImage} src='/static/images/login.svg' alt={t('sign_in_icon')}/>
                </div>
              </Link>
          }
        </div>
      </header>
    );
  }
}

const styles = createStyles({
  root: {
    display: 'flex',
    marginBottom: '20px',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '15px 0',
    '& img': {
      display: 'block'
    }
  },
  logo: {
    marginRight: '30px',
    cursor: 'pointer',
    maxWidth: '120px',
    maxHeight: '40px'
  },
  rightBlock: {
    display: 'flex',
    '& > * + *': {
      marginLeft: '24px'
    },
    '@media (max-width: 600px)': {
      '& > * + *': {
        marginLeft: '10px'
      }
    },
    '& .icon': {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      width: '30px',
      height: '30px',
      borderRadius: '50%',
      cursor: 'pointer',
      '&:hover': {
        backgroundColor: '#F6F6F8'
      }
    }
  },
  settingsLink: {
    display: 'flex',
    paddingRight: '3px',
    textDecoration: 'none',
    color: color.darkBlue,
    borderRadius: '15px 5px 5px 15px',
    '&:hover': {
      backgroundColor: '#F6F6F8'
    }
  },
  avatar: {
    width: '30px',
    height: '30px',
    borderRadius: '50%',
    backgroundColor: '#202949 !important'
  },
  email: {
    marginLeft: '3px',
    lineHeight: '30px',
    maxWidth: '200px',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  '@media (max-width: 500px)': {
    email: {
      maxWidth: '100px'
    }
  },
  '@media (max-width: 400px)': {
    email: {
      maxWidth: '50px'
    }
  },
  signInImage: {
    width: '20px',
    height: '20px'
  }
});

export default withRouter(
  withStyles(styles)(
    withTranslation('common')(Header)
  )
);
