import { createStyles, WithStyles, withStyles } from '@material-ui/core';
import React                                    from 'react';
import classnames                               from 'classnames';

import { inject, observer } from 'mobx-react';

import Header from './header';
import Footer from './footer';

// tslint:disable-next-line:interface-name
interface Props extends WithStyles<typeof styles> {
  children: React.ReactNode;
  showHeader?: boolean;
  centered?: boolean;
  embedded?: boolean;
  pageTitle?: string;
  store?: import('../../store').default;
}

class Layout extends React.Component<Props> {
  render() {
    const { children, classes, store, showHeader, centered, embedded, pageTitle } = this.props;
    const { sessionStore } = store!;

    const isHeader = !embedded && showHeader !== false && sessionStore.view === 'app';
    const isFooter = !embedded;

    //@ts-ignore
    const userBackground: any = sessionStore && sessionStore.user && sessionStore.user.background;

    return (
      <div className={classnames(classes.root, embedded ? classes.rootEmbedded : null)} style={{ backgroundImage: `url("${userBackground}")` }}>
        <div className={classnames(classes.allExceptFooter, {
          [classes.centered]: centered
        })}>
          {isHeader && <Header/>}
          <div className={classes.mainWrapper}>
            <main className={classes.content}>
              {
                pageTitle &&
                <h1 className={classes.pageTitle}>{pageTitle}</h1>
              }
              {children}
            </main>
          </div>
        </div>
        {isFooter &&
        <div className={classes.footerWrapper}>
          <Footer/>
        </div>
        }
      </div>
    );
  }
}

const styles = createStyles({
  '@global': {
    html: {
      height: '100%'
    },
    body: {
      height: '100%',
      backgroundColor: '#fff',
      color: '#202949',
      fontFamily: 'IBM Plex Sans, sans-serif'
    },
    '#application-container': {
      height: '100%'
    }
  },
  rootEmbedded: {
    padding: '0!important'
  },
  root: {
    margin: '0 auto',
    minWidth: '320px',
    maxWidth: '1280px',
    padding: '30px 65px',
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100%',
    backgroundSize: '100%',
    backgroundRepeat: 'repeat-y'
  },
  '@media (max-width: 800px)': {
    root: {
      padding: '20px 40px'
    }
  },
  '@media (max-width: 500px)': {
    root: {
      padding: '10px 20px'
    }
  },
  centered: {
    minHeight: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  allExceptFooter: {
    width: '100%',
    flex: '1 0 auto'
  },
  footerWrapper: {
    width: '100%',
    flex: '0 0 auto'
  },
  mainWrapper: {
    width: '100%',
    maxWidth: '1000px',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  content: {},
  pageTitle: {
    margin: '0 0 1em',
    fontSize: '2.5em',
    textAlign: 'center',
    '@media (max-width: 800px)': {
      fontSize: '1.8em'
    }
  }
});

// export default withRouter(observer(withStyles(styles)(Layout)));
export default withStyles(styles)(
  inject('store')(observer(Layout))
);
