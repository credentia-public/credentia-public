import React, { Component }                 from 'react';
import { WithStyles }                       from '@material-ui/core';
import { createStyles, withStyles }         from '@material-ui/styles';
import { withTranslation, WithTranslation } from 'react-i18next';

import LangSelector from '../common/LangSelector';

// tslint:disable-next-line:interface-name
interface Props extends WithTranslation, WithStyles<typeof styles> {
  store?: import('../../store').default;
}

class Footer extends Component<Props> {

  render() {
    const { t, classes } = this.props;

    return (
      <footer className={classes.root}>
        <div className={classes.copyright}>© 2019-&infin; Credentia – {t('digital_credentials')}</div>
        <div>
          <LangSelector/>
        </div>
      </footer>
    );
  }
}

const styles = createStyles({
  root: {
    display: 'flex',
    marginTop: '50px',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  '@media (max-width: 500px)': {
    root: {
      marginTop: '35px'
    }
  },
  copyright: {
    marginRight: '1em',
    fontSize: '11px',
    color: '#8f94a4'
  }
});

export default withStyles(styles)(
  withTranslation('common')(Footer)
);
