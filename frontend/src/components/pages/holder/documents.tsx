import React, { Component }                      from 'react';
import { WithStyles }                            from '@material-ui/core';
import { createStyles, withStyles }              from '@material-ui/styles';
import { inject, MobXProviderContext, observer } from 'mobx-react';
import { withTranslation, WithTranslation/*, useTranslation*/ }      from 'react-i18next';
import { withRouter, RouteComponentProps }       from 'react-router-dom';
import classnames                                from 'classnames';

import { RequestState }    from '../../../sharedTypes';
import Layout              from '../../../components/layout';
import Loader              from '../../../components/common/Loader';
import { ITemplate }       from '../../../store/domain/templates';
import { color }           from '../../../lib/sharedStyles';

enum Tab {
  HolderInfo,
  HolderDocs
}

enum Visibility {
  Public,
  Private,
  Any
}

// tslint:disable-next-line:interface-name
interface Props extends WithTranslation, RouteComponentProps, WithStyles<typeof styles> {
  store: import('../../../store').default;
}

class Certificates extends Component<Props> {
  static contextType = MobXProviderContext;

  context!: React.ContextType<typeof MobXProviderContext>;

  static async getInitialProps(_ctx: any) {
    return {
      namespacesRequired: ['common']
    };
  }

  constructor(props: Props) {
    super(props);
  }

  public state = {
    isLoaded: false as boolean,
    currentTab: Tab.HolderDocs as Tab,
    filter: {
      excludeIssuers: [] as any,
      visibility: Visibility.Any as Visibility
    }
  }

  async componentDidMount(): Promise<void> {
    await this.context.store.certsStore.fetchCerts();
    this.setState({
      ...this.state,
      isLoaded: true
    });
  }

  toggleFilterIssuers(issuerName: string) {
    const newState = this.state;

    if (newState.filter.excludeIssuers.includes(issuerName)) {
      newState.filter.excludeIssuers = newState.filter.excludeIssuers.filter((item: any) => item !== issuerName);
    } else {
      newState.filter.excludeIssuers.push(issuerName);
    }

    this.setState(newState);
  }

  setVisibility(visibility: any) {
    const newState = this.state;
    newState.filter.visibility = visibility;
    this.setState(newState);
  }

  showDocDetails = (code: string) => {
    this.props.history.push(`/app/document/${code}`);
  };

  render() {
    if (!this.state) {
      return (<Loader />);
    }
    const state = this.state;
    const { t, classes } = this.props;
    const { certsStore, sessionStore } = this.props.store;

    //const { i18n } = useTranslation();
    //const currentLang = i18n.language || 'en';
    const currentLang = 'ru';

    const isLoaded = state.isLoaded;

    const user = sessionStore.user;

    //@ts-ignore
    const isShowUserInfo = user && user.avatar && user.description;

    const iotSuffix = '_iot@credentia.me';
    const isIotMode = user && user.email && user.email.includes(iotSuffix);


    const holderTitle: any = user ?
      //@ts-ignore
      [user.surname, user.name, user.secondName].filter(_ => _).join(' ')
      || isIotMode && user.email.replace(iotSuffix, '')
      || user.email
      : null;

    const certs = certsStore.certificates;

    const filteredCerts = certs.filter(cert => {
      //@ts-ignore
      const issuerName = cert.credentialSubject && cert.credentialSubject.issuerData && cert.credentialSubject.issuerData.name;
      if (this.state.filter.excludeIssuers.includes(issuerName)) {
        return false;
      }
      if (
        this.state.filter.visibility == Visibility.Public && !cert.visible ||
        this.state.filter.visibility == Visibility.Private && cert.visible
      ) {
        return false;
      }
      return true;
    });

    const issuerNames = certs.reduce((acc: string[], cert) => {
      //@ts-ignore
      const issuerName = cert.credentialSubject && cert.credentialSubject.issuerData && cert.credentialSubject.issuerData.name;
      if (!acc.includes(issuerName)) {
        acc.push(issuerName);
      }
      return acc;
    }, []);

    const categorizedDocs: Object = filteredCerts.reduce((acc: any, cert) => {
      //@ts-ignore
      const issuerName = cert.credentialSubject && cert.credentialSubject.issuerData && cert.credentialSubject.issuerData.name;
      if (!acc[issuerName]) {
        acc[issuerName] = [];
      }
      acc[issuerName].push(cert);
      return acc;
    }, {});

    //@ts-ignore
    const userAvatar = user && user.avatar;
    //@ts-ignore
    const userDescription = user && user.description

    return (
      <Layout>

        {isShowUserInfo &&
        <div className={classes.tabs}>
          <a
            className={classnames(
              classes.tab,
              state.currentTab === Tab.HolderDocs && 'active'
            )}
            onClick={() => {this.setState({
              currentTab: Tab.HolderDocs
            })}}
          >
            {t('holder.tab_documents')}
            {isLoaded && certs.length > 0 &&
            <span>&nbsp;({certs.length})</span>
            }
          </a>
          <a
            className={classnames(
              classes.tab,
              state.currentTab === Tab.HolderInfo && 'active'
            )}
            onClick={() => {this.setState({
              currentTab: Tab.HolderInfo
            })}}
          >
            {t('holder.tab_information')}
          </a>
        </div>
        }

        <div className={classes.holderPanel}>

          <div className={classnames(
            classes.holderDocs,
            state.currentTab === Tab.HolderDocs && 'active'
          )}>

            <div className={classes.barsWrapper}>
              
              {isLoaded && certs.length > 0 &&
              <aside className={classes.sidebar}>
                <div className={classes.docFilters}>
                  <img className={classes.filterIcon} src="/static/images/filter.svg" />
                  <div>
                    <h3 className={classes.filterTitle}>{t('holder.filter.doc_author')}</h3>
                    {issuerNames.map((issuerName, index) =>
                    <a
                      className={classnames(
                        classes.filterVariant,
                        !state.filter.excludeIssuers.includes(issuerName) ? 'active' : null
                      )}
                      key={index}
                      onClick={() => { this.toggleFilterIssuers(issuerName) }}
                    >
                      {issuerName}
                    </a>
                    )}
                  </div>
                  <div>
                    <h3 className={classes.filterTitle}>{t('holder.filter.visibility.title')}</h3>
                    <select className={classes.visibilitySelect} value={this.state.filter.visibility} onChange={(e) => this.setVisibility(e.target.value)}>
                      <option value={Visibility.Any as number}> -- {t('holder.filter.visibility.not_selected')} -- </option>
                      <option value={Visibility.Public}>{t('holder.filter.visibility.public')}</option>
                      <option value={Visibility.Private}>{t('holder.filter.visibility.private')}</option>
                    </select>
                  </div>
                </div>
              </aside>
              }

              <div className={classes.rightBar}>
                <h1 className={classes.title}>
                  <span>
                    {isIotMode ? t('holder.documents') : t('holder.your_documents')}
                  </span>
                  {filteredCerts.length !== certs.length &&
                  <span className={classes.filterCounter}>
                  {' '}({filteredCerts.length}/{certs.length})
                  </span>
                  }
                </h1>

                {!isLoaded &&
                <div className={classes.oneStringResult}>
                  <Loader/>
                </div>
                }

                {isLoaded && certs.length === 0 &&
                <div className={classes.oneStringResult}>
                  <div className={classes.noDocsMessage}>{t('holder.no_docs')}</div>
                </div>
                }
                
                {isLoaded && certs.length > 0 &&
                <div className={classes.docList}>

                  {Object.keys(categorizedDocs).length === 0 &&
                  <div className={classes.docsNotFound}>{t('holder.docs_not_found')}</div>
                  }

                  {Object.entries(categorizedDocs).map(([category, certs]) => (
                    <div key={category}>
                      <hr />
                      <h2 className={classes.categoryTitle}>{category}</h2>
                      <div className={classes.categoryDocs}>
                        {certs.map((c: any, index: number) => {
                          const { textColor, bgColor, logoPath } = c.template as ITemplate;

                          const docItemStyle: any = {
                            color: textColor,
                            backgroundColor: bgColor,
                            backgroundImage: logoPath ? `url(${logoPath})` : null,
                          };

                          const signDateFormatted = (c.proof && c.proof.created) &&
                            (new Date(c.proof.created)).toLocaleString(currentLang, {
                              year: '2-digit',
                              month: '2-digit',
                              day: 'numeric'
                            });

                          return (
                            <div className={classes.docItem} key={c._id}>
                              <div
                                className={classes.docPreview}
                                key={c._id}
                                data-index={index}
                                style={docItemStyle}
                                onClick={() => this.showDocDetails(c.shareableLinkCode)}
                              >
                                {c.credentialSubject.title}
                              </div>
                              <footer className={classes.docInfo}>
                                {signDateFormatted &&
                                <div className={classes.docInfoItem} title={t('holder.sign_date')}>
                                  <img src='/static/images/signature.svg' />
                                  <span>{signDateFormatted}</span>
                                </div>
                                }
                                <div
                                  className={classnames(classes.docInfoItem, !c.visible && classes.disabled)}
                                  title={c.visible ? t('holder.public') : t('holder.not_public')}
                                >
                                  <img src='/static/images/shared.svg' />
                                  <img src='/static/images/public.svg' />
                                </div>
                                <div className={classes.docInfoItem} title={t('holder.number_of_views')}>
                                  <img src='/static/images/views.svg' />
                                  <span>{c.views ? c.views : 0}</span>
                                </div>
                              </footer>
                            </div>
                          );
                        })}
                      </div>
                    </div>
                  ))}
                </div>
                }

              </div>

            </div>

          </div>

          {isShowUserInfo &&
          <div className={classnames(
            classes.holderInfo,
            state.currentTab === Tab.HolderInfo && 'active'
          )}>
            <h1 className={classes.title}>{t('holder.tab_information')}</h1>

            <div className={classes.holderAvatarPlace}>
              {
              userAvatar
              ?
              <div className={classes.holderAvatar} style={{ backgroundImage: `url("${userAvatar}")` }}></div>
              :
              <div className={classes.noImage}></div>
              }
            </div>

            <div className={classes.holderTitle} title={holderTitle}>{holderTitle}</div>

            {userDescription &&
            <div>
              <hr />
              <div className={classes.holderDescription} dangerouslySetInnerHTML={{ __html: userDescription }}></div>
              </div>
            }
          </div>
          }

        </div>

      </Layout>
    );
  }
}

const panelPart = {
  border: '1px solid rgba(230,230,230,0.9)',
  borderRadius: 6,
  padding: '10px',
  backgroundColor: 'rgba(240,240,240,0.8)'
};

const mobile = '@media (max-width: 1000px)';

const styles = createStyles({
  tabs: {
    display: 'flex',
    marginBottom: '1em'
  },
  tab: {
    width: '50%',
    border: '1px solid rgba(230,230,230,0.9)',
    backgroundColor: 'rgba(240,240,240,0.7)',
    padding: '10px',
    textAlign: 'center',
    fontSize: '16px',
    cursor: 'pointer',
    userSelect: 'none',
    '@media (max-width: 500px)': {
      fontSize: '14px',
    },
    '&:hover': {
      borderBottom: `1px solid ${color.darkBlue}`,
    },
    '&.active': {
      border: 'none',
      backgroundColor: color.darkBlue,
      color: color.white,
      fontWeight: 'bold'
    }
  },
  holderPanel: {
  },
  holderDocs: {
    ...panelPart,
    display: 'none',
    '&.active': {
      display: 'block'
    }
  },
  barsWrapper: {
    display: 'flex',
    [mobile]: {
      display: 'block'
    }
  },
  sidebar: {
    marginRight: '10px',
    width: '300px',
    borderRight: `1px solid ${color.darkBlue}66`,
    [mobile]: {
      marginRight: 0,
      width: '100%',
      borderRight: 'none',
      borderBottom: `1px solid ${color.darkBlue}66`,
      paddingBottom: '2em'
    }
  },
  docFilters: {
    position: 'sticky',
    top: 0,
    padding: '1em 10px 4em 0',
    [mobile]: {
      position: 'relative',
      padding: 0,
      textAlign: 'center'
    }
  },
  filterIcon: {
    display: 'block',
    width: '20px',
    margin: '0 auto',
    [mobile]: {
      //display: 'none'
    }
  },
  filterTitle: {
    margin: '1.2em 0 0.7em',
    fontWeight: 300,
    [mobile]: {
      margin: '0.5em 0',
    }
  },
  filterVariant: {
    display: 'inline-block',
    margin: '0.3em',
    border: `1px solid ${color.disabled.border}`,
    borderRadius: '2em',
    backgroundColor: `${color.disabled.bg}`,
    color: `${color.disabled.text}`,
    padding: '0.4em',
    fontSize: '15px',
    lineHeight: '15px',
    wordBreak: 'break-word',
    userSelect: 'none',
    cursor: 'pointer',
    '&.active': {
      border: `1px solid ${color.darkBlue}99`,
      backgroundColor: `${color.darkBlue}bb`,
      color: `${color.white}`
    },
  },
  visibilitySelect: {
    width: '100%',
    height: '30px',
    [mobile]: {
      maxWidth: '300px'
    }
  },
  rightBar: {
    width: '100%'
  },
  docList: {
    maxWidth: '856px',
    margin: '0 auto'
  },
  title: {
    textAlign: 'center',
  },
  filterCounter: {
    verticalAlign: 'middle',
    fontSize: '16px',
    fontWeight: 500,
    opacity: 0.5
  },
  docsNotFound: {
    textAlign: 'center',
    fontSize: '15px',
    [mobile]: {
      marginBottom: '3em'
    }
  },
  categoryTitle: {
    fontSize: '20px',
    lineHeight: '1em',
    textAlign: 'center',
    wordWrap: 'break-word'
  },
  categoryDocs: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center'
  },
  docItem: {
    position: 'relative',
    margin: '0 7px 30px 7px',
    borderRadius: '8px',
    border: '1px solid #ccc',
    color: '#fff',
    cursor: 'pointer',
    overflow: 'hidden',
    '&:hover': {
      border: `1px solid ${color.orange}`
    }
  },
  docPreview: {
    height: '276px',
    width: '184px',
    padding: '12px',
    background: '#414042 center bottom 12px no-repeat',
    backgroundSize: '100px',
    fontSize: '16px',
    lineHeight: '106%',
    wordWrap: 'break-word'
  },
  docInfo: {
    display: 'flex',
    justifyContent: 'space-between',
    borderTop: `1px solid ${color.darkBlue}33`,
    padding: '4px 5px',
    backgroundColor: '#f8f8f8',
    color: color.darkBlue,
    fontSize: '13px',
    lineHeight: '1em'
  },
  docInfoItem: {
    display: 'flex',
    alignItems: 'center',
    opacity: 0.5,
    '& img': {
      height: '16px'
    },
    '& span': {
      marginLeft: '0.15em'
    },
  },
  disabled: {
    opacity: 0.1
  },
  oneStringResult: {
    margin: '3em 0'
  },
  noDocsMessage: {
    textAlign: 'center',
    margin: '1em 0'
  },
  holderInfo: {
    ...panelPart,
    paddingBottom: 40,
    display: 'none',
    '&.active': {
      display: 'block'
    }
  },
  holderAvatarPlace: {
    margin: '0 auto',
    width: 180,
    height: 180,
    border: '1px solid #ccc',
    backgroundColor: '#eee'
  },
  holderAvatar: {
    width: '100%',
    height: '100%',
    backgroundSize: '100%',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center'
  },
  noImage: {
    width: '100%',
    height: '100%',
    backgroundImage: 'url("/static/images/photo.svg")',
    backgroundSize: '50%',
    backgroundPosition: 'center center',
    backgroundRepeat: 'no-repeat',
    opacity: 0.1
  },
  holderTitle: {
    margin: '1em 0',
    fontWeight: 'bold',
    fontSize: '17px',
    textAlign: 'center',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  holderDescription: {
    margin: '1em 0',
    fontSize: '14px',
    wordWrap: 'break-word',
    '& table': {
      border: '1px solid gray',
      borderCollapse: 'collapse'
    },
    '& td': {
      border: '1px solid gray'
    }
  }
});

export default withRouter(
  withTranslation('common')(
    withStyles(styles)(
      inject('store')(observer(Certificates))
    )
  )
);
