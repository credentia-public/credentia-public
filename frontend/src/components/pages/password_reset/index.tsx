import React                                from 'react';
import { createStyles, withStyles }         from '@material-ui/styles';
import { WithStyles }                       from '@material-ui/core';
import { Formik, FormikActions }            from 'formik';
import classnames                           from 'classnames';
import { withTranslation, WithTranslation } from 'react-i18next';
import { withRouter, RouteComponentProps }  from 'react-router-dom';

import { button, formMessage as formMessageStyle } from '../../../lib/sharedStyles';
import { ErrorMessages }                           from '../../../lib/errors';
import validator                                   from '../../../lib/validator';
import Layout                                      from '../../layout';
import TextField                                   from '../../common/TextField';
import {
  resetPasswordSendLink,
  resetPasswordSetNew
}                                                  from '../../../api/user';

// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, RouteComponentProps, WithStyles<typeof styles> {
}

// tslint:disable-next-line:interface-name no-empty-interface
export interface State {
  token: string | null;
  email?: string;
  newPassword?: string;
}

type FormValues = Omit<State, 'token'>;

class PasswordReset extends React.Component<Props, State> {

  constructor(props: any) {
    super(props);

    const query = new URLSearchParams(props.location.search);
    const token = query.get('token');

    this.state = {
      token,
      email: '',
      newPassword: ''
    };
  }

  validateSendLink = (values: FormValues) => {
    const { t } = this.props;
    const errors: FormValues = {};

    if (!values.email) {
      errors.email = t('form_errors.required');
    } else if (!validator.isValidEmail(values.email)) {
      errors.email = t('form_errors.invalid_email_address');
    }

    return errors;
  };

  sendLink = async (values: FormValues, { setSubmitting, setStatus }: FormikActions<State>) => {
    try {
      await resetPasswordSendLink({ email: values.email! });
      setStatus('ok');
    } catch (err) {
      setStatus(err.message);
    } finally {
      setSubmitting(false);
    }
  };

  validateSetNewPassword = (values: FormValues) => {
    const { t } = this.props;
    const errors: FormValues = {};

    if (!values.newPassword) {
      errors.newPassword = t('form_errors.required');
    }

    return errors;
  };

  setNewPassword = async (values: FormValues, { setSubmitting, setStatus }: FormikActions<State>) => {
    const { history } = this.props;

    try {
      await resetPasswordSetNew({
        token: this.state.token!,
        newPassword: values.newPassword!
      });
      history.replace('/app/login?password_change=success');
    } catch (err) {
      setStatus(err.message);
    } finally {
      setSubmitting(false);
    }
  };

  render() {
    const { token } = this.state;
    const { classes, t } = this.props;

    let formMessage: any;

    return (
      <Layout pageTitle={t('password_reset.title')}>
        <div className={classes.root}>
          {!token &&
          <div>
            <Formik
              initialValues={this.state}
              validate={this.validateSendLink}
              onSubmit={this.sendLink}
            >
              {({
                  values,
                  errors,
                  touched,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isSubmitting,
                  isValid,
                  status
                }) => {

                const formErrors: any = {
                  [ErrorMessages.NotFound]: t('password_reset.not_registered'),
                  [ErrorMessages.TooManyRequests]: t('password_reset.too_many_requests')
                };

                if (status === 'ok') {
                  formMessage = {
                    type: 'success',
                    text:
                      <div>
                        <div>{t('password_reset.check_email')}</div>
                        <div>{t('password_reset.check_email_advice')}</div>
                      </div>
                  };
                }

                if (status && formErrors[status]) {
                  formMessage = {
                    type: 'error',
                    text: formErrors[status]
                  };
                }

                return (
                  <form className={classes.form} onSubmit={handleSubmit}>
                    {
                      !formMessage
                        ?
                        <div className={classes.step1message}>
                          <p>{t('password_reset.enter_email')}</p>
                          <p>{t('password_reset.link_will_be_sent')}</p>
                        </div>
                        :
                        <div className={classnames(classes.formMessage, formMessage.type)}>
                          {formMessage.text}
                        </div>
                    }
                    {status !== 'ok' &&
                    <div>
                      <TextField
                        classes={{ root: classes.input }}
                        id='email'
                        name='email'
                        value={values.email}
                        labelText={t('password_reset.email')}
                        errorMessage={touched.email && errors.email || undefined}
                        autoComplete='email'
                        autoFocus
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                      <button className={classes.submit} type='submit' disabled={isSubmitting || !isValid}>
                        {t('password_reset.send')}
                      </button>
                    </div>
                    }
                  </form>
                );
              }}
            </Formik>
          </div>
          }

          {token &&
          <div>
            <Formik
              initialValues={this.state}
              validate={this.validateSetNewPassword}
              onSubmit={this.setNewPassword}
            >
              {({
                  values,
                  errors,
                  touched,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isSubmitting,
                  isValid,
                  status
                }) => {

                const formErrors: any = {
                  [ErrorMessages.OperationNotAllowed]: t('password_reset.invalid_link')
                };

                if (status && formErrors[status]) {
                  formMessage = {
                    type: 'error',
                    text: formErrors[status]
                  };
                }

                return (
                  <form className={classes.form} onSubmit={handleSubmit}>
                    {
                      formMessage &&
                      <div className={classnames(classes.formMessage, formMessage.type)}>
                        {formMessage.text}

                        {status === ErrorMessages.OperationNotAllowed &&
                        <div>
                          <a href='/app/password-reset'>{t('password_reset.try_to_start_over')}</a>
                        </div>
                        }
                      </div>
                    }
                    <TextField
                      type='password'
                      classes={{ root: classes.input }}
                      id='newPassword'
                      name='newPassword'
                      value={values.newPassword}
                      labelText={t('password_reset.new_password')}
                      errorMessage={touched.newPassword && errors.newPassword || undefined}
                      autoFocus
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                    <button className={classes.submit} type='submit' disabled={isSubmitting || !isValid}>
                      {t('password_reset.apply')}
                    </button>
                  </form>
                );
              }}
            </Formik>
          </div>
          }
        </div>
      </Layout>
    );
  }
}

const styles = createStyles({
  root: {
    maxWidth: '400px',
    margin: '0 auto'
  },
  step1message: {
    marginBottom: '2em'
  },
  form: {
    width: '100%'
  },
  formMessage: {
    extend: formMessageStyle
  },
  input: {
    marginBottom: '30px'
  },
  submit: {
    extend: button,
    height: '49px',
    marginBottom: '8px'
  }
});

export default withRouter(
  withTranslation('common')(
    withStyles(styles)(PasswordReset)
  )
);
