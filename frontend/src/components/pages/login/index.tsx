import React                                from 'react';
import { MobXProviderContext }              from 'mobx-react';
import { createStyles, withStyles }         from '@material-ui/styles';
import { WithStyles }                       from '@material-ui/core';
import { Formik, FormikActions }            from 'formik';
import classnames                           from 'classnames';
import { withTranslation, WithTranslation } from 'react-i18next';
import { withRouter, RouteComponentProps }  from 'react-router-dom';

import { button, formMessage as formMessageStyles } from '../../../lib/sharedStyles';
import { UserErrorMessages }                        from '../../../lib/errors';
import validator                                    from '../../../lib/validator';
import { UserType }                                 from '../../../store/domain/user';
import Layout                                       from '../../layout';
import TextField                                    from '../../common/TextField';

// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, RouteComponentProps, WithStyles<typeof styles> {
}

// tslint:disable-next-line:interface-name no-empty-interface
export interface State {
  email?: string;
  password?: string;
}

class SignIn extends React.Component<Props, State> {
  static contextType = MobXProviderContext;

  context!: React.ContextType<typeof MobXProviderContext>;

  constructor(props: any) {
    super(props);

    this.state = {
      email: '',
      password: ''
    };
  }

  login = async (values: State, { setSubmitting, setStatus }: FormikActions<State>) => {
    const { history } = this.props;
    const { email, password } = values;
    const sessionStore = this.context.store.sessionStore;

    try {
      await sessionStore.signin(email, password);
      const user = sessionStore.user;

      switch (user.type) {
        case UserType.IssuerAmbassador: {
          history.replace('/app/issuer/documents');
          break;
        }
        case UserType.Student: {
          history.replace('/app/holder/documents');
          break;
        }
        default:
          break;
      }
    } catch (err) {
      setStatus(err.message);
    } finally {
      setSubmitting(false);
    }
  };

  validate = (values: State) => {
    const { t } = this.props;
    const errors: State = {};

    if (!values.email) {
      errors.email = t('login_errors.required');
    } else if (!validator.isValidEmail(values.email)) {
      errors.email = t('login_errors.invalid_email_address');
    }
    if (!values.password) {
      errors.password = t('login_errors.required');
    }

    return errors;
  };

  render() {
    const { classes, t, location, history } = this.props;
    const urlParams = new URLSearchParams(location.search);
    const confirmationStatus = urlParams.get('confirmation');
    const passwordChangeStatus = urlParams.get('password_change');

    let formMessage: any;

    if (confirmationStatus === 'success') {
      formMessage = {
        type: 'success',
        text: t('auth.sign_up_success')
      };
    }

    if (passwordChangeStatus === 'success') {
      formMessage = {
        type: 'success',
        text: t('auth.password_change_success')
      };
    }

    const host = window.location.hostname;
    const showOnlySignin = host.includes('e-diplom.kz');

    return (
      <Layout showHeader={false} centered={true}>
        <div className={classes.root}>
          <img className={classes.logo} src='/static/images/logo.svg' alt={t('credentia_logo')}/>
          <span className={classes.title}>{t('auth.sign_in_title')}</span>
          <Formik onSubmit={this.login} initialValues={this.state} validate={this.validate}>
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                isValid,
                status
                /* and other goodies */
              }) => {

              const formErrors: any = {
                [UserErrorMessages.NotRegistered]: t('login_errors.user_not_registered'),
                [UserErrorMessages.NotVerified]: t('login_errors.user_not_verified'),
                [UserErrorMessages.InvalidPassword]: t('login_errors.invalid_password')
              };

              if (status && formErrors[status]) {
                formMessage = {
                  type: 'error',
                  text: formErrors[status]
                };
              }

              return (
                <form className={classes.form} onSubmit={handleSubmit}>
                  {
                    formMessage &&
                    <div className={classnames(classes.formMessage, formMessage.type)}>
                      {formMessage.text}.

                      {status === UserErrorMessages.InvalidPassword &&
                      <span>
                      {' '}
                        <a href='/app/password-reset'>{t('login_errors.forgot_password')}</a>
                    </span>
                      }
                    </div>
                  }
                  <TextField
                    classes={{ root: classes.input }}
                    id='email'
                    name='email'
                    value={values.email}
                    labelText={t('auth.email_address')}
                    errorMessage={touched.email && errors.email || undefined}
                    autoComplete='email'
                    autoFocus
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                <div className={classes.passwordInputPlace}>
                  <TextField
                    classes={{ root: classes.secondInput }}
                    id='password'
                    name='password'
                    value={values.password}
                    type='password'
                    labelText={t('auth.password')}
                    errorMessage={(touched.password && errors.password) || undefined}
                    autoComplete='password'
                    onChange={handleChange}
                    onBlur={handleBlur}
                    style={{ paddingRight: 45 }}
                  />
                  <a href='https://credentia.ru/webauthn.html' title='WebAuthn' className={classes.webAuthnLink}></a>
                </div>
                  <button className={classes.submit} type='submit' disabled={isSubmitting || !isValid}>
                    {t('auth.sign_in')}
                  </button>
                  {!showOnlySignin &&
                  <div>
                    <a className={classes.secondaryVariant} href='/app/signup'>
                      {t('auth.sign_up')}
                    </a>
                    <a className={classes.secondaryVariant}  href='https://esia.gosuslugi.ru/'>
                      <span>{t('auth.sign_in_with')}</span>
                      <img className={classes.gosuslugiLogo} src='/static/images/gosuslugi.svg'/>
                    </a>
                  </div>
                  }
                </form>
              );
            }}
          </Formik>
        </div>
      </Layout>
    );
  }
}

const styles = createStyles({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    display: 'block',
    width: '50px',
    height: '50px',
    marginBottom: '20px'
  },
  title: {
    marginBottom: '10px',
    fontSize: '20px'
  },
  form: {
    width: '100%',
    maxWidth: '400px'
  },
  formMessage: {
    extend: formMessageStyles
  },
  input: {
    marginBottom: '30px'
  },
  passwordInputPlace: {
    position: 'relative',
  },
  webAuthnLink: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    width: '40px',
    height: '40px',
    backgroundImage: 'url("/static/images/fingerprint.svg")',
    backgroundSize: '80%',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    opacity: 0.5,
    '&:hover': {
      opacity: 1
    }
  },
  secondInput: {
    marginBottom: '48px'
  },
  submit: {
    extend: button,
    height: '49px',
    marginBottom: '8px'
  },
  secondaryVariant: {
    display: 'block',
    textDecoration: 'none',
    width: '60%',
    margin: '0 auto',
    height: '40px',
    lineHeight: '40px',
    borderRadius: '8px',
    textAlign: 'center',
    fontSize: '13px',
    color: '#605b5b',
    cursor: 'pointer',
    userSelect: 'none',
    '&:hover': {
      backgroundColor: '#F6F6F8'
    }
  },
  gosuslugiLogo: {
    display: 'inline-block',
    width: '66px',
    marginLeft: '5px',
    transform: 'translateY(3px)'
  }
});
export default withRouter(
  withTranslation('common')(
    withStyles(styles)(SignIn)
  )
);
