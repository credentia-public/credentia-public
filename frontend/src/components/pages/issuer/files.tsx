import React, { Component }                      from 'react';
import { inject, MobXProviderContext, observer } from 'mobx-react';
import { WithStyles }                            from '@material-ui/core';
import { withStyles, createStyles }              from '@material-ui/styles';
import { withTranslation, WithTranslation }      from 'react-i18next';
import mime                                      from 'mime-types';

import { button }                            from '../../../lib/sharedStyles';
import { getCertificateByCode, getTxDetail } from '../../../api/certificate';
import { IUploadError, uploadXHR }           from '../../../lib/ajaxProgress';
import Layout                                from '../../../components/layout';
import Back                                  from '../../../components/common/Back';
import Icon                                  from '../../../components/common/Icon';
import env                                   from '../../../environments/environment';
import { ITxDetail }                         from '../../../sharedTypes';
import { RouteComponentProps }               from 'react-router-dom';
import Loader from '../../common/Loader';

type IFiles = import('../../../store/domain/certificates').IFiles;
type ICertificate = import('../../../store/domain/certificates').ICertificate;

// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, RouteComponentProps<{ id: string }>, WithStyles<typeof styles> {
  store: import('../../../store').default;
  code: string;
  cloudFiles: IFiles;
  isLoadingArr: [];
}

// tslint:disable-next-line:interface-name
interface State {
  fileList: IFiles;
  fileLoading: boolean;
  progress: number;
  loading: boolean;
  tx?: ITxDetail;
  cert?: ICertificate;
}

class CertFiles extends Component<Props, State> {
  static contextType = MobXProviderContext;

  // static async getInitialProps(ctx: IPageContext) {
  //   const req: Request = ctx.req;
  //   const res: Response = ctx.res;
  //
  //   const code: string = ctx.query.code! as string;
  //   try {
  //     const { data, cookie } = await getCertificateByCode(code, {
  //       headers: req ? req.headers : null
  //     });
  //     const cert = data;
  //
  //     if (!cert) {
  //       res.writeHead(302, {
  //         Location: '/app/error/document-not-found'
  //       });
  //       res.end();
  //     }
  //
  //     const { cloudFiles } = cert.credentialSubject;
  //     let url = '';
  //     let origin = '';
  //
  //     if (req) {
  //       res.setHeader('set-cookie', cookie);
  //
  //       const hostname = req.hostname;
  //       const protocol = req.protocol;
  //       origin = `${protocol}://${hostname}`;
  //       url = origin + req.url;
  //     } else {
  //       url = location.href;
  //       origin = location.origin;
  //     }
  //
  //     const isEmbedded = ctx.query.embedded;
  //
  //     return {
  //       code,
  //       origin,
  //       url,
  //       cert,
  //       isEmbedded,
  //       cloudFiles: cloudFiles || [],
  //       namespacesRequired: ['common']
  //     };
  //   } catch (err) {
  //
  //   }
  // }

  context!: React.ContextType<typeof MobXProviderContext>;

  constructor(props: any) {
    super(props);
    this.state = {
      fileList: this.props.cloudFiles,
      loading: true,
      fileLoading: false,
      progress: 0
    };
  }

  async componentDidMount(): Promise<void> {
    const { match, history } = this.props;
    const id = match.params.id;

    try {
      let tx: ITxDetail;
      const cert = await getCertificateByCode(id);

      if (cert.txId) {
        tx = await getTxDetail(cert.chain, cert.txId);
      }

      if (!cert) {
        history.replace('/app/error/document-not-found');
      }

      this.setState({
        loading: false,
        // @ts-ignore
        tx: tx || undefined,
        cert
      });
    } catch (err) {
      console.error(err);
      history.replace('/app/error/document-not-found');
    }
  }

  uploadFile = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const code = this.props.code;
    const data = new FormData(e.currentTarget);
    await new Promise(res => {
      this.setState({ fileLoading: true }, () => {
        res();
      });
    });

    const path = `/certificates/${code}/files`;

    uploadXHR({
      method: 'POST',
      url: `${env.apiURL}${path}`,
      body: data,
      onProgress: num => {
        this.setState({ progress: num });
      },
      onCanceled: () => {
        this.setState({ fileLoading: false });
      },
      onFailure: (err: IUploadError) => {
        console.error(err);
        this.setState({ fileLoading: false });
      },
      onComplete: json => {
        if (json.status === 'ok') {
          this.setState({ fileLoading: false, fileList: json.data.credentialSubject.cloudFiles });
        }
      }
    });

    // const response = await fetch(`${env.apiURL}${path}`,
    //   Object.assign({ credentials: 'include', mode: 'cors' }, opts /*{ headers }*/)
    // );

    // const text = await response.text();
    // const outData = JSON.parse(text);
    //
    // if (outData.status === 'ok') {
    //   this.setState({ fileLoading: false, fileList: outData.data.credentialSubject.cloudFiles });
    // }
  };

  deleteFile = async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    const code = this.props.code;
    const fileHash = e.currentTarget.dataset.hash!;
    try {
      await this.context.store.certsStore.deleteFile(code, fileHash);
      const fileList = Object.assign({}, this.state.fileList);
      delete fileList[fileHash];
      this.setState({
        fileList
      });
    } catch (e) {
      console.error(e);
    }
  };

  render() {
    if (this.state.loading) {
      return <Loader/>;
    }

    const { classes, t } = this.props;
    const { fileList, fileLoading, progress } = this.state;
    return (
      <Layout pageTitle={t('attachments')} classes={{ content: classes.container }}>
        <div>

          <Back to='/app/issuer/documents'/>

          <form onSubmit={this.uploadFile}>
            <input className={classes.customFile} type='file' name='cert-files' multiple/>
            <input className={classes.button} type='submit' value='Upload'/>
          </form>
          {fileLoading && (
            <div className={classes.progressBar}>
              <div className='bar' style={{ width: `${progress * 100}%` }}/>
            </div>
          )}

          <ul className={classes.fileList}>
            { fileList &&
              Object.entries(fileList).map(([hash, f]) => {
                const type = mime.lookup(f.name || '');
                const canPreview = type && (type.includes('video') || type.includes('image'));

                return (
                  <li key={f.key} className={classes.file}>
                    <div className={classes.fileName}>
                      {f.name}
                    </div>
                    <div className={classes.fileControl}>
                      <a href={`/files/${f.key}`} download>
                        <Icon url={'/static/images/download.svg'} size={32}/>
                      </a>
                    </div>
                    <div className={classes.fileControl}>
                      <button onClick={this.deleteFile} data-hash={hash}>
                        <Icon url={'/static/images/delete.svg'} size={32}/>
                      </button>
                    </div>
                    {canPreview && (
                      <div className={classes.fileControl}>
                        <a href={`/files/${f.key}`} target='_blank'>
                          <Icon url={'/static/images/preview.svg'} size={32}/>
                        </a>
                      </div>
                    )}
                  </li>
                );
              })
            }
          </ul>
        </div>
      </Layout>
    );
  }
}

const styles = createStyles({
  fileList: {
    listStyle: 'none',
    padding: 0,
    margin: 0
  },
  file: {
    display: 'flex',
    alignItems: 'center',
    padding: '8px 16px',
    borderBottom: '1px solid rgba(0, 0, 0, 0.12)'
  },
  fileName: {
    flex: 1
  },
  fileControl: {
    '& a': {
      display: 'block'
    },
    '& button': {
      padding: 0,
      border: 'none',
      outline: 'none',
      background: 'none',
      cursor: 'pointer'
    }
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
    margin: '0 auto 40px',
    width: '600px',
    '& label': {
      display: 'block',
      marginBottom: '10px',
      fontWeight: 'bold'
    },
    '& textarea': {
      '&:disabled': {
        backgroundColor: '#eee'
      }
    }
  },
  data: {
    marginBottom: '20px',
    height: '200px',
    resize: 'vertical'
  },
  customFile: {
    border: '1px solid #ccc',
    display: 'inline-block',
    padding: '6px 12px',
    cursor: 'pointer'
  },
  button: {
    extend: button,
    height: '49px',
    margin: '20px 0',
    width: 200
  },
  signature: {
    resize: 'vertical'
  },
  '@keyframes progress-active ': {
    '0%': {
      opacity: .3,
      width: 0
    },
    '100%': {
      opacity: 0,
      width: '100%'
    }
  },
  progressBar: {
    position: 'relative',
    display: 'flex',
    maxWidth: '100%',
    border: 'none',
    margin: '1em 0em 1em',
    background: '#e9e9e9',
    padding: '0em',
    borderRadius: '.28571rem',
    overflow: 'hidden',
    '& .bar': {
      height: '1em',
      display: 'inline-block',
      lineHeight: 1,
      position: 'relative',
      width: '0%',
      minWidth: '2em',
      background: '#FF9356',
      borderRadius: '.28571429rem',
      transition: 'width 0.5s ease',
      '&::after': {
        content: "' '",
        opacity: 0,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        background: '#fff',
        borderRadius: '.28571429rem',
        animation: '$progress-active 2s ease infinite'
      }
    }
  }
});

// @keyframes progress-active {
//   0% {
//     opacity: .3;
//   width: 0
// }
//
//   100% {
//     opacity: 0;
//   width: 100%
// }
// }

export default withTranslation('common')(
  withStyles(styles)(
    inject('store')(observer(CertFiles))
  )
);
