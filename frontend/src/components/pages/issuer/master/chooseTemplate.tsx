import { inject, MobXProviderContext, observer } from 'mobx-react';
import React                                     from 'react';
import { WithStyles }                            from '@material-ui/core';
import { createStyles, withStyles }              from '@material-ui/styles';
import { withTranslation, WithTranslation }      from 'react-i18next';
import { withRouter, RouteComponentProps }       from 'react-router-dom';

import Layout     from '../../../../components/layout';
import { button } from '../../../../lib/sharedStyles';

type ITemplate = import('../../../../store/domain/templates').ITemplate;

// tslint:disable-next-line:interface-name
interface Props extends WithTranslation, RouteComponentProps, WithStyles<typeof styles> {
  store: import('../../../../store').default;
}

class SelectType extends React.Component<Props> {
  static contextType = MobXProviderContext;

  context!: React.ContextType<typeof MobXProviderContext>;

  constructor(props: any) {
    super(props);
  }

  componentDidMount(): void {
    this.context.store.templatesStore.fetchTemplates();
  }

  handleTypeClick = (e: React.MouseEvent<HTMLLIElement, MouseEvent>) => {
    const { history } = this.props;
    const templateId = e.currentTarget.dataset.id;
    const template = this.context.store.templatesStore.templates.find((t: ITemplate) => t._id === templateId);

    this.context.store.certMasterStore.setTemplate(template);
    history.push('/app/issuer/master/fillData');
  };

  render() {
    const { t, classes, store } = this.props;
    const issuer = store.sessionStore.issuer;
    const templates: ITemplate[] = store.templatesStore.templates;

    return (
      <Layout pageTitle={t('master.choose_template_type')}>
        <section className={classes.root}>
          <ul className={classes.templates}>
            {templates!.map(temp => {

              const bgUrl = temp.imagePreviewPath || temp.logoPath;
              
              const templateStyles: any = {
                backgroundImage: bgUrl ? `url("${bgUrl}"` : null,
                backgroundColor: temp.bgColor ? temp.bgColor : null,
              };

              return (
                <li key={temp._id} data-id={temp._id} onClick={this.handleTypeClick}>
                  <div className={classes.template} style={templateStyles}/>
                  <div className={classes.templateName}>{t(`master.${issuer!.shortName}.${temp.name}`)}</div>
                </li>
              );
            })}
          </ul>
        </section>
      </Layout>

    );
  }
}

const background = 'linear-gradient(0deg, #F4F4F6, #F4F4F6), #226390';
const border = '1px solid #F4F4F6';
const borderRadius = '8px';
const hoverBackground = {
  backgroundColor: 'rgba(255, 147, 86, 0.04)',
  borderColor: '#FF9356'
};

const styles = createStyles({
  root: {
    margin: '0 auto',
    textAlign: 'center'
  },
  button: {
    extend: button,
    height: '49px',
    marginTop: '20px'
  },
  templates: {
    display: 'flex',
    justifyContent: 'space-around',
    listStyle: 'none',
    padding: 0,
    margin: 0,
    height: '400px'
  },
  template: {
    width: '300px',
    height: '300px',
    margin: '0',
    marginRight: '10%',
    border,
    background,
    borderRadius,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    cursor: 'pointer',
    '&:hover': hoverBackground
  },
  templateName: {
    marginTop: '1em'
  }
});

// export default observer(withTranslation('common')(withStyles(styles)(Degrees)));
export default withRouter(
  withTranslation('common')(
    withStyles(styles)(
      inject('store')(observer(SelectType))
    )
  )
);
