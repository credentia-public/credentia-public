import { inject, MobXProviderContext, observer } from 'mobx-react';
import React                                     from 'react';
import { WithStyles }                            from '@material-ui/core';
import { createStyles, withStyles }              from '@material-ui/styles';
import { withTranslation, WithTranslation }      from 'react-i18next';
import { withRouter, RouteComponentProps }       from 'react-router-dom';
//@ts-ignore
import QrReader                                  from 'react-qr-scanner';

import Layout                       from '../../../../components/layout';
import TextField                    from '../../../../components/common/TextField';
import { color, button, inputText } from '../../../../lib/sharedStyles';
import validator                    from '../../../../lib/validator';

type Store = import('../../../../store').default;

// tslint:disable-next-line:interface-name
interface Props extends WithTranslation, RouteComponentProps, WithStyles<typeof styles> {
  store: Store;
}

// tslint:disable-next-line:interface-name
interface State {
  isShowGrid: boolean;
  isShowNewGroupInput: boolean;
  isIotMode: boolean;
  scanResult: string;
  grid: any;
  amount: number;
  group: string;
  isValid: boolean;
  isBusy: boolean;
}

class Holders extends React.Component<Props, State> {
  static contextType = MobXProviderContext;

  context!: React.ContextType<typeof MobXProviderContext>;

  constructor(props: any) {
    super(props);

    const isIotMode = props.store.sessionStore.issuer.iot;

    this.state = {
      isShowGrid: false,
      isShowNewGroupInput: false,
      isIotMode,
      grid: [],
      amount: 1,
      group: '',
      scanResult: '',
      isValid: false,
      isBusy: false
    };
  }

  setCertsNumber = (e: React.MouseEvent<HTMLInputElement, MouseEvent>) => {
    const amount = parseInt(e.currentTarget.value, 10) || 0;

    this.setState({
      amount
    });
  };

  setGroupVariant = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedGroupVariant = e.currentTarget.value;

    if (selectedGroupVariant === '__NEW__') {
      this.setState({
        isShowNewGroupInput: true,
        group: ''
      });
    } else {
      this.setState({
        isShowNewGroupInput: false,
        group: selectedGroupVariant
      });
    }

  };

  setGroupValue = (e: React.MouseEvent<HTMLInputElement, MouseEvent>) => {
    const group = e.currentTarget.value;
    this.setState({
      group
    });
  };

  handleScanResult = (newValue: string) => {
    if (!newValue) {
      return;
    }
    const prevValue = this.state.scanResult;
    if (newValue !== prevValue) {
      this.setState({
        scanResult: newValue
      });

      const grid = this.state.grid;

      for (let i = 0; i < grid.length; i++) {
        if (grid[i].email.value === newValue) {
          return;
        }
        if (!grid[i].email.value) {
          grid[i].email.value = newValue;
          break;
        }
      }

      this.setState({
        grid,
        isValid: this.validateGrid(grid)
      });
    }
  };

  handleScanError = (err: Error) => {
    console.error(err);
  };

  createNewGrid = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();

    const { amount, grid } = this.state;
    const newGrid = grid;

    for (let i = 0; i < amount; i++) {
      newGrid.push({
        email: {
          value: '',
          error: ''
        },
        name: {
          value: '',
          error: ''
        }
      });
    }

    this.setState({
      isShowGrid: true,
      grid: newGrid,
      isValid: false,
      isBusy: false
    });
  };

  handleCellChange = (e: any) => {
    const cell = e.target;
    const row = cell.dataset.row;
    const field = cell.dataset.field;
    const value = cell.value.trim();

    const { grid } = this.state;
    const newGrid = [...grid];

    newGrid[row][field] = { ...grid[row][field], value };

    this.setState({
      grid: newGrid,
      isValid: this.validateGrid(newGrid)
    });
  };

  validateGrid = (grid: State['grid']) => {
    const emails: string[] = [];
    let isGridValid = true;

    const isIotMode = this.state.isIotMode;

    grid.map((row: any) => {

      if (!isIotMode) {
        const name = row.name.value;
        row.name.error = '';

        if (name) {
          if (!validator.isValidFullName(name)) {
            row.name.error = 'invalid';
            isGridValid = false;
          }
        } else {
          isGridValid = false;
        }
      }

      const email = row.email.value;
      row.email.error = '';

      if (email) {
        if (!isIotMode) {
          if (!validator.isValidEmail(email)) {
            row.email.error = 'invalid';
            isGridValid = false;
          }
        }
        if (emails.includes(email)) {
          row.email.error = 'not_unique';
          isGridValid = false;
        }
        emails.push(email);
      } else {
        isGridValid = false;
      }

    });

    return isGridValid;
  };

  generateCerts = async () => {
    this.setState({
      isBusy: true
    }, async () => {
      const group = this.state.group;
      const template = this.context.store.certMasterStore.template;
      const title = this.context.store.certMasterStore.title;

      let holders;

      if (this.state.isIotMode) {
        holders = this.state.grid.map((row: any) => {
          return {
            email: row.email.value + '_iot@credentia.me'
          };
        });
      } else {
        holders = this.state.grid.map((row: any) => {
          const [surname, name, secondName] = row.name.value.split(' ');
          return {
            name,
            surname,
            secondName,
            email: row.email.value
          };
        });
      }

      const newCert = {
        template: template._id,
        group,
        credentialSubject: {
          title,
        },
        holders
      }

      if (this.props.store.sessionStore.issuer && this.props.store.sessionStore.issuer.name === 'РТ-Техприемка') {
        // @ts-ignore
        newCert.credentialSubject.rtDemo1 = this.context.store.certMasterStore.rtDemo1
        // @ts-ignore
        newCert.credentialSubject.rtDemo2 = this.context.store.certMasterStore.rtDemo2
        // @ts-ignore
        newCert.credentialSubject.rtDemo3 = this.context.store.certMasterStore.rtDemo3
      }

      await this.context.store.certsStore.createCerts(newCert);

      this.props.history.push('/app/issuer/documents');
    });
  };

  render() {
    const { t, classes } = this.props;
    const { isShowGrid, isIotMode, amount, group, isShowNewGroupInput, grid, isValid, isBusy } = this.state;

    const existingGroups = this.context.store.certsStore.certificates
    .map((cert: any) => cert.group)
    // tslint:disable-next-line:no-shadowed-variable
    .filter((group: any) => group)
    // tslint:disable-next-line:no-shadowed-variable
    .reduce((unique: any, group: any) => {
      return unique.includes(group) ? unique : [...unique, group];
    }, []);

    return (
      <Layout pageTitle={t('third')}>
        <section className={classes.root}>

          {isShowGrid ? (
            <>

              {isIotMode
                ?
                <h2 className={classes.subtitle}>Отсканируйте QR-код</h2>
                :
                <h2 className={classes.subtitle}>{t('master.creating_holders_list')}</h2>
              }

              {isIotMode && typeof window !== 'undefined' &&
              <div className={classes.qrReaderContainer}>
                <QrReader
                  style={{ width: 240, height: 180 }}
                  delay={100}
                  onError={this.handleScanError}
                  onScan={this.handleScanResult}
                />
                <input
                  className={classes.qrScannedValue}
                  type='text'
                  data-field='name'
                  value={this.state.scanResult}
                  disabled={true}
                />
              </div>
              }

              <table className={classes.table}>
                <thead>
                {isIotMode
                  ?
                  <tr>
                    <th>&nbsp;</th>
                    <th>ID</th>
                  </tr>
                  :
                  <tr>
                    <th>&nbsp;</th>
                    <th>{t('master.name')}</th>
                    <th>{t('email')}</th>
                  </tr>
                }
                </thead>
                <tbody>
                {grid.map((row: any, i: number) =>
                  <tr key={i}>
                    <td>{i + 1}</td>

                    {!isIotMode &&
                    <td>
                      <input
                        type='text'
                        onBlur={this.handleCellChange}
                        data-row={i}
                        data-field='name'
                      />
                      {
                        row.name.error &&
                        <div className={classes.errorMessage}>
                          {t('master.holder_invalid_name')}
                        </div>
                      }
                    </td>
                    }

                    <td>
                      {isIotMode
                        ?
                        <input
                          type='text'
                          data-row={i}
                          data-field='email'
                          value={grid[i].email.value}
                          disabled={true}
                        />
                        :
                        <input
                          type='text'
                          data-row={i}
                          data-field='email'
                          onBlur={this.handleCellChange}
                        />
                      }
                      {
                        row.email.error &&
                        <div className={classes.errorMessage}>
                          {
                            row.email.error == 'not_unique'
                              ?
                              <div>
                                {isIotMode
                                  ?
                                  t('master.holder_not_unique_value')
                                  :
                                  t('master.holder_not_unique_email')
                                }
                              </div>
                              :
                              <div>
                                <div>{t('master.holder_invalid_email')}</div>
                                <div>{t('master.holder_email_example')}: <strong>user@server.ru</strong></div>
                              </div>
                          }
                        </div>
                      }
                    </td>
                  </tr>
                )}
                </tbody>
              </table>

              <button
                className={classes.button}
                type='button'
                onClick={this.generateCerts}
                disabled={!isValid || isBusy}
              >
                {!isBusy ? t('master.generate') : t('master.generation_in_process')}
              </button>
            </>
          ) : (
            <>
              <form>

                <h2>{t('master.certs_number_label')}</h2>
                <TextField
                  id='certs_number'
                  name='certs_number'
                  value={amount}
                  autoComplete='certificate_title'
                  autoFocus
                  onChange={this.setCertsNumber}/>

                <h2>{t('master.documents_group')}</h2>

                <select
                  className={classes.selector}
                  onChange={this.setGroupVariant}
                >
                  <option value=''>--- {t('master.no_group')} ---</option>
                  <option value='__NEW__'>--- {t('master.create_new_group')} ---</option>
                  {existingGroups.map((group: any) =>
                    <option key={group} value={group}>{group}</option>
                  )}
                </select>

                {
                  isShowNewGroupInput &&
                  <TextField
                    id='group'
                    name='group'
                    value={group}
                    labelText={t('master.enter_new_group_name')}
                    autoFocus
                    onChange={this.setGroupValue}/>
                }

                <button className={classes.button} type='button' onClick={this.createNewGrid}>
                  {t('next')}
                </button>
              </form>
            </>
          )}
        </section>
      </Layout>
    );
  }
}

const styles = createStyles({
  root: {
    width: '600px',
    margin: '0 auto'
  },
  subtitle: {
    margin: '0 0 20px',
    textAlign: 'center'
  },
  selector: {
    marginBottom: '1em',
    width: '100%',
    height: '36px'
  },
  table: {
    margin: '30px 0',
    width: '100%',
    padding: '10px',
    borderCollapse: 'collapse',
    '& th': {
      textAlign: 'center',
      fontWeight: 'bold'
    },
    '& th, & td': {
      width: '50%',
      padding: '7px 8px',
      '&:first-child': {
        width: '1%',
        lineHeight: '30px'
      }
    },
    '& tr': {
      '&:nth-child(2n) td': {
        backgroundColor: color.orange + '11'
      },
      '&:nth-child(2n+1) td': {
        backgroundColor: color.orange + '33'
      }
    },
    '& td': {
      textAlign: 'left',
      verticalAlign: 'top'
    },
    '& input': {
      extend: inputText
    }
  },
  errorMessage: {
    color: color.error,
    fontSize: '12px'
  },
  button: {
    extend: button,
    height: '49px',
    marginTop: '20px'
  },
  qrReaderContainer: {
    margin: '0 auto',
    width: 240,
    height: 180,
    backgroundColor: '#000'
  },
  qrScannedValue: {
    width: 240,
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  }
});

export default withRouter(
  withTranslation('common')(
    withStyles(styles)(
      inject('store')(observer(Holders))
    )
  )
);
