import { inject, MobXProviderContext, observer } from 'mobx-react';
import React, { Fragment }                       from 'react';
import loadable                                  from '@loadable/component';
import { WithStyles }                            from '@material-ui/core';
import { createStyles, withStyles }              from '@material-ui/styles';
import { withTranslation, WithTranslation }      from 'react-i18next';
import { withRouter, RouteComponentProps }       from 'react-router-dom';
import { Formik, FormikActions }                 from 'formik';

import Layout              from '../../../../components/layout';
import TextField           from '../../../../components/common/TextField';
import { button }          from '../../../../lib/sharedStyles';

interface IAsyncCertificateProps {
  title: string;
  company: string;
  template: string;
  date: string;
  documentNumber: string;
  name: string;
  surname: string;
  rtDemo1?: string;
  rtDemo2?: string;
  rtDemo3?: string;
}

const AsyncCertificate = loadable((props: IAsyncCertificateProps) => import(`../../../../components/templates/${props.company}/${props.template}`), {
  cacheKey: (props: IAsyncCertificateProps) => `${props.company}/${props.template}`
});

// tslint:disable-next-line:interface-name
interface Props extends WithTranslation, RouteComponentProps, WithStyles<typeof styles> {
  store: import('../../../../store').default;
}

// tslint:disable-next-line:interface-name
interface State {
  title?: string;
  rtDemo1?: string;
  rtDemo2?: string;
  rtDemo3?: string;
}

class Template extends React.Component<Props> {
  static contextType = MobXProviderContext;

  context!: React.ContextType<typeof MobXProviderContext>;

  handleSubmit = async (values: State, { setSubmitting, setStatus }: FormikActions<State>) => {
    const { history } = this.props;

    history.push('/app/issuer/master/fillHolders');
    setSubmitting(false);
  };

  validate = (values: State) => {
    const { t } = this.props;
    const errors: State = {};

    if (!values.title) {
      errors.title = t('login_errors.required');
    }

    this.context.store.certMasterStore.setTitle(values.title);

    this.context.store.certMasterStore.setRtDemo1(values.rtDemo1);
    this.context.store.certMasterStore.setRtDemo2(values.rtDemo2);
    this.context.store.certMasterStore.setRtDemo3(values.rtDemo3);

    return errors;
  };

  render() {
    const { t, classes, store } = this.props;

    const date = new Date();
    const dateString = date.toISOString();

    const certMasterStore = store.certMasterStore;
    const sessionStore = store.sessionStore;
    const issuer = sessionStore.issuer!;

    return (
      <Layout pageTitle={t('second')}>
        <div className={classes.content}>
          <div className={classes.formBlock}>
            <h2 className={classes.subtitle}>{t('master.fill_document')}</h2>
            <Formik
              initialValues={{
                title: certMasterStore.title,
                rtDemo1: '',
                rtDemo2: '',
                rtDemo3: ''
              }}
              isInitialValid={Boolean(certMasterStore.title)}
              validate={this.validate}
              onSubmit={this.handleSubmit}
            >
              {({
                  values,
                  errors,
                  touched,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isValid
                }) => (
                  <form onSubmit={handleSubmit}>
                    <TextField
                      classes={{ root: classes.input }}
                      id='title'
                      name='title'
                      value={values.title}
                      labelText={t('master.cert_title')}
                      errorMessage={touched.title && errors.title || undefined}
                      autoComplete='certificate_title'
                      autoFocus
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />

                    {/* rtDemo */}
                    {this.props.store.sessionStore.issuer && this.props.store.sessionStore.issuer.name === 'РТ-Техприемка' &&
                      <>
                        <TextField
                          classes={{ root: classes.input }}
                          id='rtDemo1'
                          name='rtDemo1'
                          value={values.rtDemo1}
                          labelText='Объём поставки'
                          errorMessage={touched.rtDemo1 && errors.rtDemo1 || undefined}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                        <TextField
                          classes={{ root: classes.input }}
                          id='rtDemo2'
                          name='rtDemo2'
                          value={values.rtDemo2}
                          labelText='Механические свойства'
                          errorMessage={touched.rtDemo2 && errors.rtDemo2 || undefined}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                        <TextField
                          classes={{ root: classes.input }}
                          id='rtDemo3'
                          name='rtDemo3'
                          value={values.rtDemo3}
                          labelText='Химический состав, %'
                          errorMessage={touched.rtDemo3 && errors.rtDemo3 || undefined}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                      </>
                    }

                    <div className={classes.tooltip}>
                      {t('master.fio_in_next_step')}
                    </div>
                    <button className={classes.button} type='submit' disabled={!isValid}>
                      {t('next')}
                    </button>
                  </form>)}
            </Formik>
          </div>
          <div className={classes.certificateBlock}>
            <AsyncCertificate
              company={issuer.shortName}
              template={certMasterStore.template!.name}
              title={certMasterStore.title}
              date={dateString}
              documentNumber='0000000'
              name={t('master.full_name_example')}
              surname=''
              rtDemo1={certMasterStore.rtDemo1}
              rtDemo2={certMasterStore.rtDemo2}
              rtDemo3={certMasterStore.rtDemo3}
            />
          </div>
        </div>
      </Layout>
    );
  }
}

const styles = createStyles({
  content: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'end',
    minWidth: '660px',
  },
  formBlock: {
    width: '40%',
    margin: '0 auto'
  },
  certificateBlock: {
    width: '50%',
    border: '1px solid #eee',
    boxShadow: '0 0 10px 0px #eee',
    overflow: 'hidden'
  },
  subtitle: {
    margin: '0 0 20px',
    textAlign: 'left'
  },
  input: {
    textAlign: 'left',
    marginBottom: '25px'
  },
  button: {
    extend: button,
    height: '49px',
    marginTop: '20px'
  },
  tooltip: {
    color: '#a29b9b;',
    fontSize: '12px',
    textAlign: 'left'
  }
});

export default withRouter(
  withTranslation('common')(
    withStyles(styles)(
      inject('store')(observer(Template))
    )
  )
);
