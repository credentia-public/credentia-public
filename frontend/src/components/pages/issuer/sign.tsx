import React, { Fragment }                       from 'react';
import { RouteComponentProps }                   from 'react-router-dom';
import { inject, MobXProviderContext, observer } from 'mobx-react';
import { WithStyles }                            from '@material-ui/core';
import { withStyles, createStyles }              from '@material-ui/styles';
import { withTranslation, WithTranslation }      from 'react-i18next';

import { button }               from '../../../lib/sharedStyles';
import Layout                   from '../../../components/layout';
import Back                     from '../../../components/common/Back';
import { getCertificateByCode } from '../../../api/certificate';
import Loader                   from '../../common/Loader';

// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, RouteComponentProps<{ id: string }>, WithStyles<typeof styles> {
  store: import('../../../store').default;
}

// tslint:disable-next-line:interface-name
export interface State {
  loading: boolean;
  cert?: import('../../../store/domain/certificates').ICertificate;
  data?: string;
}

class CertSign extends React.Component<Props, State> {
  static contextType = MobXProviderContext;

  // static async getInitialProps(ctx: IPageContext) {
  //   const code: string = ctx.query.code! as string;
  //   const { data: cert } = await getCertificateByCode(code);
  //
  //   return {
  //     code,
  //     cert,
  //     data: JSON.stringify(cert.credentialSubject)
  //   };
  // }

  context!: React.ContextType<typeof MobXProviderContext>;

  constructor(props: Props) {
    super(props);

    this.state = {
      loading: true
    };
  }

  async componentDidMount(): Promise<void> {
    const { match, history } = this.props;
    const id = match.params.id;

    try {
      const cert = await getCertificateByCode(id);

      if (!cert) {
        history.replace('/app/error/document-not-found');
      }

      this.setState({
        loading: false,
        cert,
        data: JSON.stringify(cert.credentialSubject)
      });
    } catch (err) {
      console.error(err);
      history.replace('/app/error/document-not-found');
    }

    [
      // 'https://www.cryptopro.ru/sites/default/files/products/cades/es6-promise.min.js',
      // 'https://www.cryptopro.ru/sites/default/files/products/cades/demopage/ie_eventlistner_polyfill.js',
      // 'window.allow_firefox_cadesplugin_async=1',
      'https://www.cryptopro.ru/sites/default/files/products/cades/cadesplugin_api.js',
      '/static/js/forsign.js'
    ].forEach(s => {
      const el = document.createElement('script');
      el.src = s;
      document.querySelector('head')!.appendChild(el);
    });
  }

  createSignature = async () => {
    const { t } = this.props;

    try {
      (window as any).createSignature();
    } catch (err) {
      alert(t('master.sign_error'));
    }
  };

  render() {
    if (this.state.loading) {
      return <Loader/>;
    }

    const cert = this.state.cert!;
    const { classes, t } = this.props;
    return (
      <Layout pageTitle={t('sign_document')} classes={{ content: classes.container }}>
        <Back to='/app/issuer/documents'/>

        <label htmlFor='source'>{t('data')}</label>
        <textarea className={classes.data}
                  id='source'
                  defaultValue={JSON.stringify(cert.credentialSubject, null, '\t')}
                  disabled={true}>
        </textarea>
        <label htmlFor='CertName'>{t('master.available_e_sig')}</label>
        <select id='CertName'/>
        <button className={classes.button} type='button' onClick={this.createSignature}>
          {t('sign')}
        </button>
        <label htmlFor='CertName'>{t('signature')}</label>
        <textarea className={classes.signature} id='signature' disabled={true}/>
      </Layout>
    );
  }
}

const styles = createStyles({

  container: {
    display: 'flex',
    flexDirection: 'column',
    margin: '0 auto 40px',
    width: '600px',
    '& label': {
      display: 'block',
      marginBottom: '10px',
      fontWeight: 'bold'
    },
    '& textarea': {
      '&:disabled': {
        backgroundColor: '#eee'
      }
    }
  },
  data: {
    marginBottom: '20px',
    height: '200px',
    resize: 'vertical'
  },
  button: {
    extend: button,
    height: '49px',
    margin: '20px 0'
  },
  signature: {
    resize: 'vertical'
  }
});

export default withTranslation('common')(
  withStyles(styles)(
    inject('store')(observer(CertSign))
  )
);
