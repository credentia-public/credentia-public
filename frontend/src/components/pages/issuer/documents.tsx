// @ts-nocheck
import { inject, MobXProviderContext, observer } from 'mobx-react';
import React, { Component }                      from 'react';
import { WithStyles }                            from '@material-ui/core';
import { createStyles, withStyles }              from '@material-ui/styles';
import classnames                                from 'classnames';
import { withTranslation, WithTranslation }      from 'react-i18next';
import { withRouter, Link }                      from 'react-router-dom';

import { button }       from '../../../lib/sharedStyles';
import { RequestState } from '../../../sharedTypes';
import utils            from '../../../lib/utils';
import Layout           from '../../../components/layout';
import Preloader        from '../../../components/common/Loader';
import Checkbox         from '../../../components/common/Checkbox';
import { sendCertCreationEmail } from '../../../api/certificate';

type Store = import('../../../store').default;
type ICertificate = import('../../../store/domain/certificates').ICertificate;
type ITemplate = import('../../../store/domain/templates').ITemplate;
import { Chains } from '../../../store/domain/issuer';


// tslint:disable-next-line:interface-name
interface Props extends WithTranslation, WithStyles<typeof styles> {
  store: Store;
}

// tslint:disable-next-line:interface-name
interface State {
  certStatus?: string;
  filter: {
    isActive: boolean;
    group: string;
    type: string;
  };
  selectedCertificates: Set<string>;
  minimizedGroups: any;
}

class Certificates extends Component<Props, State> {
  static contextType = MobXProviderContext;

  context!: React.ContextType<typeof MobXProviderContext>;

  constructor(props: any) {
    super(props);

    this.state = {
      certStatus: undefined,
      filter: {
        isActive: false,
        group: '__ANY__',
        type: '__ANY__'
      },
      selectedCertificates: new Set(),
      minimizedGroups: {}
    };
  }

  toggleFilter = () => {
    const wasActive = this.state.filter.isActive;
    if (!wasActive) {
      this.setState({
        filter: {
          ...this.state.filter,
          isActive: true
        }
      });
    } else {
      this.setState({
        filter: {
          isActive: false,
          group: '__ANY__',
          type: '__ANY__'
        }
      });
    }
  };

  selectFilterGroup = (e: any) => {
    const group = e.target.value;
    this.setState({
      filter: {
        ...this.state.filter,
        group
      }
    });
  };

  sendEmail = async (id: string) => {
    if (confirm('Вы действительно хотите отправить уведомление?')) {
      await sendCertCreationEmail({certId: id});
    }
  }
  
  selectFilterDocType = (e: any) => {
    const type = e.target.value;
    this.setState({
      filter: {
        ...this.state.filter,
        type
      }
    });
  };

  toggleCertSelection = (e: React.ChangeEvent<HTMLInputElement>) => {
    const id: string = e.currentTarget.dataset.id!;
    const certs = this.state.selectedCertificates;
    certs.has(id) ? certs.delete(id) : certs.add(id);

    this.setState({
      selectedCertificates: new Set(certs)
    });
  };

  minimizeGroup = (groupTitle: any) => {
    const prevMinimizedGroups = this.state.minimizedGroups;
    const previousValue = this.state.minimizedGroups[groupTitle];
    this.setState({
      minimizedGroups: {
        ...prevMinimizedGroups,
        [groupTitle]: !previousValue
      }
    });
  };

  componentDidMount(): void {
    this.context.store.certsStore.fetchCerts();
  }

  publishCerts = async () => {
    this.setState({
      certStatus: 'publishing'
    });

    const selectedCerts = this.state.selectedCertificates;
    await this.context.store.certsStore.publishCerts(Array.from(selectedCerts.values()));

    const error = this.context.store.certsStore.error;

    if (error) {
      this.setState({
        certStatus: 'error'
      });
    } else {
      this.setState({
        certStatus: undefined
      });
    }
  };

  deleteCerts = async () => {
    this.setState({
      certStatus: 'publishing'
    });

    const issuer = this.context.store.sessionStore.issuer;
    const selectedCerts = this.state.selectedCertificates;
    await this.context.store.certsStore.deleteCerts(issuer._id, Array.from(selectedCerts.values()));
    await this.context.store.certsStore.fetchCerts();

    const error = this.context.store.certsStore.error;

    if (error) {
      this.setState({
        certStatus: 'error'
      });
    } else {
      this.setState({
        certStatus: undefined
      });
    }
  };

  getDocsGroups = (docs: ICertificate[]) => {
    return [
      ...docs
      .map(doc => doc.group)
      .filter(group => group)
      .reduce((unique, group) => {
        return unique.includes(group) ? unique : [...unique, group];
      }, []),
      // show docs without group in the end of the list
      ...docs.map(doc => doc.group).filter(group => !group).length ? [''] : []
    ];
  };

  getDocsTypes = (docs: any) => {
    return docs
    .map(doc => doc.template.name)
    .filter(type => type)
    .reduce((unique, type) => {
      return unique.includes(type) ? unique : [...unique, type];
    }, []);
  };

  groupedDocs = (docs: ICertificate[]) => {
    const groups = this.getDocsGroups(docs);

    return groups.map(group => {
      return {
        title: group,
        docs: docs.filter(doc => group ? doc.group === group : !doc.group)
      };
    });
  };

  render() {
    const { t, classes, store } = this.props;
    const { certStatus, filter, selectedCertificates, minimizedGroups } = this.state;
    const certsStore = store.certsStore;
    const sessionStore = store.sessionStore;
    const issuer = this.context.store.sessionStore.issuer;

    const isLoading = certsStore.state === RequestState.Pending;
    const isLoaded = certsStore.state === RequestState.Done;

    const isDocuments = certsStore.certificates && certsStore.certificates.length > 0;

    const allDocs = certsStore.certificates ? certsStore.certificates : [];

    const allGroups = this.getDocsGroups(allDocs);
    const allDocTypes = this.getDocsTypes(allDocs);

    const allGroupedDocs = this.groupedDocs(allDocs);
    const allDocsGroupsLengths = allGroupedDocs.reduce((acc, group) => {
      acc[group.title] = group.docs.length;
      return acc;
    }, {});

    let groupedDocsToShow;

    const allDocsNumber = allDocs.length;
    let filteredDocsNumber;

    if (!filter.isActive) {
      groupedDocsToShow = allGroupedDocs;
      filteredDocsNumber = allDocsNumber;
    } else {
      let filteredDocs = allDocs;
      if (filter.group !== '__ANY__') {
        filteredDocs = filteredDocs.filter(doc => {
          return filter.group ? doc.group === filter.group : !doc.group;
        });
      }
      if (filter.type !== '__ANY__') {
        filteredDocs = filteredDocs.filter(doc => doc.template.name === filter.type);
      }
      groupedDocsToShow = this.groupedDocs(filteredDocs);
      filteredDocsNumber = filteredDocs.length;
    }

    return (
      <Layout>
        <h1 className={classes.pageTitle}>{t('issuer.docs')}</h1>

        <section className={classes.root}>

          {issuer && issuer.chain &&
          <div className={classes.chain}>
            <img className={classes.ethLogo} src='/static/images/ethereum.svg' />
            {issuer.chain == Chains.TestNetOld &&
            <span className={classes.ethChainName}>ethereum testnet / smart contract v1</span>
            }
            {issuer.chain == Chains.TestNet &&
            <span className={classes.ethChainName}>ethereum testnet / smart contract v2</span>
            }
            {issuer.chain == Chains.MainNet &&
            <span className={classes.ethChainName}>ethereum mainnet / smart contract v2</span>
            }
          </div>
          }

          {isLoading && <Preloader/>}

          {isLoaded && isDocuments &&
          <div className={classes.feed}>
            <div className={classes.topControls}>

              <div className={classes.topControlsButtons}>
                <div
                  className={classnames(classes.openFilter, filter.isActive ? 'active' : null)}
                  title={t('filter.tip')}
                  onClick={this.toggleFilter}
                >
                  <img src='/static/images/filter.svg' alt={t('open_certificate_master')}/>
                </div>
                <button className={classnames(classes.publishButton, {
                    hide: selectedCertificates.size === 0
                  })}
                  disabled={certStatus === 'publishing'}
                  onClick={this.publishCerts}
                  type='button'
                >
                  {t('publish')}
                </button>
                <button className={classnames(classes.publishButton, {
                  hide: selectedCertificates.size === 0
                })}
                  disabled={certStatus === 'publishing'}
                  onClick={this.deleteCerts}
                  type='button'
                >
                  {t('delete')}
                </button>
                <Link to='/app/issuer/master/chooseTemplate'>
                  <div
                    className={classnames(classes.openMaster, classes.openMasterTop)}
                    title={t('create_document')}
                  >
                    <img src='/static/images/plus.svg' alt={t('open_certificate_master')}/>
                  </div>
                </Link>
              </div>

              {filter.isActive &&
              <div className={classes.filter}>
                <div className={classes.filterConnection}></div>
                <div className={classes.filterMode}>
                  <div className={classes.filterModeTitle}>{t('filter.by_group')}:</div>
                  <select onChange={this.selectFilterGroup}>
                    <option value='__ANY__'>--- {t('filter.any_group')} ---</option>
                    <option value=''>--- {t('filter.without_group')} ---</option>
                    {allGroups.filter(group => group).map(group =>
                    <option key={group} value={group}>{group}</option>
                    )}
                  </select>
                </div>
                <div className={classes.filterMode}>
                  <div className={classes.filterModeTitle}>{t('filter.by_doc_type')}:</div>
                  <select onChange={this.selectFilterDocType}>
                    <option value='__ANY__'>--- {t('filter.any_doc_type')} ---</option>
                    {allDocTypes.map(docType =>
                    <option key={docType} value={docType}>{utils.getLocalDocType(docType, t)}</option>
                    )}
                  </select>
                </div>
                <div>
                  <div className={classes.filteredDocsTitle}>
                    <span>{t('filter.results')}:</span>
                    {' '}
                    <span className={classes.filteredDocsCounter}>({filteredDocsNumber}/{allDocsNumber})</span>
                  </div>
                  
                  {filteredDocsNumber == 0 &&
                  <div className={classes.filterNoResults}>{t('filter.no_results')}</div>
                  }
                </div>
              </div>
              }

            </div>

            <div className={classes.documents}>
              {groupedDocsToShow.map(group => (
                <div className={classnames(classes.group, group.title ? 'titled' : null)} key={group.title}>
                  {
                    group.title &&
                    <div className={classes.groupTitle}>
                      <span className={classes.groupItemsCounter}>
                        {
                          filter.isActive
                            ?
                            <span>({group.docs.length}/{allDocsGroupsLengths[group.title]})</span>
                            :
                            <span>({group.docs.length})</span>
                        }
                      </span>
                      <img className={classes.groupIcon} src='/static/images/group.svg'/>
                      <span>{group.title}</span>
                      <div className={classnames(classes.groupMinimizer, {
                        active: minimizedGroups[group.title]
                      })}
                           onClick={() => {
                             this.minimizeGroup(group.title);
                           }}/>
                    </div>
                  }
                  {
                    !minimizedGroups[group.title] &&
                    group.docs.map((cert: ICertificate) => {

                      const { credentialSubject, shareableLinkCode } = cert;
                      const { title, surname, name, secondName, email, cloudFiles } = credentialSubject;
                      const template = cert.template as ITemplate;

                      let fullName = [surname, name, secondName].filter(part => part).join(' ');
                      
                      if (!fullName && email.toString().includes('_iot@credentia.me')) {
                        fullName = email.toString().replace('_iot@credentia.me', '');
                      }

                      const localDocType = utils.getLocalDocType(template.name);

                      const certUri = `/app/document/${shareableLinkCode}`;
                      const signUri = `/app/document/${shareableLinkCode}/sign`;
                      const attachmentsUri = `/app/document/${shareableLinkCode}/files`;

                      const isSigned = cert.proof;
                      const isPublished = cert.txId;

                      return (
                        <div className={classes.document} key={shareableLinkCode}>
                          <div className={classnames(classes.documentCheckbox, {
                            hide: cert.txId
                          })}>
                            {cert.holder && <Checkbox
                              data-id={cert._id}
                              onChange={this.toggleCertSelection}
                              checked={selectedCertificates.has(cert._id)}/>}
                          </div>
                          <div className={classes.documentLeft}>
                            <div className={classes.documentTitle} title={title}>
                              {title}
                            </div>
                            <div className={classes.documentFullName} title={fullName}>
                              {
                                fullName.includes('0x') && fullName.length > 10
                                ?
                                `${fullName.substr(0, 6)}...${fullName.substr(-4)}`
                                :
                                fullName
                              }
                            </div>
                            <div className={classes.documentType}>{localDocType}</div>
                            <Link to="#" onClick={()=>{this.sendEmail(cert._id)}}>
                                <div>Отправить уведомление</div>
                            </Link>
                          </div>
                          <div className={classes.documentRight}>
                            <div className={classes.attachmentsStatus}>
                              {
                                cloudFiles && cloudFiles.length
                                  ?
                                  <div>{t('attachments')}: {cloudFiles.length}</div>
                                  :
                                  <div className={classes.noAttachments}>{t('no_attachments')}</div>
                              }
                              <Link to={attachmentsUri}>
                                <div>{t('change')}</div>
                              </Link>
                            </div>
                            <div className={classes.signStatus}>
                              {
                                isSigned
                                  ?
                                  <span>{t('signed')}</span>
                                  :
                                  <div>
                                    <div>{t('not_signed')}</div>
                                    <Link to={signUri}>
                                      <span>{t('sign')}</span>
                                    </Link>
                                  </div>
                              }
                            </div>
                            <div className={classes.blockchainStatus}>
                              {
                                isPublished
                                  ?
                                  <div>{/*t('published_on_blockchain')*/t('published_anywhere')}</div>
                                  :
                                  <div>{/*t('not_published_on_blockchain')*/t('not_published_anywhere')}</div>
                              }
                            </div>
                          </div>
                          <Link to={certUri} className={classes.documentLink}>
                            <div className={classes.goSign}>&#8250;</div>
                          </Link>
                        </div>);
                    })
                  }
                </div>
              ))}

            </div>
          </div>
          }

          {isLoaded && !isDocuments &&
          <div className={classes.noDocs}>
            <div className={classes.noDocsMessage}>{t('master.no_certs')} <span className='plus-sign-entity'>&#8853;</span></div>
            <Link to='/app/issuer/master/chooseTemplate'>
              <div
                className={classnames(classes.openMaster, classes.openMasterBottom)}
                title={t('create_document')}>
                <img src='/static/images/plus.svg' alt={t('open_certificate_master')}/>
              </div>
            </Link>
          </div>
          }

        </section>
      </Layout>
    );
  }
}

const styles = createStyles({
  root: {
    maxWidth: '600px',
    margin: '0 auto'
  },
  pageTitle: {
    margin: '0 0 0.5em',
    fontSize: '2.5em',
    textAlign: 'center',
    '@media (max-width: 800px)': {
      fontSize: '1.8em'
    }
  },
  chain: {
    marginBottom: '1em',
    textAlign: 'center',
    fontSize: '13px',
    opacity: 0.6
  },
  ethLogo: {
    height: '1em',
    verticalAlign: 'middle'
  },
  ethChainName: {
    verticalAlign: 'middle'
  },
  error: {
    color: 'red'
  },
  feed: {
    position: 'relative',
    zIndex: 0
  },
  topControls: {
    position: 'sticky',
    top: 0,
    zIndex: 1,
    backgroundColor: '#fff',
    boxShadow: '0px 0px 6px 4px white',
    padding: '10px 0'
  },
  topControlsButtons: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  publishButton: {
    extend: button,
    width: '150px'
  },
  openFilter: {
    width: '50px',
    height: '50px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    border: '1px solid #FF9356',
    borderRadius: '50%',
    cursor: 'pointer',
    userSelect: 'none',
    '&.active': {
      backgroundColor: '#FF9356'
    },
    '& img': {
      width: '20px',
      transform: 'translateY(2px)'
    }
  },
  filter: {
    marginTop: '20px',
    border: '2px solid #FF9356',
    borderRadius: '5px',
    padding: '10px',
    position: 'relative',
    '& select': {
      width: '100%',
      height: '30px'
    }
  },
  filterConnection: {
    position: 'absolute',
    top: '-22px',
    left: '22px',
    height: '20px',
    width: '2px',
    backgroundColor: '#FF9356'
  },
  filterMode: {
    marginBottom: '0.5em',
    '&:last-child': {
      marginBottom: 0
    }
  },
  filterModeTitle: {
    marginBottom: '0.3em'
  },
  group: {
    marginBottom: '1.5em',
    '&.titled': {
      border: '1px solid #FF9356',
      borderRadius: '3px',
      padding: '2px',
      backgroundColor: '#FF935611'
    }
  },
  groupTitle: {
    textAlign: 'center',
    fontWeight: 'bold',
    margin: '1em 0',
    padding: '0 40px 0 20px',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    position: 'relative'
  },
  groupIcon: {
    width: '20px',
    opacity: 0.6,
    marginRight: '6px',
    transform: 'translateY(4px)'
  },
  groupItemsCounter: {
    marginRight: '0.4em',
    color: '#FF9356'
  },
  groupMinimizer: {
    position: 'absolute',
    top: '2px',
    right: '10px',
    width: '22px',
    height: '22px',
    backgroundImage: 'url(/static/images/eye-opened.svg)',
    backgroundRepeat: 'no-repeat',
    backgroundSize: '100%',
    backgroundPosition: 'center center',
    opacity: 0.6,
    cursor: 'pointer',
    userSelect: 'none',
    '&.active': {
      backgroundImage: 'url(/static/images/eye-closed.svg)'
    }
  },
  filteredDocsTitle: {
    textAlign: 'center'
  },
  filteredDocsCounter: {
    verticalAlign: 'middle',
    fontSize: '16px',
    opacity: 0.4
  },
  filterNoResults: {
    textAlign: 'center'
  },
  documents: {
    marginTop: '15px'
  },
  document: {
    display: 'flex',
    marginBottom: '10px',
    border: '1px solid #8f94a4',
    borderRadius: '5px',
    backgroundColor: '#f9f9f9'
  },
  documentCheckbox: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 5px 0 10px;'
  },
  documentLeft: {
    width: '50%',
    padding: '5px',
    '@media (min-width: 680px)': {
      width: '40%'
    }
  },
  documentTitle: {
    marginBottom: '0.3em',
    fontSize: '14px',
    fontWeight: '600',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    display: '-webkit-box',
    lineClamp: 2,
    boxOrient: 'vertical'
  },
  documentFullName: {
    fontSize: '16px',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    display: '-webkit-box',
    lineClamp: 3,
    boxOrient: 'vertical'
  },
  documentType: {
    marginTop: '6px',
    fontSize: '11px',
    fontStyle: 'italic',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    display: '-webkit-box',
    lineClamp: 1,
    boxOrient: 'vertical'
  },
  documentRight: {
    display: 'block',
    width: '50%',
    borderLeft: '1px solid #eee',
    padding: '5px',
    fontSize: '13px',
    '@media (min-width: 680px)': {
      display: 'flex',
      width: '60%'
    }
  },
  attachmentsStatus: {
    marginBottom: '12px',
    '@media (min-width: 680px)': {
      width: '35%',
      paddingRight: '5px'
    }
  },
  signStatus: {
    marginBottom: '12px',
    '@media (min-width: 680px)': {
      width: '30%',
      paddingRight: '5px'
    }
  },
  blockchainStatus: {
    marginBottom: '12px',
    '@media (min-width: 680px)': {
      width: '35%'
    }
  },
  blockchainPublish: {},
  noAttachments: {
    opacity: 0.5
  },
  documentLink: {
    width: '40px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ddd',
    textDecoration: 'none',
    color: 'inherit !important',
    '&:hover': {
      backgroundColor: '#ccc'
    }
  },
  goSign: {
    fontSize: '24px',
    lineHeight: '24px',
    opacity: '0.4'
  },
  openMaster: {
    marginLeft: 'auto',
    width: '50px',
    height: '50px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '50%',
    backgroundColor: '#FF9356',
    cursor: 'pointer',
    userSelect: 'none'
  },
  noDocs: {
    marginTop: '2em'
  },
  openMasterNoDocuments: {
    marginRight: 'auto'
  },
  openMasterBottom: {
    marginTop: '20px'
  }
});

export default withRouter(
  withTranslation('common')(
    withStyles(styles)(
      inject('store')(observer(Certificates))
    )
  )
);
