// @ts-nocheck
import React                        from 'react';
import { createStyles, withStyles } from '@material-ui/styles';
import { withTranslation }          from 'react-i18next';

import Layout from '../layout';

function Error({ statusCode = 404, classes, t }) {
  return (
    <Layout pageTitle={statusCode}>
      {
        statusCode === 401 &&
        <p className={classes.message}>{t('app_error.not_authenticated')}</p>
      }
      {
        statusCode === 403 &&
        <p className={classes.message}>{t('app_error.not_authenticated')}</p>
      }
      {
        statusCode === 404 &&
        <p className={classes.message}>{t('app_error.page_not_found')}</p>
      }
      {
        statusCode === 503 &&
        <p className={classes.message}>{t('app_error.service_unavailable')}</p>
      }
    </Layout>
  );
}

Error.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return { statusCode };
};

const styles = createStyles({
  message: {
    textAlign: 'center'
  }
});

export default withTranslation('common')(
  withStyles(styles)(Error)
);
