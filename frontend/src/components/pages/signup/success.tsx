import React                        from 'react';
import { MobXProviderContext }      from 'mobx-react';
import { WithStyles }               from '@material-ui/core';
import { createStyles, withStyles } from '@material-ui/styles';
import { withTranslation, WithTranslation } from 'react-i18next';
import { Link, withRouter, RouteComponentProps }                             from 'react-router-dom';

import Layout              from '../../../components/layout';

// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, RouteComponentProps, WithStyles<typeof styles> {
}

class SignUpSuccess extends React.Component<Props> {
  static contextType = MobXProviderContext;

  email: string | null;

  constructor(props: any) {
    super(props);

    const query = new URLSearchParams(this.props.location.search);

    this.email = query.get('email');
  }

  render() {
    const { classes, t } = this.props;

    return (
      <Layout showHeader={false} centered={true}>
        <div className={classes.root}>
          <p>
            {t('auth.check_email')} {' '}
            <span className={classes.email}>{this.email}</span> {' '}
            ({t('auth.possibly_spam_folder')}).
          </p>
          <p>
            {t('auth.follow_and_login')} {' '}
            <Link to='/app/login'>
              {t('auth.here')}
            </Link>.
          </p>
        </div>
      </Layout>
    );
  }
}

const styles = createStyles({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center'
  },
  email: {
    fontWeight: 'bold'
  }
});

export default withRouter(
  withTranslation('common')(
    withStyles(styles)(SignUpSuccess)
  )
);
