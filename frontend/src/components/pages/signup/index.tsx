import React                                from 'react';
import { MobXProviderContext }              from 'mobx-react';
import { WithStyles }                       from '@material-ui/core';
import { createStyles, withStyles }         from '@material-ui/styles';
import classnames                           from 'classnames';
import { Formik, FormikActions }            from 'formik';
import { withTranslation, WithTranslation } from 'react-i18next';
import { withRouter, RouteComponentProps }  from 'react-router-dom';

import Layout                               from '../../layout';
import TextField                            from '../../common/TextField';
import Checkbox                             from '../../common/Checkbox';
import { button }                           from '../../../lib/sharedStyles';
import { ErrorMessages, UserErrorMessages } from '../../../lib/errors';
import validator                            from '../../../lib/validator';
// import { FormErrors }      from '../../lib/errors';

// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, RouteComponentProps, WithStyles<typeof styles> {
}

// tslint:disable-next-line:interface-name no-empty-interface
export interface State {
  email?: string;
  password?: string;
  name?: string;
  surname?: string;
  secondName?: string;
  documentRegNumber?: string;
  privacy: boolean;
  issuer: string | null;
}

// tslint:disable-next-line:interface-name
export interface FormErrors {
  email?: string;
  password?: string;
  privacy?: string;
}

class SignUp extends React.Component<Props, State> {
  static contextType = MobXProviderContext;

  context!: React.ContextType<typeof MobXProviderContext>;

  constructor(props: any) {
    super(props);

    const query = new URLSearchParams(props.location.search);
    const issuer = query.get('issuer');

    this.state = {
      email: '',
      password: '',
      name: '',
      surname: '',
      secondName: '',
      documentRegNumber: '',
      privacy: false,
      issuer
    };
  }

  signup = async (values: State, { setSubmitting, setStatus }: FormikActions<State>) => {
    const { issuer } = this.state;
    const { history } = this.props;
    const { name, surname, secondName, documentRegNumber, email, password, privacy } = values;
    const sessionStore = this.context.store.sessionStore;

    try {
      await sessionStore.signup({
        userType: 'student',
        issuer,
        name,
        surname,
        secondName,
        documentRegNumber,
        email,
        password,
        privacy
      });
      history.replace(`/app/signup/success?email=${email}`);
    } catch (err) {
      setStatus(err.message);
    } finally {
      setSubmitting(false);
    }
  };

  validate = (values: State) => {
    const { issuer } = this.state;
    const { t } = this.props;
    const errors: FormErrors = {};
    const requiredFields = [
      'email',
      'password'
    ];

    if (issuer === 'pgu') {
      requiredFields.push('name', 'surname', 'secondName', 'documentRegNumber');
    }

    requiredFields.forEach(f => {
      // @ts-ignore
      if (!values[f]) {
        // @ts-ignore
        errors[f] = t('form_errors.required');
      }
    });

    if (!validator.isValidEmail(values.email)) {
      errors.email = t('form_errors.invalid_email_address');
    }

    if (!values.privacy) {
      errors.privacy = t('form_errors.required');
    }

    return errors;
  };

  render() {
    const { issuer } = this.state;
    const { classes, t, history } = this.props;

    let formMessage: { type: any; text: any; };

    return (
      <Layout showHeader={false} centered={true}>
        <div className={classes.root}>
          <img className={classes.logo} src='/static/images/logo.svg' alt={t('credentia_logo')}/>
          <span className={classes.title}>{t('auth.sign_up')}</span>
          <Formik onSubmit={this.signup} initialValues={this.state} validate={this.validate}>
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                isValid,
                status
                /* and other goodies */
              }) => {

              const formErrors: any = {
                [UserErrorMessages.AlreadyExist]: t('form_errors.already_exist'),
                [ErrorMessages.NoMatchedDocuments]: t('form_errors.no_matched_documents')
              };

              if (status && formErrors[status]) {
                formMessage = {
                  type: 'error',
                  text: formErrors[status]
                };
              }

              return (
                <form className={classes.form} onSubmit={handleSubmit}>
                  {
                    formMessage &&
                    <div className={classnames(classes.formMessage, formMessage.type)}>{formMessage.text}</div>
                  }
                  {
                    issuer === 'pgu' &&
                    <TextField
                      classes={{ root: classes.input }}
                      id='name'
                      name='name'
                      type='text'
                      value={values.name}
                      labelText={t('name')}
                      errorMessage={touched.name && errors.name || undefined}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  }
                  {
                    issuer === 'pgu' &&
                    <TextField
                      classes={{ root: classes.input }}
                      id='surname'
                      name='surname'
                      type='text'
                      value={values.surname}
                      labelText={t('surname')}
                      errorMessage={touched.surname && errors.surname
                      || undefined}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  }
                  {
                    issuer === 'pgu' &&
                    <TextField
                      classes={{ root: classes.input }}
                      id='secondName'
                      name='secondName'
                      type='text'
                      value={values.secondName}
                      labelText={t('second_name')}
                      errorMessage={touched.secondName && errors.secondName || undefined}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  }
                  {
                    issuer === 'pgu' &&
                    <TextField
                      classes={{ root: classes.input }}
                      id='documentRegNumber'
                      name='documentRegNumber'
                      value={values.documentRegNumber}
                      labelText={t('master.diploma_number')}
                      errorMessage={touched.documentRegNumber && errors.documentRegNumber || undefined}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  }
                  <TextField
                    classes={{ root: classes.input }}
                    id='email'
                    name='email'
                    value={values.email}
                    labelText={t('auth.email_address')}
                    errorMessage={touched.email && errors.email || undefined}
                    autoComplete='email'
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <TextField
                    classes={{ root: classes.secondInput }}
                    id='password'
                    name='password'
                    type='password'
                    value={values.password}
                    labelText={t('auth.password')}
                    errorMessage={touched.password && errors.password || undefined}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <div className={classes.bottomContent}>
                    <div className={classes.privacy}>
                      <Checkbox
                        classes={{ root: classes.privacyCheckbox }}
                        id='privacy'
                        name='privacy'
                        onChange={handleChange}
                        onBlur={handleBlur}
                        checked={values.privacy}
                      />
                      <label className={classes.privacyText} htmlFor='privacy'>{t('auth.privacy')}</label>
                    </div>
                    <button
                      className={classes.submit}
                      type='submit'
                      disabled={isSubmitting || !isValid}
                    >
                      {t('auth.sign_up')}
                    </button>
                    <a className={classes.secondaryVariant} href='/app/login'>
                      {t('auth.sign_in')}
                    </a>
                    <a className={classes.secondaryVariant} href='https://esia.gosuslugi.ru/'>
                      <span>{t('auth.sign_in_with')}</span>
                      <img className={classes.gosuslugiLogo} src='/static/images/gosuslugi.svg'/>
                    </a>
                  </div>
                </form>
              );
            }}
          </Formik>
        </div>
      </Layout>
    );
  }
}

const styles = createStyles({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  bottomContent: {
    display: 'block'
  },
  buttonsInLine: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  logo: {
    display: 'block',
    width: '50px',
    height: '50px',
    marginBottom: '20px'
  },
  title: {
    marginBottom: '10px',
    fontSize: '20px'
  },
  form: {
    width: '100%',
    maxWidth: '400px'
  },
  formMessage: {
    margin: '2em 0',
    border: '1px solid #bbb',
    borderRadius: '8px',
    backgroundColor: '#eee',
    backgroundPosition: '15px 15px',
    backgroundSize: '20px',
    backgroundRepeat: 'no-repeat',
    padding: '15px 15px 15px 45px',
    '&.error': {
      border: '1px solid #FC605C',
      backgroundColor: '#FC605C22',
      backgroundImage: 'url(/static/images/error.svg)',
      color: '#FC605C'
    },
    '&.success': {
      border: '1px solid #34c749',
      backgroundColor: '#34c74911',
      backgroundImage: 'url(/static/images/success.svg)',
      color: '#34c749'
    }
  },
  input: {
    marginBottom: '30px'
  },
  secondInput: {
    marginBottom: '10px'
  },
  submit: {
    extend: button,
    height: '49px',
    marginBottom: '8px',
    '&:hover': {
      opacity: 0.9
    }
  },
  privacy: {
    display: 'flex',
    marginTop: '30px',
    marginBottom: '30px',
    alignItems: 'center'
  },
  privacyCheckbox: {
    flexShrink: 0,
    marginRight: '10px'
  },
  privacyText: {
    fontSize: '13px'
  },
  secondaryVariant: {
    display: 'block',
    textDecoration: 'none',
    width: '60%',
    margin: '0 auto',
    height: '40px',
    lineHeight: '40px',
    borderRadius: '8px',
    textAlign: 'center',
    fontSize: '13px',
    color: '#605b5b',
    cursor: 'pointer',
    userSelect: 'none',
    '&:hover': {
      backgroundColor: '#F6F6F8'
    }
  },
  gosuslugiLogo: {
    display: 'inline-block',
    width: '66px',
    marginLeft: '5px',
    transform: 'translateY(3px)'
  },
});

export default withRouter(
  withTranslation('common')(
    withStyles(styles)(SignUp)
  )
);
