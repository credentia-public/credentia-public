import React, { Component }          from 'react';
import { WithStyles }               from '@material-ui/core';
import { createStyles, withStyles } from '@material-ui/styles';
import { Formik, FormikActions }    from 'formik';
import classnames                   from 'classnames';
import { withTranslation, WithTranslation } from 'react-i18next';
import { withRouter, RouteComponentProps }                             from 'react-router-dom';

import {
  button,
  formMessage as formMessageStyles
}                            from '../../../lib/sharedStyles';
import { ErrorMessages }     from '../../../lib/errors';
import Layout                from '../../../components/layout';
import TextField             from '../../../components/common/TextField';
import Back                  from '../../../components/common/Back';
import { changePassword }    from '../../../api/user';

// tslint:disable-next-line:interface-name
interface Props extends WithTranslation, RouteComponentProps, WithStyles<typeof styles> {
}

// tslint:disable-next-line:interface-name no-empty-interface
export interface State {
  oldPassword?: string;
  newPassword?: string;
}

class SettingsPassword extends Component<Props, State> {

  constructor(props: Props) {
    super(props);

    this.state = {
      oldPassword: '',
      newPassword: ''
    };

  }

  validate = (values: State) => {
    const { t } = this.props;
    const errors: State = {};

    if (!values.oldPassword) {
      errors.oldPassword = t('form_errors.required');
    }

    if (!values.newPassword) {
      errors.newPassword = t('form_errors.required');
    }

    return errors;
  };

  changePassword = async (values: State, { setSubmitting, setStatus }: FormikActions<State>) => {
    const { history } = this.props;
    const { oldPassword = '', newPassword = '' } = values;

    try {
      const answer = await changePassword({
        oldPassword,
        newPassword
      });
      history.replace('/app/login?password_change=success');
    } catch (err) {
      setStatus(err.message);
    } finally {
      setSubmitting(false);
    }
  };

  render() {
    const { t, classes } = this.props;

    let formMessage: any;

    return (
      <Layout pageTitle={t('settings.password.title')}>
        <div className={classes.settings}>
          <Back to='/app/settings'/>
          <Formik onSubmit={this.changePassword} initialValues={this.state} validate={this.validate}>
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                isValid,
                status
                /* and other goodies */
              }) => {

            const formErrors: any = {
              [ErrorMessages.OperationNotAllowed]: t('settings.password.invalid_old')
            };

            if (status && formErrors[status]) {
              formMessage = {
                type: 'error',
                text: formErrors[status]
              };
            }

            return (
              <form onSubmit={handleSubmit}>
                {
                  formMessage &&
                  <div className={classnames(classes.formMessage, formMessage.type)}>{formMessage.text}</div>
                }
                <TextField
                  type='password'
                  classes={{ root: classes.input }}
                  id='oldPassword'
                  name='oldPassword'
                  value={values.oldPassword}
                  labelText={t('settings.password.old')}
                  errorMessage={touched.oldPassword && errors.oldPassword || undefined}
                  autoComplete='password'
                  autoFocus
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <TextField
                  type='password'
                  classes={{ root: classes.input }}
                  id='newPassword'
                  name='newPassword'
                  value={values.newPassword}
                  labelText={t('settings.password.new')}
                  errorMessage={(touched.newPassword && errors.newPassword) || undefined}
                  autoComplete='password'
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <button className={classes.submit} type='submit' disabled={isSubmitting || !isValid}>
                  {t('settings.password.change')}
                </button>
              </form>
            );}}
          </Formik>
        </div>
      </Layout>
    );
  }
}

const styles = createStyles({
  settings: {
    margin: '0 auto',
    maxWidth: '600px'
  },
  input: {
    marginBottom: '30px'
  },
  formMessage: {
    extend: formMessageStyles,
  },
  submit: {
    extend: button,
    height: '49px',
    marginBottom: '8px',
  },
});

export default withRouter(
  withTranslation('common')(
    withStyles(styles)(SettingsPassword)
  )
);
