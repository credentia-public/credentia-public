import React, { Component }                 from 'react';
import { WithStyles }                       from '@material-ui/core';
import { createStyles, withStyles }         from '@material-ui/styles';
import { withTranslation, WithTranslation } from 'react-i18next';
import { Link }                             from 'react-router-dom';

import { color, panel } from '../../../lib/sharedStyles';
import Layout           from '../../../components/layout';
import Back             from '../../../components/common/Back';

// tslint:disable-next-line:interface-name
interface Props extends WithTranslation, WithStyles<typeof styles> {
}

class Settings extends Component<Props> {
  render() {
    const { t, classes } = this.props;

    return (
      <Layout pageTitle={t('settings.title')}>
        <div className={classes.settings}>
          <Back to='/'/>

          <div className={classes.settingsMenu}>
            <h2 className={classes.settingsMenuHeader}>{t('settings.account')}</h2>
            <div className={classes.settingsMenuItems}>
              <Link to='/app/settings/password' className={classes.settingsMenuLink}>
                {t('settings.password.title')}
              </Link>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

const styles = createStyles({
  settings: {
    margin: '0 auto',
    maxWidth: '600px'
  },
  settingsMenu: {
    extend: panel
  },
  settingsMenuHeader: {
    margin: '0 0.5em',
    padding: '0.5em 0',
    textAlign: 'center'
  },
  settingsMenuItems: {
    paddingBottom: '10px'
  },
  settingsMenuLink: {
    display: 'block',
    padding: '0.7em 2em 0.7em 1em',
    color: color.darkBlue,
    backgroundColor: 'white',
    borderTop: '1px solid #ccc',
    borderBottom: '1px solid #ccc',
    textDecoration: 'none',
    backgroundImage: 'url(/static/images/arrow-right.svg)',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: '96% center',
    backgroundSize: '10px',
    '&:hover': {
      backgroundColor: color.orange + '33'
    }
  }
});

export default withTranslation('common')(
  withStyles(styles)(Settings)
);
