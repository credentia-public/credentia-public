import React, { Component }                 from 'react';
import { WithStyles }                       from '@material-ui/core';
import { createStyles, withStyles }         from '@material-ui/styles';
import loadable                             from '@loadable/component';
import { withTranslation, WithTranslation } from 'react-i18next';
import { withRouter, RouteComponentProps }  from 'react-router-dom';

import env from '../../../environments/environment';
import { getCertificateByCode } from '../../../api/certificate';
import { ITemplate } from '../../../store/domain/templates';


type ICertificate = import('../../../store/domain/certificates').ICertificate;
type IIssuer = import('../../../store/domain/issuer').IIssuer;

interface IAsyncCertificateProps {
  company: string;
  template: string;
  sign?: string;
  date?: string;
}

const AsyncCertificate = loadable((props: IAsyncCertificateProps) => import(`../../../components/templates/${props.company}/${props.template}`), {
  cacheKey: (props: IAsyncCertificateProps) => `${props.company}/${props.template}`
});

// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, RouteComponentProps<{ id: string }>, WithStyles<typeof styles> {
  cert: ICertificate;
}

export interface State {
  cert?: ICertificate;
}

class DocumentPDF extends Component<Props> {

  constructor(props: Props) {
    super(props);
    this.state = {
      loading: true,
    }
  }

  async componentDidMount(): Promise<void> {
    const { match, history } = this.props;
    const uuid = match.params.id;

    try {
      const cert = await getCertificateByCode(uuid);

      if (!cert) {
        history.replace('/app/error/document-not-found');
      }

      this.setState({
        loading: false,
        cert
      });
    } catch (err) {
      console.error(err);
      history.replace('/app/error/document-not-found');
    }
  }

  render() {
    //@ts-ignore
    const { cert } = this.state;

    if (!cert) {
      return null;
    }

    const issuer = cert.issuer as IIssuer;
    const template = cert.template as ITemplate;

    const frontendURL = env.frontendURL;
    const shareableLinkCode = cert.shareableLinkCode;
    const url = `${frontendURL}/document/${shareableLinkCode}`;

    return (
      cert && <AsyncCertificate
        company={issuer.shortName}
        template={template.name}
        sign={cert.proof && cert.proof.jws}
        date={cert.proof && cert.proof.created}
        shareableLinkCode={shareableLinkCode}
        url={url}
        {...cert.credentialSubject}/>
    );
  }
}

const styles = createStyles({
});

export default withRouter(
  withTranslation('common')(
    withStyles(styles)(DocumentPDF)
  )
);
