import React, { Fragment }                       from 'react';
import { inject, MobXProviderContext, observer } from 'mobx-react';
import { WithStyles }                            from '@material-ui/core';
import { withTranslation, WithTranslation }      from 'react-i18next';
import { withStyles, createStyles }              from '@material-ui/styles';
import { withRouter, RouteComponentProps }       from 'react-router-dom';

import Layout                                        from '../../../components/layout';
import { getCertificateByCode, getCertHashByCertId } from '../../../api/certificate';

// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, RouteComponentProps<{id: string}>, WithStyles<typeof styles> {
  store: import('../../../store').default;
}

// tslint:disable-next-line:interface-name
export interface State {
  txLink: string;
  contractLink: string;
}

class VerifyManual extends React.Component<Props, State> {
  static contextType = MobXProviderContext;

  context!: React.ContextType<typeof MobXProviderContext>;

  async componentDidMount(): Promise<void> {
    const { match } = this.props;
    const uuid = match.params.id;
    const cert = await getCertificateByCode(uuid);
    const hash = await getCertHashByCertId(cert.chain, cert._id);
    const explorerSubdomain = cert.chain === 'mainnet' ? '' : 'ropsten.';

    this.setState({
      txLink: `https://${explorerSubdomain}etherscan.io/tx/${cert.txId}`,
      contractLink: `https://${explorerSubdomain}etherscan.io/address/${hash}`
    });
  }

  render() {
    const { classes, t } = this.props;
    const {  txLink, contractLink } = this.state;

    return (
      <Fragment>
        <Layout pageTitle={t('verify_manual.page_title')}>

          <p>{t('verify_manual.p1')}</p>
          <p>{t('verify_manual.p2')}</p>

          <h2 className={classes.subtitle}>{t('verify_manual.check_blockchain')}</h2>

          <p>{t('verify_manual.p3')} <a href={txLink} target='_blank'>{t('verify_manual.p3_link_text')}</a>.</p>
          <p>{t('verify_manual.p4')}</p>

          <div className={classes.screenshotContainer}>
            <img className={classes.screenshot} src='/static/images/verifyManual1.svg'/>
          </div>

          <p>{t('verify_manual.p5')} <a href={contractLink} target='_blank'>{t('verify_manual.p5_link_text')}</a>.</p>
          <p>{t('verify_manual.p6')}</p>

          <h2 className={classes.subtitle}>{t('verify_manual.check_document_content')}</h2>

          <p>{t('verify_manual.p7')}</p>
          <p>{t('verify_manual.p8')} <a href='https://remix.ethereum.org/' target='_blank'>{t('verify_manual.p8_link_text')}</a>.</p>

        </Layout>
      </Fragment>
    );
  }
}

const styles = createStyles({
  subtitle: {
    textAlign: 'center',
    marginTop: '1.5em'
  },
  screenshotContainer: {
    margin: '2em 0'
  },
  screenshot: {
    display: 'block',
    width: '100%',
    maxWidth: '800px',
    margin: '0 auto',
    outline: '1px solid #aaa'
  }
});

export default withRouter(
  withTranslation('common')(
    withStyles(styles)(
      inject('store')(observer(VerifyManual))
    )
  )
);
