import React, { Fragment }                       from 'react';
import { inject, MobXProviderContext, observer } from 'mobx-react';
import { WithStyles }                            from '@material-ui/core';
import { Theme }                                 from '@material-ui/core';
import { withStyles, createStyles }              from '@material-ui/styles';
import classnames                                from 'classnames';
import { SHA3 }                                  from 'sha3';
import { withTranslation, WithTranslation }      from 'react-i18next';
import { withRouter, RouteComponentProps }       from 'react-router-dom';

import {
  getCertificateByCode,
  getTxDetail,
  getCertHashByCertId,
  getCertByHash
} from '../../../api/certificate';
import Layout                     from '../../../components/layout';
import Loader                     from '../../../components/common/Loader';

// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, RouteComponentProps<{id: string}>, WithStyles<typeof styles> {
  store: import('../../../store').default;
}

// tslint:disable-next-line:interface-name
export interface State {
  code: string;
  cert: import('../../../store/domain/certificates').ICertificate;
  hash: string;
  txId: string;
  txUrl: string;
  contractUrl: string;
  verifyManualLink: string;
  blockNumber: number;
  cs: string;
  csHashFromChain: string;
}

class Verify extends React.Component<Props, State> {
  static contextType = MobXProviderContext;

  context!: React.ContextType<typeof MobXProviderContext>;

  async componentDidMount(): Promise<void> {
    const { match } = this.props;
    const uuid = match.params.id;
    const cert = await getCertificateByCode(uuid);
    const txDetail = await getTxDetail(cert.chain, cert.txId!);
    let hash;
    let csHashFromChain;

    if (txDetail) {
      hash = await getCertHashByCertId(cert.chain, cert._id);
      csHashFromChain = (await getCertByHash(cert.chain, hash)).csHash;
    }
    const explorerSubdomain = cert.chain === 'mainnet' ? '' : 'ropsten.';

    this.setState({
      code: uuid,
      cert,
      blockNumber: txDetail.blockNumber,
      txId: cert.txId!,
      hash,
      txUrl: `https://${explorerSubdomain}etherscan.io/tx/${cert.txId!}`,
      contractUrl: `https://${explorerSubdomain}etherscan.io/address/${hash}#internaltx`,
      verifyManualLink: `/app/document/${uuid}/verifyManual`,
      csHashFromChain,
    });
  }

  render() {
    const { classes, t, } = this.props;
    if (!this.state) {
      return (
        <Fragment>
          <Layout pageTitle={t('verify.page_title')}>
            <Loader/>
          </Layout>
        </Fragment>
       );
    }
    const { cert, hash, blockNumber, csHashFromChain, txId, txUrl, contractUrl, verifyManualLink } = this.state;
    const digest = (cert.proof && cert.proof.jws) ? cert.proof.jws : '';
    const cs = JSON.stringify(cert.credentialSubject);

    const sha3 = new SHA3(256);
    sha3.update(cs);
    const csHash = sha3.digest('hex');

    const results = {
      unknown: {
        iconUrl: '/static/images/question.svg',
        title: t('verify.status_unknown'),
        description: t('verify.no_checksum')
      },
      success: {
        iconUrl: '/static/images/success.svg',
        title: t('verify.status_success'),
        description: t('verify.checksum_ok')
      },
      fail: {
        iconUrl: '/static/images/error.svg',
        title: t('verify.status_failed'),
        description: t('verify.checksum_error')
      }
    };

    const result =
      (!csHash || !csHashFromChain) ?
        results.unknown :
      (csHash === csHashFromChain) ?
        results.success : results.fail;

    return (
      <Fragment>
        <Layout pageTitle={t('verify.page_title')}>
          <div className={classes.container}>

            <div className={classes.result}>
              <div className={classes.resultLeft}>
                <img src={result.iconUrl} />
              </div>
              <div className={classes.resultRight}>
                <div className={classes.resultTitle}>{result.title}</div>
                <div className={classes.resultDescription}>{result.description}</div>
              </div>
            </div>

            <br />

            <h2 className={classes.subtitle}>{t('verify.blockchain_record')}</h2>

            <div className={classes.links}>
              <a href={txUrl} target='_blank'>
                <img src='/static/images/external.svg' />
                <span>{t('verify.view_tx')}</span>
              </a>
              <a href={contractUrl} target='_blank'>
                <img src='/static/images/external.svg' />
                <span>{t('verify.view_contract')}</span>
             </a>
            </div>

            <form>
              <div className={classes.field}>
                <label htmlFor='hash'>{t('verify.tx_hash')}</label>
                <div className={classes.input}>
                  <input id='hash' name='hash' type='text' readOnly value={txId}/>
                </div>
              </div>
              <div className={classes.field}>
                <label htmlFor='contractAddr'>{t('verify.contract_addr')}</label>
                <div className={classes.input}>
                  <input id='contractAddr' name='contractAddr' type='text' readOnly value={hash}/>
                </div>
              </div>
              <div className={classes.field}>
                <label htmlFor='blockNumber'>{t('verify.block_number')}</label>
                <div className={classnames([classes.input, classes.blockNumberInput])}>
                  <input id='blockNumber' name='blockNumber' type='text'readOnly value={blockNumber}/>
                </div>
              </div>
              <div className={classes.field}>
                <label className={classes.checksumLabel} htmlFor='docSumFromChain' title='sha3-256'>
                  {t('verify.checksum')}
                </label>
                <div className={classes.input}>
                  { cert.proof &&
                  <input id='docSumFromChain' name='docSumFromChain' type='text' readOnly value={csHashFromChain}/>
                  }
                </div>
              </div>
            </form>

            <hr className={classes.divider} />

            <h2 className={classes.subtitle}>{t('verify.presented_document')}</h2>
            <form>
              <div className={classes.field}>
                <label htmlFor='cs'>{t('verify.doc_content')}</label>
                <div className={classnames([classes.input, classes.inputTextarea])}>
                  <textarea className={classes.textarea} id='cs' name='cs' rows={10} readOnly value={cs}/>
                </div>
              </div>
              <div className={classes.field}>
                <label htmlFor='digest'>{t('verify.digest')}</label>
                <div className={classes.input}>
                  { cert.proof &&
                  <input id='digest' name='digest' type='text' readOnly value={digest}/>
                  }
                </div>
              </div>
              <div className={classes.field}>
                <label className={classes.checksumLabel} htmlFor='docSum' title='sha3-256'>
                  {t('verify.checksum')}
                </label>
                <div className={classes.input}>
                  { cert.proof &&
                    <input id='docSum' name='docSum' type='text' readOnly value={csHash}/>
                  }
                </div>
              </div>
            </form>

            {/*<div className={classes.bottomButtons}>
              <a className={classes.checkOnBlockchain} href={txUrl}>{t('verify.check_ropsten')}</a>
              <a className={classes.manualValidation} href={verifyManualLink}>{t('verify.manual_validation')}</a>
            </div>*/}

          </div>
        </Layout>
      </Fragment>
    );
  }
}

const styles = (theme: Theme) => createStyles({
  container: {
    maxWidth: '500px',
    margin: '0 auto'
  },
  result: {
    border: '1px solid #ddd',
    display: 'flex',
    padding: '10px'
  },
  resultLeft: {
    width: '25px',
    paddingTop: '2px'
  },
  resultRight: {
    paddingLeft: '10px',
  },
  resultTitle: {
    fontSize: '20px'
  },
  resultDescription: {
    marginTop: '0.5em',
    fontSize: '14px'
  },
  subtitle: {
    fontSize: '1.17em'
  },
  divider: {
    margin: '1.5em 0',
    height: 1,
    border: 'none',
    backgroundColor: '#ccc'
  },
  field: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: '10px',
    fontSize: '14px',
    lineHeight: '128%',
    color: '#000',
    '& label': {
      width: '110px',
      marginRight: '0.3em',
      cursor: 'pointer'
    },
    '& input': {
      width: '100%',
      border: 'none',
      outline: 'none',
      background: 'transparent',
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    }
  },
  checksumLabel: {
    textDecorationLine: 'underline',
    textDecorationStyle: 'dotted'
  },
  input: {
    flex: 1,
    padding: '10px',
    position: 'relative',
    borderRadius: '4px',
    background: '#F6F6F8'
  },
  blockNumberInput: {
    maxWidth: '100px'
  },
  inputTextarea: {
    padding: 0
  },
  textarea: {
    display: 'block',
    width: '100%',
    height: '120px',
    marginBottom: 0,
    padding: '10px',
    resize: 'none',
    border: 'none',
    outline: 'none',
    borderRadius: '4px',
    background: '#F6F6F8'
  },
  links: {
    display: 'flex',
    justifyContent: 'space-around',
    margin: '1em 0',
    textAlign: 'center',
    '& img': {
      width: '12px',
      marginRight: '3px'
    }
  },
  bottomButtons: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  checkOnBlockchain: {
    padding: '13px 27px',
    borderRadius: '4px',
    background: '#F6F6F8',
    color: '#8F94A4',
    textDecoration: 'none',
    textAlign: 'center'
  },
  manualValidation: {
    padding: '13px 27px',
    color: '#202949',
    textAlign: 'center'
  }
});

export default withRouter(
  withTranslation('common')(
    withStyles(styles)(
      inject('store')(observer(Verify))
    )
  )
);
