import React, { Fragment, Component }            from 'react';
import { inject, MobXProviderContext, observer } from 'mobx-react';
import loadable                                  from '@loadable/component';
import { WithStyles }                            from '@material-ui/core';
import { createStyles, withStyles }              from '@material-ui/styles';
import classnames                                from 'classnames';
import mime                                      from 'mime-types';
import { withTranslation, WithTranslation }      from 'react-i18next';
import { withRouter, RouteComponentProps }       from 'react-router-dom';
import { Helmet }                                from 'react-helmet';

import {
  FacebookShareButton,
  TwitterShareButton,
  TelegramShareButton,
  VKShareButton,
  LinkedinShareButton
} from 'react-share';

import { getCertificateByCode, toggleVisibility, getTxDetail } from '../../../api/certificate';
import download                                                from '../../../lib/download';
import utils                                                   from '../../../lib/utils';
import Layout                                                  from '../../../components/layout';
import Loader                                                  from '../../common/Loader';
import env                                                     from '../../../environments/environment';
import { IUser }                                               from '../../../store/domain/user';
import { ITxDetail }                                           from '../../../sharedTypes';

type IIssuer = import('../../../store/domain/issuer').IIssuer;
type ITemplate = import('../../../store/domain/templates').ITemplate;
type ICertificate = import('../../../store/domain/certificates').ICertificate;

interface IAsyncCertificateProps {
  [key: string]: any;

  company: string;
  template: string;
  sign?: string;
  date?: string;
}

const AsyncCertificate = loadable((props: IAsyncCertificateProps) => import(`../../../components/templates/${props.company}/${props.template}`), {
  cacheKey: (props: IAsyncCertificateProps) => `${props.company}/${props.template}`
});

// tslint:disable-next-line:interface-name
export interface Props extends WithTranslation, RouteComponentProps<{ id: string }>, WithStyles<typeof styles> {
  store: import('../../../store/').default;
}

// tslint:disable-next-line:interface-name
export interface State {
  visible: boolean;
  loading: boolean;
  tx?: ITxDetail;
  cert?: ICertificate;
}

class Certificate extends Component<Props, State> {
  static contextType = MobXProviderContext;

  url: string;
  origin: string;
  isEmbedded: boolean;
  context!: React.ContextType<typeof MobXProviderContext>;

  constructor(props: Props) {
    super(props);
    const query = new URLSearchParams(this.props.location.search);

    this.isEmbedded = Boolean(query.get('embedded'));
    this.url = location.href;
    this.origin = location.origin;
    this.state = {
      visible: true,
      loading: true
    };
  }

  async componentDidMount(): Promise<void> {
    const { match, history } = this.props;
    const uuid = match.params.id;

    try {
      let tx: ITxDetail;
      const cert = await getCertificateByCode(uuid);

      if (cert.txId) {
        tx = await getTxDetail(cert.chain, cert.txId);
      }

      if (!cert) {
        history.replace('/app/error/document-not-found');
      }

      this.setState({
        loading: false,
        // @ts-ignore
        tx: tx || undefined,
        cert
      });
    } catch (err) {
      console.error(err);
      history.replace('/app/error/document-not-found');
    }
  }

  toggleVisibility = () => {
    const { match } = this.props;
    const uuid = match.params.id;
    const visible = this.state.visible;
    // this.context.store.certsStore.toggleVisibility()
    this.setState({
      visible: !visible
    }, async () => {
      try {
        await toggleVisibility(uuid, !visible);
      } catch (err) {
        this.setState({
          visible
        });
      }
    });
  };

  showVerificationPage = () => {
    const { match } = this.props;
    const uuid = match.params.id;
    this.props.history.push(`/app/document/${uuid}/verify`);
  };

  copyLink = () => {
    const copyText: HTMLInputElement = document.querySelector<HTMLInputElement>('#shareableLink')!;
    const textArea: HTMLTextAreaElement = document.createElement('textarea');
    textArea.value = copyText.value;
    document.body.appendChild(textArea);
    textArea.select();
    document.execCommand('copy');
    textArea.remove();
  };

  downloadJSON = () => {
    const cert = this.state.cert!;
    const issuer = cert.issuer as IIssuer;
    const { shareableLinkCode, credentialSubject, proof } = cert;
    const frontendURL = env.frontendURL;
    const apiURL = env.apiURL;

    // https://www.w3.org/TR/vc-data-model/#json
    // > Empty values SHOULD be represented as a null value.

    const data = {
      "@context": "https://www.w3.org/2018/credentials/v1",
      "id": `${frontendURL}/document/${shareableLinkCode}`,
      "type": "VerifiableCredential",
      "issuer": {
        "name": issuer.name,
        "address": issuer.address,
        "phone": issuer.phone,
        "publicKey": issuer.publicKey
      },
      "issuanceDate": proof && proof.created ? proof.created : null,
      credentialSubject,
      "credentialStatus": {
        "id": `${apiURL}/documents/${shareableLinkCode}/status`,
        "type": "CredentialStatusList2017"
      },
      "proof": proof ? proof : null
    };
    download.JSONfile(data, 'Certificate.json');
  };

  createPdf = () => {
    const { match } = this.props;
    const uuid = match.params.id;
    if (!uuid) {
      console.error('Missing cert code');
      return;
    }
    const pdfFileUrl = `/app/document/${uuid}/pdf`;
    window.open(pdfFileUrl, '_blank');
  };

  renderCertificateDetails() {
    const { url, origin } = this;
    const cert = this.state.cert!;
    const { classes, t } = this.props;
    const cs = cert && cert.credentialSubject;
    const imageURL = `${origin}/api/certificates/${cert.shareableLinkCode}/image`;
    const attachments = cs.cloudFiles || {};

    return (
      <Fragment>
        <div className={classes.detailsContainer}>
          <div className={classnames(classes.shareContainer)}>
            <div className={classes.shareTitle}>
              <h3>{t('diploma.share_achievements')}</h3>
            </div>
            <div className={classes.shareLinkContainer}>
              <input
                id='shareableLink'
                type='text'
                className={classes.shareLink}
                value={this.url} disabled
              />
              <button className={classes.copyShareLinkButton} onClick={this.copyLink}>{t('copy')}</button>
            </div>
            <div className={classes.shareSocialContainer}>
              <div className='social'>
                <VKShareButton
                  url={url}
                  title={t('certificate')}
                  description={cs.title}
                  image={imageURL}
                >
                  <img src='/static/images/vk.svg' alt=''/>
                </VKShareButton>
                <FacebookShareButton url={url} quote={cs.title}>
                  <img src='/static/images/facebook.svg' alt=''/>
                </FacebookShareButton>
                <TwitterShareButton url={url} title={cs.title}>
                  <img src='/static/images/twitter.svg' alt=''/>
                </TwitterShareButton>
                <TelegramShareButton url={url} title={cs.title}>
                  <img src='/static/images/telegram.svg' alt=''/>
                </TelegramShareButton>
                <LinkedinShareButton url={url}>
                  <img src='/static/images/linkedin.svg' alt=''/>
                </LinkedinShareButton>
              </div>
              <div className='export'>
                <span>{t('diploma.export_as')} </span>
                <button onClick={this.createPdf}>PDF</button>
                {' '}
                <button onClick={this.downloadJSON}>JSON</button>
              </div>
            </div>
          </div>
        </div>

        {Object.keys(attachments).length > 0 &&
        <div className={classes.attachments}>
          <h3 className={classes.attachmentsTitle}>{t('attachments')}</h3>
          {Object.entries(attachments).map(([_, item]) => {
              const type = mime.lookup(item.name || '');
              const canPreview = type && (type.includes('video') || type.includes('image'));

              return (
                <div key={item.name} className={classes.attachmentContainer}>
                  <a
                    className={classes.attachmentDownloadLink}
                    href={`/files/${item.key}`} download={item.name}>
                    {item.name}
                  </a>
                  {canPreview && (
                    <a className={classes.attachmentPreviewLink}
                       href={`/files/${item.key}`} target='_blank'>
                      {t('show')}
                    </a>
                  )}
                </div>
              );
            }
          )}
        </div>
        }
      </Fragment>
    );
  }

  render() {
    if (this.state.loading) {
      return <Loader/>;
    }

    const cert = this.state.cert!;
    const { url, origin, isEmbedded } = this;
    const { visible, tx } = this.state;
    const { classes, t, i18n, store } = this.props;

    const currentUser = store.sessionStore.user;

    let holder;
    let holderId;
    if (holder) {
      holder = cert.holder as IUser;
      holderId = holder._id;
    }
    const issuer = cert.issuer as IIssuer;
    const template = cert.template as ITemplate;
    const cs = cert && cert.credentialSubject;
    const isHolder = currentUser && currentUser._id === holderId;

    // @ts-ignore
    const lang: string = env.languages[i18n.language];
    const otherLangs = Object.values(env.languages).filter(l => l !== lang);
    const imageURL = `${origin}/api/certificates/${cert.shareableLinkCode}/image`;

    const isLinguaLeoCert = issuer && issuer.shortName === 'lingualeo';

    const showVerifyButton = !isLinguaLeoCert && tx;

    let sharedTitle;
    let sharedDescription = '';

    if (isLinguaLeoCert && cs.name && cs.course) {
      sharedTitle = `${t('certificate')}: ${cs.course ? cs.course : ''}`;
      sharedDescription = `${cs.name} yспешно завершил(а) интерактивный курс английского языка «${cs.course}»`;
    } else {
      sharedTitle = utils.getLocalDocType(template.name);
      sharedDescription = `${cs.title}`;
    }

    return (
      <Fragment>
        <Helmet>
          <title>{t('certificate')} {cs.title}</title>
          <meta property='og:title' content={sharedTitle}/>
          <meta property='og:type' content='website'/>
          <meta property='og:url' content={url}/>
          <meta property='og:description' content={sharedDescription}/>
          <meta property='og:image' content={imageURL}/>
          <meta property='og:image:type' content='image/png'/>
          <meta property='og:image:width' content='300'/>
          <meta property='og:image:height' content='300'/>
          <meta property='og:locale' content={lang}/>
          {otherLangs.map(l => {
            return (
              <meta key={l} property='og:locale:alternate' content={l}/>
            );
          })}
          <meta property='og:site_name' content={t('credentia')}/>
        </Helmet>
        <Layout classes={{ content: classes.container }} embedded={isEmbedded}>
          {isHolder && (
            <div className={classnames(classes.visibilityToggle, {
              active: visible
            })} onClick={this.toggleVisibility}/>
          )}
          <div className={classes.cert}>
            {cert && <AsyncCertificate
              company={issuer.shortName}
              template={template.name}
              sign={cert.proof && cert.proof.jws}
              date={cert.proof && cert.proof.created}
              shareableLinkCode={cert.shareableLinkCode}
              url={url}
              {...cert.credentialSubject}>
              {cert && this.renderCertificateDetails()}
            </AsyncCertificate>}
          </div>
          {showVerifyButton &&
          <div className={classes.verifyButtonContainer}>
            <button className={classes.verifyButton} onClick={this.showVerificationPage}>
              {t('verify.verify_document')}
            </button>
          </div>
          }
        </Layout>
      </Fragment>
    );
  }
}

const styles = createStyles({
  container: {
    maxWidth: '860px',
    margin: '0 auto'
  },
  cert: {
    overflow: 'hidden'
  },
  visibilityToggle: {
    marginLeft: 'auto',
    marginBottom: '30px',
    width: '30px',
    height: '30px',
    backgroundImage: 'url(/static/images/eye-closed.svg)',
    backgroundRepeat: 'no-repeat',
    backgroundSize: '100%',
    backgroundPosition: 'center center',
    opacity: 0.6,
    cursor: 'pointer',
    userSelect: 'none',
    '&.active': {
      backgroundImage: 'url(/static/images/eye-opened.svg)'
    }
  },
  titleContainer: {
    display: 'flex',
    flexDirection: 'column',
    height: '650px', // TODO remove after implementing cert for testing
    padding: '24px',
    margin: '0 auto',
    borderRadius: '8px',
    textAlign: 'center',
    fontSize: '10px',
    backgroundColor: '#226390',
    color: '#fff'
  },
  issuer: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '80px'
  },
  issuerNameContainer: {
    width: '200px',
    lineHeight: '150%',
    fontFamily: '"IBM Plex Sans", sans-serif',
    fontSize: '1em'
  },
  issuerName: {
    marginBottom: '15px',
    letterSpacing: '0.055em',
    textTransform: 'uppercase',
    textAlign: 'left'
  },
  documentAddInfo: {
    display: 'flex',
    justifyContent: 'space-between',
    color: 'rgba(255, 255, 255, 0.5)'
  },
  title: {
    flex: 1,
    fontSize: '3em',
    lineHeight: '130%'
  },
  signatureBlock: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: '20px'
  },
  signature: {
    width: '90px',
    textAlign: 'left',
    '& img': {
      marginBottom: '10px'
    }
  },
  ids: {
    textAlign: 'center',
    color: 'rgba(255, 255, 255, 0.5)'
  },
  detailsContainer: {
    margin: '24px auto',
    backgroundColor: '#fff',
    color: '#202949',
    '@media (max-width: 575px)': {
      marginTop: '40px'
    }
  },
  detailsTitle: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    fontSize: '41px',
    lineHeight: '103%',
    fontWeight: 'bold',
    color: '#202949',
    '& img': {
      cursor: 'pointer'
    }
  },
  detailsSubtitle: {
    marginBottom: '50px',
    fontSize: '41px',
    fontWeight: 'bold',
    lineHeight: '103%',
    color: '#202949',
    opacity: 0.2
  },
  detailsTabs: {
    display: 'flex',
    marginBottom: '40px',
    borderBottom: '1px solid #F4F4F4;',
    fontSize: '15px',
    lineHeight: '126%',
    color: '#8F94A4'
  },
  detailsTab: {
    padding: '15px',
    cursor: 'pointer',
    '&.active': {
      color: '#202949',
      borderBottom: '1px solid #F58343'
    }
  },
  shareContainer: {
    padding: '24px 20px',
    border: '1px solid #eee',
    borderRadius: '7px',
    backgroundColor: '#FBFBFB',
    fontFamily: '"IBM Plex Sans", sans-serif',
    '@media (max-width: 575px)': {
      border: 'none',
      backgroundColor: 'transparent',
      padding: 0
    }
  },
  shareTitle: {
    display: 'flex',
    marginBottom: '23px',
    justifyContent: 'space-between',
    alignItems: 'center',
    '& h3': {
      margin: 0,
      fontWeight: 500
    },
    '& img': {
      cursor: 'pointer'
    }
  },
  shareLinkContainer: {
    display: 'flex',
    marginBottom: '26px',
    border: '0.5px solid #ECECEC',
    borderRadius: '6px',
    overflow: 'hidden',
    boxShadow: '0px 7px 9px rgba(0, 0, 0, 0.01)'
  },
  shareLinkIcon: {
    width: '44px',
    height: '44px',
    borderRadius: '50%',
    '&:hover': {
      backgroundColor: '#F6F6F8'
    }
  },
  shareLink: {
    flex: 1,
    width: '1%',
    padding: '10px 12px',
    border: 'none',
    backgroundColor: '#fff',
    outline: 'none',
    textOverflow: 'ellipsis'
  },
  copyShareLinkButton: {
    padding: '11px 20px',
    border: 'none',
    outline: 'none',
    color: '#fff',
    backgroundColor: '#F36414',
    cursor: 'pointer',
    fontFamily: 'inherit',
    '@media (max-width: 575px)': {
      padding: '10px 14px 12px',
      fontSize: '14px'
    }
  },
  shareSocialContainer: {
    display: 'flex',
    '@media (max-width: 575px)': {
      flexDirection: 'column',
      alignItems: 'center'
    },
    '& .social': {
      flex: 1,
      display: 'flex',
      alignItems: 'center',
      '@media (max-width: 575px)': {
        marginBottom: '15px'
      },
      '& img': {
        marginRight: '18px',
        opacity: 0.4,
        cursor: 'pointer',
        '&:hover': {
          opacity: 1
        }
      }
    },
    '& .export': {
      '@media (max-width: 575px)': {
        fontSize: '18px'
      },
      '& button': {
        padding: '4px 6px',
        border: 'none',
        outline: 'none',
        borderRadius: '5px',
        background: 'rgba(243, 100, 20, 0.1);',
        color: '#F36414',
        cursor: 'pointer',
        fontFamily: 'inherit'
      }
    }
  },
  detailsBlock: {
    marginBottom: '20px',
    padding: '24px',
    background: 'rgba(244, 244, 246, 0.4)',
    borderRadius: '6px',
    '& table': {
      tableLayout: 'fixed',
      borderCollapse: 'collapse'
    },
    '& table th': {
      padding: '5px',
      borderBottom: '1px solid rgba(32, 41, 73, 0.2);'
    },
    '& table tr th:first-child': {
      textAlign: 'left'
    },
    '& table td': {
      textAlign: 'right',
      padding: '5px'
    },
    '& table td:first-child': {
      textAlign: 'left'
    }
  },
  attachments: {
    maxWidth: '500px',
    margin: '30px auto',
    padding: '24px 20px',
    border: '1px solid #eee',
    borderRadius: '7px',
    backgroundColor: '#FBFBFB',
    '@media (max-width: 575px)': {
      border: 'none',
      backgroundColor: 'transparent',
      padding: 0
    }
  },
  attachmentsTitle: {
    margin: 0,
    marginBottom: '14px',
    fontWeight: 500
  },
  attachmentContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '20px',
    '&:last-child': {
      marginBottom: 0
    }
  },
  attachmentDownloadLink: {
    display: 'block',
    color: '#F36414',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  attachmentPreviewLink: {
    textDecoration: 'none',
    color: 'inherit',
    '&:visited': {
      color: 'inherit'
    }
  },
  verifyButtonContainer: {
    marginTop: '30px',
    display: 'flex',
    justifyContent: 'center',
    '& button': {
      display: 'block',
      padding: '20px 65px',
      background: '#F4F4F6',
      borderRadius: '8px',
      fontSize: '14px',
      '@media (max-width: 800px)': {
        padding: '15px 35px'
      },
      '@media (max-width: 500px)': {
        padding: '10px 10px'
      }
    }
  },
  verifyButton: {
    cursor: 'pointer'
  }
});

export default withRouter(
  withTranslation('common')(
    withStyles(styles)(
      inject('store')(observer(Certificate))
    )
  )
);
