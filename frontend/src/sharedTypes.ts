export enum RequestState {
  Pending = 'pending',
  Done = 'done',
  Error = 'error'
}

export interface ITxDetail {
  blockNumber: number;
  blockHash: string;
}
