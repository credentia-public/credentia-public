import React from 'react';
import ReactDOM from 'react-dom';

import './lib/i18n';
import Application from './components/app';

const elem = document.getElementById('application-container');

if (elem === null) {
  throw new Error("Can't get root element");
}

ReactDOM.render(
  <Application />,
  elem,
);
