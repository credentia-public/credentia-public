const origin = typeof location !== 'undefined' ? location.origin : null;

export default {
  env: process.env.APP_ENV || 'local',
  isDev: process.env.NODE_ENV === 'development',
  apiURL: origin && `${origin}/api`,
  frontendURL: origin && `${origin}/app`,
  port: process.env.APP_PORT || 8000,
  logger: {
    level: process.env.LOG_LEVEL || 'info',
    useLevelLabels: true,
    name: 'app',
    base: null,
    prettyPrint: {
      levelFirst: true
    }
  },
  bluebird: {
    // Enable warnings
    warnings: true,
    // Enable long stack traces
    longStackTraces: true,
    // Enable cancellation
    cancellation: false,
    // Enable monitoring
    monitoring: false
  },
  helmet: {

  },
  cloudStorage: {
    endpointUrl: '_YOUR_VALUE_HERE_'
  },
  i18n: {
    backend: {
      loadPath: '/static/locales/{{lng}}/{{ns}}.json',
    },
    detection: {
      order: [ 'querystring', 'cookie', 'localStorage', 'header' ],
      lookupCookie: 'lang',
      lookupLocalStorage: 'lang',
      lookupQuerystring: 'lang',
      caches: ['localStorage', 'cookie'],
    },
    ns: 'common',
    defaultNS: 'common',
    fallbackLng: 'en',
    whitelist: ['en', 'ru', 'tr'],
    lowerCaseLng: true,
    nonExplicitWhitelist: true,
    load: 'languageOnly',
    caches: ['localStorage'],
    debug: Boolean(process.env.I18N_DEBUG) || false,
  },
  languages: {
    en: 'en_US',
    ru: 'ru_RU',
    tr: 'tr_TR'
  },
};
