import local from './environment.local';

export default Object.assign({}, local, {
  env: 'production',
  isDev: false
});
