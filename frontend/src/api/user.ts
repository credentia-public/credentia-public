import request   from './request';

type IUser = import('../store/domain/user').IUser;
type IPasswordChange = import('../store/domain/user').IPasswordChange;
type IResetPasswordSendLink = import('../store/domain/user').IResetPasswordSendLink;
type IResetPasswordSetNew = import('../store/domain/user').IResetPasswordSetNew;

export function getUser(opts = {}): Promise<IUser> {
  return request('/users',
    {
      ...opts,
      method: 'GET'
    },
  );
}

export function changePassword(params: IPasswordChange) {
  return request('/user/password', {
      method: 'PUT',
      body: params
    },
  );
}

export function resetPasswordSendLink(params: IResetPasswordSendLink) {
  return request('/user/password/reset/send-link', {
      method: 'POST',
      body: params
    },
  );
}

export function resetPasswordSetNew(params: IResetPasswordSetNew) {
  return request('/user/password/reset/new', {
      method: 'POST',
      body: params
    },
  );
}
