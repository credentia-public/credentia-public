import 'isomorphic-unfetch';

import env          from '../environments/environment';
import { ApiError } from '../lib/errors';

export default async function request(path: string, opts: any = {}, withCookies = false) {
  const headers = Object.assign({},
    opts.headers,
    {
      'Content-type': 'application/json; charset=UTF-8'
    }
  );

  if (opts.body) {
    opts.body = JSON.stringify(opts.body);
  }

  // const qs = (opts.qs && `?${makeQueryString(opts.qs)}`) || '';
  const response = await fetch(`${env.apiURL}${path}`,
    Object.assign({ credentials: 'include', mode: 'cors' }, opts, { headers })
  );

  const contentType = response.headers.get('content-type');

  try {

    if (contentType && contentType.includes('application/json')) {
      const text = await response.text();
      const data = JSON.parse(text);

      if (data.status === 'ok') {
        return data.data;
      }

      if (data.status === 'error') {
        throw new ApiError(data.message, data.statusCode, data);
      }

    }

    if (contentType && ~contentType.indexOf('application/octet-stream')) {
      const blob = await response.blob();
      return blob;
    }

  } catch (err) {
    console.error(err);
    if (!(err instanceof ApiError)) {
      throw err;
    }

    throw err;
  }
}
