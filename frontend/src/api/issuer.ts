import request     from './request';

type IIssuer = import('../store/domain/issuer').IIssuer;

export function getIssuer(issuerId: string, opts = {}): Promise<IIssuer> {
  return request(`/issuers/${issuerId}`,
    {
      ...opts,
      method: 'GET'
    },
  );
}
