import request           from './request';

type ISignUpParams = import('../store/ui/session').ISignUpParams;

export const signin = (email: string, password: string) => {
  return request('/auth/login', {
      method: 'POST',
      body: { email, password }
    }
  );
};

export const signup = (params: ISignUpParams) => {
  const issuer = params.issuer || '';

  return request(`/auth/signup/${issuer}`, {
      method: 'POST',
      body: params
    }
  );
};

export const logout = () => {
  return request('/auth/logout', {
      method: 'POST'
    }
  );
};
