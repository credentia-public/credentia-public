import request     from './request';

type ITemplate = import('../store/domain/templates').ITemplate;

export function getTemplates(opts = {}): Promise<ITemplate[]> {
  return request('/templates',
    {
      ...opts,
      method: 'GET'
    },
  );
}
