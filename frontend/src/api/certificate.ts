import request       from './request';
import { ITxDetail } from '../sharedTypes';

type ICertificate = import('../store/domain/certificates').ICertificate;
type ICreateCertificateParams = import('../store/domain/certificates').ICreateCertificateParams;
type ICreateLLCertificateParams = import('../store/domain/certificates').ICreateLLCertificateParams;

export function getCertificates(opts = {}): Promise<ICertificate[]> {
  return request('/certificates',
    {
      ...opts,
      method: 'GET'
    },
  );
}

export function getCertificateByCode(code: string, opts = {}): Promise<ICertificate> {
  return request(`/certificates/${code}`,
    {
      ...opts,
      method: 'GET'
    },
    true
  );
}

export function setCertificateByCode(code: string, visible: boolean, opts = {}): Promise<ICertificate> {
  return request(`/certificates/${code}/visibility`,
    {
      ...opts,
      method: 'POST',
      body: { visible }
    }
  );
}

export function getOrCreateCert(externalId: string, certData: ICreateLLCertificateParams, opts = {}): Promise<ICertificate> {
  return request(`/certificates/lingualeo/${externalId}`,
    {
      ...opts,
      method: 'POST',
      body: certData
    },
  );
}

export function getTxDetail(chain: string, txId: string, opts = {}): Promise<ITxDetail> {
  return request(`/blockchain/txs/${chain}/${txId}`,
    {
      ...opts,
      method: 'GET'
    },
  );
}

export function getCertHashByCertId(chain: string, certId: string, opts = {}) {
  return request(`/blockchain/certificates?certId=${certId}&chain=${chain}`,
    {
      ...opts,
      method: 'GET'
    },
  );
}

export function getCertByHash(chain: string, hash: string, opts = {}) {
  return request(`/blockchain/certificates?hash=${hash}&chain=${chain}`,
    {
      ...opts,
      method: 'GET'
    },
  );
}

export function getCertFile(certCode: string, filename: string, opts = {}) {
  return request(`/certificates/dwn?code=${certCode}&filename=${filename}`,
    {
      ...opts,
      method: 'GET'
    }
  );
}

export function createCertificates(certs: ICreateCertificateParams, opts = {}): Promise<ICertificate[]> {
  return request('/certificates',
    {
      ...opts,
      method: 'POST',
      body: certs
    },
  );
}

export function sendCertCreationEmail(cert: {certId: string}, opts = {}): Promise<{success: string}> {
  return request('/sendEmail',
    {
      ...opts,
      method: 'POST',
      body: cert
    },
  );
}

export function deleteCerts(issuer: string, certsIds: string[], opts = {}): Promise<ICertificate[]> {
  return request('/certificates',
    {
      ...opts,
      method: 'DELETE',
      body: {
        issuer,
        certsIds
      }
    },
  );
}

export function publishCertificates(certIds: string[], opts = {}): Promise<ICertificate[]> {
  return request('/certificates/publish',
    {
      ...opts,
      method: 'POST',
      body: {certIds}
    },
  );
}

export function signCertificates(certIds: string[], opts = {}): Promise<ICertificate[]> {
  return request('/certificates/sign',
    {
      ...opts,
      method: 'POST',
      body: {certIds}
    },
  );
}

export function toggleVisibility(uuid: string, visible: boolean, opts: {} = {}): Promise<ICertificate> {
  return request(`/certificates/${uuid}/visibility`,
    {
      ...opts,
      method: 'POST',
      body: {
        visible
      }
    });
}

export function deleteCertFile(uuid: string, fileHash: string, opts = {}): Promise<ICertificate[]> {
  return request(`/certificates/${uuid}/files/${fileHash}`,
    {
      ...opts,
      method: 'DELETE'
    },
  );
}
