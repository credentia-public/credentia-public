import { Container }     from 'inversify';

import { Application } from './app';
import { Logger }      from './services/Logger';

// declare metadata by @controller annotation

const container = new Container();

container.bind<Logger>(Logger).to(Logger);
container.bind<Application>(Application).to(Application);

export { container };
