import url       from 'url';
import puppeteer from 'puppeteer';

let oneBrowser: any;

const getBrowser = async () => {
  if (!oneBrowser) {
    oneBrowser = await puppeteer.launch({
      //headless: false, // uncomment to debug
      args: ['--no-sandbox']
    });
    console.log('Renderer: the browser was launched');
  }
  return oneBrowser;
};

const renderHTML = async (urlStr: string) => {
  console.time('Renderer: HTML time');

  const browser = await getBrowser();
  const page = await browser.newPage();

  await page.goto(urlStr, {
    waitUntil: 'networkidle2'
  });
  
  let html = await page.evaluate(() => document.documentElement.outerHTML);

  await page.close();

  console.timeEnd('Renderer: HTML time');
  return html;
}

const renderImage = async (urlStr: string, width: number, height: number) => {
  console.time('Renderer: image time');

  const browser = await getBrowser();
  const page = await browser.newPage();
  await page.setViewport({
    width,
    height
  });
  await page.goto(urlStr, {
    waitUntil: 'networkidle2'
  });
  // @ts-ignore
  // await page.evaluateHandle('document.fonts.ready');
  const image = await page.screenshot({
    type: 'jpeg',
    quality: 90,
    clip: {
      x: 0,
      y: 0,
      width,
      height
    },
    encoding: 'binary'
  });
  await page.close();

  console.timeEnd('Renderer: image time');
  return Buffer.from(image);
};

const renderPDF = async (urlStr: string, cookies: any = {}) => {
  console.time('Renderer: PDF time');

  if (!urlStr) {
    throw new Error('Missing url');
  }

  const browser = await getBrowser();
  const page = await browser.newPage();
  const parsedURL = url.parse(urlStr);

  const formattedCookies = Object.entries(cookies).map(([name, value]) => {
    return {
      name,
      value,
      domain: parsedURL.hostname,
      path: '/',
      hostOnly: false,
      httpOnly: false,
      sameSite: 'Lax',
      secure: false,
      session: false,
      expires: Date.now() + 86400
    };
  });
  await page.setCookie(...formattedCookies);
  await page.goto(urlStr, {
    waitUntil: 'networkidle2'
  });

  let height = await page.evaluate(() => document.documentElement.offsetHeight);
  let width = await page.evaluate(() => document.documentElement.offsetWidth);

  // Documents are not rendered pixel-perfect
  // This is necessary to avoid a second page
  height += 1;
  width += 1;

  let size;

  if (width && height) {
    size = {
      height: height + 'px',
      width: width + 'px'
    };
  } else {
    size = {
      format: 'letter',
      landscape: true
    };
  }

  const pdf = await page.pdf({
    printBackground: true,
    margin: 0,
    preferCSSPageSize: true,
    ...size
  });

  await page.close();

  console.timeEnd('Renderer: PDF time');
  return pdf;
};

// await browser.close();
export default { renderHTML, renderImage, renderPDF };
