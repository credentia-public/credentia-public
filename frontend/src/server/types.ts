import { Request } from 'express';

type IUser = import('../store/domain/user').IUser;
type IIssuer = import('../store/domain/issuer').IIssuer;

export interface IAppRequest extends Request {
  user?: IUser;
  issuer?: IIssuer;
}

