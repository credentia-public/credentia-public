// import 'express-async-errors';

import express, { NextFunction, Response } from 'express';
import http                                from 'http';
import path                                from 'path';
import stoppable                           from 'stoppable';
import { injectable }                      from 'inversify';

import env                         from '../environments/environment';
import localeDiffs                 from '../lib/localeDiffs';
import { Logger }                  from './services/Logger';
import routes                      from './routes';
// import i18nextConfig               from '../i18n';

type AddressInfo = import('net').AddressInfo;
const indexFilePath = path.resolve('dist/static/index.html');

@injectable()
export class Application {
  readonly expressApp: express.Application;
  readonly httpServer: stoppable.StoppableServer;

  constructor(private logger: Logger) {
    const expressApp: express.Express = express();
    const httpServer = stoppable(http.createServer(expressApp));

    this.expressApp = expressApp;
    this.httpServer = httpServer;
  }

  start = async () => {
    const localeDiffsList = localeDiffs.getList();

    if (localeDiffsList) {
      this.logger.error(`There are differences in the locales: \n${localeDiffsList}`);
    }

    this.setMiddlewares();

    this.httpServer.on('listening', () => {
      const { address } = <AddressInfo>this.httpServer.address();

      this.logger.info(`Server started on ${address}:${env.port}`);
    });

    this.httpServer.on('error', (err: Error) => {
      this.logger.error(err, 'Error starting server');
    });

    this.httpServer.listen(env.port);
  };

  stop(): Promise<void> {
    return new Promise(res => {
      this.httpServer.stop(() => {
        res();
      });
    });
  }

  private setMiddlewares() {
    if (!env.isDev) {
      this.expressApp.set('trust proxy', 1); // sets req.hostname, req.ip
    }
    this.expressApp.use(express.static('dist'));
    // this.expressApp.use(nextI18NextMiddleware(i18nextConfig));

    routes(this.expressApp);

    this.expressApp.get('*', (req, res) => {
      res.sendFile(indexFilePath);
    });
  }
}
