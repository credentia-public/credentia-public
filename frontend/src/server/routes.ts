import express, { Response, NextFunction } from 'express';
import isBot                               from 'isbot';

import { IAppRequest } from './types';
import env             from '../environments/environment';
import renderer        from './services/renderer';

isBot.exclude([
  'HeadlessChrome/'
]);

export default function routes(expressApp: express.Application) {

  expressApp.get('/app/document/:code', async (req: IAppRequest, res: Response, next: NextFunction) => {
    const { code } = req.params;

    const userAgent: any = req.headers['user-agent'];
    const showPrerenderedMarkup = isBot(userAgent);

    if (showPrerenderedMarkup) {
      const origin = `${req.protocol}://${req.hostname}`;
      const pageURL = `${origin}/app/document/${code}`;
      const html = await renderer.renderHTML(pageURL);
      res.status(200).send(html);
    } else {
      next();
    }
  });

  expressApp.get('/app/document/:code/pdf', async (req: IAppRequest, res: Response) => {

    const { code } = req.params;
    const cookies = req.cookies;

    //console.log(`Doc to PDF render request (${code})`);

    if (!code) {
      //console.error(`Wrong code - ${code}`);
      res.status(503).send('503');
      return;
    }

    const appPort = env.port;

    if (!appPort) {
      //console.error(`Wrong port - ${appPort}`);
      res.status(503).send('503');
      return;
    }

    // @ts-ignore
    // const i18n = req.i18n;
    // const userLang = i18n!.language || i18n!.options.defaultLanguage;
    const userLang = 'ru';
    const origin = `${req.protocol}://${req.hostname}`;

    const rawPdfUrl = `${origin}/app/document/${code}/pdf/raw?forcedLang=${userLang}`;

    try {
      const pdf = await renderer.renderPDF(rawPdfUrl, cookies);
      res.contentType('application/pdf');
      res.send(pdf);
    } catch (e) {
      //console.error(`Can't render: ${e}`);
      res.status(503).send('503');
    }
  });

  expressApp.get('/app/document/:code/image/:type', async (req: IAppRequest, res: Response) => {
    const imageTypes = {
      preview: [184, 276],
      social: [300, 300]
    };
    const { code, type } = req.params;

    //console.log(`Doc to JPG render request (${code})`);

    const origin = `${req.protocol}://${req.hostname}`;
    const imageURL = `${origin}/app/document/${code}/image/${type}/raw`;
    // @ts-ignore
    const imageType = imageTypes[type];

    try {
      const [width, height] = imageType;
      const image = await renderer.renderImage(imageURL, width, height);
      res.contentType('image/jpeg');
      res.send(image);
    } catch (err) {
      // console.error(err);
      res.status(503).send('503');
    }
  });
}
