import * as mobx from 'mobx';

import Certificates from './domain/certificates';
import Templates    from './domain/templates';

import Session       from './ui/session';
import CertMaster    from './ui/certMaster';
import Notifications from './ui/notifications';

import env from '../environments/environment';

mobx.configure({ enforceActions: 'observed' });

let store: Store;

class Store {

  static init(initialState = {}) {
    const isServer = typeof window === 'undefined';

    if (isServer) {
      return new Store({ initialState });
    } else {
      const win: any = window;

      if (!store) {
        if (env.isDev) {
          // save initialState globally and use saved state when initialState is empty
          // initialState becomes "empty" on some HMR
          if (!win.__INITIAL_STATE__) {
            // TODO: when store changed, save it to win.__INITIAL_STATE__. So we can keep latest store for HMR
            win.__INITIAL_STATE__ = initialState;
          } else if (Object.keys(initialState).length === 0) {
            initialState = win.__INITIAL_STATE__;
          }
        }

        store = new Store({ initialState });

        if (env.isDev) {
          win.__STORE__ = store;
        }
      }

      return store || win.__STORE__;
    }
  }

  static getStore() {
    return (typeof window !== 'undefined' && (window as any).__STORE__) || store;
  }

  // domain store
  certsStore: Certificates;
  templatesStore: Templates;
  // ui stores
  sessionStore: Session;
  certMasterStore: CertMaster;
  notificationsStore: Notifications;

  constructor({ initialState = {} }: {
    initialState?: any;
  }) {
    this.certsStore = new Certificates(this, initialState.certsStore);
    this.templatesStore = new Templates(this, initialState.templatesStore);
    this.sessionStore = new Session(this, initialState.sessionStore);
    this.certMasterStore = new CertMaster(this);
    this.notificationsStore = new Notifications(this);
  }
}

export default Store;
