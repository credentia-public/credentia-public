import { decorate, observable, action } from 'mobx';

export enum Chains {
  MainNet = 'mainnet', TestNet = 'testnet', TestNetOld = 'testnet-old'
}

export interface IIssuer {
  _id: string;
  name: string;
  shortName: string;
  address: string;
  phone: string;
  ethAddress: string;
  publicKey: string;
  createdAt: Date;
  updatedAt: Date;
  chain: Chains;
}

class Issuer implements IIssuer {
  _id!: string;
  name!: string;
  shortName!: string;
  address!: string;
  phone!: string;
  ethAddress!: string;
  publicKey!: string;
  createdAt!: Date;
  updatedAt!: Date;
  chain!: Chains;

  constructor(issuer: IIssuer) {
    this.update(issuer);
  }

  update(issuer: Partial<IIssuer>) {
    Object.assign(this, issuer);
  }
}

decorate<Issuer>(Issuer,{
  name: observable,
  shortName: observable,
  address: observable,
  phone: observable,
  ethAddress: observable,
  publicKey: observable,
  updatedAt: observable,
  createdAt: observable,
  chain: observable,

  update: action
});

export default Issuer;
