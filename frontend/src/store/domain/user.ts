import { decorate, observable, action } from 'mobx';

export enum UserStatus {
  Unverified = 'unverified', Active = 'active'
}

export enum UserType {
  IssuerAmbassador = 'issuer_ambassador', Student = 'student', Admin = 'admin'
}

export interface IUser {
  _id: string;
  email: string;
  type: UserType;
  status: UserStatus;
  issuerIds: string[];
  ethAddress: string;
  publicKey: string;
  updatedAt: string;
  createdAt: string;
}

class User implements IUser {
  _id!: string;
  email!: string;
  type!: UserType;
  status!: UserStatus;
  issuerIds = observable<string>([]);
  ethAddress!: string;
  publicKey!: string;
  updatedAt!: string;
  createdAt!: string;

  constructor(user: IUser) {
    this.update(user);
  }

  update(user: Partial<IUser>) {
    Object.assign(this, user);
  }
}

decorate<User>(User,{
  _id: observable,
  email: observable,
  type: observable,
  status: observable,
  issuerIds: observable,
  ethAddress: observable,
  publicKey: observable,
  updatedAt: observable,
  createdAt: observable,

  update: action
});

export interface IPasswordChange {
  oldPassword: string;
  newPassword: string;
}

export interface IResetPasswordSendLink {
  email: string;
}

export interface IResetPasswordSetNew {
  token: string;
  newPassword: string;
}

export default User;
