import { flow, IObservableArray, observable, decorate, action } from 'mobx';

import { RequestState }                        from '../../sharedTypes';
import { getTemplates } from '../../api/template';

type Store = import('../').default;

export interface ITemplate {
  _id: string;
  issuerId: string;
  name: string;
  textColor: string;
  bgColor: string;
  logoPath: string;
  imagePreviewPath: string;
  updatedAt: string;
  createdAt: string;
}

// tslint:disable-next-line:max-classes-per-file
class TemplatesStore {

  templates: IObservableArray<ITemplate> = observable([]);
  state: RequestState = RequestState.Done;
  error?: string;

  fetchTemplates = flow(function *(this: TemplatesStore): Generator<Promise<any>, void, any> {
    this.state = RequestState.Pending;

    try {
      const templates = yield getTemplates();
      this.update(templates);
      this.state = RequestState.Done;
    } catch (err) {
      this.error = err.message;
      this.state = RequestState.Error;
    }
  });

  private rootStore: import('../').default;

  constructor(store: Store, templates?: ITemplate[]) {
    this.rootStore = store;
    this.update(templates || []);
  }

  update(templates: ITemplate[]) {
    this.templates.replace(templates);
  }
}

decorate<TemplatesStore>(TemplatesStore, {
  templates: observable,
  state: observable,
  error: observable,

  fetchTemplates: action,
  update: action
});

export default TemplatesStore;
