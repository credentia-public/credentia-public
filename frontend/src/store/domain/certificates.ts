import { flow, IObservableArray, observable, decorate, action } from 'mobx';

import { RequestState } from '../../sharedTypes';
import {
  createCertificates,
  getCertificates,
  deleteCerts,
  publishCertificates,
  signCertificates,
  toggleVisibility
}                       from '../../api/certificate';
import i18n             from '../../lib/i18n';
import { IIssuer }      from './issuer';
import { IUser }        from './user';

type Store = import('../').default;
type ITemplate = import('./templates').ITemplate;

export interface IFiles {
  [key: string]: {
    name: string;
    key: string;
    date: string;
  };
}

export interface ICreateLLCertificateParams {
  issuerName: string;
  templateName: string;
  course: string;
  courseId: string;
  emailHash: string;
  name: string;
  surname: string;
  secondName: string;
  date: string;
}

export interface ICreateCertificateParams {
  template: string;
  group: string;
  title: string;
  holders: Array<{
    email: string;
    name: string;
    surname: string;
    secondName: string;
  }>;
}

export type CredentialSubject = {
  [key: string]: string | number;
} & {
  title: string;
  name: string;
  surname?: string;
  secondName?: string;
  files?: string[];
  cloudFiles?: IFiles;
};

export interface ICertificate {
  _id: string;
  issuer: string | IIssuer;
  holder: string | IUser;
  template: string | ITemplate;
  group: string;
  visible: boolean;
  chain: string;
  views: number;
  shareableLinkCode: string;
  certificateSocialMediaImage: string;
  credentialSubject: CredentialSubject;
  proof?: {
    proofType: string;
    verificationMethod: string;
    jws: string;
    created: string;
  };
  txId?: string;
}

// tslint:disable-next-line:max-classes-per-file
class CertificatesStore {

  certificates: IObservableArray<ICertificate> = observable([]);
  state: RequestState = RequestState.Done;
  error?: string;

  fetchCerts = flow(function *(this: CertificatesStore): Generator<Promise<any>, void, any> {
    this.state = RequestState.Pending;

    try {
      const certificates = yield getCertificates();
      this.replace(certificates);
      this.state = RequestState.Done;
    } catch (err) {
      this.error = err.message;
      this.state = RequestState.Error;
    }
  });

  createCerts = flow(function *(this: CertificatesStore, certs: ICreateCertificateParams):
    Generator<Promise<any>, void, any> {

    this.state = RequestState.Pending;

    try {
      const certificates = yield createCertificates(certs);
      this.replace(certificates);
      this.state = RequestState.Done;
    } catch (err) {
      this.error = err.message;
      this.state = RequestState.Error;
    }
  });

  deleteCerts = flow(function *(this: CertificatesStore, issuer: string, certsIds: string[]):
    Generator<Promise<any>, void, any> {

    // this.state = RequestState.Pending;

    try {
      yield deleteCerts(issuer, certsIds);
      // this.replace(certificates);
      this.state = RequestState.Done;
    } catch (err) {
      this.error = err.message;
      this.state = RequestState.Error;
    }
  });

  signCerts = flow(function *(this: CertificatesStore, certIds: string[]):
    Generator<Promise<any>, void, any> {

    this.state = RequestState.Pending;

    try {
      const certificates = yield signCertificates(certIds);
      this.update(certificates);
      this.state = RequestState.Done;
    } catch (err) {
      this.error = err.message;
      this.state = RequestState.Error;
    }
  });

  publishCerts = flow(function *(this: CertificatesStore, certIds: string[]):
    Generator<Promise<any>, void, any> {

    // this.state = RequestState.Pending;

    try {
      const certs = yield publishCertificates(certIds);
      this.update(certs);
      this.state = RequestState.Done;
    } catch (err) {
      this.error = err.message;
      this.state = RequestState.Done;
      this.rootStore.notificationsStore.error(i18n.t(`api_errors.${err.message}`));
    }
  });

  toggleVisibility = flow(function *(this: CertificatesStore, uuid: string, visible: boolean):
    Generator<Promise<any>, void, any> {
    let cert = this.certificates.find(c => c.shareableLinkCode === uuid);

    cert!.visible = visible;

    try {
      cert = yield toggleVisibility(uuid, visible);
      this.update([cert!]);
    } catch (err) {
      cert!.visible = !visible;
      this.error = err.message;
      this.rootStore.notificationsStore.error(i18n.t(`api_errors.${err.message}`));
    }
  });

  private rootStore: import('../').default;

  constructor(store: Store, certificates?: ICertificate[]) {
    this.rootStore = store;
    this.replace(certificates || []);
  }

  replace(certificates: ICertificate[]) {
    this.certificates.replace(certificates);
  }

  update(certificates: ICertificate[]) {
    for (const cert of certificates) {
      const i = this.certificates.findIndex(c => c._id === cert._id);

      if (i !== -1) {
        this.certificates[i] = cert;
      } else {
        this.certificates.push(cert);
      }
    }
  }
}

decorate<CertificatesStore>(CertificatesStore, {
  certificates: observable,
  state: observable,
  error: observable,

  fetchCerts: action,
  createCerts: action,
  update: action
});

export default CertificatesStore;
