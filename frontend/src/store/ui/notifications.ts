import { action, decorate } from 'mobx';
import { toast }             from 'react-toastify';

type Store = import('../').default;

class Notifications {

  private rootStore: Store;

  constructor(store: Store) {
    this.rootStore = store;
  }
  success = (msg: string) => {
    toast.success(msg);
  };

  info = (msg: string) => {
    toast.info(msg);
  };

  warn = (msg: string) => {
    toast.warn(msg);
  };

  error = (msg: string) => {
    toast.error(msg);
  };

  show = (msg: string) => {
    toast(msg);
  };
}

decorate(Notifications, {
  success: action,
  info: action,
  warn: action,
  error: action,
  show: action,
});

export default Notifications;
