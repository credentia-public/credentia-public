import { observable, action, decorate, flow } from 'mobx';

import { signup, signin, logout } from '../../api/auth';
import { getUser } from '../../api/user';
import User, { IUser }            from '../domain/user';
import Issuer, { IIssuer }        from '../domain/issuer';

type Store = import('../').default;

export interface ISessionParams {
  view?: 'app' | 'widget';
  user?: IUser;
  issuer?: IIssuer;
}

export interface ISignUpParams {
  userType: string;
  issuer?: string;
  name?: string;
  surname?: string;
  secondName?: string;
  documentNumber?: string;
  email: string;
  password: string;
  privacy: boolean;
}

class Session {
  view: 'app' | 'widget' = 'app';
  user?: User;
  issuer?: Issuer;
  error?: string;
  loading = false;

  signup = flow(function *(this: Session, params: ISignUpParams) {
    this.error = undefined;
    this.loading = true;

    try {
      yield signup(params);
      this.loading = false;
    } catch (err) {
      this.error = err.message;
      throw err;
    } finally {
      this.loading = false;
    }
  });

  signin = flow(function *(this: Session, login: string, password: string): Generator<Promise<any>, void, any> {
    this.error = undefined;
    this.loading = true;

    try {
      const { user, issuer } = yield signin(login, password);
      this.user = new User(user);
      this.issuer = issuer ? new Issuer(issuer) : undefined;
      this.loading = false;
    } catch (err) {
      this.error = err.message;
      throw err;
    } finally {
      this.loading = false;
    }
  });

  logout = flow(function *(this: Session) {
    try {
      yield logout();
      this.user = undefined;
      this.issuer = undefined;
    } catch (err) {
      this.error = err.message;
      throw err;
    } finally {
      this.loading = false;
    }
  });

  getUser = flow(function *(this: Session): Generator<Promise<any>, void, any> {
    this.error = undefined;
    this.loading = true;

    try {
      const { user, issuer } = yield getUser();
      this.user = new User(user);
      this.issuer = issuer ? new Issuer(issuer) : undefined;
      this.loading = false;
    } catch (err) {
      this.error = err.message;
      throw err;
    } finally {
      this.loading = false;
    }
  });

  private rootStore: Store;

  constructor(store: Store, data: ISessionParams = {}) {
    const { view, user, issuer } = data;

    this.rootStore = store;
    this.view = view || 'app';
    this.user = user ? new User(user) : undefined;
    this.issuer = issuer ? new Issuer(issuer) : undefined;
  }
}

decorate(Session, {
  view: observable,
  user: observable,
  error: observable,
  loading: observable,

  signup: action,
  signin: action,
  logout: action
});

export default Session;
