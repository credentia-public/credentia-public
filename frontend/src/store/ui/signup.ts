import { observable, action, runInAction } from 'mobx';

import { signup, logout } from '../../api/auth';

type Store = import('../').default;
type ISignUpParams = import('../ui/session').ISignUpParams;

class Signup {
  @observable name = '';
  @observable surname = '';
  @observable secondName = '';
  @observable birthDate = '';
  @observable email = '';
  @observable password = '';
  @observable loading = false;
  @observable errors?: string;

  private rootStore: Store;

  constructor(store: Store) {
    this.rootStore = store;
  }

  @action.bound setEmail(login: string) {
    this.email = login;
  }

  @action.bound setPassword(password: string) {
    this.password = password;
  }

  @action.bound signup(params: ISignUpParams) {
    this.errors = undefined;
    this.loading = true;

    return signup(params)
      .then(() => {
        runInAction(() => {
          this.name = '';
          this.surname = '';
          this.secondName = '';
          this.birthDate = '';
          this.email = '';
          this.password = '';
          this.loading = false;
        });
        // this.rootStore.changeCurrentUrl('/app/login');
      })
      .catch((err: Error) => {
        console.error(err);
        runInAction(() => {
          this.errors = err.message;
          this.loading = false;
        });
      });
  }

  @action.bound logout() {
    return logout()
      .then(_ => {
        location.replace('/');
      })
      .catch(err => {
        console.error(err);
      });
  }
}

export default Signup;
