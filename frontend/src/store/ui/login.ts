import { observable, action } from 'mobx';

import Store from '../index';

class Login {
  @observable login = '';
  @observable password = '';
  @observable loading = false;
  @observable errors?: string;

  private rootStore: Store;

  constructor(store: Store) {
    this.rootStore = store;
  }

  @action.bound setLogin(login: string) {
    this.login = login;
  }

  @action.bound setPassword(password: string) {
    this.password = password;
  }

  // @action.bound validate() {
  //   const validSymbols = /[0-9a-z]+/;
  //
  //   if (this.login.length === 0) {
  //     this.errors = 'Введите логин';
  //     return;
  //   }
  //
  //   if (this.password.length === 0) {
  //     this.errors = 'Введите пароль';
  //     return;
  //   }
  //
  //   if (!validSymbols.test(this.login)) {
  //     this.errors = 'Поле логин содержит невалидные символы';
  //   }
  //
  //   if (!validSymbols.test(this.password)) {
  //     this.errors = 'Поле пароль содержит невалидные символы';
  //   }
  // }
}

export default Login;
