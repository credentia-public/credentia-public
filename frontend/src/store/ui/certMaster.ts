import { observable, action, IObservableArray } from 'mobx';

type ICertificate = import('../domain/certificates').ICertificate;
type ITemplate = import('../domain/templates').ITemplate;
type Store = import('../').default;

class CertMaster {
  @observable template?: ITemplate;
  @observable title = '';
  @observable numberOfStudents = 0;

  @observable rtDemo1 = '';
  @observable rtDemo2 = '';
  @observable rtDemo3 = '';

  grid: IObservableArray<ICertificate> = observable([]);

  private rootStore: Store;

  constructor(store: Store) {
    this.rootStore = store;
  }

  @action.bound setTitle(title: string) {
    this.title = title;
  }

  @action.bound setNumberOfStudents(numberOfStudents: number) {
    this.numberOfStudents = numberOfStudents;
  }

  @action.bound setRtDemo1(value: string) {
    this.rtDemo1 = value;
  }
  @action.bound setRtDemo2(value: string) {
    this.rtDemo2 = value;
  }
  @action.bound setRtDemo3(value: string) {
    this.rtDemo3 = value;
  }

  @action.bound setGrid(grid: any) {
    this.grid = grid;
  }

  @action.bound setTemplate(template: ITemplate) {
    this.template = template;
  }
}

export default CertMaster;
